import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManagerGuard } from './core/guards/manager/manager.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/landing/landing.module').then((m) => m.LandingModule),
  },
  {
    path: 'noten',
    loadChildren: () => import('./features/player-rating/player-rating.module').then((m) => m.PlayerRatingModule),
    canActivate: [ManagerGuard],
  },
  {
    path: 'spielerdatenbank',
    loadChildren: () => import('./features/player/player.module').then((m) => m.PlayerModule),
    canActivate: [ManagerGuard],
  },
  {
    path: 'kalender',
    loadChildren: () => import('./features/calendar/calendar.module').then((m) => m.CalendarModule),
    canActivate: [ManagerGuard],
  },
  {
    path: 'manager',
    loadChildren: () => import('./features/manager/manager.module').then((m) => m.ManagerModule),
    canActivate: [ManagerGuard],
  },
  {
    path: 'club',
    loadChildren: () => import('./features/club/club.module').then((m) => m.ClubModule),
    canActivate: [ManagerGuard],
  },
  {
    path: 'länder',
    loadChildren: () => import('./features/country/country.module').then((m) => m.CountryModule),
    canActivate: [ManagerGuard],
  },
  {
    path: 'datenabgleich',
    loadChildren: () => import('./features/data-sync/data-sync.module').then((m) => m.DataSyncModule),
    canActivate: [ManagerGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
