import { Component, isDevMode } from '@angular/core';
import { ApiService } from './core/services/api/api.service';
import { StateService } from './core/services/state/state.service';
import { AuthService } from './core/services/auth/auth.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isDevMode: boolean;
  expandedNavigation: boolean;

  currentRoute: string;

  constructor(public apiService: ApiService, public stateService: StateService, public authService: AuthService, private router: Router) {
    this.isDevMode = isDevMode();

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.currentRoute = val.url;
      }
    });
  }
}
