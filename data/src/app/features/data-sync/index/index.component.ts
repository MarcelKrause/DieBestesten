import { Component, OnInit } from '@angular/core';
import { Notification } from 'src/app/core/models/notification/notification';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  header = 'ID;Vorname;Nachname;Angezeigter Name;Angezeigter Name;Verein;Position;Marktwert;Punkte;Notendurchschnitt';
  csvFile;

  validPlayer = [];
  divergentPoints = [];
  miscalculatedPoints = [];
  missingPlayer = [];
  remainingPlayer = [];

  isLoading = false;
  isPlayerRatingCalculating = false;
  checkCompleted = false;

  constructor(public apiService: ApiService, private notificationService: NotificationService) {}

  ngOnInit(): void {}

  public onFileChange(event: Event): void {
    const element = event.currentTarget as HTMLInputElement;
    const files: FileList | null = element.files;
    this.csvFile = files[0];
    this.apiService.checkPlayerRating(this.csvFile).subscribe({
      next: (response) => {
        response.forEach((player) => {
          this.divergentPoints.push(player);
        });
      },
      error: (error) => {},
      complete: () => {
        this.divergentPoints.sort(this.sortbyClub);
        this.checkCompleted = true;
        this.isLoading = false;
      },
    });
    this.isLoading = true;
  }

  private sortbyClub(a: any, b: any): number {
    if (a.club >= b.club) {
      return 1;
    } else {
      return -1;
    }
  }

  public abs(value: number): number {
    return Math.abs(value);
  }

  public onFinishMatchday(): void {
    const season = this.apiService.status.season.season_id;
    const matchday = this.apiService.status.matchday.number;
    if (confirm(`Sicher? Spieltag ${matchday} wirklich abschließen? Das kann nicht rückgängig gemacht werden.`)) {
      this.apiService.finishMatchday(season, matchday).subscribe({
        next: (response) => {
          this.notificationService.push(new Notification('positive', 'Spieltag erfolgreich abgeschlossen'));
          this.checkCompleted = false;
        },
        error: (error) => {
          console.error(error);
          this.notificationService.push(new Notification('negative', error.message));
        },
      });
    }
  }
}
