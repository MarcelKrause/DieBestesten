import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataSyncRoutingModule } from './data-sync.routing';
import { IndexComponent } from './index/index.component';

@NgModule({
  declarations: [IndexComponent],
  imports: [CommonModule, DataSyncRoutingModule],
})
export class DataSyncModule {}
