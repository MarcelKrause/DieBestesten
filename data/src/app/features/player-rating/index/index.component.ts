import { Component, OnInit } from '@angular/core';
import { Club } from 'src/app/core/models/club/club';
import { Notification } from 'src/app/core/models/notification/notification';
import { PlayerRating } from 'src/app/core/models/player-rating/player-rating';
import { Player } from 'src/app/core/models/player/player';
import { Position } from 'src/app/core/models/position/position';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  clubIsLoading: boolean;
  clubList: Club[];
  selectedMatchday: number;
  selectedClub: Club;
  playerList: Player[] = [];
  isEditable: boolean;
  isMatchdayWithoutRating: boolean;

  constructor(public apiService: ApiService, private notificationService: NotificationService) {
    this.getCurrentMatchday();
    this.getClubList();
  }

  ngOnInit(): void {}

  private getCurrentMatchday(): void {
    if (this.apiService.status) {
      this.selectedMatchday = this.apiService.status.matchday.number;
    } else {
      setTimeout(() => {
        this.getCurrentMatchday();
      }, 100);
    }
  }

  private getClubList(): void {
    if (this.apiService.clubList) {
      this.clubList = this.apiService.clubList.filter((club) => club.isBundesliga === true);
    } else {
      setTimeout(() => {
        this.getClubList();
      }, 100);
    }
  }

  public getStartLineup(): Player[] {
    return this.playerList.filter((player) => (player.ratingList[0] ? player.ratingList[0].startLineup : false));
  }

  public getSubstitution(): Player[] {
    return this.playerList.filter((player) => (player.ratingList[0] ? player.ratingList[0].substitution : false));
  }

  public onEditRating(player: Player): void {
    if (player.seasonList[0].getPositionString() === 'Tor' && player.ratingList[0].startLineup) {
      if (this.getStartLineup().filter((e) => e.seasonList[0].getPositionString() === 'Tor').length > 1) {
        player.ratingList[0].startLineup = false;
        this.notificationService.push(new Notification('negative', 'Es darf maximal 1 Torhüter in der Startelf stehen'));
        return;
      }
    }

    if (player.ratingList[0].startLineup) {
      if (this.getStartLineup().length > 11) {
        player.ratingList[0].startLineup = false;
        this.notificationService.push(new Notification('negative', 'Es dürfen maximal 11 Spieler in der Startelf stehen'));
        return;
      }
    }

    if (player.ratingList[0].substitution) {
      if (this.getSubstitution().length > 5) {
        player.ratingList[0].substitution = false;
        this.notificationService.push(new Notification('negative', 'Es dürfen maximal 5 Spieler eingewechselt werden'));
        return;
      }
    }

    if (player.ratingList[0].sds) {
      if (this.playerList.filter((e) => e.ratingList[0].sds).length > 1) {
        player.ratingList[0].sds = false;
        this.notificationService.push(new Notification('negative', 'Es darf maximal 1 Spieler SDS sein'));
        return;
      }
    }

    player.ratingList[0].calcPoints(player.seasonList[0].position);
    this.apiService.patchPlayerRating(player.ratingList[0]).subscribe(
      (data) => {},
      (error) => {
        this.notificationService.push(new Notification('negative', 'Fehler beim Speichern'));
        console.error(error);
      }
    );
  }

  public onAddPlayerRating(): void {
    let subscribeCount = 0;
    this.playerList.forEach((player) => {
      subscribeCount++;
      this.apiService.addPlayerRating(player.playerId, this.selectedClub.clubId, this.selectedMatchday).subscribe(
        (data) => {
          player.ratingList[0] = new PlayerRating(data);
        },
        (error) => {},
        () => {
          subscribeCount--;

          if (subscribeCount === 0) {
            this.loadData();
          }
        }
      );
    });
  }

  public onSelectMatchday(matchdayNumber: number): void {
    if (matchdayNumber < 1 || matchdayNumber > this.apiService.status.matchday.number) {
      return;
    }

    this.selectedMatchday = matchdayNumber;
    this.loadData();
  }

  public onSelectClub(club: Club): void {
    this.selectedClub = club;
    this.loadData();
  }

  public onToggleStartLineup(player: Player): void {
    player.ratingList[0].startLineup = !player.ratingList[0].startLineup;

    this.playerList.sort(this.sortByStartLineup);
  }

  private loadData(): void {
    if (!this.selectedClub || !this.selectedMatchday) {
      return;
    }

    this.clubIsLoading = true;
    this.apiService.getPlayerRatingByClubAndMatchday(this.selectedClub.clubId, this.selectedMatchday).subscribe(
      (data) => {
        this.playerList = [];
        let isRating = false;
        data.forEach((element) => {
          const player = new Player(element);
          if (player.ratingList[0]) {
            this.playerList.push(player);
            isRating = true;
          } else {
            console.log('no rating', player.displayname);
            /*this.notificationService.push(
              new Notification(
                'negative',
                `Kein Spieltagsobjekt für ${player.displayname} gefunden. Bitte Marcel manuell hinzufügen lassen.`
              )
            );*/
            this.playerList.push(player);
          }
        });

        if (!isRating) {
          this.isMatchdayWithoutRating = true;
        } else {
          this.isMatchdayWithoutRating = false;
        }
      },
      (error) => {},
      () => {
        this.clubIsLoading = false;

        if (!this.isMatchdayWithoutRating) {
          this.playerList.sort(this.sortByStartLineup);
        }
      }
    );

    this.checkEditableStatus();
  }

  private checkEditableStatus(): void {
    if (+this.selectedMatchday === +this.apiService.status.matchday.number) {
      this.isEditable = true;
    } else {
      this.isEditable = false;
    }
    this.isEditable = true;
  }

  private sortByStartLineup(a: Player, b: Player): number {
    const aRating = a.ratingList[0];
    const bRating = b.ratingList[0];
    if (!aRating || !bRating) {
      return 0;
    }

    if (aRating.startLineup === bRating.startLineup) {
      if (aRating.substitution === bRating.substitution) {
        return 0;
      } else if (aRating.substitution < bRating.substitution) {
        return 1;
      } else {
        return -1;
      }
    } else {
      if (aRating.startLineup < bRating.startLineup) {
        return 1;
      } else {
        return -1;
      }
    }
  }
}
