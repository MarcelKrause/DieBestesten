import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Notification } from 'src/app/core/models/notification/notification';
import { Player } from 'src/app/core/models/player/player';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-player-rating-row',
  templateUrl: './player-rating-row.component.html',
  styleUrls: ['./player-rating-row.component.scss'],
})
export class PlayerRatingRowComponent implements OnInit {
  @Input() player: Player;
  @Input() isEditable: boolean;
  @Output() editRating = new EventEmitter<Player>();

  selectGrade = false;
  selectLigainsiderGrade = false;

  constructor(private notificationService: NotificationService) {}

  ngOnInit(): void {}

  public onToggleStartLineup(): void {
    if (!this.isEditable) {
      return;
    }
    this.player.ratingList[0].startLineup = !this.player.ratingList[0].startLineup;

    if (!this.player.ratingList[0].startLineup) {
      this.resetRating();
    }
    this.editRating.emit(this.player);
  }

  public onToggleSubstitution(): void {
    if (!this.isEditable) {
      return;
    }
    this.player.ratingList[0].substitution = !this.player.ratingList[0].substitution;

    if (!this.player.ratingList[0].substitution) {
      this.resetRating();
    }
    this.editRating.emit(this.player);
  }

  public onToggleSds(): void {
    if (!this.isEditable) {
      return;
    }
    this.player.ratingList[0].sds = !this.player.ratingList[0].sds;
    this.editRating.emit(this.player);
  }

  public onToggleCleanSheet(): void {
    if (!this.isEditable) {
      return;
    }
    this.player.ratingList[0].cleanSheet = !this.player.ratingList[0].cleanSheet;
    this.editRating.emit(this.player);
  }

  public onEditGoal(value: number): void {
    if (!this.isEditable) {
      return;
    }
    this.player.ratingList[0].goals += value;
    if (this.player.ratingList[0].goals < 0) {
      this.player.ratingList[0].goals = 0;
    }
    this.editRating.emit(this.player);
  }

  public onEditAssist(value: number): void {
    if (!this.isEditable) {
      return;
    }
    this.player.ratingList[0].assists += value;
    if (this.player.ratingList[0].assists < 0) {
      this.player.ratingList[0].assists = 0;
    }
    this.editRating.emit(this.player);
  }

  public onToggleYellowRedCard(): void {
    if (!this.isEditable) {
      return;
    }

    if (this.player.ratingList[0].redCard) {
      this.notificationService.push(
        new Notification('negative', 'Es darf nicht gleichzeitig eine Gelb-Rote-Karte und eine Rote-Karte gesetzt sein')
      );
      return;
    }

    this.player.ratingList[0].yellowRedCard = !this.player.ratingList[0].yellowRedCard;
    this.editRating.emit(this.player);
  }

  public onToggleRedCard(): void {
    if (!this.isEditable) {
      return;
    }
    if (this.player.ratingList[0].yellowRedCard) {
      this.notificationService.push(
        new Notification('negative', 'Es darf nicht gleichzeitig eine Gelb-Rote-Karte und eine Rote-Karte gesetzt sein')
      );
      return;
    }
    this.player.ratingList[0].redCard = !this.player.ratingList[0].redCard;
    this.editRating.emit(this.player);
  }

  public onOpenGradeSelection(): void {
    if ((!this.player.ratingList[0].startLineup && !this.player.ratingList[0].substitution) || !this.isEditable) {
      return;
    }

    this.selectGrade = true;
  }

  public onOpenLigainsiderGradeSelection(): void {
    if ((!this.player.ratingList[0].startLineup && !this.player.ratingList[0].substitution) || !this.isEditable) {
      return;
    }

    this.selectLigainsiderGrade = true;
  }

  public onCloseGradeSelection(): void {
    this.selectGrade = false;
  }

  public onCloseLigainsiderGradeSelection(): void {
    this.selectLigainsiderGrade = false;
  }

  public onSelectGrade(grade: number): void {
    if ((!this.player.ratingList[0].startLineup && !this.player.ratingList[0].substitution) || !this.isEditable) {
      return;
    }

    this.player.ratingList[0].grade = grade;
    this.editRating.emit(this.player);
    this.selectGrade = false;
  }

  public onSelectLigainsiderGrade(grade: number): void {
    if ((!this.player.ratingList[0].startLineup && !this.player.ratingList[0].substitution) || !this.isEditable) {
      return;
    }

    this.player.ratingList[0].ligainsider_grade = grade;
    this.editRating.emit(this.player);
    this.selectLigainsiderGrade = false;
  }

  public arrayOne(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  private resetRating(): void {
    this.player.ratingList[0].startLineup = false;
    this.player.ratingList[0].substitution = false;
    this.player.ratingList[0].grade = null;
    this.player.ratingList[0].goals = 0;
    this.player.ratingList[0].assists = 0;
    this.player.ratingList[0].sds = false;
    this.player.ratingList[0].cleanSheet = false;
    this.player.ratingList[0].redCard = false;
    this.player.ratingList[0].yellowRedCard = false;
    this.player.ratingList[0].points = 0;
  }
}
