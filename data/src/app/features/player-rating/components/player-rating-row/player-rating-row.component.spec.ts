import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerRatingRowComponent } from './player-rating-row.component';

describe('PlayerRatingRowComponent', () => {
  let component: PlayerRatingRowComponent;
  let fixture: ComponentFixture<PlayerRatingRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayerRatingRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerRatingRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
