import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerRatingRoutingModule } from './player-rating.routing';
import { IndexComponent } from './index/index.component';
import { PlayerRatingRowComponent } from './components/player-rating-row/player-rating-row.component';
import { PlayerRatingFilterPipe } from 'src/app/core/pipes/player-rating-filter/player-rating-filter.pipe';

@NgModule({
  imports: [CommonModule, PlayerRatingRoutingModule],
  declarations: [IndexComponent, PlayerRatingRowComponent, PlayerRatingFilterPipe],
})
export class PlayerRatingModule {}
