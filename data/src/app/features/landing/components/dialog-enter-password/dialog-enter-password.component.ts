import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Notification } from 'src/app/core/models/notification/notification';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-dialog-enter-password',
  templateUrl: './dialog-enter-password.component.html',
  styleUrls: ['./dialog-enter-password.component.scss'],
})
export class DialogEnterPasswordComponent implements OnInit {
  password = '';
  confirmPassword = '';

  constructor(
    private authService: AuthService,
    private notificationService: NotificationService,
    private dialog: MatDialogRef<DialogEnterPasswordComponent>
  ) {}

  ngOnInit(): void {}

  public onEnterPassword(): void {
    if (this.password != this.confirmPassword) {
      this.notificationService.push(new Notification('negative', 'Passwörter stimmen nicht überein'));
      return;
    }

    this.authService.changePassword(this.password).subscribe((data) => {
      if (data.success) {
        this.authService.setToken(data.token);
        this.dialog.close(true);
      } else {
        this.notificationService.push(new Notification('negative', 'Datenbank Fehler'));
        // error
      }
    });
  }
}
