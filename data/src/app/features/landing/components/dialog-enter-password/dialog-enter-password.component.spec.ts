import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEnterPasswordComponent } from './dialog-enter-password.component';

describe('DialogEnterPasswordComponent', () => {
  let component: DialogEnterPasswordComponent;
  let fixture: ComponentFixture<DialogEnterPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEnterPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEnterPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
