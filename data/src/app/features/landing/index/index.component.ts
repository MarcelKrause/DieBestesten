import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Notification } from 'src/app/core/models/notification/notification';
import { ApiService } from 'src/app/core/services/api/api.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { StateService } from 'src/app/core/services/state/state.service';
import { DialogEnterPasswordComponent } from './../components/dialog-enter-password/dialog-enter-password.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  map: HTMLObjectElement;
  mapType = 'clubs';
  activity: any;

  playerCount: any;
  playerHeightDistribution: any[];
  playerWeightDistribution: any[];
  playerDataProgress: any[];
  seasonChart: any[];

  dataCompleteness = {
    country: [],
    city: [],
    dateOfBirth: [],
    height: [],
    weight: [],
    hasPhoto: [],
  };

  schema = {
    domain: ['#1e90ff'],
  };

  progressSchema = {
    domain: ['#1e90ff', '#dfe6e9'],
  };

  constructor(
    private authService: AuthService,
    public apiService: ApiService,
    public stateService: StateService,
    public dialog: MatDialog,
    private notificationService: NotificationService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.getActivity(1);
    this.getPlayerCount();
    this.getPlayerHeightDistribution();
    this.getPlayerWeightDistribution();
    this.getPlayerDataProgress();
    // this.getSeasonChart();

    // this.getKickerPlayer();
    if (this.authService.getManager().status === 'pending') {
      this.onOpenEditGeneralDialog();
    }

    this.map = document.getElementById('map') as HTMLObjectElement;
    this.map.onload = () => {
      this.fillWorldMap(this.mapType);
    };
  }

  public onOpenEditGeneralDialog(): void {
    const dialogRef = this.dialog.open(DialogEnterPasswordComponent, {
      width: '480px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe(() => {
      this.notificationService.push(new Notification('positive', 'Password erfolgreich gesetzt.'));
    });
  }

  public getActivity(page: number): void {
    this.apiService.getActivity(page).subscribe((activity) => {
      this.activity = activity;
    });
  }

  private getPlayerCount(): void {
    this.apiService.getPlayerCount().subscribe((data) => {
      this.playerCount = data;
    });
  }

  private getPlayerHeightDistribution(): void {
    this.apiService.getPlayerHeightDistribution().subscribe((data) => {
      this.playerHeightDistribution = data;
    });
  }

  private getPlayerWeightDistribution(): void {
    this.apiService.getPlayerWeightDistribution().subscribe((data) => {
      this.playerWeightDistribution = data;
    });
  }

  private getSeasonChart(): void {
    this.apiService.getSeasonChart().subscribe((data) => {
      this.seasonChart = data;
    });
  }

  private getPlayerDataProgress(): void {
    this.apiService.getPlayerDataProgress().subscribe((data) => {
      this.dataCompleteness.country = [
        {
          name: 'Nation',
          series: [
            {
              name: 'Eingetragen',
              value: data.country,
            },
            {
              name: 'Fehlen',
              value: data.total - data.country,
            },
          ],
        },
      ];

      this.dataCompleteness.city = [
        {
          name: 'Geburtsort',
          series: [
            {
              name: 'Eingetragen',
              value: data.city,
            },
            {
              name: 'Fehlen',
              value: data.total - data.city,
            },
          ],
        },
      ];

      this.dataCompleteness.dateOfBirth = [
        {
          name: 'Geburtsdatum',
          series: [
            {
              name: 'Eingetragen',
              value: data.dateOfBirth,
            },
            {
              name: 'Fehlen',
              value: data.total - data.dateOfBirth,
            },
          ],
        },
      ];

      this.dataCompleteness.height = [
        {
          name: 'Größe',
          series: [
            {
              name: 'Eingetragen',
              value: data.height,
            },
            {
              name: 'Fehlen',
              value: data.total - data.height,
            },
          ],
        },
      ];

      this.dataCompleteness.weight = [
        {
          name: 'Gewicht',
          series: [
            {
              name: 'Eingetragen',
              value: data.weight,
            },
            {
              name: 'Fehlen',
              value: data.total - data.weight,
            },
          ],
        },
      ];
      this.dataCompleteness.hasPhoto = [
        {
          name: 'Foto',
          series: [
            {
              name: 'Mit Foto',
              value: data.hasPhoto,
            },
            {
              name: 'Ohne Foto',
              value: data.total - data.hasPhoto,
            },
          ],
        },
      ];
    });
  }

  public fillWorldMap(type: string): void {
    this.mapType = type;
    const svgDoc = this.map.contentDocument;

    const allPathList = svgDoc.getElementsByTagName('path');
    for (const path of allPathList) {
      path.setAttribute('fill', '#dcdde1');
      path.setAttribute('fill-opacity', '1');
    }

    this.apiService.getWorldmapData().subscribe((data) => {
      let max = 0;
      let list = [];
      if (type === 'clubs') {
        max = +data.max.clubs_by_country;
        list = data.clubs_by_country;
      } else if (type === 'player') {
        max = +data.max.player_by_country;
        list = data.player_by_country;
      }

      list.forEach((country) => {
        let countryCode: string = country.country_code;
        if (countryCode === 'en' || countryCode === 'sx' || countryCode === 'wa') {
          countryCode = 'gb';
        }
        const svGcountry = svgDoc.getElementById(countryCode.toUpperCase());
        if (svGcountry) {
          const pathList = svGcountry.getElementsByTagName('path');
          for (const path of pathList) {
            const weight = (+country.count / max) * 0.7 + 0.3;
            path.setAttribute('fill', '#1e90ff');
            path.setAttribute('fill-opacity', weight.toString());
          }
        }
      });
    });
  }

  public getDateString(date: Date): string {
    date = new Date(date);
    const today = new Date();

    if (today.getDate() === date.getDate() && today.getMonth() === date.getMonth() && today.getFullYear() === date.getFullYear()) {
      return 'Heute, ';
    } else if (
      today.getDate() === date.getDate() - 1 &&
      today.getMonth() === date.getMonth() - 1 &&
      today.getFullYear() === date.getFullYear() - 1
    ) {
      return 'Gestern, ';
    } else {
      return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}, `;
    }
  }

  public formatLastcheckin(diff: number): string {
    if (diff < 60) {
      return 'Jetzt Online';
    } else if (diff >= 60 && diff < 120) {
      return 'Vor 1 Minute';
    } else if (diff >= 120 && diff < 3600) {
      return `Vor ${Math.floor(diff / 60)} Minuten`;
    } else if (diff >= 3600 && diff < 7200) {
      return `Vor 1 Stunde`;
    } else if (diff >= 7200 && diff < 86400) {
      return `Vor ${Math.floor(diff / 60 / 60)} Stunden`;
    } else if (diff >= 86400 && diff < 172800) {
      return `Vor 1 Tag`;
    } else {
      return `Vor ${Math.floor(diff / 60 / 60 / 24)} Tagen`;
    }
  }
}
