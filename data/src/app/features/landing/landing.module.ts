import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingRoutingModule } from './landing.routing';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DialogEnterPasswordComponent } from './components/dialog-enter-password/dialog-enter-password.component';
import { ImageModule } from 'src/app/core/image.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    LandingRoutingModule,
    NgxChartsModule,
    FormsModule,
    RouterModule,
    MatInputModule,
    MatFormFieldModule,
    MatTooltipModule,
    ImageModule,
  ],
  declarations: [IndexComponent, LoginComponent, DialogEnterPasswordComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LandingModule {}
