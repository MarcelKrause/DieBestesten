// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './login/login.component';

// Guards
import { ManagerGuard } from './../../core/guards/manager/manager.guard';
import { GuestGuard } from './../../core/guards/guest/guest.guard';

const routes: Routes = [
  { path: '', component: IndexComponent, canActivate: [ManagerGuard] },
  { path: 'anmelden', component: LoginComponent, canActivate: [GuestGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class LandingRoutingModule {}
