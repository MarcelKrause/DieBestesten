import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  name: string;
  password: string;

  constructor(public authService: AuthService) {}

  ngOnInit(): void {}

  public onLogin(): void {
    this.authService.login(this.name, this.password);
  }
}
