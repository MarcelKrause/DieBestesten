import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';

import { ManagerRoutingModule } from './manager.routing';

@NgModule({
  declarations: [IndexComponent],
  imports: [CommonModule, ManagerRoutingModule],
})
export class ManagerModule {}
