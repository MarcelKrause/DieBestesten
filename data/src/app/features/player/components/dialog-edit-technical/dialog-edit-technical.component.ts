import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Player } from 'src/app/core/models/player/player';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-dialog-edit-technical',
  templateUrl: './dialog-edit-technical.component.html',
  styleUrls: ['./dialog-edit-technical.component.scss'],
})
export class DialogEditTechnicalComponent implements OnInit {
  originalPlayer: Player;
  editPlayer: Player;

  error = {
    kickerId: {
      duplicate: false,
    },
    ligainsiderId: {
      duplicate: false,
    },
  };

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<DialogEditTechnicalComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.originalPlayer = data.player;
    this.editPlayer = JSON.parse(JSON.stringify(this.originalPlayer));
  }

  ngOnInit(): void {}

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    this.apiService.patchPlayer(this.editPlayer).subscribe((data) => {
      this.dialog.close(true);
    });
  }

  public dataIsInvalid(): boolean {
    if (!this.dataIsModified()) {
      return true;
    }

    if (this.error.kickerId.duplicate || this.error.ligainsiderId.duplicate) {
      return true;
    }

    return false;
  }

  private dataIsModified(): boolean {
    if (this.originalPlayer.kickerId !== this.editPlayer.kickerId) {
      return true;
    } else if (this.originalPlayer.ligainsiderId !== this.editPlayer.ligainsiderId) {
      return true;
    } else {
      return false;
    }
  }

  public onValidateInput(inputField: string): void {
    if (inputField === 'kickerId') {
      if (
        this.apiService.playerList.filter((player) => player.kickerId === +this.editPlayer.kickerId).length > 0 &&
        +this.editPlayer.kickerId !== +this.originalPlayer.kickerId
      ) {
        this.error.kickerId.duplicate = true;
      } else {
        this.error.kickerId.duplicate = false;
      }
    }
    if (inputField === 'ligainsiderId') {
      if (
        this.apiService.playerList.filter((player) => player.ligainsiderId === +this.editPlayer.ligainsiderId).length > 0 &&
        +this.editPlayer.ligainsiderId !== +this.originalPlayer.ligainsiderId
      ) {
        this.error.ligainsiderId.duplicate = true;
      } else {
        this.error.ligainsiderId.duplicate = false;
      }
    }
  }
}
