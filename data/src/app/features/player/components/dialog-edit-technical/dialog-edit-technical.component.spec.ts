import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEditTechnicalComponent } from './dialog-edit-technical.component';

describe('DialogEditTechnicalComponent', () => {
  let component: DialogEditTechnicalComponent;
  let fixture: ComponentFixture<DialogEditTechnicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEditTechnicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEditTechnicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
