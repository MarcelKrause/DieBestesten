import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Notification } from 'src/app/core/models/notification/notification';
import { PlayerInSeason } from 'src/app/core/models/player-in-season/player-in-season';
import { Position } from 'src/app/core/models/position/position';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-dialog-edit-player-in-season',
  templateUrl: './dialog-edit-player-in-season.component.html',
  styleUrls: ['./dialog-edit-player-in-season.component.scss'],
})
export class DialogEditPlayerInSeasonComponent implements OnInit {
  playerInSeason: PlayerInSeason;
  price: number;
  initialPrice: number;
  initialPosition: Position;
  initialIsCaptain: boolean;

  preview = false;
  selectedPhoto;

  seasonValuesChanged = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<DialogEditPlayerInSeasonComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.playerInSeason = Object.assign({}, data.playerInSeason);
    this.price = this.playerInSeason.price / 1000000;

    this.initialPrice = this.price;
    this.initialPosition = this.playerInSeason.position;
    this.initialIsCaptain = this.playerInSeason.is_captain;
  }

  ngOnInit(): void {}

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    if (
      this.playerInSeason.position === this.initialPosition &&
      this.price === this.initialPrice &&
      this.playerInSeason.is_captain === this.initialIsCaptain
    ) {
      this.notificationService.push(new Notification('negative', 'Daten sind gleich'));
    } else {
      this.apiService.editPlayerInSeason(this.playerInSeason).subscribe((data) => {
        this.dialog.close(true);
      });
    }

    if (this.preview) {
      this.apiService.editPlayerImage(this.playerInSeason.playerId, this.playerInSeason.seasonId, this.selectedPhoto).subscribe((data) => {
        this.dialog.close(true);
      });
    }
  }

  public onFileChange(event): void {
    if (event.target.files && event.target.files[0]) {
      this.preview = true;
      const reader = new FileReader();

      reader.onload = (e: any) => {
        document.getElementById('preview').setAttribute('src', e.target.result);
      };
      reader.readAsDataURL(event.target.files[0]);

      this.selectedPhoto = event.target.files[0];
    }
  }

  public isPosition(position: string): boolean {
    return position === this.playerInSeason.position;
  }

  public setPosition(position: string): void {
    this.playerInSeason.position = position as Position;
  }

  public onChangePrice(): void {
    this.playerInSeason.price = this.price * 1000000;
  }
}
