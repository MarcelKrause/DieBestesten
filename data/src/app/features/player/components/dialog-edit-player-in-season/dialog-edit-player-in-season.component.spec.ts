import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEditPlayerInSeasonComponent } from './dialog-edit-player-in-season.component';

describe('DialogEditPlayerInSeasonComponent', () => {
  let component: DialogEditPlayerInSeasonComponent;
  let fixture: ComponentFixture<DialogEditPlayerInSeasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEditPlayerInSeasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEditPlayerInSeasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
