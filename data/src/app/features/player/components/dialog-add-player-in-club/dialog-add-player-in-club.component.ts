import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Notification } from 'src/app/core/models/notification/notification';
import { PlayerInClub } from 'src/app/core/models/player-in-club/player-in-club';
import { Player } from 'src/app/core/models/player/player';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-dialog-add-player-in-club',
  templateUrl: './dialog-add-player-in-club.component.html',
  styleUrls: ['./dialog-add-player-in-club.component.scss'],
})
export class DialogAddPlayerInClubComponent implements OnInit {
  player: Player;
  isActiveClub = true;
  playerInClub: PlayerInClub = new PlayerInClub({ club_id: null });

  fromDateDay: number;
  fromDateMonth: number;
  fromDateYear: number;

  toDateDay: number;
  toDateMonth: number;
  toDateYear: number;

  isValidFromDate: boolean;
  isValidToDate: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<DialogAddPlayerInClubComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.player = data.player;
  }

  ngOnInit(): void {}

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    if (this.isActiveClub) {
      this.playerInClub.toDate = null;
    }

    this.apiService.addPlayerInClub(this.player.playerId, this.playerInClub).subscribe((data) => {
      this.dialog.close({
        success: true,
        club: data,
      });
    });
  }

  public onChangeIsActiveClub(): void {
    this.isActiveClub = !this.isActiveClub;

    if (this.isActiveClub) {
      this.toDateDay = null;
      this.toDateMonth = null;
      this.toDateYear = null;
      this.playerInClub.toDate = null;
    }
  }

  public onChangeFromDate(): void {
    if (this.fromDateDay && this.fromDateMonth && this.fromDateYear && this.fromDateYear.toString().length === 4) {
      const date = new Date(`${this.fromDateYear}-${this.fromDateMonth}-${this.fromDateDay}`);
      if (date.toString() !== 'Invalid Date') {
        this.isValidFromDate = true;
        this.playerInClub.fromDate = date;
      } else {
        this.isValidFromDate = false;
        this.playerInClub.fromDate = null;
      }
    } else {
      this.isValidFromDate = false;
      this.playerInClub.fromDate = null;
    }
  }

  public onChangeToDate(): void {
    if (this.toDateDay && this.toDateMonth && this.toDateYear && this.toDateYear.toString().length === 4) {
      const date = new Date(`${this.toDateYear}-${this.toDateMonth}-${this.toDateDay}`);
      if (date.toString() !== 'Invalid Date') {
        this.isValidToDate = true;
        this.playerInClub.toDate = date;
      } else {
        this.isValidToDate = false;
        this.playerInClub.toDate = null;
      }
    } else {
      this.isValidToDate = false;
      this.playerInClub.toDate = null;
    }
  }
}
