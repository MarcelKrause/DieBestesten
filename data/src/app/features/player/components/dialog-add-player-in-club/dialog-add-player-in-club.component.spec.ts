import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddPlayerInClubComponent } from './dialog-add-player-in-club.component';

describe('DialogAddPlayerInClubComponent', () => {
  let component: DialogAddPlayerInClubComponent;
  let fixture: ComponentFixture<DialogAddPlayerInClubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogAddPlayerInClubComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddPlayerInClubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
