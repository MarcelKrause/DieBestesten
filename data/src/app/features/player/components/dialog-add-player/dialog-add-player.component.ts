import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { Notification } from 'src/app/core/models/notification/notification';
import { Player } from 'src/app/core/models/player/player';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-dialog-add-player',
  templateUrl: './dialog-add-player.component.html',
  styleUrls: ['./dialog-add-player.component.scss'],
})
export class DialogAddPlayerComponent implements OnInit {
  player: Player = new Player([]);

  day: number;
  month: number;
  year: number;
  isValidDate: boolean;

  warning = {
    firstnamelastname: {
      duplicate: false,
    },
  };

  error = {
    kickerId: {
      missing: false,
      duplicate: false,
    },
    displayname: {
      missing: false,
      duplicate: false,
    },
  };

  constructor(
    private dialog: MatDialogRef<DialogAddPlayerComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.player.kickerId = null;
  }

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    this.apiService.addPlayer(this.player).subscribe((data) => {
      this.notificationService.push(new Notification('positive', 'Spieler erfolgreich hinzugefügt'));
      this.dialog.close(new Player(data));
    });
  }

  public onChangeDateOfBirth(): void {
    if (this.year && this.month && this.day && this.year.toString().length === 4) {
      const date = new Date(`${this.year}-${this.month}-${this.day}`);
      if (date.toString() !== 'Invalid Date') {
        this.isValidDate = true;
        this.player.dateOfBirth = date;
      } else {
        this.isValidDate = false;
        this.player.dateOfBirth = null;
      }
    } else {
      this.isValidDate = false;
      this.player.dateOfBirth = null;
    }
  }

  public dataIsValid(): boolean {
    if (this.player.displayname && !this.error.kickerId.duplicate && !this.error.displayname.duplicate) {
      return true;
    } else {
      return false;
    }
  }

  public onValidateInput(inputField: string): void {
    if (inputField === 'kickerId') {
      if (this.player.kickerId) {
        if (this.apiService.playerList.filter((player) => player.kickerId === +this.player.kickerId).length > 0) {
          this.error.kickerId.duplicate = true;
        } else {
          this.error.kickerId.duplicate = false;
        }
      }
    }
    if (inputField === 'displayname') {
      if (!this.player.displayname) {
        this.error.displayname.missing = true;
      } else {
        this.error.displayname.missing = false;
        if (this.apiService.playerList.filter((player) => player.displayname === this.player.displayname).length > 0) {
          this.error.displayname.duplicate = true;
        } else {
          this.error.displayname.duplicate = false;
        }
      }
    }
    if (inputField === 'firstname' || inputField === 'lastname') {
      if (
        this.apiService.playerList.filter(
          (player) => player.firstname === this.player.firstname && player.lastname === this.player.lastname
        ).length > 0
      ) {
        this.warning.firstnamelastname.duplicate = true;
      } else {
        this.warning.firstnamelastname.duplicate = false;
      }
    }
  }
}
