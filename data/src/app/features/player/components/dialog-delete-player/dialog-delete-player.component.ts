import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Notification } from 'src/app/core/models/notification/notification';
import { Player } from 'src/app/core/models/player/player';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-dialog-delete-player',
  templateUrl: './dialog-delete-player.component.html',
  styleUrls: ['./dialog-delete-player.component.scss'],
})
export class DialogDeletePlayerComponent implements OnInit {
  player: Player;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<DialogDeletePlayerComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.player = data.player;
  }

  ngOnInit(): void {}

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onDelete(): void {
    this.apiService.deletePlayer(this.player.playerId).subscribe((data) => {
      this.dialog.close(true);
    });
  }
}
