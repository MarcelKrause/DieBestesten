import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogDeletePlayerComponent } from './dialog-delete-player.component';

describe('DialogDeletePlayerComponent', () => {
  let component: DialogDeletePlayerComponent;
  let fixture: ComponentFixture<DialogDeletePlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogDeletePlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogDeletePlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
