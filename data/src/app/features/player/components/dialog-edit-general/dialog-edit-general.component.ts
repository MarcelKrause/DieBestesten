import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Player } from 'src/app/core/models/player/player';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Notification } from 'src/app/core/models/notification/notification';

@Component({
  selector: 'app-dialog-edit-general',
  templateUrl: './dialog-edit-general.component.html',
  styleUrls: ['./dialog-edit-general.component.scss'],
})
export class DialogEditGeneralComponent implements OnInit {
  originalPlayer: Player;
  editPlayer: Player;

  day: number;
  month: number;
  year: number;
  isValidDate: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<DialogEditGeneralComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.originalPlayer = data.player;
    this.editPlayer = JSON.parse(JSON.stringify(this.originalPlayer));

    if (this.originalPlayer.dateOfBirth) {
      this.day = this.originalPlayer.dateOfBirth.getDate();
      this.month = this.originalPlayer.dateOfBirth.getMonth() + 1;
      this.year = this.originalPlayer.dateOfBirth.getFullYear();
      this.isValidDate = true;
    }
  }

  ngOnInit(): void {}

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    this.apiService.patchPlayer(this.editPlayer).subscribe((data) => {
      this.dialog.close(true);
    });
  }

  public onChangeDateOfBirth(): void {
    if (this.year && this.month && this.day && this.year.toString().length === 4) {
      const date = new Date(`${this.year}-${this.month}-${this.day}`);
      if (date.toString() !== 'Invalid Date') {
        this.isValidDate = true;
        this.editPlayer.dateOfBirth = date;
      } else {
        this.isValidDate = false;
        this.editPlayer.dateOfBirth = null;
      }
    } else {
      this.isValidDate = false;
      this.editPlayer.dateOfBirth = null;
    }
  }

  public dataModified(): boolean {
    if (this.originalPlayer.firstname !== this.editPlayer.firstname) {
      return true;
    } else if (this.originalPlayer.lastname !== this.editPlayer.lastname) {
      return true;
    } else if (this.originalPlayer.displayname !== this.editPlayer.displayname) {
      return true;
    } else if (this.originalPlayer.countryCode !== this.editPlayer.countryCode) {
      return true;
    } else if (this.originalPlayer.city !== this.editPlayer.city) {
      return true;
    } else if (this.originalPlayer.dateOfBirth == null && this.editPlayer.dateOfBirth != null) {
      return true;
    } else if (this.originalPlayer.dateOfBirth && !this.editPlayer.dateOfBirth) {
      return true;
    } else if (!this.originalPlayer.dateOfBirth && this.editPlayer.dateOfBirth) {
      return true;
    } else if (this.originalPlayer.height !== this.editPlayer.height) {
      return true;
    } else if (this.originalPlayer.weight !== this.editPlayer.weight) {
      return true;
    } else if (this.originalPlayer.dateOfBirth && this.editPlayer.dateOfBirth) {
      if (this.originalPlayer.dateOfBirth.getDate() !== new Date(this.editPlayer.dateOfBirth).getDate()) {
        return true;
      } else if (this.originalPlayer.dateOfBirth.getMonth() !== new Date(this.editPlayer.dateOfBirth).getMonth()) {
        return true;
      } else if (this.originalPlayer.dateOfBirth.getFullYear() !== new Date(this.editPlayer.dateOfBirth).getFullYear()) {
        return true;
      }
    } else {
      return false;
    }
  }
}
