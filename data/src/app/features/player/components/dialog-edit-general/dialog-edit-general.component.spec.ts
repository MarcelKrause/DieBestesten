import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEditGeneralComponent } from './dialog-edit-general.component';

describe('DialogEditGeneralComponent', () => {
  let component: DialogEditGeneralComponent;
  let fixture: ComponentFixture<DialogEditGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEditGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEditGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
