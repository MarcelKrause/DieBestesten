import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEditPlayerInClubComponent } from './dialog-edit-player-in-club.component';

describe('DialogEditPlayerInClubComponent', () => {
  let component: DialogEditPlayerInClubComponent;
  let fixture: ComponentFixture<DialogEditPlayerInClubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogEditPlayerInClubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEditPlayerInClubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
