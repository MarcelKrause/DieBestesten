import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PlayerInClub } from 'src/app/core/models/player-in-club/player-in-club';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Notification } from 'src/app/core/models/notification/notification';
import { Player } from 'src/app/core/models/player/player';

@Component({
  selector: 'app-dialog-edit-player-in-club',
  templateUrl: './dialog-edit-player-in-club.component.html',
  styleUrls: ['./dialog-edit-player-in-club.component.scss'],
})
export class DialogEditPlayerInClubComponent implements OnInit {
  playerInClub: PlayerInClub;

  isActiveClub = true;

  fromDateDay: number;
  fromDateMonth: number;
  fromDateYear: number;

  toDateDay: number;
  toDateMonth: number;
  toDateYear: number;

  isValidFromDate: boolean;
  isValidToDate: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<DialogEditPlayerInClubComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.playerInClub = Object.assign({}, data.playerInClub);
    this.fromDateDay = this.playerInClub.fromDate.getDate();
    this.fromDateMonth = this.playerInClub.fromDate.getMonth() + 1;
    this.fromDateYear = this.playerInClub.fromDate.getFullYear();
    this.isValidFromDate = true;

    if (this.playerInClub.toDate) {
      this.toDateDay = this.playerInClub.toDate.getDate();
      this.toDateMonth = this.playerInClub.toDate.getMonth() + 1;
      this.toDateYear = this.playerInClub.toDate.getFullYear();
      this.isValidToDate = true;
      this.isActiveClub = false;
    }
  }

  ngOnInit(): void {}

  public onDelete(): void {
    this.apiService.deletePlayerInClub(this.playerInClub.playerInClubId).subscribe((data) => {
      this.dialog.close(true);
    });
  }

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    if (this.isActiveClub) {
      this.playerInClub.toDate = null;
    }

    this.apiService.patchPlayerInClub(this.playerInClub).subscribe((data) => {
      this.dialog.close(true);
    });
  }

  public onChangeIsActiveClub(): void {
    this.isActiveClub = !this.isActiveClub;

    if (this.isActiveClub) {
      this.toDateDay = null;
      this.toDateMonth = null;
      this.toDateYear = null;
      this.playerInClub.toDate = null;
    }
  }

  public onChangeFromDate(): void {
    if (this.fromDateDay && this.fromDateMonth && this.fromDateYear && this.fromDateYear.toString().length === 4) {
      const date = new Date(`${this.fromDateYear}-${this.fromDateMonth}-${this.fromDateDay}`);
      if (date.toString() !== 'Invalid Date') {
        this.isValidFromDate = true;
        this.playerInClub.fromDate = date;
      } else {
        this.isValidFromDate = false;
        this.playerInClub.fromDate = null;
      }
    } else {
      this.isValidFromDate = false;
      this.playerInClub.fromDate = null;
    }
  }

  public onChangeToDate(): void {
    if (this.toDateDay && this.toDateMonth && this.toDateYear && this.toDateYear.toString().length === 4) {
      const date = new Date(`${this.toDateYear}-${this.toDateMonth}-${this.toDateDay}`);
      if (date.toString() !== 'Invalid Date') {
        this.isValidToDate = true;
        this.playerInClub.toDate = date;
      } else {
        this.isValidToDate = false;
        this.playerInClub.toDate = null;
      }
    } else {
      this.isValidToDate = false;
      this.playerInClub.toDate = null;
    }
  }
}
