import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';
import { Player } from 'src/app/core/models/player/player';
import { MatDialog } from '@angular/material/dialog';
import { DialogAddPlayerComponent } from '../components/dialog-add-player/dialog-add-player.component';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/core/services/storage/storage.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  filter;
  selectedFilterCount: number;
  isLoading = false;
  filteredPlayerList: Player[];

  limit = 25;

  constructor(public apiService: ApiService, public dialog: MatDialog, private router: Router, private storageService: StorageService) {
    this.filter = this.storageService.getItem('filter');
    this.updateSelectedFilterCount();
    this.initialFilterPlayerList();
  }

  ngOnInit(): void {}

  public onToggleFilterView(): void {
    this.filter.player.isExpanded = !this.filter.player.isExpanded;
    this.storageService.setItem('filter', this.filter);
  }

  public onChangeFilter(item = null): void {
    if (item === 'missingCurrentClub') {
      if (this.filter.player.items.missingCurrentClub) {
        this.filter.player.items.onlyBundesligaClub = false;
        this.filter.player.items.clubId = null;
      }
    }
    if (item === 'onlyBundesligaClub') {
      if (this.filter.player.items.onlyBundesligaClub) {
        this.filter.player.items.missingCurrentClub = false;
        this.filter.player.items.clubId = null;
      }
    }
    if (item === 'clubId') {
      if (this.filter.player.items.clubId) {
        this.filter.player.items.missingCurrentClub = false;
        this.filter.player.items.onlyBundesligaClub = false;
      }
    }

    this.storageService.setItem('filter', this.filter);
    this.updateSelectedFilterCount();
    this.filterPlayerList();
  }

  private updateSelectedFilterCount(): void {
    this.selectedFilterCount = 0;
    if (this.filter.player.items.missingCountry) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingCity) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingDateOfBirth) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingHeight) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingWeight) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingPosition) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingPrice) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingPhoto) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingCurrentClub) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.onlyBundesligaClub) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.clubId) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingKickerId) {
      this.selectedFilterCount++;
    }
    if (this.filter.player.items.missingLigainsiderId) {
      this.selectedFilterCount++;
    }
  }

  private initialFilterPlayerList(): void {
    this.isLoading = true;
    if (this.apiService.playerList.length === 0) {
      setTimeout(() => {
        this.initialFilterPlayerList();
      }, 100);
    } else {
      this.filterPlayerList();
    }
  }

  private filterPlayerList(): void {
    this.isLoading = true;
    this.filteredPlayerList = [];
    this.apiService.playerList.forEach((player) => {
      if (this.isPlayerVisible(player)) {
        this.filteredPlayerList.push(player);
      }
    });
    this.isLoading = false;
  }

  public onShowMore(): void {
    this.limit += 25;
  }

  public isPlayerVisible(player: Player): boolean {
    // general data
    if (this.filter.player.items.missingCountry === true && player.countryCode) {
      return false;
    }
    if (this.filter.player.items.missingCity === true && player.city) {
      return false;
    }
    if (this.filter.player.items.missingDateOfBirth === true && player.dateOfBirth) {
      return false;
    }
    if (this.filter.player.items.missingHeight === true && player.height) {
      return false;
    }
    if (this.filter.player.items.missingWeight === true && player.weight) {
      return false;
    }

    // current season
    if (this.filter.player.items.missingPosition === true && player.seasonList[0].position) {
      return false;
    }
    if (this.filter.player.items.missingPrice === true && player.seasonList[0].price) {
      return false;
    }
    if (this.filter.player.items.missingPhoto === true && player.seasonList[0].hasPhoto) {
      return false;
    }

    // club assignment
    if (this.filter.player.items.missingCurrentClub === true && player.getCurrentClub()) {
      return false;
    }
    if (this.filter.player.items.onlyBundesligaClub === true) {
      if (!player.getCurrentClub()) {
        return false;
      } else {
        if (!player.getCurrentClub().isBundesliga) {
          return false;
        }
      }
    }
    if (this.filter.player.items.clubId) {
      if (!player.getCurrentClub()) {
        return false;
      } else {
        if (player.getCurrentClub().clubId !== this.filter.player.items.clubId) {
          return false;
        }
      }
    }

    // technical data
    if (this.filter.player.items.missingKickerId === true && player.kickerId) {
      return false;
    }
    if (this.filter.player.items.missingLigainsiderId === true && player.ligainsiderId) {
      return false;
    }

    return true;
  }

  public onOpenAddPlayerDialog(): void {
    const dialogRef = this.dialog.open(DialogAddPlayerComponent, {
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.apiService.getPlayerList();
        this.router.navigate([`/spielerdatenbank/${result.playerId}`]);
      }
    });
  }

  private sortByVisibility(a: Player, b: Player): number {
    if (a.isVisible < b.isVisible) {
      return 1;
    } else if (a.isVisible > b.isVisible) {
      return -1;
    } else {
      if (a.lastname > b.lastname) {
        return 1;
      } else if (a.lastname < b.lastname) {
        return -1;
      }
    }
  }
}
