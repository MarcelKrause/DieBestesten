import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerRoutingModule } from './player.routing';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageModule } from 'src/app/core/image.module';
import { CoreModule } from 'src/app/core/core.module';

// Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';

// Components
import { IndexComponent } from './index/index.component';
import { DetailsComponent } from './pages/details/details.component';
import { DialogEditGeneralComponent } from './components/dialog-edit-general/dialog-edit-general.component';
import { DialogAddPlayerInClubComponent } from './components/dialog-add-player-in-club/dialog-add-player-in-club.component';
import { DialogEditPlayerInClubComponent } from './components/dialog-edit-player-in-club/dialog-edit-player-in-club.component';
import { DialogEditPlayerInSeasonComponent } from './components/dialog-edit-player-in-season/dialog-edit-player-in-season.component';
import { DialogDeletePlayerComponent } from './components/dialog-delete-player/dialog-delete-player.component';
import { DialogAddPlayerComponent } from './components/dialog-add-player/dialog-add-player.component';
import { DialogEditTechnicalComponent } from './components/dialog-edit-technical/dialog-edit-technical.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    PlayerRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDialogModule,
    MatCheckboxModule,
    MatRadioModule,
    ImageModule,
  ],
  declarations: [
    IndexComponent,
    DetailsComponent,
    DialogEditGeneralComponent,
    DialogAddPlayerInClubComponent,
    DialogEditPlayerInClubComponent,
    DialogEditPlayerInSeasonComponent,
    DialogDeletePlayerComponent,
    DialogAddPlayerComponent,
    DialogEditTechnicalComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PlayerModule {}
