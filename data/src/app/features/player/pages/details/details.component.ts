import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Player } from 'src/app/core/models/player/player';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogEditGeneralComponent } from '../../components/dialog-edit-general/dialog-edit-general.component';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { Notification } from 'src/app/core/models/notification/notification';
import { DialogAddPlayerInClubComponent } from '../../components/dialog-add-player-in-club/dialog-add-player-in-club.component';
import { Title } from '@angular/platform-browser';
import { DialogEditPlayerInClubComponent } from '../../components/dialog-edit-player-in-club/dialog-edit-player-in-club.component';
import { PlayerInClub } from 'src/app/core/models/player-in-club/player-in-club';
import { DialogEditPlayerInSeasonComponent } from '../../components/dialog-edit-player-in-season/dialog-edit-player-in-season.component';
import { DialogDeletePlayerComponent } from '../../components/dialog-delete-player/dialog-delete-player.component';
import { DialogEditTechnicalComponent } from '../../components/dialog-edit-technical/dialog-edit-technical.component';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  player: Player;
  activity: any;
  invalidParam: boolean;

  constructor(
    public apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private notificationService: NotificationService,
    private titleService: Title,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params) => {
      this.getPlayer(params.playerId);
    });
  }

  public onOpenEditGeneralDialog(): void {
    const dialogRef = this.dialog.open(DialogEditGeneralComponent, {
      data: {
        player: this.player,
      },
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationService.push(new Notification('positive', `Allgemeine Daten von ${this.player.displayname} geändert.`));
        this.getPlayer(this.route.snapshot.paramMap.get('playerId'));
      }
    });
  }

  public onOpenEditTechnicalDialog(): void {
    const dialogRef = this.dialog.open(DialogEditTechnicalComponent, {
      data: {
        player: this.player,
      },
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationService.push(new Notification('positive', `Technische Daten von ${this.player.displayname} geändert.`));
        this.getPlayer(this.route.snapshot.paramMap.get('playerId'));
      }
    });
  }

  public onOpenEditPlayerInSeasonDialog(seasonId: string): void {
    const dialogRef = this.dialog.open(DialogEditPlayerInSeasonComponent, {
      data: {
        playerInSeason: this.player.seasonList.filter((playerInSeason) => playerInSeason.seasonId === seasonId)[0],
      },
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationService.push(new Notification('positive', `Saison Daten von ${this.player.displayname} geändert.`));
        this.getPlayer(this.route.snapshot.paramMap.get('playerId'));
      }
    });
  }

  public onOpenAddClubDialog(): void {
    const dialogRef = this.dialog.open(DialogAddPlayerInClubComponent, {
      data: {
        player: this.player,
      },
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.success) {
        this.notificationService.push(
          new Notification('positive', `${result.club.club_name} als Club von ${this.player.displayname} eingetragen.`)
        );
        this.apiService.getPlayerList();
        this.getPlayer(this.route.snapshot.paramMap.get('playerId'));
      }
    });
  }

  public onOpenEditPlayerInClubDialog(playerInClub: PlayerInClub): void {
    const dialogRef = this.dialog.open(DialogEditPlayerInClubComponent, {
      data: {
        playerInClub,
      },
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationService.push(new Notification('positive', `Clubzugehörigkeit erfolgreich geändert`));
        this.apiService.getPlayerList();
        this.getPlayer(this.route.snapshot.paramMap.get('playerId'));
      }
    });
  }

  public onOpenDeletePlayerDialog(player: Player): void {
    const dialogRef = this.dialog.open(DialogDeletePlayerComponent, {
      data: {
        player,
      },
      width: '420px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.notificationService.push(new Notification('positive', `Spieler erfolgreich gelöscht`));
        this.apiService.getPlayerList();
        this.router.navigate(['/spielerdatenbank']);
      }
    });
  }

  public copyToClipboard(content: string): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = content;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.notificationService.push(new Notification('positive', `${content} erfolgreich in die Zwischenablage kopiert`));
  }

  private getPlayer(playerId: string): void {
    this.player = null;
    this.apiService.getPlayer(playerId).subscribe(
      (data) => {
        this.invalidParam = false;
        this.player = new Player(data);
        this.titleService.setTitle(`${this.player.firstname} ${this.player.lastname} | DieBestesten`);
      },
      (error) => {},
      () => {
        if (!this.player) {
          this.invalidParam = true;
        } else {
          this.getActivity(1);
        }
      }
    );
  }

  public getActivity(page: number): void {
    this.apiService.getActivityByReference(page, this.player.playerId).subscribe((activity) => {
      this.activity = activity;
    });
  }

  public getDateString(date: Date): string {
    date = new Date(date);
    const today = new Date();

    if (today.getDate() === date.getDate() && today.getMonth() === date.getMonth() && today.getFullYear() === date.getFullYear()) {
      return 'Heute, ';
    } else if (
      today.getDate() === date.getDate() - 1 &&
      today.getMonth() === date.getMonth() - 1 &&
      today.getFullYear() === date.getFullYear() - 1
    ) {
      return 'Gestern, ';
    } else {
      return `${date.getDate()}.${date.getMonth()}.${date.getFullYear()}, `;
    }
  }

  public isForeignCity(city: string): string {
    const possibleCountryCode = city.substr(city.length - 4, 4);
    if (possibleCountryCode.charAt(0) === '(' && possibleCountryCode.charAt(3) === ')') {
      return possibleCountryCode.substr(1, 2);
    }
    return null;
  }
}
