// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { IndexComponent } from './index/index.component';
import { DetailsComponent } from './pages/details/details.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: ':playerId', component: DetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PlayerRoutingModule {}
