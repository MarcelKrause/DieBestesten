import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarRoutingModule } from './calendar.routing';
import { IndexComponent } from './index/index.component';

@NgModule({
  imports: [CommonModule, CalendarRoutingModule],
  declarations: [IndexComponent],
})
export class CalendarModule {}
