import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';
import { StorageService } from 'src/app/core/services/storage/storage.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  filter;
  selectedFilterCount: number;

  constructor(public apiService: ApiService, private storageService: StorageService) {
    this.filter = this.storageService.getItem('filter');
    this.updateSelectedFilterCount();
  }

  ngOnInit(): void {}

  public onToggleFilterView(): void {
    this.filter.country.isExpanded = !this.filter.country.isExpanded;
    this.storageService.setItem('filter', this.filter);
  }

  public onChangeFilter(): void {
    this.storageService.setItem('filter', this.filter);
    this.updateSelectedFilterCount();
  }

  private updateSelectedFilterCount(): void {
    this.selectedFilterCount = 0;
    if (this.filter.country.items.countriesWithPlayer) {
      this.selectedFilterCount++;
    }
  }

  public isCountryVisible(country: any): boolean {
    if (this.filter.country.items.countriesWithPlayer) {
      if (this.getPlayerCountByCountry(country.country_code) === 0) {
        return false;
      }
    }
    return true;
  }

  public getPlayerCountByCountry(countryCode: string): number {
    return this.apiService.playerList.filter((player) => player.countryCode === countryCode).length;
  }
}
