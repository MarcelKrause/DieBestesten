import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Club } from 'src/app/core/models/club/club';
import { Notification } from 'src/app/core/models/notification/notification';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { AddClubComponent } from '../dialogs/add-club/add-club.component';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  filter;
  selectedFilterCount: number;

  bundesliga: Club[] = [];
  rest: Club[] = [];

  constructor(
    public apiService: ApiService,
    public dialog: MatDialog,
    private storageService: StorageService,
    private notificationService: NotificationService
  ) {
    this.filter = this.storageService.getItem('filter');
    this.updateSelectedFilterCount();
    this.apiService.clubList.forEach((club) => {
      if (club.isBundesliga) {
        this.bundesliga.push(club);
      } else {
        this.rest.push(club);
      }
    });
  }

  ngOnInit(): void {}

  public onToggleFilterView(): void {
    this.filter.club.isExpanded = !this.filter.club.isExpanded;
    this.storageService.setItem('filter', this.filter);
  }

  public onChangeFilter(): void {
    this.storageService.setItem('filter', this.filter);
    this.updateSelectedFilterCount();
  }

  private updateSelectedFilterCount(): void {
    this.selectedFilterCount = 0;
    if (this.filter.club.items.germanClubs) {
      this.selectedFilterCount++;
    }
  }

  public isClubVisible(club: Club): boolean {
    if (this.filter.club.items.germanClubs) {
      if (club.countryCode !== 'de') {
        return false;
      }
    }
    return true;
  }

  public onOpenAddClubDialog(): void {
    const dialogRef = this.dialog.open(AddClubComponent, {
      width: '320px',
      disableClose: true,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.apiService.getClubList();
      }
    });
  }

  public copyToClipboard(content: string): void {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = content;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.notificationService.push(new Notification('positive', `${content} erfolgreich in die Zwischenablage kopiert`));
  }
}
