import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClubRoutingModule } from './club.routing';
import { FormsModule } from '@angular/forms';

// Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';

// Components
import { IndexComponent } from './index/index.component';
import { AddClubComponent } from './dialogs/add-club/add-club.component';

@NgModule({
  declarations: [IndexComponent, AddClubComponent],
  imports: [
    CommonModule,
    ClubRoutingModule,
    FormsModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
  ],
})
export class ClubModule {}
