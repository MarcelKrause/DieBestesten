import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Club } from 'src/app/core/models/club/club';
import { Notification } from 'src/app/core/models/notification/notification';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-add-club',
  templateUrl: './add-club.component.html',
  styleUrls: ['./add-club.component.scss'],
})
export class AddClubComponent implements OnInit {
  club: Club = new Club([]);

  error = {
    name: {
      missing: false,
      duplicate: false,
    },
  };

  constructor(
    private dialog: MatDialogRef<AddClubComponent>,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.club.countryCode = 'de';
  }

  ngOnInit(): void {}

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onSave(): void {
    this.apiService.addClub(this.club).subscribe((data) => {
      this.notificationService.push(new Notification('positive', 'Club erfolgreich hinzugefügt'));
      this.dialog.close(new Club(data));
    });
  }

  public dataIsValid(): boolean {
    if (this.club.name && !this.error.name.duplicate) {
      return true;
    } else {
      return false;
    }
  }

  public onValidateInput(inputField: string): void {
    if (inputField === 'name') {
      if (!this.club.name) {
        this.error.name.missing = true;
      } else {
        this.error.name.missing = false;
        if (this.apiService.clubList.filter((club) => club.name.toLowerCase() === this.club.name.toLowerCase()).length > 0) {
          this.error.name.duplicate = true;
        } else {
          this.error.name.duplicate = false;
        }
      }
    }
  }
}
