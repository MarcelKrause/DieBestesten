import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router, UrlSerializer } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PlayerInClub } from '../../models/player-in-club/player-in-club';
import { Player } from '../../models/player/player';
import { Club } from '../../models/club/club';
import { AuthService } from '../auth/auth.service';
import { PlayerInSeason } from '../../models/player-in-season/player-in-season';
import { PlayerRating } from '../../models/player-rating/player-rating';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public status;
  public countryList: any[];
  public clubList: Club[];
  public playerList: Player[] = [];
  public sessionList: any[];

  constructor(private http: HttpClient, private authService: AuthService, private router: Router, private serializer: UrlSerializer) {
    if (localStorage.getItem('token')) {
      this.getStatus();
      this.getCountryList();
      this.getClubList();
      this.getSessionList();
      if (this.playerList.length === 0) {
        this.getPlayerList();
      }
    }
  }

  public getStatus(): void {
    this.http.get(`${environment.api}/status`).subscribe((data) => {
      this.status = data;
    });
  }

  public getClubList(): void {
    this.clubList = [];
    this.http.get(`${environment.api}/club`).subscribe((data: any[]) => {
      data.forEach((element) => {
        this.clubList.push(new Club(element));
      });
    });
  }

  public getCountryList(): void {
    this.http.get(`${environment.api}/country`).subscribe((data: any[]) => {
      this.countryList = data;
    });
  }

  public getMiscalculatedPoints(): Observable<any> {
    return this.http.get(`${environment.api}/player_rating/conflict`);
  }

  public getPlayerRatingByClubAndMatchday(clubId: string, matchday: number): Observable<any> {
    return this.http.get(`${environment.api}/player_rating/matchday?club_id=${clubId}&matchday=${matchday}`);
  }

  public patchPlayerRating(playerRating: PlayerRating): Observable<any> {
    const body = new URLSearchParams();

    if (playerRating.grade) {
      body.set('grade', playerRating.grade.toString());
    }
    if (playerRating.ligainsider_grade) {
      body.set('ligainsider_grade', playerRating.ligainsider_grade.toString());
    }
    body.set('start_lineup', Number(playerRating.startLineup).toString());
    body.set('substitution', Number(playerRating.substitution).toString());
    body.set('goals', playerRating.goals.toString());
    body.set('assists', playerRating.assists.toString());
    body.set('sds', Number(playerRating.sds).toString());
    body.set('clean_sheet', Number(playerRating.cleanSheet).toString());
    body.set('red_card', Number(playerRating.redCard).toString());
    body.set('yellow_red_card', Number(playerRating.yellowRedCard).toString());
    body.set('points', playerRating.points.toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/player_rating/${playerRating.playerRatingId}`, body.toString(), options);
  }

  public getPlayerList(): void {
    this.http.get(`${environment.api}/player`).subscribe((data: any[]) => {
      this.playerList = [];
      data.forEach((element) => {
        this.playerList.push(new Player(element));
      });
    });
  }

  public getBundesligaPlayerList(): Observable<any> {
    return this.http.get(`${environment.api}/player?filter=kicker-abgleich`);
  }

  public getSessionList(): void {
    this.http.get(`${environment.api}/session/active`).subscribe((data: any[]) => {
      this.sessionList = [];
      data.forEach((element) => {
        this.sessionList.push(element);
      });
    });
  }

  public getPlayer(playerId: string): Observable<any> {
    return this.http.get(`${environment.api}/player/${playerId}`);
  }

  public addPlayerRating(player_id: string, club_id: string, matchdayNumber: number): Observable<any> {
    const body = new URLSearchParams();
    body.set('player_id', player_id);
    body.set('season_id', this.status.season.season_id);
    body.set('club_id', club_id);

    body.set('matchday_number', matchdayNumber.toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post(`${environment.api}/player_rating`, body.toString(), options);
  }

  public addPlayer(player: Player): Observable<any> {
    const body = new URLSearchParams();
    if (player.kickerId) {
      body.set('kicker_id', player.kickerId.toString().trim());
    }
    body.set('firstname', player.firstname.trim());
    body.set('lastname', player.lastname.trim());
    body.set('displayname', player.displayname.trim());
    if (player.city) {
      body.set('city', player.city.trim());
    }
    if (player.countryCode) {
      body.set('country_code', player.countryCode);
    }
    if (player.dateOfBirth) {
      body.set('date_of_birth', this.formatDate(player.dateOfBirth));
    }
    if (player.height) {
      body.set('height', player.height.toString());
    }
    if (player.weight) {
      body.set('weight', player.weight.toString());
    }

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post(`${environment.api}/player`, body.toString(), options);
  }

  public patchPlayer(player: Player): Observable<any> {
    const body = new URLSearchParams();
    body.set('firstname', player.firstname);
    body.set('lastname', player.lastname);
    body.set('displayname', player.displayname);
    if (player.city) {
      body.set('city', player.city);
    }
    if (player.countryCode) {
      body.set('country_code', player.countryCode);
    }
    if (player.dateOfBirth) {
      body.set('date_of_birth', this.formatDate(player.dateOfBirth));
    }
    if (player.height) {
      body.set('height', player.height.toString());
    }
    if (player.weight) {
      body.set('weight', player.weight.toString());
    }

    if (player.kickerId) {
      body.set('kicker_id', player.kickerId.toString().trim());
    }
    if (player.ligainsiderId) {
      body.set('ligainsider_id', player.ligainsiderId.toString().trim());
    }

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/player/${player.playerId}`, body.toString(), options);
  }

  public addPlayerInClub(playerId: string, playerInClub: PlayerInClub): Observable<any> {
    let fromDate = null;
    let toDate = null;

    if (playerInClub.fromDate) {
      fromDate = `${playerInClub.fromDate.getFullYear()}-${playerInClub.fromDate.getMonth() + 1}-${playerInClub.fromDate.getDate()}`;
    }

    if (playerInClub.toDate) {
      toDate = `${playerInClub.toDate.getFullYear()}-${playerInClub.toDate.getMonth() + 1}-${playerInClub.toDate.getDate()}`;
    }

    const body = new FormData();
    body.append('player_id', playerId);
    body.append('club_id', playerInClub.clubId);
    body.append('from_date', fromDate);
    if (playerInClub.toDate) {
      body.append('to_date', toDate);
    }
    body.set('is_loan', Number(playerInClub.isLoan).toString());

    return this.http.post(`${environment.api}/player_in_club`, body);
  }

  public patchPlayerInClub(playerInClub: PlayerInClub): Observable<any> {
    const body = new URLSearchParams();
    body.set('from_date', this.formatDate(playerInClub.fromDate));
    if (playerInClub.toDate) {
      body.set('to_date', this.formatDate(playerInClub.toDate));
    }
    body.set('is_loan', Number(playerInClub.isLoan).toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/player_in_club/${playerInClub.playerInClubId}`, body.toString(), options);
  }

  public deletePlayer(playerId: string): Observable<any> {
    return this.http.delete(`${environment.api}/player/${playerId}`);
  }

  public editPlayerInSeason(playerInSeason: PlayerInSeason): Observable<any> {
    const body = new URLSearchParams();

    if (playerInSeason.position) {
      body.set('position', playerInSeason.position.toString());
    }
    if (playerInSeason.price && playerInSeason.price > 0) {
      body.set('price', playerInSeason.price.toString());
    }
    body.set('is_captain', Number(playerInSeason.is_captain).toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };
    return this.http.patch(`${environment.api}/player_in_season/${playerInSeason.playerInSeasonId}`, body.toString(), options);
  }

  public addPrice(displayname: string, price: number): Observable<any> {
    const body = new URLSearchParams();
    body.set('displayname', displayname);
    body.set('price', price.toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };
    return this.http.patch(`${environment.api}/player_in_season`, body.toString(), options);
  }

  public deletePlayerInClub(playerInClubId: string): Observable<any> {
    return this.http.delete(`${environment.api}/player_in_club/${playerInClubId}`);
  }

  public getPlayerCount(): Observable<any> {
    return this.http.get(`${environment.api}/stats/player_count`);
  }

  public getPlayerHeightDistribution(): Observable<any> {
    return this.http.get(`${environment.api}/stats/height_distribution`);
  }

  public getPlayerWeightDistribution(): Observable<any> {
    return this.http.get(`${environment.api}/stats/weight_distribution`);
  }

  public getPlayerDataProgress(): Observable<any> {
    return this.http.get(`${environment.api}/stats/data_progress`);
  }

  public getSeasonChart(): Observable<any> {
    return this.http.get(`${environment.api}/stats/season_chart`);
  }

  public getActivity(page: number = 1): Observable<any> {
    return this.http.get(`${environment.api}/activity?page=${page}`);
  }

  public getActivityByReference(page: number = 1, referenceId: string): Observable<any> {
    return this.http.get(`${environment.api}/activity?page=${page}&ref_id=${referenceId}`);
  }

  public setSeasonData(kickerId: number, position: string, price: number): Observable<any> {
    const body = {
      kickerId,
      position,
      price,
    };
    return this.http.patch(`${environment.api}/player_in_season`, body);
  }

  public getSearchResult(searchRequest: string): Observable<any> {
    return this.http.get(`${environment.api}/search/${searchRequest}`);
  }

  public getWorldmapData(): Observable<any> {
    return this.http.get(`${environment.api}/stats/worldmap`);
  }

  public addClub(club: Club): Observable<any> {
    const body = new URLSearchParams();
    body.set('name', club.name.trim());
    if (club.countryCode) {
      body.set('country_code', club.countryCode);
    }

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post(`${environment.api}/club`, body.toString(), options);
  }

  public editPlayerImage(playerId: string, seasonId: string, photo: File): Observable<any> {
    const formData = new FormData();
    formData.append('player_id', playerId);
    formData.append('season_id', seasonId);
    formData.append('image', photo);
    return this.http.post(`${environment.image_api}/player/`, formData);
  }

  public checkPlayerRating(file: File): Observable<any> {
    const formData = new FormData();
    formData.append('data', file);

    return this.http.post(`${environment.api}/player_rating_check`, formData, {
      reportProgress: true,
      responseType: 'json',
    });
  }

  private formatDate(input: Date): string {
    if (input == null) {
      return null;
    }

    input = new Date(input);

    const month = input.getMonth() + 1;
    return input.getFullYear() + '-' + month + '-' + input.getDate();
  }

  public finishMatchday(season_id: string, matchday_number: number): Observable<any> {
    const formData = new FormData();
    formData.append('season_id', season_id);
    formData.append('matchday_number', matchday_number.toString());
    return this.http.post(`${environment.api}/matchday/`, formData);
  }
}
