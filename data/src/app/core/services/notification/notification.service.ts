import { Injectable } from '@angular/core';
import { Notification } from 'src/app/core/models/notification/notification';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  notificationStack: Notification[] = [];

  constructor() {}

  public push(notification: Notification): void {
    this.notificationStack.push(notification);

    let ttl;
    if (notification.type === 'positive') {
      ttl = 4000;
    }
    if (notification.type === 'negative') {
      ttl = 6000;
    }

    setTimeout(() => {
      this.notificationStack.shift();
    }, ttl);
  }
}
