import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NotificationService } from '../notification/notification.service';
import { Notification } from '../../models/notification/notification';
import { Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private notificationService: NotificationService) {}

  public isLoggedIn(): boolean {
    const token = localStorage.getItem('token');

    if (token) {
      // TODO check token
      return true;
    } else {
      return false;
    }
  }

  public setToken(token: string): any {
    return localStorage.setItem('token', token);
  }

  public getToken(): any {
    return localStorage.getItem('token');
  }

  public getManager(): any {
    return jwt_decode(localStorage.getItem('token'));
  }

  public login(name: string, password: string): void {
    if (name === '' || password === '') {
      this.notificationService.push(new Notification('negative', 'Nicht alle Felder ausgefüllt'));
      return;
    }

    const body = new FormData();
    body.append('name', name);
    body.append('password', password);

    this.http.post(`${environment.api}/auth`, body).subscribe(
      (data: any) => {
        localStorage.clear();
        localStorage.setItem('token', data.token);
        window.location.reload();
      },
      (error) => {
        this.notificationService.push(new Notification('negative', error));
      }
    );
  }

  public logout(): void {
    this.http.delete(`${environment.api}/auth`).subscribe((data) => {
      localStorage.removeItem('token');
      localStorage.clear();
      window.location.href = environment.baseRef;
    });
  }

  public changePassword(password: string): Observable<any> {
    const body = {
      password,
    };

    return this.http.patch(`${environment.api}/auth`, body);
  }
}
