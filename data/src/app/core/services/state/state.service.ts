import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  sidebar: any;

  constructor() {
    this.sidebar = JSON.parse(localStorage.getItem('sidebar'));
    if (!this.sidebar) {
      this.initSidebarSettings();
    }
  }

  public toggleSidebarVisibility(): boolean {
    this.sidebar.visible = !this.sidebar.visible;
    localStorage.setItem('sidebar', JSON.stringify(this.sidebar));
    return this.sidebar.visible;
  }

  public toggleSidebarWidth(): boolean {
    this.sidebar.expanded = !this.sidebar.expanded;
    localStorage.setItem('sidebar', JSON.stringify(this.sidebar));
    return this.sidebar.expanded;
  }

  private initSidebarSettings(): void {
    localStorage.setItem('sidebar', JSON.stringify({ expanded: true, visible: true }));
    this.sidebar = JSON.parse(localStorage.getItem('sidebar'));
  }
}
