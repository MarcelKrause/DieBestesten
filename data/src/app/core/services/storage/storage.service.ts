import { Injectable } from '@angular/core';

const initialFilter = {
  player: {
    isExpanded: false,
    items: {
      missingCountry: false,
      missingCity: false,
      missingDateOfBirth: false,
      missingHeight: false,
      missingWeight: false,
      missingPosition: false,
      missingPrice: false,
      missingPhoto: false,
      missingCurrentClub: false,
      onlyBundesligaClub: null,
      clubId: null,
      missingKickerId: false,
      missingLigainsiderId: false,
    },
  },
  country: {
    isExpanded: false,
    items: {
      countriesWithPlayer: false,
    },
  },
  club: {
    isExpanded: false,
    items: {
      germanClubs: false,
    },
  },
};

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {
    if (!localStorage.getItem('filter')) {
      localStorage.setItem('filter', JSON.stringify(initialFilter));
    } else {
      // this.updateFilter();
    }
  }

  public setItem(key: string, item: JSON): void {
    localStorage.setItem(key, JSON.stringify(item));
  }

  public getItem(key: string): JSON {
    return JSON.parse(localStorage.getItem(key));
  }
}
