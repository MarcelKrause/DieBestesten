import { Injectable, isDevMode } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { NotificationService } from '../../services/notification/notification.service';
import { Notification } from '../../models/notification/notification';
import { AuthService } from '../../services/auth/auth.service';
import { environment } from 'src/environments/environment';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private notificationService: NotificationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      tap((event) => {}),
      catchError((error: any) => {
        if (error.status >= 400 && error.status < 500) {
          // CLIENT ERROR
          if (error.status === 401) {
            localStorage.removeItem('token');
            window.location.href = environment.baseRef;
          } else {
            this.notificationService.push(new Notification('negative', error.error.message));
          }
        }
        if (error.status >= 500 && error.status < 600) {
          // SERVER ERROR
          //this.notificationService.push(new Notification('negative', error.statusText));
        }
        return of(error);
      })
    );
  }
}
