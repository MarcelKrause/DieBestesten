import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageComponent } from './ui/image/image.component';
import { AuthImagePipe } from './pipes/auth-image/auth-image.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [ImageComponent, AuthImagePipe],
  exports: [ImageComponent],
})
export class ImageModule {}
