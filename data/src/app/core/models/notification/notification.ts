export class Notification {
  type: 'positive' | 'negative';
  message: string;

  constructor(type: 'positive' | 'negative', message: string) {
    this.type = type;
    this.message = message;
  }
}
