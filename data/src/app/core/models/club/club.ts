export class Club {
  clubId: string;
  countryCode: string;
  name: string;
  isBundesliga: boolean;

  constructor(element) {
    this.clubId = element.club_id;
    this.countryCode = element.country_code;
    this.name = element.name;
    this.isBundesliga = Boolean(Number(element.is_bundesliga));
  }
}
