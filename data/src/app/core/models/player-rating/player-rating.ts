import { Position } from '../position/position';

export class PlayerRating {
  playerRatingId: string;
  matchdayNumber: number;
  startLineup: boolean;
  substitution: boolean;
  grade: number;
  ligainsider_grade: number;
  goals: number;
  assists: number;
  sds: boolean;
  cleanSheet: boolean;
  yellowRedCard: boolean;
  redCard: boolean;
  points: number;

  constructor(element) {
    this.playerRatingId = element.player_rating_id;
    this.matchdayNumber = +element.matchday;
    this.startLineup = Boolean(Number(element.start_lineup));
    this.substitution = Boolean(Number(element.substitution));
    this.grade = +element.grade;
    this.ligainsider_grade = +element.ligainsider_grade;
    this.goals = +element.goals;
    this.assists = +element.assists;
    this.sds = Boolean(Number(element.sds));
    this.cleanSheet = Boolean(Number(element.clean_sheet));
    this.yellowRedCard = Boolean(Number(element.yellow_red_card));
    this.redCard = Boolean(Number(element.red_card));
    this.points = +element.points;
  }

  public calcPoints(position: Position): void {
    let points = 0;
    if (this.startLineup) {
      points += 2;
    }
    if (this.substitution) {
      points += 1;
    }
    if (this.redCard) {
      points -= 6;
    }
    if (this.yellowRedCard) {
      points -= 3;
    }
    if (this.sds) {
      points += 3;
    }
    if (this.cleanSheet) {
      points += 2;
    }
    points += this.assists;

    if (this.goals > 0) {
      if (position.toString() === 'FORWARD') {
        points += this.goals * 3;
      } else if (position.toString() === 'MIDFIELDER') {
        points += this.goals * 4;
      } else if (position.toString() === 'DEFENDER') {
        points += this.goals * 5;
      } else if (position.toString() === 'GOALKEEPER') {
        points += this.goals * 6;
      }
    }

    switch (this.grade) {
      case 1.0:
        points += 10;
        break;
      case 1.5:
        points += 8;
        break;
      case 2.0:
        points += 6;
        break;
      case 2.5:
        points += 4;
        break;
      case 3.0:
        points += 2;
        break;
      case 3.5:
        points += 0;
        break;
      case 4.0:
        points -= 2;
        break;
      case 4.5:
        points -= 4;
        break;
      case 5.0:
        points -= 6;
        break;
      case 5.5:
        points -= 8;
        break;
      case 6.0:
        points -= 10;
        break;
      default:
        break;
    }
    this.points = points;
  }
}
