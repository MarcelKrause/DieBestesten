import { PlayerInSeason } from '../player-in-season/player-in-season';
import { PlayerInClub } from '../player-in-club/player-in-club';
import { PlayerRating } from '../player-rating/player-rating';

export class Player {
  playerId: string;
  kickerId: number;
  ligainsiderId: number;
  countryCode: string;
  countryName: string;
  firstname: string;
  lastname: string;
  displayname: string;
  city: string;
  dateOfBirth: Date;
  height: number = null;
  weight: number = null;
  points: number = null;

  seasonList: PlayerInSeason[];
  clubList: PlayerInClub[];
  ratingList: PlayerRating[];

  isVisible: boolean;

  constructor(element) {
    this.playerId = element.player_id;
    if (element.kicker_id) {
      this.kickerId = +element.kicker_id;
    }
    if (element.ligainsider_id) {
      this.ligainsiderId = +element.ligainsider_id;
    }
    this.countryCode = element.country_code;
    this.countryName = element.country_name;
    this.firstname = element.firstname;
    this.lastname = element.lastname;
    this.displayname = element.displayname;
    this.city = element.city;

    if (element.date_of_birth) {
      this.dateOfBirth = new Date(element.date_of_birth);
    }
    if (element.height) {
      this.height = +element.height;
    }
    if (element.weight) {
      this.weight = +element.weight;
    }
    if (element.points) {
      this.points = +element.points;
    }

    this.seasonList = [];
    if (element.season_list) {
      element.season_list.forEach((playerInSeason) => {
        this.seasonList.push(new PlayerInSeason(playerInSeason));
      });
    }

    this.clubList = [];
    if (element.club_list) {
      element.club_list.forEach((playerInClub) => {
        this.clubList.push(new PlayerInClub(playerInClub));
      });
    }

    this.ratingList = [];
    if (element.rating_list) {
      element.rating_list.forEach((playerRating) => {
        this.ratingList.push(new PlayerRating(playerRating));
      });
    }

    this.isVisible = true;
  }

  public getCurrentClub(): PlayerInClub {
    return this.clubList.filter((playerInClub) => playerInClub.toDate == null)[0];
  }

  public getSeason(seasonId: string): PlayerInSeason {
    return this.seasonList.filter((playerInSeason) => playerInSeason.seasonId === seasonId)[0];
  }

  public getAge(): number {
    const today = new Date();
    let age = today.getFullYear() - this.dateOfBirth.getFullYear();
    const m = today.getMonth() - this.dateOfBirth.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < this.dateOfBirth.getDate())) {
      age--;
    }

    return age;
  }
}
