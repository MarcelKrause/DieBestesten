import { Position } from '../position/position';

export class PlayerInSeason {
  playerInSeasonId: string;
  playerId: string;
  seasonId: string;
  seasonName: string;
  position: Position;
  price: number;
  is_captain: boolean;
  hasPhoto: boolean;

  constructor(element) {
    this.playerInSeasonId = element.player_in_season_id;
    this.playerId = element.player_id;
    this.seasonId = element.season_id;
    this.seasonName = element.season_name;
    this.position = element.position;
    this.price = element.price;

    this.is_captain = Boolean(JSON.parse(element.is_captain));

    if (element.has_photo) {
      this.hasPhoto = Boolean(JSON.parse(element.has_photo));
    }
  }

  public getPositionString(): string {
    return Position[this.position];
  }

  public getPriceString(): string {
    return (this.price / 1000000).toFixed(1);
  }
}
