export class PlayerInClub {
  playerInClubId: string;
  clubId: string;
  clubName: string;
  fromDate: Date;
  toDate: Date;
  isBundesliga: boolean;
  isLoan: boolean;

  constructor(element) {
    this.playerInClubId = element.player_in_club_id;
    this.clubId = element.club_id;
    this.clubName = element.club_name;
    this.isBundesliga = Boolean(Number(element.is_bundesliga));
    this.isLoan = Boolean(Number(element.is_loan));

    if (element.from_date) {
      this.fromDate = new Date(element.from_date);
    }
    if (element.to_date) {
      this.toDate = new Date(element.to_date);
    }
  }
}
