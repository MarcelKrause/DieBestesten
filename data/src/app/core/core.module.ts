// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ImageModule } from './image.module';
import { MatDialogModule } from '@angular/material/dialog';

// Components
import { SidebarComponent } from './ui/sidebar/sidebar.component';
import { TopbarComponent } from './ui/topbar/topbar.component';
import { NotificationComponent } from './ui/notification/notification.component';
import { NotFoundComponent } from './ui/not-found/not-found.component';
import { PlayerRatingFilterPipe } from './pipes/player-rating-filter/player-rating-filter.pipe';

@NgModule({
  imports: [CommonModule, ImageModule, HttpClientModule, RouterModule, FormsModule, MatDialogModule],
  declarations: [SidebarComponent, TopbarComponent, NotificationComponent, NotFoundComponent],
  exports: [SidebarComponent, TopbarComponent, NotificationComponent, NotFoundComponent],
})
export class CoreModule {}
