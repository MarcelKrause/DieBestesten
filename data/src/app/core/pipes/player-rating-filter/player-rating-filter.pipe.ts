import { Pipe, PipeTransform } from '@angular/core';
import { Player } from '../../models/player/player';

@Pipe({
  name: 'playerRatingFilter',
  pure: false,
})
export class PlayerRatingFilterPipe implements PipeTransform {
  transform(playerList: Player[], type: string): any {
    switch (type) {
      case 'startLineup':
        return playerList.filter((player) => player.ratingList[0].startLineup);
        break;
      case 'substitution':
        return playerList.filter((player) => player.ratingList[0].substitution);
        break;
      case 'rest':
        return playerList.filter((player) => !player.ratingList[0].startLineup && !player.ratingList[0].substitution);
        break;
      default:
        return playerList;
    }
  }
}
