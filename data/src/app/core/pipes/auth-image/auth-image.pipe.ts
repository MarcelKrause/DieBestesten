import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pipe, PipeTransform, isDevMode } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Pipe({
  name: 'authImage',
})
export class AuthImagePipe implements PipeTransform {
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService // our service that provides us with the authorization token
  ) {}

  async transform(src: string): Promise<string> {
    if (isDevMode()) {
      return './assets/img/placeholder.jpg';
    }

    try {
      const token = this.authService.getToken();
      const headers = new HttpHeaders({ Authorization: `Bearer ${token}` });
      const imageBlob = await this.httpClient.get(src, { headers, responseType: 'blob' }).toPromise();
      const reader = new FileReader();

      return new Promise((resolve, reject) => {
        reader.onloadend = () => resolve(reader.result as string);
        reader.readAsDataURL(imageBlob);
      });
    } catch {
      return './assets/img/placeholder.jpg';
    }
  }
}
