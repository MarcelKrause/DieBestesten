import { Component, OnInit } from '@angular/core';
import { Notification } from '../../models/notification/notification';
import { ApiService } from '../../services/api/api.service';
import { AuthService } from '../../services/auth/auth.service';
import { NotificationService } from '../../services/notification/notification.service';
import { StateService } from '../../services/state/state.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss'],
})
export class TopbarComponent implements OnInit {
  searchInput: string;
  searchResult: any;
  manager: any;

  constructor(
    private stateService: StateService,
    private authService: AuthService,
    public apiService: ApiService,
    private notificationService: NotificationService
  ) {
    this.manager = authService.getManager();
  }

  ngOnInit(): void {}

  public onToggleSidebarVisibility(): void {
    this.stateService.toggleSidebarVisibility();
  }

  public onSearch(event): void {
    this.searchInput = event;

    if (this.searchInput.length >= 3) {
      this.apiService.getSearchResult(this.searchInput).subscribe((data) => {
        if (data.success) {
          this.searchResult = data.result;
        } else {
          this.searchResult = null;
        }
      });
    } else {
      this.searchResult = null;
    }
  }

  public onResetSearch(): void {
    this.onSearch('');
  }

  public onOpenProfile(): void {
    this.notificationService.push(new Notification('negative', 'Profilansicht fehlt noch.'));
  }

  public onLogout(): void {
    this.authService.logout();
  }
}
