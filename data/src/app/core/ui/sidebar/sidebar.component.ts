import { Component, OnInit } from '@angular/core';
import { StateService } from '../../services/state/state.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  constructor(public stateService: StateService) {}

  ngOnInit(): void {}

  public toggleSidebarWidth(): void {
    this.stateService.toggleSidebarWidth();
  }
}
