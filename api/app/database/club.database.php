<?php

trait ClubTrait
{

	function getClubList()
	{
		$query = $this->con->prepare("SELECT * FROM club ORDER BY name ASC");
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getBundesligaClubList()
	{
		$query = $this->con->prepare("SELECT * FROM club WHERE is_bundesliga = 1 ORDER BY position ASC");
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getClubById($club_id)
	{
		$query = $this->con->prepare("SELECT * FROM club WHERE club_id = :club_id LIMIT 1");
		$query->execute(array(':club_id' => $club_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getClubByName($club_name)
	{
		$query = $this->con->prepare("SELECT * FROM club WHERE name = :name LIMIT 1");
		$query->execute(array(':name' => $club_name));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function postClub($club)
	{
		$club_id = $this->generateGUID();
		$query = $this->con->prepare("INSERT INTO club (club_id, country_code, name) VALUES (:club_id, :country_code, :name)");
		$query->execute(
			array(
				':club_id' => $club_id,
				':country_code' => $club['country_code'],
				':name' => $club['name']
			)
		);
		return $club_id;
	}

}