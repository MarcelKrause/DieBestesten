<?php

trait SellTrait
{
  function addSell($player_id, $team_id, $transferwindow_id, $price)
  {
    $sell_id = $this->generateGUID();
    $sell_date = date("Y-m-d H:i:s");
    $query = $this->con->prepare("INSERT INTO sell (sell_id, player_id, team_id, transferwindow_id, sell_date, price) VALUES (:sell_id, :player_id, :team_id, :transferwindow_id, :sell_date, :price)");
    $query->execute(array(
      ':sell_id' => $sell_id,
      ':player_id' => $player_id,
      ':team_id' => $team_id,
      ':transferwindow_id' => $transferwindow_id,
      ':sell_date' => $sell_date,
      ':price' => $price,
    ));
    return $sell_id;
  }
}