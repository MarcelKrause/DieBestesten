<?php

trait PlayerRatingTrait
{

	function getPlayerRatingById($player_rating_id)
	{
		$query = $this->con->prepare("SELECT * FROM player_rating AS pr WHERE pr.player_rating_id = :player_rating_id");
		$query->execute(array(':player_rating_id' => $player_rating_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerRatingListBySeason($season_id)
	{
		$query = $this->con->prepare("SELECT pr.*, ps.position, p.displayname FROM player_rating AS pr JOIN player_in_season AS ps ON pr.player_id = ps.player_id JOIN player AS p ON pr.player_id = p.player_id WHERE ps.season_id = :season_id");
		$query->execute(array(':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerRatingListByPlayerAndSeason($player_id, $season_id)
	{
		$query = $this->con->prepare("SELECT pr.matchday, pr.points, pr.grade, pr.start_lineup, pr.substitution, pr.sds, pr.goals, pr.assists, pr.clean_sheet FROM player_rating AS pr JOIN player AS p ON pr.player_id = p.player_id WHERE p.player_id = :player_id AND pr.season_id = :season_id AND pr.points IS NOT NULL ORDER BY pr.matchday ASC");
		$query->execute(array(':player_id' => $player_id, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerRatingByPlayerAndMatchday($player_id, $matchday_number, $season_id)
	{
		$query = $this->con->prepare("SELECT * FROM player_rating AS pr WHERE pr.player_id = :player_id AND pr.matchday = :matchday_number AND pr.season_id = :season_id LIMIT 1");
		$query->execute(array(':player_id' => $player_id, ':matchday_number' => $matchday_number, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerRatingBySeasonAndMatchdayAndClub($season_id, $matchday_number, $club_id)
	{
		$query = $this->con->prepare("SELECT p.displayname, pis.position, pr.points, pr.start_lineup, pr.substitution, pr.sds, pr.goals, pr.assists, pr.clean_sheet, pr.ligainsider_grade, CASE WHEN pr.grade IS NULL OR pr.grade = 0 THEN 'pending' ELSE 'complete' END AS status FROM player_rating pr JOIN player p ON pr.player_id = p.player_id JOIN player_in_season pis ON p.player_id = pis.player_id WHERE pr.season_id = :season_id AND pr.matchday = :matchday_number AND pr.club_id = :club_id AND pis.season_id = :season_id AND (pr.start_lineup = 1 OR pr.substitution = 1) ORDER BY pr.start_lineup DESC, FIELD(pis.position, 'GOALKEEPER', 'DEFENDER', 'MIDFIELDER', 'FORWARD') ASC");
		$query->execute(array(':season_id' => $season_id, ':matchday_number' => $matchday_number, ':club_id' => $club_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerRatingSummaryByPlayerAndSeason($player_id, $season_id)
	{
		$query = $this->con->prepare("SELECT SUM(pr.points) AS points, SUM(pr.start_lineup) AS start_lineup, SUM(pr.substitution) AS substitution, SUM(pr.assists) AS assists FROM player_rating AS pr WHERE pr.player_id = :player_id AND pr.season_id = :season_id GROUP BY pr.player_id LIMIT 1");
		$query->execute(array(':player_id' => $player_id, ':season_id' => $season_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function postPlayerRating($player_id, $season_id, $club_id, $matchday_number)
	{
		$player_rating_id = $this->generateGUID();
		$query = $this->con->prepare("INSERT INTO player_rating (player_rating_id, player_id, season_id, club_id, matchday) VALUES (:player_rating_id, :player_id, :season_id, :club_id, :matchday_number)");
		$query->execute(
			array(
				':player_rating_id' => $player_rating_id,
				':player_id' => $player_id,
				':season_id' => $season_id,
				':club_id' => $club_id,
				':matchday_number' => $matchday_number
			)
		);
		return $player_rating_id;
	}

	function patchPlayerRating($player_rating)
	{
		$query = $this->con->prepare("UPDATE player_rating AS pr SET pr.grade = :grade, pr.ligainsider_grade = :ligainsider_grade, pr.start_lineup = :start_lineup, pr.substitution = :substitution, pr.goals = :goals, pr.assists = :assists, pr.sds = :sds, pr.clean_sheet = :clean_sheet, pr.red_card = :red_card, pr.yellow_red_card = :yellow_red_card, pr.points = :points WHERE pr.player_rating_id = :player_rating_id");
		$query->execute(
			array(
				':player_rating_id' => $player_rating['player_rating_id'],
				':grade' => $player_rating['grade'],
				':ligainsider_grade' => $player_rating['ligainsider_grade'],
				':start_lineup' => $player_rating['start_lineup'],
				':substitution' => $player_rating['substitution'],
				':goals' => $player_rating['goals'],
				':assists' => $player_rating['assists'],
				':sds' => $player_rating['sds'],
				':clean_sheet' => $player_rating['clean_sheet'],
				':red_card' => $player_rating['red_card'],
				':yellow_red_card' => $player_rating['yellow_red_card'],
				':points' => $player_rating['points'],
			)
		);
	}

}