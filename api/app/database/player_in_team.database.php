<?php

trait PlayerInTeamTrait
{

	function getPlayerInTeamById($player_in_team_id)
	{
		$query = $this->con->prepare("SELECT * FROM player_in_team WHERE player_in_team_id = :player_in_team_id LIMIT 1");
		$query->execute(array(':player_in_team_id' => $player_in_team_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getCurrentPlayerInTeam($team_id, $season_id)
	{
		$query = $this->con->prepare("SELECT p.player_id, pc.club_id, ps.position, p.country_code, p.firstname, p.lastname, p.displayname, ps.has_photo, ps.is_captain, SUM(pr.points) AS points, ps.price AS price FROM player AS p LEFT JOIN player_rating AS pr ON p.player_id = pr.player_id AND pr.season_id = :season_id JOIN player_in_team AS pt ON p.player_id = pt.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_in_club AS pc ON p.player_id = pc.player_id WHERE pt.team_id = :team_id AND pt.last_matchday IS NULL AND pc.to_date IS NULL AND ps.season_id = :season_id GROUP BY p.player_id, pc.club_id, ps.position, ps.price, ps.has_photo, ps.is_captain");
		$query->execute(array(':team_id' => $team_id, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getFormerPlayerInTeam($team_id, $season_id)
	{
		$query = $this->con->prepare("SELECT p.player_id, pc.club_id, ps.position, p.country_code, p.firstname, p.lastname, p.displayname, ps.price, ps.has_photo, ps.is_captain, SUM(pr.points) AS points FROM player AS p LEFT JOIN player_rating AS pr ON p.player_id = pr.player_id AND pr.season_id = :season_id JOIN player_in_team AS pt ON p.player_id = pt.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_in_club AS pc ON p.player_id = pc.player_id WHERE pt.team_id = :team_id AND pt.last_matchday IS NOT NULL AND pc.to_date IS NULL AND ps.season_id = :season_id GROUP BY p.player_id, pc.club_id, ps.position, ps.price, ps.has_photo, ps.is_captain");
		$query->execute(array(':team_id' => $team_id, ':season_id' => $season_id));
		// $query = $this->con->prepare("SELECT p.player_id, pc.club_id, ps.position, p.country_code, p.firstname, p.lastname, p.displayname, ps.price AS start_price, ps.price + (SUM(pr.points) * 20000) AS current_price, SUM(pr.points) AS points FROM player AS p JOIN player_in_team AS pt ON p.player_id = pt.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_in_club AS pc ON p.player_id = pc.player_id JOIN player_rating AS pr ON p.player_id = pr.player_id WHERE pt.team_id = :team_id AND pt.last_matchday IS NOT NULL AND pc.to_date IS NULL AND pr.matchday >= pt.first_matchday AND pr.matchday < pt.last_matchday GROUP BY p.player_id, pc.club_id, ps.position, ps.price, pr.player_id");
		// $query->execute(array(':team_id' => $team_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerInTeamByTeam($team_id)
	{
		$query = $this->con->prepare("SELECT * FROM player_in_team WHERE team_id = :team_id");
		$query->execute(array(':team_id' => $team_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerInTeamByMatchday($team_id, $matchday_number, $season_id)
	{
		$query = $this->con->prepare("SELECT p.player_id, pc.club_id, ps.position, p.country_code, p.firstname, p.lastname, p.displayname, ps.has_photo, ps.price FROM player AS p JOIN player_in_team AS pt ON p.player_id = pt.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_in_club AS pc ON p.player_id = pc.player_id WHERE pt.team_id = :team_id AND ps.season_id = :season_id AND pc.to_date IS NULL AND pt.first_matchday <= :matchday_number AND (pt.last_matchday > :matchday_number OR pt.last_matchday IS NULL) GROUP BY p.player_id, pc.club_id, ps.position, ps.price, ps.has_photo ORDER BY ps.price DESC");
		$query->execute(array(':team_id' => $team_id, ':matchday_number' => $matchday_number, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerInTeamByPlayerAndSeason($player_id, $season_id)
	{
		$query = $this->con->prepare("SELECT pt.player_in_team_id, t.season_id, t.team_id, t.team_name, pt.team_id, pt.first_matchday, pt.last_matchday, o.offer_value AS offer_price, s.price AS sell_price FROM player_in_team AS pt JOIN team AS t ON pt.team_id = t.team_id LEFT JOIN offer o ON pt.offer_id = o.offer_id LEFT JOIN sell s ON pt.sell_id = s.sell_id WHERE pt.player_id = :player_id AND t.season_id = :season_id ORDER BY first_matchday DESC, (CASE WHEN last_matchday IS NULL THEN 0 ELSE 1 END), last_matchday DESC");
		$query->execute(array(':player_id' => $player_id, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function postPlayerInTeam($team_id, $player_id, $first_matchday, $offer_id)
	{
		$player_in_team_id = $this->generateGUID();
		$query = $this->con->prepare("INSERT INTO player_in_team (player_in_team_id, team_id, player_id, first_matchday, offer_id) VALUES (:player_in_team_id, :team_id, :player_id, :first_matchday, :offer_id)");
		$query->execute(
			array(
				':player_in_team_id' => $player_in_team_id,
				':team_id' => $team_id,
				':player_id' => $player_id,
				':first_matchday' => $first_matchday,
				':offer_id' => $offer_id,
			)
		);
		return $player_in_team_id;
	}

	function patchPlayerInTeam($player_in_team)
	{
		$query = $this->con->prepare("UPDATE player_in_team AS pt SET pt.last_matchday = :last_matchday, pt.sell_id = :sell_id WHERE pt.player_in_team_id = :player_in_team_id");
		$query->execute(
			array(
				':player_in_team_id' => $player_in_team['player_in_team_id'],
				':last_matchday' => $player_in_team['last_matchday'],
				':sell_id' => $player_in_team['sell_id'],
			)
		);
	}

	function deletePlayerInTeam($player_in_team_id)
	{
		$query = $this->con->prepare("DELETE FROM player_in_team WHERE player_in_team_id = :player_in_team_id");
		$query->execute(array(':player_in_team_id' => $player_in_team_id));
	}
}