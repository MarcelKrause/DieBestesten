<?php

  trait SeasonTrait {

		function getSeasonById($season_id) {
			$query = $this->con->prepare("SELECT * FROM season AS s WHERE :season_id = s.season_id LIMIT 1");
			$query->execute(array(':season_id' => $season_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

    function getSeasonByName($season_name) {
			$query = $this->con->prepare("SELECT * FROM season AS s WHERE :season_name = s.season_name LIMIT 1");
			$query->execute(array(':season_name' => $season_name));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

		function getSeasonList() {
			$query = $this->con->prepare("SELECT * FROM season ORDER BY season_name DESC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
  }