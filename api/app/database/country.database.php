<?php

  trait CountryTrait {
  
    function getCountryList() {
			$query = $this->con->prepare("SELECT * FROM country ORDER BY country_name ASC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
    
  }