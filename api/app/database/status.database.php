<?php

  trait StatusTrait {

    function getCurrentSeason($now) {
			$query = $this->con->prepare("SELECT * FROM season AS s WHERE :now > s.start_date ORDER BY s.start_date DESC LIMIT 1");
			$query->execute(array(':now' => $now));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

		function getCurrentMatchday($now, $season_id) {
			$query = $this->con->prepare("SELECT * FROM matchday AS m WHERE :now > m.start_date AND m.season_id = :season_id ORDER BY m.start_date DESC LIMIT 1");
			$query->execute(array(':now' => $now, ':season_id' => $season_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

		function getCurrentTransferwindow($now, $matchday_id) {
			$query = $this->con->prepare("SELECT * FROM transferwindow AS t WHERE :now > t.start_date AND :now < t.end_date AND t.matchday_id = :matchday_id ORDER BY t.start_date DESC LIMIT 1");
			$query->execute(array(':now' => $now, ':matchday_id' => $matchday_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
    
  }