<?php

trait PowerrankingTrait
{

  function getOwnPowerranking($season_id)
  {
    $query = $this->con->prepare("SELECT p.id, t.team_id, t.team_name, t.season_id, p.position FROM powerranking p JOIN team t ON p.team_id = t.team_id WHERE p.manager_id = :manager_id AND p.season_id = :season_id ORDER BY p.position ASC");
    $query->execute(array(':manager_id' => $_SERVER['manager_id'], ':season_id' => $season_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getPowerrankingCountBySeason($season_id)
  {
    $query = $this->con->prepare("SELECT COUNT(p.id) / 12 AS count FROM powerranking p JOIN team t ON p.team_id = t.team_id WHERE p.season_id = :season_id");
    $query->execute(array(':season_id' => $season_id));
    $result = $query->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  function getPowerrankingByManager($manager_id, $season_id)
  {
    $query = $this->con->prepare("SELECT p.id, t.team_id, t.team_name, t.season_id, p.position FROM powerranking p JOIN team t ON p.team_id = t.team_id WHERE p.manager_id = :manager_id AND p.season_id = :season_id ORDER BY p.position ASC");
    $query->execute(array(':manager_id' => $manager_id, ':season_id' => $season_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function createPowerranking($season_id, $team_id, $position)
  {
    $id = $this->generateGUID();
    $query = $this->con->prepare("INSERT INTO powerranking (id, manager_id, season_id, team_id, position) VALUES (:id, :manager_id, :season_id, :team_id, :position)");
    $query->execute(
      array(
        ':id' => $id,
        ':manager_id' => $_SERVER['manager_id'],
        ':season_id' => $season_id,
        ':team_id' => $team_id,
        ':position' => $position,
      )
    );
    return $id;
  }

  function updatePowerranking($season_id, $team_id, $position)
  {
    $query = $this->con->prepare("UPDATE powerranking SET position = :position WHERE manager_id = :manager_id AND season_id = :season_id AND team_id = :team_id");
    $query->execute(
      array(
        ':manager_id' => $_SERVER['manager_id'],
        ':season_id' => $season_id,
        ':team_id' => $team_id,
        ':position' => $position,
      )
    );
  }

  function deletePowerranking($id)
  {
    $query = $this->con->prepare("DELETE FROM powerranking WHERE id = :id");
    $query->execute(array(':id' => $id));
  }
}