<?php

  trait MatchdayTrait {

    function getMatchdayById($matchday_id) {
			$query = $this->con->prepare("SELECT * FROM matchday WHERE matchday_id = :matchday_id");
      $query->execute(array(':matchday_id' => $matchday_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
  
    function getMatchdayBySeasonNameAndNumber($season_name, $matchday_number) {
			$query = $this->con->prepare("SELECT * FROM matchday AS m JOIN season AS s ON m.season_id = s.season_id WHERE s.season_name = :season_name AND m.number = :matchday_number");
      $query->execute(array(':season_name' => $season_name, ':matchday_number' => $matchday_number));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
  }