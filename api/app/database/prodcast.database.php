<?php

  trait ProdcastTrait {

    function getProdcastList() {
      $query = $this->con->prepare("SELECT * FROM prodcast ORDER BY published_at DESC");
      $query->execute();
      $result = $query->fetchAll(PDO::FETCH_ASSOC);
      return $result;
    }

    function getProdcastSpeakerByProdcast($prodcast_id) {
      $query = $this->con->prepare("SELECT m.manager_id, m.manager_name, m.has_photo FROM prodcast_speaker AS ps JOIN manager AS m ON ps.manager_id = m.manager_id WHERE prodcast_id = :prodcast_id");
      $query->execute(array(':prodcast_id' => $prodcast_id));
      $result = $query->fetchAll(PDO::FETCH_ASSOC);
      return $result;
    }
  }

?>