<?php
require_once 'status.database.php';
require_once 'stats.database.php';
require_once 'player.database.php';
require_once 'player_in_season.database.php';
require_once 'player_in_club.database.php';
require_once 'player_rating.database.php';
require_once 'club.database.php';
require_once 'country.database.php';
require_once 'activity.database.php';
require_once 'session.database.php';
require_once 'team.database.php';
require_once 'team_lineup.database.php';
require_once 'team_rating.database.php';
require_once 'manager.database.php';
require_once 'team_honour.database.php';
require_once 'season.database.php';
require_once 'player_in_team.database.php';
require_once 'prodcast.database.php';
require_once 'offer.database.php';
require_once 'transferwindow.database.php';
require_once 'matchday.database.php';
require_once 'powerranking.database.php';
require_once 'notification.database.php';
require_once 'sell.database.php';


class Database
{
	use StatusTrait;
	use StatsTrait;
	use PlayerTrait;
	use PlayerInSeasonTrait;
	use PlayerInClubTrait;
	use PlayerRatingTrait;
	use ClubTrait;
	use CountryTrait;
	use ActivityTrait;
	use SessionTrait;
	use TeamTrait;
	use TeamLineupTrait;
	use TeamRatingTrait;
	use ManagerTrait;
	use TeamHonourTrait;
	use SeasonTrait;
	use PlayerInTeamTrait;
	use ProdcastTrait;
	use OfferTrait;
	use TransferwindowTrait;
	use MatchdayTrait;
	use PowerrankingTrait;
	use NotificationTrait;
	use SellTrait;

	private $prod = ['host' => 'sql3.udmedia.de', 'user' => 'ud16_151', 'password' => 'Observed84udmedia', 'database' => 'usr_ud16_151_2'];
	private $dev = ['host' => 'sql3.udmedia.de', 'user' => 'ud16_151m1', 'password' => 'rBhsX5t_d66e', 'database' => 'usr_ud16_151_5'];

	#database config
	protected $host;
	protected $user;
	protected $password;
	protected $database;
	private $con;

	protected static $_instance = null;

	public static function getInstance()
	{
		if (null === self::$_instance) {
			self::$_instance = new self;
		}
		return self::$_instance;
	}

	protected function __clone()
	{
	}

	protected function __construct()
	{
		// PROD DEV
		$config = $this->prod;
		$this->host = $config['host'];
		$this->user = $config['user'];
		$this->password = $config['password'];
		$this->database = $config['database'];
		$this->connect();
	}

	function connect()
	{
		try {
			$this->con = new PDO("mysql:host=$this->host;dbname=$this->database", $this->user, $this->password);
			$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->con->exec("set names utf8");
		} catch (PDOException $e) {
			echo "Connection failed: " . $e->getMessage();
		}
	}

	function close()
	{
		$this->con->close();
	}

	function error()
	{
		return $this->con->error;
	}


	################# PLAYER ######################	

	// auto fill table
	function createPlayerSeasonObject($player_in_season_id, $player_id, $season_id)
	{
		$query = $this->con->prepare("INSERT INTO player_in_season (player_in_season_id, player_id, season_id) VALUES (:player_in_season_id, :player_id, :season_id)");
		$query->execute(array(':player_in_season_id' => $player_in_season_id, ':player_id' => $player_id, ':season_id' => $season_id));
	}


	################# SEARCH ######################

	function searchManager($request)
	{
		$query = $this->con->prepare("SELECT m.manager_id, m.manager_name, m.has_photo FROM manager AS m WHERE m.manager_name LIKE CONCAT('%', :request, '%')");
		$query->execute(array(':request' => $request));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function searchTeam($request)
	{
		$query = $this->con->prepare("SELECT t.team_id, t.team_name, s.season_id, s.season_name FROM team AS t JOIN season AS s ON t.season_id = s.season_id WHERE t.team_name LIKE CONCAT('%', :request, '%') ORDER BY s.start_date DESC");
		$query->execute(array(':request' => $request));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function searchClub($request)
	{
		$query = $this->con->prepare("SELECT c.club_id, c.name FROM club AS c WHERE c.name LIKE CONCAT('%', :request, '%')");
		$query->execute(array(':request' => $request));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function searchBundesligaClub($request)
	{
		$query = $this->con->prepare("SELECT c.club_id, c.name FROM club AS c WHERE c.name LIKE CONCAT('%', :request, '%') AND c.is_bundesliga = 1");
		$query->execute(array(':request' => $request));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}


	function searchPlayer($request, $season_id)
	{
		$query = $this->con->prepare("SELECT p.player_id, p.firstname, p.lastname, p.displayname, ps.has_photo FROM player AS p JOIN player_in_season AS ps ON p.player_id = ps.player_id WHERE (p.firstname LIKE CONCAT('%', :request, '%') OR p.lastname LIKE CONCAT('%', :request, '%') OR p.displayname LIKE CONCAT('%', :request, '%')) AND ps.season_id = :season_id");
		$query->execute(array(':request' => $request, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function searchBundesligaPlayer($request, $season_id)
	{
		$query = $this->con->prepare("SELECT p.player_id, p.firstname, p.lastname, p.displayname, ps.has_photo FROM player AS p JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_in_club AS pc ON p.player_id = pc.player_id JOIN club AS c ON pc.club_id = c.club_id WHERE (p.firstname LIKE CONCAT('%', :request, '%') OR p.lastname LIKE CONCAT('%', :request, '%') OR p.displayname LIKE CONCAT('%', :request, '%')) AND ps.season_id = :season_id AND pc.to_date IS NULL AND c.is_bundesliga = 1 ORDER BY p.lastname ASC");
		$query->execute(array(':request' => $request, ':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}



	################# AUTH ######################

	function getAuthManagerById($manager_id)
	{
		$query = $this->con->prepare("SELECT * FROM manager AS m WHERE m.manager_id = :manager_id LIMIT 1");
		$query->execute(array(':manager_id' => $manager_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getAuthManagerByName($manager_name)
	{
		$query = $this->con->prepare("SELECT * FROM manager AS m WHERE m.manager_name = :manager_name LIMIT 1");
		$query->execute(array(':manager_name' => $manager_name));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function updatePassword($manager_id, $password)
	{
		$query = $this->con->prepare("UPDATE manager AS m SET m.password = :password, m.status = 'active' WHERE m.manager_id = :manager_id");
		return $query->execute(array(':password' => $password, ':manager_id' => $manager_id));
	}

	function generateGUID()
	{
		return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
	}

	function writeLog($device, $deviceType, $browser)
	{
		$id = $this->generateGUID();
		$manager = $this->getAuthManagerById($_SERVER['manager_id'])['manager_name'];

		$query = $this->con->prepare("INSERT INTO log (id, manager, device, deviceType, browser) VALUES (:id, :manager, :device, :deviceType, :browser)");
		return $query->execute(array(':id' => $id, ':manager' => $manager, 'device' => $device, 'deviceType' => $deviceType, ':browser' => $browser));
	}


}