<?php

trait PlayerInSeasonTrait
{

	function getPlayerInSeasonById($player_in_season_id)
	{
		$query = $this->con->prepare("SELECT p.player_id, ps.player_in_season_id, ps.position, ps.price, ps.is_captain, ps.has_photo, p.displayname, s.season_name FROM player_in_season AS ps JOIN player AS p ON p.player_id = ps.player_id JOIN season AS s ON s.season_id = ps.season_id WHERE ps.player_in_season_id = :player_in_season_id LIMIT 1");
		$query->execute(array(':player_in_season_id' => $player_in_season_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerInSeasonByPlayer($player_id)
	{
		$query = $this->con->prepare("SELECT ps.player_in_season_id, ps.player_id, ps.season_id, s.season_name, ps.price, ps.position, ps.is_captain, ps.has_photo FROM player_in_season AS ps JOIN season AS s ON ps.season_id = s.season_id WHERE ps.player_id = :player_id ORDER BY s.season_name DESC");
		$query->execute(array(':player_id' => $player_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getPlayerInSeasonByPlayerAndSeason($player_id, $season_id)
	{
		$query = $this->con->prepare("SELECT ps.player_in_season_id, ps.player_id, ps.season_id, s.season_name, ps.price, ps.position, ps.is_captain, ps.has_photo FROM player_in_season AS ps JOIN season AS s ON ps.season_id = s.season_id WHERE ps.player_id = :player_id AND ps.season_id = :season_id LIMIT 1");
		$query->execute(array(':player_id' => $player_id, ':season_id' => $season_id, ));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getSeasonsOfPlayer($player_id)
	{
		$query = $this->con->prepare("SELECT ps.player_in_season_id, ps.player_id, ps.season_id, s.season_name, ps.price, ps.position, ps.is_captain, ps.has_photo FROM player_in_season AS ps JOIN season AS s ON ps.season_id = s.season_id WHERE ps.player_id = :player_id ORDER BY s.start_date DESC");
		$query->execute(array(':player_id' => $player_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function postPlayerInSeason($player_id, $season_id, $price, $position)
	{
		$player_in_season_id = $this->generateGUID();
		$query = $this->con->prepare("INSERT INTO player_in_season (player_in_season_id, player_id, season_id, price, position, has_photo) VALUES (:player_in_season_id, :player_id, :season_id, :price, :position, :has_photo)");
		$query->execute(array(
			':player_in_season_id' => $player_in_season_id,
			':player_id' => $player_id,
			':season_id' => $season_id,
			':price' => $price,
			':position' => $position,
			':has_photo' => 0
		));
		return $player_in_season_id;
	}

	function patchPlayerInSeason($playerInSeason)
	{
		$query = $this->con->prepare("UPDATE player_in_season SET position = :position, price = :price, is_captain = :is_captain WHERE player_in_season_id = :player_in_season_id");
		$query->execute(array(
			':player_in_season_id' => $playerInSeason['player_in_season_id'],
			':position' => $playerInSeason['position'],
			':price' => $playerInSeason['price'],
			':is_captain' => $playerInSeason['is_captain']
		));
	}

	function deletePlayerInSeason($player_in_season_id)
	{
		$query = $this->con->prepare("DELETE FROM player_in_season WHERE player_in_season_id = :player_in_season_id");
		$query->execute(array(':player_in_season_id' => $player_in_season_id));
	}

}