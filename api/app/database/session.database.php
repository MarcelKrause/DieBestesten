<?php

  trait SessionTrait {

    function getSessionById($session_id) {
			$query = $this->con->prepare("SELECT * FROM session AS s WHERE s.session_id = :session_id LIMIT 1");
			$query->execute(array(':session_id' => $session_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

    function postSession($manager_id, $creation_timestamp, $expiry_timestamp) {
      $session_id = $this->generateGUID();
			$query = $this->con->prepare("INSERT INTO session (session_id, manager_id, creation_timestamp, expiry_timestamp, last_checkin_timestamp, user_agent) VALUES (:session_id, :manager_id, :creation_timestamp, :expiry_timestamp, :last_checkin_timestamp, :user_agent)");
			$query->execute(array(
        ':session_id' => $session_id,
        ':manager_id' => $manager_id,
        ':creation_timestamp' => $creation_timestamp,
        ':expiry_timestamp' => $expiry_timestamp,
        ':last_checkin_timestamp' => $creation_timestamp,
        ':user_agent' => $_SERVER['HTTP_USER_AGENT']
      ));
      return $session_id;
		}

    function refreshSession() {
      $expiry_timestamp = time() + (60 * 60 * 24 * 7);
      $last_checkin_timestamp = time();
      $query = $this->con->prepare("UPDATE session AS s SET s.expiry_timestamp = :expiry_timestamp, s.last_checkin_timestamp = :last_checkin_timestamp WHERE s.session_id = :session_id");			
			$query->execute(array(
        ':expiry_timestamp' => $expiry_timestamp,
        ':last_checkin_timestamp' => $last_checkin_timestamp,
        ':session_id' => $_SERVER['session_id'] ));
      return $_SERVER['session_id'];
    }

    function deactivateSession($session_id) {
      $query = $this->con->prepare("UPDATE session AS s SET s.status = 'inactive' WHERE s.session_id = :session_id");
			$query->execute(array(':session_id' => $session_id));
      return $_SERVER['session_id'];
    }

    function getActiveSessions() {
      $query = $this->con->prepare("SELECT s.manager_id, m.manager_name, MAX(s.last_checkin_timestamp) AS last_checkin_timestamp FROM session AS s JOIN manager AS m ON s.manager_id = m.manager_id WHERE s.status = 'active' GROUP BY s.manager_id, m.manager_name ORDER BY last_checkin_timestamp DESC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
    }
    
  }