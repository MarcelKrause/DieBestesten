<?php

  trait PlayerInClubTrait {

		function getPlayerInClubById($player_in_club_id) {
			$query = $this->con->prepare("SELECT pc.player_in_club_id, pc.player_id, pc.club_id, c.name AS club_name, pc.from_date, pc.to_date, p.displayname FROM player_in_club AS pc JOIN club AS c ON pc.club_id = c.club_id JOIN player AS p ON pc.player_id = p.player_id WHERE pc.player_in_club_id = :player_in_club_id LIMIT 1");
			$query->execute(array(':player_in_club_id' => $player_in_club_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

    function getPlayerInClubByPlayer($player_id) {
			$query = $this->con->prepare("SELECT pc.player_in_club_id, pc.club_id, c.name AS club_name, pc.from_date, pc.to_date, pc.is_loan FROM player_in_club AS pc JOIN club AS c ON pc.club_id = c.club_id WHERE pc.player_id = :player_id ORDER BY pc.from_date DESC");
			$query->execute(array(':player_id' => $player_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerInClubByPlayerAndCurrentStatus($player_id) {
			$query = $this->con->prepare("SELECT pc.player_in_club_id, pc.club_id, c.name AS club_name, pc.from_date, pc.to_date, c.is_bundesliga FROM player_in_club AS pc JOIN club AS c ON pc.club_id = c.club_id WHERE pc.player_id = :player_id AND pc.to_date IS NULL LIMIT 1");
			$query->execute(array(':player_id' => $player_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function postPlayerInClub($player_in_club) {
			$player_in_club_id = $this->generateGUID();
      $query = $this->con->prepare("INSERT INTO player_in_club (player_in_club_id, player_id, club_id, from_date, to_date, is_loan) VALUES (:player_in_club_id, :player_id, :club_id, :from_date, :to_date, :is_loan)");
			$query->execute(array(
        ':player_in_club_id' => $player_in_club_id,
        ':player_id' => $player_in_club['player_id'],
        ':club_id' => $player_in_club['club_id'],
        ':from_date' => $player_in_club['from_date'],
        ':to_date' => $player_in_club['to_date'],
				':is_loan' => $player_in_club['is_loan'],
      ));
			return $player_in_club_id;
		}

		function patchPlayerInClub($player_in_club) {
      $query = $this->con->prepare("UPDATE player_in_club AS pc SET pc.from_date = :from_date, pc.to_date = :to_date, pc.is_loan = :is_loan WHERE pc.player_in_club_id = :player_in_club_id");			
			$query->execute(array(
        ':player_in_club_id' => $player_in_club['player_in_club_id'],
        ':from_date' => $player_in_club['from_date'],
        ':to_date' => $player_in_club['to_date'],
				':is_loan' => $player_in_club['is_loan']
      ));
    }

		function deletePlayerInClub($player_in_club_id) {
      $query = $this->con->prepare("DELETE FROM player_in_club WHERE player_in_club_id = :player_in_club_id");
			$query->execute(array(':player_in_club_id' => $player_in_club_id));
    }

  }