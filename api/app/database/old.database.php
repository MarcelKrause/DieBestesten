<?php
	class OldDatabase
	{
		private $prod = ['host' => 'sql3.udmedia.de', 'user' => 'ud16_151', 'password' => 'Observed84udmedia', 'database' => 'usr_ud16_151_4'];
	        
		#database config
		protected $host;
		protected $user;
		protected $password;
		protected $database;
		private $con;

		protected static $_instance = null;

		public static function getInstance(){
			if (null === self::$_instance){
				self::$_instance = new self;
			}
			return self::$_instance;
		}

		protected function __clone() {}

    protected function __construct() {
			$config = $this->prod;
			$this->host = $config['host'];
			$this->user = $config['user'];
			$this->password = $config['password'];
			$this->database = $config['database'];
      $this->connect();
    }
		
		function connect(){
			try {
				$this->con = new PDO("mysql:host=$this->host;dbname=$this->database", $this->user, $this->password);
				$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->con->exec("set names utf8");
			}
			catch(PDOException $e) {
				echo "Connection failed: " . $e->getMessage();
			}
		}

		function close(){
			$this->con->close();
		}
   
		function error(){
			return $this->con->error;
		}

		
		function postPlayerRating($player_rating_id, $player_id, $season_id, $matchday_number) {
      $query = $this->con->prepare("INSERT INTO rating (rating_id, player_id, season_id, matchday) VALUES (:player_rating_id, :player_id, :season_id, :matchday_number)");
			$query->execute(array(
        ':player_rating_id' => $player_rating_id,
        ':player_id' => $player_id,
        ':season_id' => $season_id,
        ':matchday_number' => $matchday_number
      ));
			return $player_rating_id;
    }

		function patchPlayerRating($player_rating) {
      $query = $this->con->prepare("UPDATE rating AS pr SET pr.grade = :grade, pr.start_lineup = :start_lineup, pr.substitution = :substitution, pr.goals = :goals, pr.assists = :assists, pr.sds = :sds, pr.clean_sheet = :clean_sheet, pr.red_card = :red_card, pr.yellow_red_card = :yellow_red_card, pr.points = :points WHERE pr.rating_id = :player_rating_id");			
			$query->execute(array(
        ':player_rating_id' => $player_rating['player_rating_id'],
        ':grade' => $player_rating['grade'],
        ':start_lineup' => $player_rating['start_lineup'],
        ':substitution' => $player_rating['substitution'],
				':goals' => $player_rating['goals'],
				':assists' => $player_rating['assists'],
				':sds' => $player_rating['sds'],
				':clean_sheet' => $player_rating['clean_sheet'],
				':red_card' => $player_rating['red_card'],
				':yellow_red_card' => $player_rating['yellow_red_card'],
				':points' => $player_rating['points'],
      ));
    }
    

	}