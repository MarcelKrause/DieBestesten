<?php

trait TeamLineupTrait
{
  function getTeamLineupByTeamAndMatchday($team_id, $season_id, $matchday_number)
  {
    $query = $this->con->prepare("SELECT tl.team_lineup_id, p.player_id, pc.club_id, tl.nominated, p.firstname, p.lastname, p.displayname, ps.position, ps.has_photo, ps.price, pr.start_lineup, pr.substitution, pr.points, pr.grade, pr.goals, pr.assists, pr.sds, pr.clean_sheet, pr.red_card, pr.yellow_red_card, pr.is_live FROM team_lineup AS tl JOIN player AS p ON tl.player_id = p.player_id LEFT JOIN player_rating AS pr ON p.player_id = pr.player_id AND pr.season_id = :season_id AND pr.matchday = :matchday_number JOIN player_in_season AS ps ON p.player_id = ps.player_id LEFT JOIN player_in_club AS pc ON p.player_id = pc.player_id AND pc.to_date IS NULL WHERE team_id = :team_id AND ps.season_id = :season_id AND tl.matchday = :matchday_number ORDER BY ps.price DESC");
    $query->execute(array(':team_id' => $team_id, ':season_id' => $season_id, ':matchday_number' => $matchday_number));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getTeamLineupResultByTeamAndMatchday($team_id, $season_id, $matchday_number)
  {
    $query = $this->con->prepare("SELECT SUM(pr.points) AS points, SUM(pr.sds) AS sds, SUM(pr.goals) AS goals, SUM(pr.assists) AS assists, SUM(pr.clean_sheet) AS clean_sheet, SUM(CASE WHEN ps.position LIKE 'GOALKEEPER' THEN pr.points ELSE 0 END) AS points_goalkeeper, SUM(CASE WHEN ps.position LIKE 'DEFENDER' THEN pr.points ELSE 0 END) AS points_defender, SUM(CASE WHEN ps.position LIKE 'MIDFIELDER' THEN pr.points ELSE 0 END) AS points_midfielder, SUM(CASE WHEN ps.position LIKE 'FORWARD' THEN pr.points ELSE 0 END) AS points_forward, SUM(CASE WHEN ps.position LIKE 'GOALKEEPER' THEN pr.sds WHEN ps.position LIKE 'DEFENDER' THEN pr.sds ELSE 0 END) AS sds_defender FROM team_lineup AS tl JOIN player AS p ON tl.player_id = p.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_rating AS pr ON p.player_id = pr.player_id AND pr.season_id = :season_id AND pr.matchday = :matchday_number WHERE team_id = :team_id AND ps.season_id = :season_id AND tl.matchday = :matchday_number AND tl.nominated = 1");
    $query->execute(array(':team_id' => $team_id, ':season_id' => $season_id, ':matchday_number' => $matchday_number));
    $result = $query->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  function getPointsListByTeamAndMatchdayAndPosition($team_id, $season_id, $matchday_number, $position)
  {
    $query = $this->con->prepare("SELECT pr.points AS points FROM team_lineup AS tl JOIN player AS p ON tl.player_id = p.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_rating AS pr ON p.player_id = pr.player_id AND pr.season_id = :season_id AND pr.matchday = :matchday_number WHERE team_id = :team_id AND ps.season_id = :season_id AND tl.matchday = :matchday_number AND ps.position = :position ORDER BY points DESC");
    $query->execute(array(':team_id' => $team_id, ':season_id' => $season_id, ':matchday_number' => $matchday_number, ':position' => $position));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getMissedGoals($team_id, $season_id, $matchday_number)
  {
    $query = $this->con->prepare("SELECT SUM(pr.goals) AS missed_goals FROM team_lineup AS tl JOIN player AS p ON tl.player_id = p.player_id JOIN player_rating AS pr ON p.player_id = pr.player_id AND pr.season_id = :season_id AND pr.matchday = :matchday_number WHERE team_id = :team_id AND tl.matchday = :matchday_number AND tl.nominated = 0");
    $query->execute(array(':team_id' => $team_id, ':season_id' => $season_id, ':matchday_number' => $matchday_number));
    $result = $query->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  function setTeamLineupByTeamAndMatchday($team_id, $player, $matchday_number)
  {
    $team_lineup_id = $this->generateGUID();
    $query = $this->con->prepare("INSERT INTO team_lineup (team_lineup_id, team_id, player_id, matchday, nominated) VALUES (:team_lineup_id, :team_id, :player_id, :matchday_number, :nominated)");
    $query->execute(array(':team_lineup_id' => $team_lineup_id, ':team_id' => $team_id, ':player_id' => $player['player_id'], ':matchday_number' => $matchday_number, ':nominated' => $player['nominated']));
    return $team_lineup_id;
  }

  function postTeamLineup($team_id, $matchday_number, $player_id, $nominated)
  {
    $team_lineup_id = $this->generateGUID();
    $query = $this->con->prepare("INSERT INTO team_lineup (team_lineup_id, team_id, player_id, matchday, nominated) VALUES (:team_lineup_id, :team_id, :player_id, :matchday_number, :nominated)");
    $query->execute(array(
      ':team_lineup_id' => $team_lineup_id,
      ':team_id' => $team_id,
      ':player_id' => $player_id,
      ':matchday_number' => $matchday_number,
      ':nominated' => $nominated
    ));
    return $team_lineup_id;
  }

  function patchTeamLineup($player)
  {
    $query = $this->con->prepare("UPDATE team_lineup AS tl SET tl.nominated = :nominated WHERE tl.team_lineup_id = :team_lineup_id");
    $query->execute(array(
      ':nominated' => intval($player->nominated),
      ':team_lineup_id' => $player->team_lineup_id
    ));
  }
}