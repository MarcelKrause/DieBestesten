<?php

  trait ActivityTrait {
  
		// all
    function getActivityByPage($page) {
			$offset = 10 * ($page - 1);
			$query = $this->con->prepare("SELECT a.activity_id, a.method, a.ref_id, a.ref_table, a.ref_column, a.ref_route, m.manager_id, m.manager_name, m.has_photo, a.activity_timestamp, a.message FROM activity AS a JOIN manager AS m ON a.author_id = m.manager_id ORDER BY activity_timestamp DESC LIMIT 10 OFFSET $offset");
			$query->execute(array());
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getActivityCount() {
			$query = $this->con->prepare("SELECT COUNT(*) AS count FROM activity");
			$query->execute();
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}


		// Reference
		function getActivityByPageAndReference($page, $ref_id) {
			$offset = 10 * ($page - 1);
			$query = $this->con->prepare("SELECT a.activity_id, a.method, a.ref_id, a.ref_table, a.ref_column, a.ref_route, m.manager_id, m.manager_name, m.has_photo, a.activity_timestamp, a.message FROM activity AS a JOIN manager AS m ON a.author_id = m.manager_id WHERE a.ref_id = :ref_id ORDER BY activity_timestamp DESC LIMIT 10 OFFSET $offset");
			$query->execute(array(':ref_id' => $ref_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getActivityCountByReference($ref_id) {
			$query = $this->con->prepare("SELECT COUNT(*) AS count FROM activity AS a WHERE a.ref_id = :ref_id");
			$query->execute(array(':ref_id' => $ref_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
    

		function postActivity($method, $ref_id, $ref_table, $ref_column, $ref_route, $message) {
			$activity_id = $this->generateGUID();
      $author_id = $_SERVER['manager_id'];
			$timestamp = date('Y-m-d H:i:s');
			$query = $this->con->prepare("INSERT INTO activity (activity_id, author_id, method, ref_id, ref_table, ref_column, ref_route, activity_timestamp, message) VALUES (:activity_id, :author_id, :method, :ref_id, :ref_table, :ref_column, :ref_route, :timestamp, :message) ");
			$query->execute(array(
				':activity_id' => $activity_id,
				':author_id' => $author_id,
				':method' => $method,
				':ref_id' => $ref_id,
				':ref_table' => $ref_table,
				':ref_column' => $ref_column,
				':ref_route' => $ref_route,
				':timestamp' => $timestamp,
				':message' => $message
			));
		}
  }