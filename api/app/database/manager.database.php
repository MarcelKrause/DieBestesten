<?php

trait ManagerTrait
{

	function getManagerById($manager_id)
	{
		$query = $this->con->prepare("SELECT m.manager_id, m.manager_name, m.alias, m.status, m.has_photo FROM manager AS m WHERE m.manager_id = :manager_id LIMIT 1");
		$query->execute(array(':manager_id' => $manager_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getManagerByTeam($team_id)
	{
		$query = $this->con->prepare("SELECT m.manager_id, m.manager_name, m.alias, m.has_photo FROM manager AS m JOIN team AS t ON m.manager_id = t.manager_id WHERE t.team_id = :team_id LIMIT 1");
		$query->execute(array(':team_id' => $team_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getManagerByName($manager_name)
	{
		$query = $this->con->prepare("SELECT m.manager_id, m.manager_name, m.alias, m.has_photo, m.birthdate, m.city FROM manager AS m WHERE m.manager_name = :manager_name LIMIT 1");
		$query->execute(array(':manager_name' => $manager_name));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getManagerList()
	{
		$query = $this->con->prepare("SELECT m.manager_id, m.manager_name, m.alias, m.has_photo FROM manager AS m");
		$query->execute();
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}