<?php

trait TransferwindowTrait
{

	function getTransferwindowById($transferwindow_id)
	{
		$query = $this->con->prepare("SELECT t.*, m.number AS matchday_number FROM transferwindow AS t JOIN matchday AS m ON t.matchday_id = m.matchday_id WHERE transferwindow_id = :transferwindow_id LIMIT 1");
		$query->execute(array(':transferwindow_id' => $transferwindow_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getTransferwindowListBySeason($season_id)
	{
		$query = $this->con->prepare("SELECT t.*, COUNT(o.offer_id) AS count FROM transferwindow AS t LEFT JOIN offer AS o ON t.transferwindow_id = o.transferwindow_id AND o.status != 'cancelled' AND o.status != 'rejected' JOIN matchday AS m ON t.matchday_id = m.matchday_id WHERE m.season_id = :season_id GROUP BY t.transferwindow_id ORDER BY t.end_date DESC");
		$query->execute(array(':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function postTransferwindow($matchday_id, $start_date, $end_date)
	{
		$transferwindow_id = $this->generateGUID();
		$query = $this->con->prepare("INSERT INTO transferwindow (transferwindow_id, matchday_id, start_date, end_date) VALUES (:transferwindow_id, :matchday_id, :start_date, :end_date)");
		$query->execute(array(
			':transferwindow_id' => $transferwindow_id,
			':matchday_id' => $matchday_id,
			':start_date' => $start_date,
			':end_date' => $end_date,
		));
		return $transferwindow_id;
	}

	function deleteTransferwindow($transferwindow_id)
	{
		$query = $this->con->prepare("DELETE FROM transferwindow WHERE transferwindow_id = :transferwindow_id");
		$query->execute(array(':transferwindow_id' => $transferwindow_id));
	}
}

?>