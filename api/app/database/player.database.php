<?php

  trait PlayerTrait {

    function getPlayerById($player_id) {
      $query = $this->con->prepare("SELECT * FROM player AS p LEFT JOIN country AS c ON p.country_code = c.country_code WHERE p.player_id = :player_id LIMIT 1");
      $query->execute(array('player_id' => $player_id));
      $result = $query->fetch(PDO::FETCH_ASSOC);
      return $result;
    }

    function getPlayerByKickerId($kicker_id) {
			$query = $this->con->prepare("SELECT * FROM player AS p WHERE p.kicker_id = :kicker_id LIMIT 1");
			$query->execute(array('kicker_id' => $kicker_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

    function getPlayerByDisplayname($displayname) {
			$query = $this->con->prepare("SELECT p.*, c.country_name FROM player AS p JOIN country AS c ON p.country_code = c.country_code WHERE p.displayname = :displayname LIMIT 1");
			$query->execute(array('displayname' => $displayname));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

    function getPlayerList() {
			$query = $this->con->prepare("SELECT p.*, SUM(pr.points) AS points FROM player AS p LEFT JOIN player_rating AS pr ON p.player_id = pr.player_id GROUP BY p.player_id ORDER BY lastname ASC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

    function getBundesligaPlayerList() {
			$query = $this->con->prepare("SELECT p.player_id, p.firstname, p.lastname, p.displayname, p.kicker_id FROM player AS p JOIN player_in_club AS pc ON p.player_id = pc.player_id JOIN club AS c ON pc.club_id = c.club_id WHERE pc.to_date IS NULL AND c.is_bundesliga = 1 GROUP BY p.player_id ORDER BY displayname ASC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

    function getMarketPlayerList($season_id) {
			$query = $this->con->prepare("SELECT p.player_id, p.firstname, p.lastname, p.displayname, ps.price, ps.position FROM player AS p JOIN player_in_club AS pc ON p.player_id = pc.player_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN club AS c ON pc.club_id = c.club_id WHERE pc.to_date IS NULL AND c.is_bundesliga = 1 AND ps.season_id = :season_id AND ps.price IS NOT NULL AND ps.position IS NOT NULL GROUP BY p.player_id, ps.player_id, ps.price, ps.position ORDER BY ps.price DESC");
			$query->execute(array(':season_id' => $season_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

    function getSoldPlayers($season_id, $matchday_number) {
      $query = $this->con->prepare("SELECT p.player_id, p.displayname, t.team_id, t.team_name, ps.season_id, ps.price, ps.position, ps.has_photo, pc.club_id FROM player AS p JOIN player_in_team AS pt ON p.player_id = pt.player_id JOIN team AS t ON t.team_id = pt.team_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN player_in_club AS pc ON p.player_id = pc.player_id WHERE t.season_id = :season_id AND ps.season_id = :season_id AND pt.last_matchday = :matchday_number AND pc.to_date IS NULL GROUP BY p.player_id, t.team_id, ps.season_id, ps.price, ps.position, ps.has_photo, pc.club_id ORDER BY ps.price DESC");
			$query->execute(array(':season_id' => $season_id, ':matchday_number' => $matchday_number));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
    }

    function getPlayerListByClubId($club_id) {
			$query = $this->con->prepare("SELECT * FROM player AS p JOIN player_in_club AS pc ON p.player_id = pc.player_id WHERE pc.club_id = :club_id AND pc.to_date IS NULL");
			$query->execute(array('club_id' => $club_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

    function postPlayer($player) {
      $player_id = $this->generateGUID();
      $query = $this->con->prepare("INSERT INTO player (player_id, kicker_id, country_code, firstname, lastname, displayname, city, date_of_birth, height, weight) VALUES (:player_id, :kicker_id, :country_code, :firstname, :lastname, :displayname, :city, :date_of_birth, :height, :weight)");
			$query->execute(array(
        ':player_id' => $player_id,
        ':kicker_id' => $player['kicker_id'],
        ':firstname' => $player['firstname'],
        ':lastname' => $player['lastname'],
        ':displayname' => $player['displayname'],
        ':city' => $player['city'],
        ':country_code' => $player['country_code'],
        ':date_of_birth' => $player['date_of_birth'], 
        ':height' => $player['height'],
        ':weight' => $player['weight']
      ));
			return $player_id;
    }

    function patchPlayer($player) {
      $query = $this->con->prepare("UPDATE player AS p SET p.firstname = :firstname, p.lastname = :lastname, p.displayname = :displayname, p.city = :city, p.country_code = :country_code, p.date_of_birth = :date_of_birth, p.height = :height, p.weight = :weight, p.kicker_id = :kicker_id, p.ligainsider_id = :ligainsider_id WHERE p.player_id = :player_id");			
			$query->execute(array(
        ':player_id' => $player['player_id'],
        ':firstname' => $player['firstname'],
        ':lastname' => $player['lastname'],
        ':displayname' => $player['displayname'],
        ':city' => $player['city'],
        ':country_code' => $player['country_code'],
        ':date_of_birth' => $player['date_of_birth'],
        ':height' => $player['height'],
        ':weight' => $player['weight'],
        ':kicker_id' => $player['kicker_id'],
        ':ligainsider_id' => $player['ligainsider_id'],
      ));
    }

    function deletePlayer($player_id) {
      $query = $this->con->prepare("DELETE FROM player WHERE player_id = :player_id");
			$query->execute(array(':player_id' => $player_id));
    }

  }

