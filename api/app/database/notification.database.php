<?php

trait NotificationTrait
{

  function getNotificationList()
  {
    $query = $this->con->prepare("SELECT * FROM notification AS n WHERE n.receiver_id = :receiver_id ORDER BY notification_date DESC");
    $query->execute(array('receiver_id' => $_SERVER['manager_id']));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getUnreadNotificationsCount()
  {
    $query = $this->con->prepare("SELECT * FROM notification WHERE receiver_id = :receiver_id AND read_at IS NULL");
    $query->execute(array('receiver_id' => $_SERVER['manager_id']));
    $result = count($query->fetchAll(PDO::FETCH_ASSOC));
    return $result;
  }

  function pushNotification($manager_id, $title, $message)
  {
    $notification_id = $this->generateGUID();
    $query = $this->con->prepare("INSERT INTO notification (notification_id, receiver_id, title, message) VALUES (:notification_id, :receiver_id, :title, :message)");
    $query->execute(array(
      ':notification_id' => $notification_id,
      ':receiver_id' => $manager_id,
      ':title' => $title,
      ':message' => $message,
    ));
    return $notification_id;
  }

  function setNotificationToRead($notification_id)
  {
    $query = $this->con->prepare("UPDATE notification SET read_at = :read_at WHERE notification_id = :notification_id");
    $query->execute(array(
      ':notification_id' => $notification_id,
      ':read_at' => date('Y-m-d H:i:s')
    ));
  }
}