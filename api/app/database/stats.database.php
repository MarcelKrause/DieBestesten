<?php

  trait StatsTrait {

		function getAllPlayer() {
			$query = $this->con->prepare("SELECT * FROM player AS p");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}


    function getPlayerHeightDistribution() {
			$query = $this->con->prepare("SELECT p.height AS name, COUNT(p.player_id) AS value FROM player AS p WHERE p.height IS NOT NULL GROUP BY p.height ORDER BY p.height ASC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerWeightDistribution() {
			$query = $this->con->prepare("SELECT p.weight AS name, COUNT(p.player_id) AS value FROM player AS p WHERE p.weight IS NOT NULL GROUP BY p.weight ORDER BY p.weight ASC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

    function getPlayerWithWeight() {
			$query = $this->con->prepare("SELECT * FROM player AS p WHERE p.weight IS NOT NULL");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerWithHeight() {
			$query = $this->con->prepare("SELECT * FROM player AS p WHERE p.height IS NOT NULL");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerWithCity() {
			$query = $this->con->prepare("SELECT * FROM player AS p WHERE p.city IS NOT NULL");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerWithBirthday() {
			$query = $this->con->prepare("SELECT * FROM player AS p WHERE p.date_of_birth IS NOT NULL");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerWithCountry() {
			$query = $this->con->prepare("SELECT * FROM player AS p WHERE p.country_code IS NOT NULL");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerWithPhoto($season_id) {
			$query = $this->con->prepare("SELECT * FROM player AS p JOIN player_in_season AS ps ON p.player_id = ps.player_id WHERE ps.season_id = :season_id AND ps.has_photo = 1");
			$query->execute(array(':season_id' => $season_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

    function getAvailablePlayer() {
			$query = $this->con->prepare("SELECT * FROM player AS p JOIN player_in_club AS pc ON p.player_id = pc.player_id JOIN club AS c ON c.club_id = pc.club_id WHERE pc.to_date IS NULL AND c.is_bundesliga = 1");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getGeneratedPlayerInClub() {
			$query = $this->con->prepare("SELECT * FROM player AS p JOIN player_in_club AS pc ON p.player_id = pc.player_id WHERE pc.from_date = '1970-1-1'");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getClubCountByCountry() {
			$query = $this->con->prepare("SELECT c.country_code, COUNT(c.country_code) AS count FROM club AS c GROUP BY c.country_code ORDER BY count DESC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getPlayerCountByCountry() {
			$query = $this->con->prepare("SELECT p.country_code, COUNT(p.country_code) AS count FROM player AS p GROUP BY p.country_code ORDER BY count DESC");
			$query->execute();
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

  }
