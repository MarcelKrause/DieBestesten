<?php

  trait TeamHonourTrait {

		function getHonoursByTeam($team_id) {
			$query = $this->con->prepare("SELECT th.team_honour_id, h.honour_id, h.honour_name, h.weight, s.season_name FROM team_honour AS th JOIN honour AS h ON th.honour_id = h.honour_id JOIN season AS s ON th.season_id = s.season_id WHERE th.team_id = :team_id");
			$query->execute(array(':team_id' => $team_id ));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getHonoursByManager($manager_id) {
			$query = $this->con->prepare("SELECT th.team_honour_id, h.honour_id, h.honour_name, h.weight, s.season_name FROM team_honour AS th JOIN honour AS h ON th.honour_id = h.honour_id JOIN season AS s ON th.season_id = s.season_id JOIN team AS t ON th.team_id = t.team_id JOIN manager AS m ON t.manager_id = m.manager_id WHERE m.manager_id = :manager_id ORDER BY h.weight DESC, s.season_name ASC");
			$query->execute(array(':manager_id' => $manager_id ));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
  }