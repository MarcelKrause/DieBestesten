<?php

trait TeamTrait
{

	function getTeamById($team_id)
	{
		$query = $this->con->prepare("SELECT s.season_id, t.team_id, t.manager_id, t.team_name, t.color, t.budget, SUM(tr.points) AS points FROM team AS t JOIN season AS s ON t.season_id = s.season_id LEFT JOIN team_rating AS tr ON t.team_id = tr.team_id WHERE t.team_id = :team_id GROUP BY t.team_id, tr.team_id, s.season_id LIMIT 1");
		$query->execute(array(':team_id' => $team_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getTeamBySeasonAndName($season_name, $team_name)
	{
		$query = $this->con->prepare("SELECT s.season_id, t.team_id, t.team_name, t.color, SUM(tr.points) AS points FROM team AS t JOIN season AS s ON t.season_id = s.season_id JOIN team_rating AS tr ON t.team_id = tr.team_id WHERE t.team_name = :team_name AND s.season_name = :season_name GROUP BY tr.team_id, s.season_id LIMIT 1");
		$query->execute(array(':season_name' => $season_name, ':team_name' => $team_name));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function getTeamsBySeason($season_id)
	{
		$query = $this->con->prepare("SELECT * FROM team WHERE season_id = :season_id");
		$query->execute(array(':season_id' => $season_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getTeamsByManager($manager_id)
	{
		$query = $this->con->prepare("SELECT t.team_id, t.season_id, s.season_name, t.team_name, SUM(tr.points) AS points, SUM(CASE WHEN tr.points IS NOT NULL THEN 1 ELSE 0 END) AS matchdays FROM team AS t JOIN team_rating AS tr ON t.team_id = tr.team_id JOIN season AS s ON t.season_id = s.season_id WHERE manager_id = :manager_id GROUP BY tr.team_id, s.season_id ORDER BY s.season_name DESC");
		$query->execute(array(':manager_id' => $manager_id));
		$result = $query->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	function getTeamByManagerAndSeason($manager_id, $season_id)
	{
		$query = $this->con->prepare("SELECT t.team_id, t.season_id, s.season_name, t.team_name, t.budget, SUM(tr.points) AS points FROM team AS t LEFT JOIN team_rating AS tr ON t.team_id = tr.team_id JOIN season AS s ON t.season_id = s.season_id WHERE t.manager_id = :manager_id AND t.season_id = :season_id GROUP BY t.team_id, tr.team_id, s.season_id ORDER BY s.season_name DESC LIMIT 1");
		$query->execute(array(':manager_id' => $manager_id, ':season_id' => $season_id));
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}

	function postTeam($manager_id, $season_id, $team_name, $budget)
	{
		$team_id = $this->generateGUID();
		$query = $this->con->prepare("INSERT INTO team (team_id, manager_id, season_id, team_name, budget) VALUES (:team_id, :manager_id, :season_id, :team_name, :budget)");
		$query->execute(array(
			':team_id' => $team_id,
			':manager_id' => $manager_id,
			':season_id' => $season_id,
			':team_name' => $team_name,
			':budget' => $budget,
		));
		return $team_id;
	}

	function reduceTeamBudget($team_id, $offer_value)
	{
		$query = $this->con->prepare("UPDATE team AS t SET t.budget = (t.budget - :offer_value) WHERE t.team_id = :team_id");
		$query->execute(array(
			':team_id' => $team_id,
			':offer_value' => $offer_value,
		));
	}

	function increaseTeamBudget($team_id, $offer_value)
	{
		$query = $this->con->prepare("UPDATE team AS t SET t.budget = (t.budget + :offer_value) WHERE t.team_id = :team_id");
		$query->execute(array(
			':team_id' => $team_id,
			':offer_value' => $offer_value,
		));
	}

	function patchTeam($team)
	{
		$query = $this->con->prepare("UPDATE team AS t SET t.budget = :budget WHERE t.team_id = :team_id");
		$query->execute(array(
			':team_id' => $team['team_id'],
			':budget' => $team['budget'],
		));
	}
}
