<?php

trait OfferTrait
{

  function getOfferById($offer_id)
  {
    $query = $this->con->prepare("SELECT * FROM offer AS o WHERE o.offer_id = :offer_id LIMIT 1");
    $query->execute(array('offer_id' => $offer_id));
    $result = $query->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  function getPlayerListByTransferwindow($transferwindow_id)
  {
    $query = $this->con->prepare("SELECT p.player_id, p.displayname, COUNT(o.offer_id) AS count FROM offer AS o JOIN transferwindow AS t ON o.transferwindow_id = t.transferwindow_id JOIN player AS p ON p.player_id = o.player_id WHERE t.transferwindow_id = :transferwindow_id AND o.status != 'cancelled' AND o.status != 'rejected' GROUP BY p.player_id ORDER BY count DESC");
    $query->execute(array(':transferwindow_id' => $transferwindow_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getOfferByPlayerByTransferwindow($transferwindow_id, $player_id)
  {
    $query = $this->con->prepare("SELECT o.*, team.team_name FROM offer AS o JOIN transferwindow AS t ON o.transferwindow_id = t.transferwindow_id JOIN team AS team ON team.team_id = o.team_id WHERE t.transferwindow_id = :transferwindow_id AND o.player_id = :player_id AND o.status != 'cancelled' ORDER BY o.offer_value DESC, o.offer_id ASC");
    $query->execute(array(':transferwindow_id' => $transferwindow_id, ':player_id' => $player_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getOfferListByTransferwindow($transferwindow_id)
  {
    $query = $this->con->prepare("SELECT o.* FROM offer AS o JOIN transferwindow AS t ON o.transferwindow_id = t.transferwindow_id WHERE t.transferwindow_id = :transferwindow_id");
    $query->execute(array(':transferwindow_id' => $transferwindow_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getOfferListByTeam($team_id, $season_id)
  {
    $query = $this->con->prepare("SELECT o.offer_id, t.season_id, o.owner_team_id, o.offer_value, o.offer_date, o.status, p.player_id, p.firstname, p.lastname, p.displayname, ps.has_photo, ps.position FROM offer AS o JOIN player AS p ON o.player_id = p.player_id JOIN team AS t ON o.team_id = t.team_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN transferwindow AS tr ON o.transferwindow_id = tr.transferwindow_id WHERE o.team_id = :team_id AND ps.season_id = :season_id AND o.status != 'cancelled' ORDER BY o.offer_date DESC, tr.start_date DESC, FIELD(ps.position, 'FORWARD','MIDFIELDER','DEFENDER','GOALKEEPER'), o.offer_value DESC");
    $query->execute(array(':team_id' => $team_id, ':season_id' => $season_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getOfferListToTeam($team_id, $season_id)
  {
    $query = $this->con->prepare("SELECT o.offer_id, t.season_id, o.team_id, o.owner_team_id, o.offer_value, o.offer_date, o.status, p.player_id, p.firstname, p.lastname, p.displayname, ps.has_photo, ps.position FROM offer AS o JOIN player AS p ON o.player_id = p.player_id JOIN team AS t ON o.team_id = t.team_id JOIN player_in_season AS ps ON p.player_id = ps.player_id JOIN transferwindow AS tr ON o.transferwindow_id = tr.transferwindow_id WHERE o.owner_team_id = :owner_team_id AND ps.season_id = :season_id AND o.status != 'cancelled' ORDER BY tr.start_date DESC, FIELD(ps.position, 'FORWARD','MIDFIELDER','DEFENDER','GOALKEEPER'), o.offer_value DESC");
    $query->execute(array(':owner_team_id' => $team_id, ':season_id' => $season_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function getIncomingOffersCount($team_id)
  {
    $query = $this->con->prepare("SELECT COUNT(o.offer_id) AS count FROM offer AS o WHERE o.owner_team_id = :owner_team_id AND o.status = 'pending'");
    $query->execute(array(':owner_team_id' => $team_id));
    $result = $query->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  function getPendingOfferListByTeam($team_id, $season_id)
  {
    $query = $this->con->prepare("SELECT o.offer_id, t.season_id, o.offer_value, o.offer_date, o.status, p.player_id, p.firstname, p.lastname, p.displayname, ps.has_photo FROM offer AS o JOIN player AS p ON o.player_id = p.player_id JOIN team AS t ON o.team_id = t.team_id JOIN player_in_season AS ps ON p.player_id = ps.player_id WHERE o.team_id = :team_id AND ps.season_id = :season_id AND o.status = 'pending' ORDER BY o.offer_date DESC");
    $query->execute(array(':team_id' => $team_id, ':season_id' => $season_id));
    $result = $query->fetchAll(PDO::FETCH_ASSOC);
    return $result;
  }

  function postOffer($player_id, $team_id, $owner_team_id, $transferwindow_id, $offer_value, $price_snapshot)
  {
    $offer_id = $this->generateGUID();
    $offer_date = date("Y-m-d H:i:s");
    $query = $this->con->prepare("INSERT INTO offer (offer_id, player_id, team_id, owner_team_id, transferwindow_id, offer_value, offer_date, price_snapshot) VALUES (:offer_id, :player_id, :team_id, :owner_team_id, :transferwindow_id, :offer_value, :offer_date, :price_snapshot)");
    $query->execute(array(
      ':offer_id' => $offer_id,
      ':player_id' => $player_id,
      ':team_id' => $team_id,
      ':owner_team_id' => $owner_team_id,
      ':transferwindow_id' => $transferwindow_id,
      ':offer_value' => $offer_value,
      ':offer_date' => $offer_date,
      ':price_snapshot' => $price_snapshot
    ));
    return $offer_id;
  }

  function patchOffer($offer)
  {
    $query = $this->con->prepare("UPDATE offer AS o SET o.status = :status, o.offer_value = :offer_value WHERE o.offer_id = :offer_id");
    $query->execute(array(
      ':offer_id' => $offer['offer_id'],
      ':status' => $offer['status'],
      ':offer_value' => $offer['offer_value']
    ));
  }

  function cancelOffer($offer_id)
  {
    $query = $this->con->prepare("UPDATE offer AS o SET o.status = 'cancelled' WHERE o.offer_id = :offer_id");
    $query->execute(array(
      ':offer_id' => $offer_id
    ));
  }

  function deleteOffer($offer_id)
  {
    $query = $this->con->prepare("DELETE FROM offer WHERE offer_id = :offer_id");
    $query->execute(array(':offer_id' => $offer_id));
  }
}