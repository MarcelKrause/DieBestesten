<?php

  trait TeamRatingTrait {
    function getTeamRatingByTeamAndMatchday($team_id, $matchday_number) {
			$query = $this->con->prepare("SELECT t.team_id, t.team_name, tr.matchday_number, tr.points FROM team_rating AS tr JOIN team AS t ON tr.team_id = t.team_id WHERE tr.team_id = :team_id AND tr.matchday_number = :matchday_number LIMIT 1");
			$query->execute(array(':team_id' => $team_id, ':matchday_number' => $matchday_number));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

		function getLiveTeamRatingByTeamAndMatchday($team_id, $matchday_number, $season_id) {
			$query = $this->con->prepare("SELECT COUNT(tl.team_lineup_id) AS player_count, SUM(pr.start_lineup) AS start_lineup, SUM(pr.substitution) AS substitution, SUM(pr.points) AS points, SUM(pr.sds) AS sds, SUM(pr.goals) AS goals, SUM(pr.assists) AS assists, SUM(pr.clean_sheet) AS clean_sheet FROM team_lineup AS tl LEFT JOIN player_rating AS pr ON tl.player_id = pr.player_id WHERE tl.matchday = :matchday_number AND tl.team_id = :team_id AND tl.nominated = 1 AND pr.matchday = :matchday_number AND pr.season_id = :season_id");
			$query->execute(array(':team_id' => $team_id, ':matchday_number'=> $matchday_number, ':season_id' => $season_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

		function getTeamRatingBySeasonId($season_id) {
			$query = $this->con->prepare("SELECT RANK() OVER (ORDER BY SUM(tr.points) DESC) AS position, t.team_id, t.team_name, SUM(tr.points) AS points FROM team AS t JOIN team_rating AS tr ON t.team_id = tr.team_id JOIN season AS s On t.season_id = s.season_id WHERE s.season_id = :season_id GROUP BY tr.team_id ORDER BY points DESC, t.team_name ASC");
			$query->execute(array(':season_id' => $season_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getTeamRatingBySeasonAndMatchday($season_name, $from_matchday_number, $to_matchday_number) {
			$query = $this->con->prepare("SELECT RANK() OVER (ORDER BY SUM(tr.points) DESC) AS position, t.team_id, t.team_name, SUM(tr.points) AS points, SUM(tr.sds) AS sds, SUM(tr.goals) AS goals, SUM(tr.assists) AS assists, SUM(tr.clean_sheet) AS clean_sheet, SUM(tr.points_goalkeeper) AS points_goalkeeper, SUM(tr.points_defender) AS points_defender, SUM(tr.points_midfielder) AS points_midfielder, SUM(tr.points_forward) AS points_forward, CASE WHEN (DENSE_RANK() OVER (ORDER BY points)) = 1 AND points IS NOT NULL THEN 300 WHEN (DENSE_RANK() OVER (ORDER BY points)) = 2 AND points IS NOT NULL THEN 200 WHEN (DENSE_RANK() OVER (ORDER BY points)) = 3 AND points IS NOT NULL THEN 150 wHEN (DENSE_RANK() OVER (ORDER BY points)) = 4 AND points IS NOT NULL THEN 100 ELSE 0 END AS fine, SUM(tr.343) AS formation_343, SUM(tr.352) AS formation_352, SUM(tr.433) AS formation_433, SUM(tr.442) AS formation_442, SUM(tr.451) AS formation_451, SUM(tr.max_points) AS maximal_points, SUM(tr.invalid) AS invalid, SUM(tr.missed_goals) AS missed_goals FROM team AS t JOIN team_rating AS tr ON t.team_id = tr.team_id JOIN season AS s On t.season_id = s.season_id WHERE s.season_name = :season_name AND tr.matchday_number >= :from_matchday_number AND tr.matchday_number <= :to_matchday_number GROUP BY tr.team_id, tr.points ORDER BY points DESC, t.team_name ASC");
			$query->execute(array(':season_name' => $season_name, ':from_matchday_number' => $from_matchday_number, ':to_matchday_number' => $to_matchday_number));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getTeamRatingBySeasonAndMatchdayCompleteSeason($season_name, $from_matchday_number, $to_matchday_number) {
			$query = $this->con->prepare("SELECT RANK() OVER (ORDER BY SUM(tr.points) DESC) AS position, t.team_id, t.team_name, SUM(tr.sds) AS sds, SUM(tr.goals) AS goals, SUM(tr.assists) AS assists, SUM(tr.clean_sheet) AS clean_sheet, SUM(tr.points) AS points, SUM(tr.fine) AS fine, SUM(tr.points_goalkeeper) AS points_goalkeeper, SUM(tr.points_defender) AS points_defender, SUM(tr.points_midfielder) AS points_midfielder, SUM(tr.points_forward) AS points_forward, SUM(tr.343) AS formation_343, SUM(tr.352) AS formation_352, SUM(tr.433) AS formation_433, SUM(tr.442) AS formation_442, SUM(tr.451) AS formation_451, SUM(tr.max_points) AS maximal_points, SUM(tr.missed_goals) AS missed_goals FROM team AS t JOIN team_rating AS tr ON t.team_id = tr.team_id JOIN season AS s On t.season_id = s.season_id WHERE s.season_name = :season_name AND tr.matchday_number >= :from_matchday_number AND tr.matchday_number <= :to_matchday_number GROUP BY tr.team_id ORDER BY points DESC, sds DESC, goals DESC, assists DESC, t.team_name ASC");
			$query->execute(array(':season_name' => $season_name, ':from_matchday_number' => $from_matchday_number, ':to_matchday_number' => $to_matchday_number));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getTeamRatingBySeasonIdAndMatchday($season_id, $matchday_number) {
			$query = $this->con->prepare("SELECT RANK() OVER (ORDER BY SUM(tr.points) DESC) AS position, t.team_id, t.team_name, SUM(tr.points) AS points FROM team AS t JOIN team_rating AS tr ON t.team_id = tr.team_id JOIN season AS s On t.season_id = s.season_id WHERE s.season_id = :season_id AND tr.matchday_number = :matchday_number GROUP BY tr.team_id ORDER BY points DESC");
			$query->execute(array(':season_id' => $season_id, ':matchday_number' => $matchday_number));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function getTeamRatingByTeamId($team_id) {
			$query = $this->con->prepare("SELECT tr.points, tr.goals, tr.sds_defender FROM team_rating AS tr WHERE tr.team_id = :team_id ORDER BY tr.matchday_number ASC");
			$query->execute(array(':team_id' => $team_id));
			$result = $query->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}

		function postTeamRating($team_id, $matchday_number) {
			$team_rating_id = $this->generateGUID();
      $query = $this->con->prepare("INSERT INTO team_rating (team_rating_id, team_id, matchday_number) VALUES (:team_rating_id, :team_id, :matchday_number)");
			$query->execute(array(
				':team_rating_id' => $team_rating_id,
        ':team_id' => $team_id,
        ':matchday_number' => $matchday_number,
      ));
			return $team_rating_id;
		}

		function patchTeamRating($team, $matchday_number) {
			$query = $this->con->prepare("UPDATE team_rating AS tr SET tr.points = :points, tr.sds = :sds, tr.goals = :goals, tr.assists = :assists, tr.clean_sheet = :clean_sheet, tr.points_goalkeeper = :points_goalkeeper, tr.points_defender = :points_defender, tr.points_midfielder = :points_midfielder, tr.points_forward = :points_forward, tr.sds_defender = :sds_defender, tr.max_points = :max_points, tr.343 = :formation_343, tr.352 = :formation_352, tr.433 = :formation_433, tr.442 = :formation_442, tr.451 = :formation_451, tr.missed_goals = :missed_goals WHERE tr.team_id = :team_id AND tr.matchday_number = :matchday_number");			
			$query->execute(array(
				':matchday_number' => $matchday_number,
        ':team_id' => $team['team_id'],
        ':points' => $team['rating']['points'],
        ':sds' => $team['rating']['sds'],
        ':goals' => $team['rating']['goals'],
				':assists' => $team['rating']['assists'],
				':clean_sheet' => $team['rating']['clean_sheet'],
				':points_goalkeeper' => $team['rating']['points_goalkeeper'],
				':points_defender' => $team['rating']['points_defender'],
				':points_midfielder' => $team['rating']['points_midfielder'],
				':points_forward' => $team['rating']['points_forward'],
				':sds_defender' => $team['rating']['sds_defender'],
				':max_points' => $team['rating']['max'],
				':formation_343' => $team['rating']['formation']['343'],
				':formation_352' => $team['rating']['formation']['352'],
				':formation_433' => $team['rating']['formation']['433'],
				':formation_442' => $team['rating']['formation']['442'],
				':formation_451' => $team['rating']['formation']['451'],
				':missed_goals' => $team['rating']['missed_goals'],
      ));
		}

		function setFine($team, $matchday_number) {
			$query = $this->con->prepare("UPDATE team_rating AS tr SET tr.fine = :fine WHERE tr.team_id = :team_id AND tr.matchday_number = :matchday_number");
			$query->execute(array(
				':matchday_number' => $matchday_number,
        ':team_id' => $team['team_id'],
				':fine' => intval($team['fine'])
			));
		}
  }