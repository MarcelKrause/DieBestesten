<?php
class CountryController extends _BaseController{

	protected function get(){
    $countries = $this->db->getCountryList();
		return $countries;
	}
	  
	protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }
}