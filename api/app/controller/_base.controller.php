<?php
	require_once(__DIR__.'/../database/base.database.php');
	require_once(__DIR__.'/../../vendor/autoload.php');

	abstract class _BaseController{

		public $db;

		public $endpoint;
		public $id;
		public $params;
		
    abstract protected function get();
		abstract protected function post();
		abstract protected function patch();
		abstract protected function delete();

		function __construct(){
			$this->db = Database::getInstance();
		}

		function getResponse(){
			$method = $_SERVER['REQUEST_METHOD'];
			switch($method) {
				case 'GET':
					$this->parseParams();
					return $this->get();
					break;
				case 'POST':
					return $this->post(); 
					break;
				case 'PATCH':
					$this->encodeBody();
					return $this->patch();
					break;
				case 'DELETE': 
					return $this->delete(); 
					break;
			}
		}

		function parseParams() {
			if($this->params) {
				$paramArray = explode('&', urldecode($this->params));
				$this->params = null;
				foreach($paramArray as $parameter) {
					$key = explode('=', $parameter)[0];
					$value = explode('=', $parameter)[1];
					$this->params[$key] = $value;
				}
			}


		}

		function encodeBody() {
			$body = file_get_contents('php://input');
			$params = explode('&', urldecode($body));
			foreach($params as $param) {
				$key = explode('=', $param)[0];
				$value = explode('=', $param)[1];
				$this->params[$key] = $value;
			}
		}

		function setRequest($request){
			$this->endpoint = $request['endpoint'];
			$this->id = $request['id'];
			$this->params = $request['params'];
		}

		function generateRandomString($length = 12) {
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}
		
		function generateGUID()
		{
			return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
		}
	
	}
