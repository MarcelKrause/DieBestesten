<?php
class ProdcastController extends _BaseController{

	protected function get(){
    $prodcast_list = $this->db->getProdcastList();

		foreach($prodcast_list as &$prodcast) {
			$prodcast['speaker'] = $this->db->getProdcastSpeakerByProdcast($prodcast['prodcast_id']);
		}
    return $prodcast_list;
	}
	  
	protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  	}
}