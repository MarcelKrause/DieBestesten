<?php
class PlayerController extends _BaseController
{

	protected function get()
	{
		$player_id = $this->id;

		if ($player_id) {
			$player = $this->db->getPlayerById($player_id);
			if (!$player) {
				header('HTTP/1.1 404 Not Found');
				return ['status' => 'Not Found', 'message' => 'Es konnte kein Spieler mit folgender ID gefunden werden: [' . $player_id . ']'];
			}

			$player['season_list'] = $this->db->getPlayerInSeasonByPlayer($player['player_id']);
			$player['club_list'] = $this->db->getPlayerInClubByPlayer($player['player_id']);
			return $player;
		} else {
			$now = date("Y-m-d H:i:s");
			$season_id = $this->db->getCurrentSeason($now)['season_id'];

			if ($this->params['displayname']) {
				$displayname = str_replace('_', ' ', $this->params['displayname']);
				$player = $this->db->getPlayerByDisplayname($displayname);

				//$player['season_list'] = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], $season_id);
				$player['season_list'] = $this->db->getSeasonsOfPlayer($player['player_id']);
				foreach ($player['season_list'] as &$season) {
					$season['points'] = intval($this->db->getPlayerRatingSummaryByPlayerAndSeason($player['player_id'], $season['season_id'])['points']);
					if (!$season['points']) {
						$season['points'] = 0;
					}
				}

				$player['team_list'] = $this->db->getPlayerInTeamByPlayerAndSeason($player['player_id'], $season_id);
				foreach ($player['team_list'] as &$team) {
					unset($team['budget']);
				}

				$player['club_list'] = $this->db->getPlayerInClubByPlayerAndCurrentStatus($player['player_id'], $season_id);

				return $player;
			}

			/*
							$player_list = $this->db->getPlayerList();
							foreach($player_list as &$player) {
								$this->db->postPlayerInSeason($player['player_id'], '0534e075-037e-11ed-b2e3-c81f66ca5915', 0, NULL);
								$player['season_list'] = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], '0534e075-037e-11ed-b2e3-c81f66ca5915');
							}
							return $player_list;
						*/

			if ($this->params['filter'] == 'kicker-abgleich') {
				$player_list = $this->db->getBundesligaPlayerList();
				foreach ($player_list as &$player) {
					$stats = $this->db->getPlayerRatingSummaryByPlayerAndSeason($player['player_id'], $season_id);
					$player['kicker_points'] = intval($stats['points']) + (2 * intval($stats['start_lineup'])) + intval($stats['substitution']);

				}
				return $player_list;
			}

			if ($this->params['filter'] == 'bundesliga') {

				$player_list = $this->db->getBundesligaPlayerList();

			} else if ($this->params['filter'] == 'market') {
				$player_list = $this->db->getMarketPlayerList($season_id);
			} else if ($this->params['filter'] == 'sold') {
				$now = date("Y-m-d H:i:s");
				$season = $this->db->getCurrentSeason($now);
				$current_matchday = $this->db->getCurrentMatchday($now, $season['season_id']);
				$player_list = $this->db->getSoldPlayers($season_id, $current_matchday['number']);

				usort($player_list, function ($a, $b) {
					if (intval($a['season_list'][0]['price']) == intval($b['season_list'][0]['price'])) {
						return intval($b['points']) <=> intval($a['points']);
					} else {
						return intval($b['season_list'][0]['price']) <=> intval($a['season_list'][0]['price']);
					}
				});

				return $player_list;
			} else {
				$player_list = $this->db->getPlayerList();
			}



			foreach ($player_list as $key => &$player) {
				$player['points'] = $this->db->getPlayerRatingSummaryByPlayerAndSeason($player['player_id'], $season_id)['points'];
				//$player['kicker_points'] = $this->db->getPlayerRatingSummaryByPlayerAndSeason($player['player_id'], $season_id);
				if (!$player['points']) {
					$player['points'] = 0;
				}

				$player['season_list'] = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], $season_id);
				$player['club_list'] = $this->db->getPlayerInClubByPlayerAndCurrentStatus($player['player_id'], $season_id);
				$player['team_list'] = $this->db->getPlayerInTeamByPlayerAndSeason($player['player_id'], $season_id);

				foreach ($player['season_list'] as &$season) {
					$season['price'] = intval($season['price']) + 20000 * $player['points'];
				}

				if ($player['season_list'] == []) {
					//$player['season_list'] = array(['test' => 'halo']);
					//unset($player_list[$key]);
				}
			}

			if ($this->params['filter'] == 'market') {
				usort($player_list, function ($a, $b) {
					if (intval($a['season_list'][0]['price']) == intval($b['season_list'][0]['price'])) {
						return intval($b['points']) <=> intval($a['points']);
					} else {
						return intval($b['season_list'][0]['price']) <=> intval($a['season_list'][0]['price']);
					}
				});
			} else {
				usort($player_list, function ($a, $b) {
					if (intval($a['points']) == intval($b['points'])) {
						return intval($a['season_list'][0]['price']) <=> intval($b['season_list'][0]['price']);
					} else {
						return intval($b['points']) <=> intval($a['points']);
					}
				});
			}

			return $player_list;
		}
	}

	protected function post()
	{
		if ($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 403 Forbidden');
			return ['status' => 'Forbidden', 'message' => 'Keine Berechtigung einen Spieler zu erstellen'];
		}

		if (!isset($_POST['displayname'])) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [displayname]'];
		}

		$player = $this->db->getPlayerByKickerId($_POST['kicker_id']);
		if ($player) {
			header('HTTP/1.1 409 Conflict');
			return ['status' => 'Not Found', 'message' => 'Es existiert bereits ein Spieler mit folgender Kicker-ID: [' . $_POST['kicker_id'] . ']'];
		}

		$player = $this->db->getPlayerByDisplayname($_POST['displayname']);
		if ($player) {
			header('HTTP/1.1 409 Conflict');
			return ['status' => 'Not Found', 'message' => 'Es existiert bereits ein Spieler mit folgendem Anzeigenamen: [' . $_POST['displayname'] . ']'];
		}


		$player_id = $this->db->postPlayer($_POST);

		$now = date("Y-m-d H:i:s");
		$season_id = $this->db->getCurrentSeason($now)['season_id'];
		if ($_POST['position']) {
			if (!in_array($_POST['position'], ['GOALKEEPER', 'DEFENDER', 'MIDFIELDER', 'FORWARD'])) {
				$this->db->deletePlayer($player_id);
				header('HTTP/1.1 400 	Bad Request');
				return ['status' => 'Bad Request', 'message' => '[' . $_POST['position'] . '] ist keine gültige Position. Gültig sind: [GOALKEEPER], [DEFENDER], [MIDFIELDER] und [FORWARD]'];
			}
		}
		if ($_POST['price']) {
			if (!is_numeric($_POST['price'])) {
				$this->db->deletePlayer($player_id);
				header('HTTP/1.1 400 	Bad Request');
				return ['status' => 'Bad Request', 'message' => '[' . $_POST['price'] . '] ist keine Zahl und daher eine ungültige Eingabe'];
			}
		}
		$this->db->postPlayerInSeason($player_id, $season_id, $_POST['price'], $_POST['position']);


		if ($player_id) {
			$player = $this->db->getPlayerById($player_id);
			$this->db->postActivity('POST', $player_id, 'player', null, 'spielerdatenbank', 'Spieler [' . $player['displayname'] . '] erstellt');
			header('HTTP/1.1 201 Created');
			return $player;
		} else {
			header('HTTP/1.1 500 Internal Server Error');
			return ['status' => 'Internal Server Error', 'message' => 'Aktion konnte serverseitig nicht durchgeführt werden'];
		}
	}

	protected function patch()
	{
		if ($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 401 Unauthorized');
			return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung einen Spieler zu bearbeiten'];
		}

		$player_id = $this->id;
		if (!isset($player_id)) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_id]'];
		}

		if (!$this->params['displayname']) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [displayname]'];
		}

		$player = $this->db->getPlayerById($player_id);
		if (!$player) {
			header('HTTP/1.1 404 Not Found');
			return ['status' => 'Not Found', 'message' => 'Es konnte kein Spieler mit folgender ID gefunden werden: [' . $player_id . ']'];
		}

		$duplicate = $this->db->getPlayerByDisplayname($this->params['displayname']);
		if ($duplicate && $duplicate['player_id'] != $player['player_id']) {
			header('HTTP/1.1 409 Conflict');
			return ['status' => 'Not Found', 'message' => 'Es existiert bereits ein Spieler mit folgendem Anzeigenamen: [' . $this->params['displayname'] . ']'];
		}

		$patched_player = $player;
		$patched_player['firstname'] = $this->params['firstname'];
		$patched_player['lastname'] = $this->params['lastname'];
		$patched_player['displayname'] = $this->params['displayname'];
		$patched_player['city'] = $this->params['city'];
		$patched_player['country_code'] = $this->params['country_code'];
		$patched_player['date_of_birth'] = $this->params['date_of_birth'];
		$patched_player['height'] = $this->params['height'];
		$patched_player['weight'] = $this->params['weight'];

		$patched_player['kicker_id'] = $this->params['kicker_id'];
		$patched_player['ligainsider_id'] = $this->params['ligainsider_id'];


		$this->db->patchPlayer($patched_player);

		$player_modified = false;
		if ($patched_player['firstname'] != $player['firstname']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'firstname', 'spielerdatenbank', 'Vorname bei [' . $player['displayname'] . '] von [' . $player['firstname'] . '] zu [' . $patched_player['firstname'] . '] geändert');
		}
		if ($patched_player['lastname'] != $player['lastname']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'lastname', 'spielerdatenbank', 'Nachname bei [' . $player['displayname'] . '] von [' . $player['lastname'] . '] zu [' . $patched_player['lastname'] . '] geändert');
		}
		if ($patched_player['displayname'] != $player['displayname']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'displayname', 'spielerdatenbank', 'Anzeigename bei [' . $player['displayname'] . '] von [' . $player['displayname'] . '] zu [' . $patched_player['displayname'] . '] geändert');
		}
		if ($patched_player['city'] != $player['city']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'city', 'spielerdatenbank', 'Geburtsort bei [' . $player['displayname'] . '] von [' . $player['city'] . '] zu [' . $patched_player['city'] . '] geändert');
		}
		if ($patched_player['country_code'] != $player['country_code']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'country_code', 'spielerdatenbank', 'Nation bei [' . $player['displayname'] . '] von [' . $player['country_code'] . '] zu [' . $patched_player['country_code'] . '] geändert');
		}
		if (strtotime($patched_player['date_of_birth']) != strtotime($player['date_of_birth'])) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'date_of_birth', 'spielerdatenbank', 'Geburtsdatum bei [' . $player['displayname'] . '] von [' . $player['date_of_birth'] . '] zu [' . $patched_player['date_of_birth'] . '] geändert');
		}
		if ($patched_player['height'] != $player['height']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'height', 'spielerdatenbank', 'Größe bei [' . $player['displayname'] . '] von [' . $player['height'] . '] cm. zu [' . $patched_player['height'] . '] cm. geändert');
		}
		if ($patched_player['weight'] != $player['weight']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'weight', 'spielerdatenbank', 'Gewicht bei [' . $player['displayname'] . '] von [' . $player['weight'] . '] kg. zu [' . $patched_player['weight'] . '] kg. geändert');
		}
		if ($patched_player['kicker_id'] != $player['kicker_id']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'kicker_id', 'spielerdatenbank', 'Kicker ID bei [' . $player['displayname'] . '] von [' . $player['kicker_id'] . '] zu [' . $patched_player['kicker_id'] . '] geändert');
		}
		if ($patched_player['ligainsider_id'] != $player['ligainsider_id']) {
			$player_modified = true;
			$this->db->postActivity('PATCH', $player_id, 'player', 'ligainsider_id', 'spielerdatenbank', 'Ligainsider ID bei [' . $player['displayname'] . '] von [' . $player['ligainsider_id'] . '] zu [' . $patched_player['ligainsider_id'] . '] geändert');
		}

		if (!$player_modified) {
			header('HTTP/1.1 406 Not Acceptable');
			return ['status' => 'Not Acceptable', 'message' => 'Es wurden keine Änderungen an den Server geschickt'];
		} else {
			$player = $this->db->getPlayerById($player_id);
			return $player;
		}

	}

	protected function delete()
	{
		if ($_SERVER['role'] != 'admin') {
			header('HTTP/1.1 401 Unauthorized');
			return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung einen Spieler zu löschen'];
		}

		$player_id = $this->id;
		if (!isset($player_id)) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_id]'];
		}
		$player = $this->db->getPlayerById($player_id);
		if (!$player) {
			header('HTTP/1.1 404 Not Found');
			return ['status' => 'Not Found', 'message' => 'Es konnte kein Spieler mit folgender ID gefunden werden: [' . $player_id . ']'];
		}

		$player_in_club_list = $this->db->getPlayerInClubByPlayer($player_id);
		$player_in_season_list = $this->db->getPlayerInSeasonByPlayer($player_id);

		$this->db->deletePlayer($player_id);
		foreach ($player_in_club_list as $player_in_club) {
			$this->db->deletePlayerInClub($player_in_club['player_in_club_id']);
		}
		foreach ($player_in_season_list as $player_in_season) {
			$this->db->deletePlayerInSeason($player_in_season['player_in_season_id']);
		}
		$deleted_player = $this->db->getPlayerById($player_id);

		if ($deleted_player) {
			header('HTTP/1.1 500 Internal Server Error');
			return ['status' => 'Internal Server Error', 'message' => 'Aktion konnte serverseitig nicht durchgeführt werden', 'player' => $player];
		} else {
			$this->db->postActivity('DELETE', $player_id, 'player', null, null, 'Spieler [' . $player['displayname'] . '] gelöscht');
			return ['status' => 'OK', 'message' => 'Player successfully deleted'];
		}
	}

}