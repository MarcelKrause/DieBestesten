<?php
class PlayerRatingCheckController extends _BaseController
{

  protected function get()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only POST Requests on this endpoint.'];
  }

  protected function post()
  {
    $now = date("Y-m-d H:i:s");
    $season_id = $this->db->getCurrentSeason($now)['season_id'];

    $count = 0;
    $player_list = [];
    if ($_FILES['data']['error'] === 0) {
      $csv = fopen($_FILES['data']['tmp_name'], 'r');
      $line = 1;
      while (($data = fgetcsv($csv, 1000, ',')) !== FALSE) {
        if ($line > 1) {
          $player_line = explode(';', $data[0]);
          $kicker_id = +substr($player_line[0], 4);
          $kicker_points = +$player_line[8];
          $player_id = $this->db->getPlayerByKickerId($kicker_id)['player_id'];

          if ($this->id === 'import') {
            $player_in_season = $this->db->getPlayerInSeasonByPlayerAndSeason($player_id, $season_id)[0];
            $player_in_season['position'] = $player_line[6];
            $player_in_season['price'] = +$player_line[7];

            $this->db->patchPlayerInSeason($player_in_season);
            $count++;
          } else {
            $player_rating = $this->db->getPlayerRatingSummaryByPlayerAndSeason($player_id, $season_id);

            $system_points = +$player_rating['points'] + 2 * +$player_rating['start_lineup'] + +$player_rating['substitution'] + +$player_rating['assists'];

            if ($system_points != $kicker_points) {
              $player['name'] = $player_line[4];
              $player['club'] = $player_line[5];
              $player['kicker_points'] = $kicker_points;
              $player['system_points'] = $system_points;
              array_push($player_list, $player);
            }
          }


        }

        $line++;
      }
      fclose($csv);
    }

    if ($this->id == 'import') {
      return '[' . $count . '] Saisonwerte importiert';
    }

    return $player_list;
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only POST Requests on this endpoint.'];
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only POST Requests on this endpoint.'];
  }

}