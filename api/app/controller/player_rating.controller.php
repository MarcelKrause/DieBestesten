<?php
// require_once(__DIR__.'/../database/old.database.php');
class PlayerRatingController extends _BaseController
{

  protected function get()
  {
    if ($this->id == 'conflict') {

      $conflicts;

      $now = date("Y-m-d H:i:s");

      $season_id = $this->db->getCurrentSeason($now)['season_id'];
      $matchday = $this->db->getCurrentMatchday($now, $season_id)['number'];


      $player_rating_conflicts = array();
      $player_rating_list = $this->db->getPlayerRatingListBySeason($season_id);
      foreach ($player_rating_list as $player_rating) {
        if ($this->calculatePoints($player_rating) != intval($player_rating['points'])) {
          array_push($player_rating_conflicts, $player_rating);
        }
      }
      $conflicts['player_rating'] = $player_rating_conflicts;

      return $conflicts;

      $team_rating_conflicts = array();
      $team_list = $this->db->getTeamsBySeason($season_id);
      foreach ($team_list as $team) {
        for ($i = 1; $i <= $matchday; $i++) {
          $team_rating = $this->db->getTeamRatingByTeamAndMatchday($team['team_id'], $i);
          $team_lineup_list = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $i);
          $points = 0;
          foreach ($team_lineup_list as $team_lineup) {
            if (boolval($team_lineup['nominated'])) {
              $player_rating = $this->db->getPlayerRatingByPlayerAndMatchday($team_lineup['player_id'], $team_lineup['matchday'], $season_id);
              $points = $points + intval($player_rating['points']);
            }
          }

          if (intval($team_rating['points']) != $points) {
            $team_rating['correct_points'] = $points;
            array_push($team_rating_conflicts, $team_rating);
          }

        }

      }
      $conflicts['team_rating'] = $team_rating_conflicts;
      return $conflicts;
    }
    if ($this->id == 'matchday') {
      $matchday = intval($this->params['matchday']);
      $club_id = $this->params['club_id'];

      if (!isset($matchday)) {
        header('HTTP/1.1 400 Bad Request');
        return ['status' => 'Bad Request', 'message' => 'Notwendiger Parameter fehlt: [matchday]'];
      }
      if (!isset($club_id)) {
        header('HTTP/1.1 400 Bad Request');
        return ['status' => 'Bad Request', 'message' => 'Notwendiger Parameter fehlt: [club_id]'];
      }

      if ($matchday < 1 || $matchday > 34) {
        header('HTTP/1.1 400 Bad Request');
        return ['status' => 'Bad Request', 'message' => 'Parameter [matchday] nicht im erlaubten Bereich [1 - 34]'];
      }

      $club = $this->db->getClubById($club_id);
      if (!$club) {
        header('HTTP/1.1 400 Bad Request');
        return ['status' => 'Bad Request', 'message' => 'Es existiert kein Club mit der ID [' . $club_id . ']'];
      }

      $now = date("Y-m-d H:i:s");
      $current_season = $this->db->getCurrentSeason($now);

      $player_list = $this->db->getPlayerListByClubId($club_id);
      foreach ($player_list as &$player) {
        unset($player['date_of_birth']);
        unset($player['kicker_id']);
        unset($player['ligainsider_id']);
        unset($player['country_code']);
        unset($player['city']);
        unset($player['date_of_birth']);
        unset($player['height']);
        unset($player['weight']);
        unset($player['player_in_club_id']);
        unset($player['club_id']);
        unset($player['from_date']);
        unset($player['to_date']);
        unset($player['is_loan']);
        $player_in_season = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], $current_season['season_id']);
        $player['season_list'] = $player_in_season;
        unset($player['season_list'][0]['player_in_season_id']);
        unset($player['season_list'][0]['player_id']);
        unset($player['season_list'][0]['season_id']);
        unset($player['season_list'][0]['season_name']);
        unset($player['season_list'][0]['has_photo']);

        $player['rating_list'] = $this->db->getPlayerRatingByPlayerAndMatchday($player['player_id'], $matchday, $current_season['season_id']);
        unset($player['rating_list'][0]['player_id']);
        unset($player['rating_list'][0]['season_id']);

        if (!$player['rating_list'][0]) {
          /*
          $player_rating_id = $this->db->postPlayerRating($player['player_id'], $current_season['season_id'], $matchday);
          $player['rating_list'][0] = $this->db->getPlayerRatingById($player_rating_id);
          
          $old_db = OldDatabase::getInstance();
          $old_db->postPlayerRating($player['rating_list'][0]['player_rating_id'], $player['player_id'], $current_season['season_id'], $matchday);
          */
        }
      }
      usort($player_list, function ($a, $b) {
        if ($a['season_list'][0]['position'] == $b['season_list'][0]['position']) {
          return $b['season_list'][0]['price'] <=> $a['season_list'][0]['price'];
        } else {
          return $this->getPositionValue($a['season_list'][0]['position']) <=> $this->getPositionValue($b['season_list'][0]['position']);
        }
      });
      return $player_list;
    } else {
      $player_id = $this->id;
      $player = $this->db->getPlayerById($player_id);

      if (!$player) {
        header('HTTP/1.1 404 Not Found');
        return ['status' => 'Not Found', 'message' => 'Es konnte kein Spieler mit folgender ID gefunden werden: [' . $player_id . ']'];
      }

      $season_id = $this->params['season_id'];

      $player_rating_in_season = $this->db->getPlayerRatingListByPlayerAndSeason($player['player_id'], $season_id);

      return $player_rating_in_season;
    }

  }

  protected function post()
  {
    if ($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
      header('HTTP/1.1 401 Unauthorized');
      return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung eine Spielerbewertung zu bearbeiten'];
    }

    if (!isset($_POST['player_id'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_id]'];
    }

    if (!isset($_POST['season_id'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [season_id]'];
    }

    if (!isset($_POST['matchday_number'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [matchday_number]'];
    }

    $player_id = $_POST['player_id'];
    $season_id = $_POST['season_id'];
    $club_id = $_POST['club_id'];
    $matchday_number = $_POST['matchday_number'];
    $player_rating = $this->db->getPlayerRatingByPlayerAndMatchday($player_id, $matchday_number, $season_id)[0];

    if ($player_rating) {
      header('HTTP/1.1 409 Conflict');
      return ['status' => 'Not Found', 'message' => 'Es existiert bereits eine Spielerbewertung von dem Spieler mit der ID [' . $player_id . '] am [' . $matchday_number . '].Spieltag'];
    }

    $player_rating_id = $this->db->postPlayerRating($player_id, $season_id, $club_id, $matchday_number);
    $player_rating = $this->db->getPlayerRatingById($player_rating_id);

    $old_db = OldDatabase::getInstance();
    $old_db->postPlayerRating($player_rating_id, $player_id, $season_id, $matchday_number);

    return $player_rating;
  }

  protected function patch()
  {
    if ($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
      header('HTTP/1.1 401 Unauthorized');
      return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung eine Spielerbewertung zu bearbeiten'];
    }

    $player_rating_id = $this->id;
    if (!isset($player_rating_id)) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_rating_id]'];
    }
    $player_rating = $this->db->getPlayerRatingById($player_rating_id);
    if (!isset($player_rating)) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Es konnte keine Spielerbewertung mit folgender ID gefunden werden: [' . $player_rating_id . ']'];
    }


    $now = date("Y-m-d H:i:s");
    $status['season'] = $this->db->getCurrentSeason($now);
    $status['matchday'] = $this->db->getCurrentMatchday($now, $status['season']['season_id']);
    if ($status['matchday']['number'] != $player_rating['matchday'] && $_SERVER['role'] != 'admin') {
      header('HTTP/1.1 401 Unauthorized');
      return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung eine alte Spielerbewertung zu bearbeiten'];
    }


    $patched_player_rating = $player_rating;

    $patched_player_rating['player_rating_id'] = $player_rating_id;
    $patched_player_rating['grade'] = floatval($this->params['grade']);
    $patched_player_rating['ligainsider_grade'] = floatval($this->params['ligainsider_grade']);
    $patched_player_rating['start_lineup'] = $this->params['start_lineup'];
    $patched_player_rating['substitution'] = $this->params['substitution'];
    $patched_player_rating['goals'] = intval($this->params['goals']);
    $patched_player_rating['assists'] = intval($this->params['assists']);
    $patched_player_rating['sds'] = $this->params['sds'];
    $patched_player_rating['clean_sheet'] = $this->params['clean_sheet'];
    $patched_player_rating['red_card'] = $this->params['red_card'];
    $patched_player_rating['yellow_red_card'] = $this->params['yellow_red_card'];
    $patched_player_rating['points'] = intval($this->params['points']);

    $this->db->patchPlayerRating($patched_player_rating);


    // $old_db = OldDatabase::getInstance();
    // $old_db->patchPlayerRating($patched_player_rating);
/*
    $player_rating_modified = false;
    $player = $this->db->getPlayerById($player_rating['player_id']);
    if ($patched_player_rating['start_lineup'] != $player_rating['start_lineup']) {
      $player_rating_modified = true;
      if ($patched_player_rating['start_lineup']) {
        $message = '[' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag zur Startelf hinzugefügt';
      } else {
        $message = 'KORREKTUR: [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag aus Startelf entfernt';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'start_lineup', 'noten', $message);
    }
    if ($patched_player_rating['substitution'] != $player_rating['substitution']) {
      $player_rating_modified = true;
      if ($patched_player_rating['substitution']) {
        $message = '[' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag als Einwechslung hinzugefügt';
      } else {
        $message = 'KORREKTUR: Einwechslung von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag zurückgenommen';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'substitution', 'noten', $message);
    }
    if ($patched_player_rating['grade'] != $player_rating['grade']) {
      $player_rating_modified = true;
      if (!$player_rating['grade']) {
        $message = '[' . $patched_player_rating['grade'] . '] als Note von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag eingetragen';
      } else {
        $message = 'KORREKTUR: [' . $patched_player_rating['grade'] . '] als Note von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag eingetragen';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'grade', 'noten', $message);
    }
    if ($patched_player_rating['goals'] != $player_rating['goals']) {
      $player_rating_modified = true;
      if ($patched_player_rating['goals'] > $player_rating['goals']) {
        $message = $patched_player_rating['goals'] . '. Tor von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag';
      } else {
        $message = 'KORREKTUR: ' . $player_rating['goals'] . '. Tor von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag zurückgenommen';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'goals', 'noten', $message);
    }
    if ($patched_player_rating['assists'] != $player_rating['assists']) {
      $player_rating_modified = true;
      if ($patched_player_rating['assists'] > $player_rating['assists']) {
        $message = $patched_player_rating['assists'] . '. Vorlage von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag';
      } else {
        $message = 'KORREKTUR: ' . $player_rating['assists'] . '. Vorlage von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag zurückgenommen';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'assists', 'noten', $message);
    }
    if ($patched_player_rating['sds'] != $player_rating['sds']) {
      $player_rating_modified = true;
      if ($patched_player_rating['sds']) {
        $message = '[' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag als SDS markiert';
      } else {
        $message = 'KORREKTUR: SDS von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag zurückgenommen';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'sds', 'noten', $message);
    }
    if ($patched_player_rating['clean_sheet'] != $player_rating['clean_sheet']) {
      $player_rating_modified = true;
      if ($patched_player_rating['clean_sheet']) {
        $message = 'KORREKTUR: Weiße Weste von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag wieder hinzugefügt';
      } else {
        $message = 'Weiße Weste von [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag entfernt';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'clean_sheet', 'noten', $message);
    }
    if ($patched_player_rating['red_card'] != $player_rating['red_card']) {
      $player_rating_modified = true;
      if ($patched_player_rating['red_card']) {
        $message = 'Rote-Karte für [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag hinzugefügt';
      } else {
        $message = 'KORREKTUR: Rote-Karte für [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag wieder entfernt';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'red_card', 'noten', $message);
    }
    if ($patched_player_rating['yellow_red_card'] != $player_rating['yellow_red_card']) {
      $player_rating_modified = true;
      if ($patched_player_rating['yellow_red_card']) {
        $message = 'Gelb-Rote-Karte für [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag hinzugefügt';
      } else {
        $message = 'KORREKTUR: Gelb-Rote-Karte für [' . $player['displayname'] . '] am ' . $patched_player_rating['matchday'] . '.Spieltag wieder entfernt';
      }
      $this->db->postActivity('PATCH', $player_rating_id, 'player_rating', 'yellow_red_card', 'noten', $message);
    }
*/

    return $player_rating;

    /*
    if (!$player_rating_modified) {
    header('HTTP/1.1 406 Not Acceptable');
    return ['status' => 'Not Acceptable', 'message' => 'Es wurden keine Änderungen an den Server geschickt'];
    } else {
    $player_rating = $this->db->getPlayerRatingById($player_rating_id);
    return $player_rating;
    }
    */
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  function getPositionValue($position)
  {
    switch ($position) {
      case 'GOALKEEPER':
        return 1;
        break;
      case 'DEFENDER':
        return 2;
        break;
      case 'MIDFIELDER':
        return 3;
        break;
      case 'FORWARD':
        return 4;
        break;
      default:
        return 5;
    }
  }

  private function calculatePoints($player_rating)
  {
    $points = 0;
    if (boolval($player_rating['start_lineup'])) {
      $points += 2;
    }
    if (boolval($player_rating['substitution'])) {
      $points += 1;
    }
    if (boolval($player_rating['red_card'])) {
      $points -= 6;
    }
    if (boolval($player_rating['yellow_red_card'])) {
      $points -= 3;
    }
    if (boolval($player_rating['sds'])) {
      $points += 3;
    }
    if (boolval($player_rating['clean_sheet'])) {
      $points += 2;
    }

    $points += intval($player_rating['assists']);

    if (intval($player_rating['goals'] > 0)) {
      switch ($player_rating['position']) {
        case 'FORWARD':
          $points += intval($player_rating['goals']) * 3;
          break;
        case 'MIDFIELDER':
          $points += intval($player_rating['goals']) * 4;
          break;
        case 'DEFENDER':
          $points += intval($player_rating['goals']) * 5;
          break;
        case 'GOALKEEPER':
          $points += intval($player_rating['goals']) * 6;
          break;
        default:
          break;
      }
    }

    switch (floatval($player_rating['grade'])) {
      case 1.0:
        $points += 10;
        break;
      case 1.5:
        $points += 8;
        break;
      case 2.0:
        $points += 6;
        break;
      case 2.5:
        $points += 4;
        break;
      case 3.0:
        $points += 2;
        break;
      case 3.5:
        $points += 0;
        break;
      case 4.0:
        $points -= 2;
        break;
      case 4.5:
        $points -= 4;
        break;
      case 5.0:
        $points -= 6;
        break;
      case 5.5:
        $points -= 8;
        break;
      case 6.0:
        $points -= 10;
        break;
      default:
        break;
    }
    return $points;
  }

}