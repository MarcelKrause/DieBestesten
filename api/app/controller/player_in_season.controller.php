<?php
class PlayerInSeasonController extends _BaseController
{

	protected function get()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only PATCH Requests on this endpoint.'];
	}

	protected function post()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only PATCH Requests on this endpoint.'];
	}

	protected function patch()
	{
		if ($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 401 Unauthorized');
			return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung SpielerInSaison zu bearbeiten'];
		}

		$player_in_season_id = $this->id;
		if (!isset($player_in_season_id)) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_in_season_id]'];
		}

		$player_in_season = $this->db->getPlayerInSeasonById($player_in_season_id);
		if (!$player_in_season) {
			header('HTTP/1.1 404 Not Found');
			return ['status' => 'Not Found', 'message' => 'Es konnte kein SpielerInSaison mit folgender ID gefunden werden: [' . $player_in_season . ']'];
		}

		$patched_player_in_season = $player_in_season;
		$patched_player_in_season['position'] = $this->params['position'];
		$patched_player_in_season['price'] = $this->params['price'];
		$patched_player_in_season['is_captain'] = $this->params['is_captain'];

		$this->db->patchPlayerInSeason($patched_player_in_season);

		$player_in_season_modified = false;
		if ($patched_player_in_season['position'] != $player_in_season['position']) {
			$player_in_season_modified = true;
			$this->db->postActivity('PATCH', $player_in_season['player_id'], 'player_in_season', 'position', 'spielerdatenbank', 'Position bei [' . $player_in_season['displayname'] . '] in der Saison [' . $player_in_season['season_name'] . '] von [' . $player_in_season['position'] . '] zu [' . $patched_player_in_season['position'] . '] geändert');
		}
		if ($patched_player_in_season['price'] != $player_in_season['price']) {
			$player_in_season_modified = true;
			$this->db->postActivity('PATCH', $player_in_season['player_id'], 'player_in_season', 'price', 'spielerdatenbank', 'Marktwert bei [' . $player_in_season['displayname'] . '] in der Saison [' . $player_in_season['season_name'] . '] von [' . $player_in_season['price'] . '] zu [' . $patched_player_in_season['price'] . '] geändert');
		}
		if ($patched_player_in_season['is_captain'] != $player_in_season['is_captain']) {
			$player_in_season_modified = true;
			$this->db->postActivity('PATCH', $player_in_season['player_id'], 'player_in_season', 'is_captain', 'spielerdatenbank', 'Kapitänsstatus bei [' . $player_in_season['displayname'] . '] in der Saison [' . $player_in_season['season_name'] . '] von [' . $player_in_season['is_captain'] . '] zu [' . $patched_player_in_season['is_captain'] . '] geändert');
		}

		if (!$player_in_season_modified) {
			header('HTTP/1.1 406 Not Acceptable');
			return ['status' => 'Not Acceptable', 'message' => 'Es wurden keine Änderungen an den Server geschickt'];
		} else {
			$player_in_season = $this->db->getPlayerInSeasonById($player_in_season_id);
			return $player_in_season;
		}
	}

	protected function delete()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only PATCH Requests on this endpoint.'];
	}
}