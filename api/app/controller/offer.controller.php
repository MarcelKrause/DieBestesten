<?php
class OfferController extends _BaseController
{

    protected function get()
    {
        $transferwindow_id = $this->params['transferwindow_id'];

        if (isset($transferwindow_id)) {
            $now = date("Y-m-d H:i:s");
            $transferwindow = $this->db->getTransferwindowById($transferwindow_id);
            if (!$transferwindow) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Transferfenster nicht gefunden: [' . $transferwindow_id . ']'];
            }
            if ($transferwindow['end_date'] > $now) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Transferfenster ist noch offen'];
            }

            // TODO: check if transferwindow is already closed
            $matchday = $this->db->getMatchdayById($transferwindow['matchday_id']);



            $offer_by_player_list = $this->db->getPlayerListByTransferwindow($transferwindow_id);
            foreach ($offer_by_player_list as &$grouped_offer) {
                $grouped_offer['season_id'] = $matchday['season_id'];
                $grouped_offer['player'] = $this->db->getPlayerById($grouped_offer['player_id']);
                $grouped_offer['player']['season_list'] = $this->db->getPlayerInSeasonByPlayerAndSeason($grouped_offer['player_id'], $matchday['season_id']);
                $grouped_offer['player']['club_list'] = $this->db->getPlayerInClubByPlayerAndCurrentStatus($grouped_offer['player_id'], $matchday['season_id']);
                $grouped_offer['offer_list'] = $this->db->getOfferByPlayerByTransferwindow($transferwindow_id, $grouped_offer['player_id']);

                // performance
                unset($grouped_offer['player']['kicker_id']);
                unset($grouped_offer['player']['ligainsider_id']);
                unset($grouped_offer['player']['city']);
                unset($grouped_offer['player']['date_of_birth']);
                unset($grouped_offer['player']['height']);
                unset($grouped_offer['player']['weight']);
                unset($grouped_offer['player']['country_name']);
                unset($grouped_offer['player']['inhabitants']);
            }

            // spieler zuweisen
            if ($transferwindow['end_date'] < $now && $offer_by_player_list[0]['offer_list'][0]['status'] == 'pending') {
                foreach ($offer_by_player_list as &$player) {
                    foreach ($player['offer_list'] as $index => &$offer) {
                        //if (isset($offer['owner_team_id']) && $offer['status'] = 'pending') {
                        //    $offer['status'] = 'rejected';
                        //} else {
                        if ($index == 0) {
                            $offer['status'] = 'success';
                            $this->db->reduceTeamBudget($offer['team_id'], $offer['offer_value']);
                            $this->db->postPlayerInTeam($offer['team_id'], $player['player_id'], $transferwindow['matchday_number'], $offer['offer_id']);
                        } else {
                            $offer['status'] = 'lost';
                        }
                        //}


                        $this->db->patchOffer($offer);
                    }
                }
            }

            usort($offer_by_player_list, function ($a, $b) {
                if ($a['offer_list'][0]['offer_value'] == $b['offer_list'][0]['offer_value']) {
                    return $b['count'] <=> $a['count'];
                } else {
                    return $b['offer_list'][0]['offer_value'] <=> $a['offer_list'][0]['offer_value'];
                }
            });

            return $offer_by_player_list;
        } else {
            $now = date("Y-m-d H:i:s");
            $season_id = $this->db->getCurrentSeason($now)['season_id'];
            $team = $this->db->getTeamByManagerAndSeason($_SERVER['manager_id'], $season_id);
            $offer_list = $this->db->getOfferListByTeam($team['team_id'], $team['season_id']);
            $offer_to_me_list = $this->db->getOfferListToTeam($team['team_id'], $team['season_id']);
            return ['offer_list' => $offer_list, 'offer_to_me_list' => $offer_to_me_list];
        }
    }

    protected function post()
    {
        $now = date("Y-m-d H:i:s");
        $season_id = $this->db->getCurrentSeason($now)['season_id'];

        if (!isset($_POST['player_id'])) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_id]'];
        }

        if (!isset($_POST['offer_value'])) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [offer_value]'];
        }

        if (!isset($_POST['team_id'])) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [team_id]'];
        }

        if (!isset($_POST['transferwindow_id'])) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [transferwindow_id]'];
        }

        if (!isset($_POST['price_snapshot'])) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [price_snapshot]'];
        }

        // CHECK PLAYER (exists / bundesliga / available)
        $player_id = $_POST['player_id'];
        $player = $this->db->getPlayerById($player_id);
        if (!$player) {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Es konnte kein Spieler mit folgender ID gefunden werden: [' . $player_id . ']'];
        }
        // offers to not available players are possible now
        /*$player['team_list'] = $this->db->getPlayerInTeamByPlayerAndSeason($player['player_id'], $season_id);
        if($player['team_list'][0] != null && $player['team_list'][0]['last_matchday'] == null) {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Der Spieler ist bereits unter Vertrag bei ['.$player['team_list'][0]['team_name'].']'];
        }*/
        $club = $this->db->getPlayerInClubByPlayerAndCurrentStatus($player['player_id'])[0];
        if ($club['is_bundesliga'] == '0') {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Der Spieler ist aktuell bei keinem Bundesligisten: [' . $club['club_name'] . ']'];
        }


        // CHECK TEAM (exists / own_team)
        $team_id = $_POST['team_id'];
        $team = $this->db->getTeamById($team_id);
        if (!$team) {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Es konnte kein Team mit folgender ID gefunden werden: [' . $team_id . ']'];
        }
        // offers to not available players are possible now
        /*
        if ($team['manager_id'] != $_SERVER['manager_id']) {
            header('HTTP/1.1 403 Forbidden');
            return ['status' => 'Forbidden', 'message' => 'Es kann kein Gebot im Namen eines fremden Teams abgegeben werden'];
        }*/

        // CHECK OWNER TEAM (exists)
        $owner_team_id = $_POST['owner_team_id'];
        if ($owner_team_id === 'null') {
            $owner_team_id = null;
        }
        if (isset($owner_team_id)) {
            $owner_team = $this->db->getTeamById($owner_team_id);
            if (!$owner_team) {
                header('HTTP/1.1 404 Not Found');
                return ['status' => 'Not Found', 'message' => 'Es konnte kein (Besitzer)Team mit folgender ID gefunden werden: [' . $team_id . ']'];
            }
        }


        // CHECK TRANSFERWINDOW (exists / open)
        $transferwindow_id = $_POST['transferwindow_id'];
        $transferwindow = $this->db->getTransferwindowById($transferwindow_id);
        if (!$transferwindow) {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Es konnte kein Transferfenster mit folgender ID gefunden werden: [' . $transferwindow_id . ']'];
        }
        if ($transferwindow['end_date'] < $now) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Transferphase abgelaufen'];
        }


        // CHECK OFFER-VALUE (numeric, >0, >price, <budget)
        // TODO: check if offer ist bigger or same than price
        $offer_value = $_POST['offer_value'];
        if (!is_numeric($offer_value)) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Ungültigen Wert übertragen', 'value' => $offer_value];
        }
        $offer_value = intval($offer_value);
        if ($offer_value < 0) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Ungültigen Wert übertragen', 'value' => $offer_value];
        }
        $offer_list = $this->db->getPendingOfferListByTeam($team['team_id'], $season_id);
        $budget = $team['budget'];
        $prev_budget = $budget;
        foreach ($offer_list as $offer) {
            $budget -= $offer['offer_value'];
        }
        if ($offer_value > $budget) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Nicht ausreichend genug Budget vorhanden', 'budget' => $budget, 'offer_list' => $offer_list, 'prev_budget' => $prev_budget];
        }

        foreach ($offer_list as $offer) {
            if ($offer['player_id'] == $player_id) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Es existiert bereits ein offenes Gebot für [' . $player['displayname'] . ']'];
            }
        }
        $price_snapshot = $_POST['price_snapshot'];

        $offer_id = $this->db->postOffer($player_id, $team_id, $owner_team_id, $transferwindow_id, $offer_value, $price_snapshot);

        if (isset($owner_team_id)) {
            $this->db->pushNotification(
                $owner_team_id,
                'Angebot für ' . $player['displayname'],
                'Es liegt ein Angebot für <b>' . $player['displayname'] . '</b> vor.<br>' .
                $team['team_name'] . ' bietet ' . $offer_value . ' <i class="fa-solid fa-peseta-sign"></i> für den Spieler.'
            );
        }
        return ['offer_id' => $offer_id];
    }

    protected function patch()
    {
        $now = date("Y-m-d H:i:s");
        $season_id = $this->db->getCurrentSeason($now)['season_id'];

        // CHECK OFFER ID
        $offer_id = $this->id;
        if (!isset($offer_id)) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [offer_id]'];
        }

        // CHECK OFFER
        $offer = $this->db->getOfferById($offer_id);
        if (!$offer) {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Es konnte kein Gebot mit folgender ID gefunden werden: [' . $offer_id . ']'];
        }

        $team = $this->db->getTeamById($offer['team_id']);
        // offers to not available players are possible now
        /*
        if ($team['manager_id'] != $_SERVER['manager_id']) {
            header('HTTP/1.1 403 Forbidden');
            return ['status' => 'Forbidden', 'message' => 'Es kann kein Gebot für ein fremdes Team abgegeben werden'];
        }*/

        if ($offer['status'] != 'pending') {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Ein altes Gebot kann nicht bearbeitet werden'];
        }

        if ($this->params['action'] === 'edit') {
            // CHECKOFFER VALUE
            if (!$this->params['offer_value']) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [offer_value]'];
            }
            $offer_value = $this->params['offer_value'];
            if (!is_numeric($offer_value)) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Ungültigen Wert übertragen', 'value' => $offer_value];
            }
            $offer_value = intval($offer_value);
            if ($offer_value < 0) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Ungültigen Wert übertragen', 'value' => $offer_value];
            }
            $prev_offer_value = $offer['offer_value'];
            $new_offer_value = $offer_value;
            $diff = $new_offer_value - $prev_offer_value;

            if ($diff == 0) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Keine Veränderung'];
            } else if ($diff > 0 && $diff > $team['budget']) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Nicht ausreichend genug Budget vorhanden'];
            }

            $offer['offer_value'] = $offer_value;
            $this->db->patchOffer($offer);
        } else if ($this->params['action'] === 'react') {
            $status = $this->params['status'];

            // CHECKOFFER VALUE
            if (!$status) {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [status]'];
            }

            if ($status == 'accepted') {
                // CHECKOFFER VALUE
                if (!$this->params['offer_value']) {
                    header('HTTP/1.1 400 Bad Request');
                    return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [offer_value]'];
                }
                $offer_value = $this->params['offer_value'];
                if (!is_numeric($offer_value)) {
                    header('HTTP/1.1 400 Bad Request');
                    return ['status' => 'Bad Request', 'message' => 'Ungültigen Wert übertragen', 'value' => $offer_value];
                }
                $offer_value = intval($offer_value);
                if ($offer_value < 0) {
                    header('HTTP/1.1 400 Bad Request');
                    return ['status' => 'Bad Request', 'message' => 'Ungültigen Wert übertragen', 'value' => $offer_value];
                }
                $offer_value = $this->params['offer_value'];

                $team = $this->db->getTeamById($offer['team_id']);
                if (!isset($team)) {
                    header('HTTP/1.1 404 Not Found');
                    return ['status' => 'Not Found', 'message' => 'Es konnte kein Team mit folgender ID gefunden werden: [' . $offer['team_id'] . ']'];
                }

                $owner_team = $this->db->getTeamById($offer['owner_team_id']);
                if (!isset($owner_team)) {
                    header('HTTP/1.1 404 Not Found');
                    return ['status' => 'Not Found', 'message' => 'Es konnte kein Team mit folgender ID gefunden werden: [' . $offer['owner_team_id'] . ']'];
                }

                // update offer object
                $offer['status'] = $status;
                $this->db->patchOffer($offer);

                // update team budgets
                $this->db->increaseTeamBudget($owner_team['team_id'], $offer_value);
                $this->db->reduceTeamBudget($team['team_id'], $offer_value);

                // add new player-in-team object
                $transferwindow = $this->db->getTransferwindowById($offer['transferwindow_id']);
                $this->db->postPlayerInTeam($offer['team_id'], $offer['player_id'], $transferwindow['matchday_number'], $offer['offer_id']);

                // add new sell object
                $sell_id = $this->db->addSell($offer['player_id'], $owner_team['team_id'], $transferwindow['transferwindow_id'], $offer_value);

                // update old player-in-team object
                $player_in_team = $this->db->getPlayerInTeamByPlayerAndSeason($offer['player_id'], $season_id)[0];
                $player_in_team['last_matchday'] = $transferwindow['matchday_number'];
                $player_in_team['sell_id'] = $sell_id;
                $this->db->patchPlayerInTeam($player_in_team);

                // push notification
                $player = $this->db->getPlayerById($offer['player_id']);
                $this->db->pushNotification(
                    $offer['team_id'],
                    'Angebot für ' . $player['displayname'] . 'akzeptiert',
                    'Dein Angebot für <b>' . $player['displayname'] . '</b> in Höhe von <b>' . $offer_value . '</b> <i class="fa-solid fa-peseta-sign"></i> wurde akzeptiert. Der Spieler ist ab sofort in deinem Team.'
                );

                return $offer;
            }

            if ($status == 'rejected') {
                $offer['status'] = $status;
                $this->db->patchOffer($offer);
                return $offer;
            }

        } else {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [action]'];
        }

        return $offer;
    }

    protected function delete()
    {
        $offer_id = $this->id;
        if (!isset($offer_id)) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [offer_id]'];
        }
        $offer = $this->db->getOfferById($offer_id);
        if (!$offer) {
            header('HTTP/1.1 404 Not Found');
            return ['status' => 'Not Found', 'message' => 'Es konnte kein Eintrag mit folgender ID gefunden werden: [' . $offer_id . ']'];
        }
        if ($offer['status'] != 'pending') {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Das Gebot kann nicht mehr gelöscht werden.'];
        }

        $team = $this->db->getTeamById($offer['team_id']);
        if ($team['manager_id'] != $_SERVER['manager_id']) {
            header('HTTP/1.1 405 Method Not Allowed');
            return ['status' => 'Method Not Allowed', 'message' => 'Keine Berechtigung dieses Gebot zu löschen.'];
        }

        $this->db->cancelOffer($offer_id);
        return true;

    }
}