<?php
require_once(__DIR__.'/../database/old.database.php');

class StatsController extends _BaseController{

	protected function get(){
    $stats_id = $this->id;

    if($stats_id == 'player_count') {
      $all_player_count = count($this->db->getPlayerList());
      $available_player_count = count($this->db->getAvailablePlayer());
      return ['all' => $all_player_count, 'available' => $available_player_count];
    }

    if($stats_id == 'height_distribution') {
      $height_distribution = $this->db->getPlayerHeightDistribution();
      return $this->fillZeroValues($height_distribution);
    }

    if($stats_id == 'weight_distribution') {
      $weight_distribution = $this->db->getPlayerWeightDistribution();
      return $this->fillZeroValues($weight_distribution);
    }

    if($stats_id == 'data_progress') {
      $now = date("Y-m-d H:i:s");
      $season_id = $this->db->getCurrentSeason($now)['season_id'];

      $result['total'] = count($this->db->getAllPlayer());
      $result['country'] = count($this->db->getPlayerWithCountry());
      $result['city'] = count($this->db->getPlayerWithCity());
      $result['dateOfBirth'] = count($this->db->getPlayerWithBirthday());
      $result['height'] = count($this->db->getPlayerWithHeight());
      $result['weight'] = count($this->db->getPlayerWithWeight());
      $result['hasPhoto'] = count($this->db->getPlayerWithPhoto($season_id));

      //$result['total_in_club'] = count($this->db->getAvailablePlayer());
      // $result['generated_in_club'] = count($this->db->getGeneratedPlayerInClub());
      return $result;
    }
/*
    if($stats_id == 'season_chart') {
      $old_db = OldDatabase::getInstance();
      $teamList = $old_db->getTeamsBySeason('f25cf1cb-43ee-4fab-8509-afe6fa4b4e5f');
      foreach($teamList as &$team) {
        $matchdayList = $old_db->getMatchdayByTeam($team['team_id']);
        $team['series'] = $matchdayList;
        foreach($team['series'] as $key=>&$matchday) {
          $matchday['name'] = intval($matchday['name']);
          $matchday['value'] = intval($matchday['value']);

          if($key > 0) {
            $matchday['value'] = $matchday['value'] + $team['series'][$key - 1]['value'];
          }
        }

        array_unshift($team['series'] , array('name' => 0, 'value' => 0));
      }
      return $teamList;
    }
*/
    if($stats_id == 'worldmap') {
      $clubs_by_country = $this->db->getClubCountByCountry();
      $max['clubs_by_country'] = $clubs_by_country[0]['count'];

      $player_by_country = $this->db->getPlayerCountByCountry();
      $max['player_by_country'] = $player_by_country[0]['count'];

      return ['max' => $max, 'clubs_by_country' => $clubs_by_country, 'player_by_country' => $player_by_country];
    }
    
    header('HTTP/1.1 400 Bad Request');
    return ['status' => 'Bad Request', 'message' => 'Unknown stats id ['.$stats_id.']'];
	}

  protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  private function fillZeroValues($array) {
		$min = intval($array[0]['name']);
		$max = intval(end($array)['name']);
		foreach(range($min, $max) as $index=>$i) {
			$element = $array[0];
			$element['name'] = ''.$i;
			$element['value'] = '0';

			$exists = false;
			foreach($array as $item) {
				if($item['name'] == $element['name']) {
					$exists = true;
				}
			}

			if(!$exists) {
				array_splice($array, $index, 0, array($element));
			}
		};
		return $array;
	}

}