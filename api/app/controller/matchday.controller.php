<?php
class MatchdayController extends _BaseController
{

  protected function get()
  {
    if ($this->params['action'] == 'market-team') {
      $matchday_number = $this->params['matchday_number'];
      $season_name = str_replace('-', '/', $this->params['season_name']);

      $season = $this->db->getSeasonByName($season_name);

      $all_player_list = $this->db->getBundesligaPlayerList();
      $player_list = array();
      foreach ($all_player_list as $player) {
        $player['team_list'] = $this->db->getPlayerInTeamByPlayerAndSeason($player['player_id'], $season['season_id']);
        $is_free = true;
        foreach ($player['team_list'] as &$team) {
          if ($team['first_matchday'] <= $matchday_number && (!$team['last_matchday'] || $team['last_matchday'] > $matchday_number)) {
            $is_free = false;
          }
        }

        unset($player['team_list']);
        $player['position'] = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], $season['season_id'])[0]['position'];
        $player['points'] = $this->db->getPlayerRatingByPlayerAndMatchday($player['player_id'], $matchday_number, $season['season_id'])[0]['points'];

        if ($is_free) {
          array_push($player_list, $player);
        }
      }

      usort($player_list, function ($a, $b) {
        if ($a['points'] > $b['points']) {
          return -1;
        } else {
          return 1;
        }
      });

      $formation['451'] = $this->getBestTeamByFormation($player_list, 4, 5, 1);
      $formation['442'] = $this->getBestTeamByFormation($player_list, 4, 4, 2);
      $formation['433'] = $this->getBestTeamByFormation($player_list, 4, 3, 3);
      $formation['343'] = $this->getBestTeamByFormation($player_list, 3, 4, 3);
      $formation['352'] = $this->getBestTeamByFormation($player_list, 3, 5, 2);

      usort($formation, function ($a, $b) {
        if ($a['points'] > $b['points']) {
          return -1;
        } else {
          return 1;
        }
      });

      return $formation[0];
    }


    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];


  }

  protected function post()
  {

    /*if ($_SERVER['role'] != 'admin') {
      header('HTTP/1.1 401 Unauthorized');
      return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung eine Spielerbewertung zu bearbeiten'];
    }*/

    $season_id = $_POST['season_id'];
    $matchday_number = $_POST['matchday_number'];

    $now = date("Y-m-d H:i:s");
    $status['season'] = $this->db->getCurrentSeason($now);
    $status['matchday'] = $this->db->getCurrentMatchday($now, $status['season']['season_id']);

    if ($matchday_number != $status['matchday']['number']) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Spieltag nicht aktuell'];
    }

    if ($now < $status['matchday']['result_date']) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Zu früh!'];
    }

    $team_list = $this->db->getTeamsBySeason($season_id);
    foreach ($team_list as &$team) {
      $team_rating = $this->db->getTeamRatingByTeamAndMatchday($team['team_id'], $matchday_number);

      if (!isset($team_rating['points'])) {
        $team['rating'] = $this->db->getTeamLineupResultByTeamAndMatchday($team['team_id'], $season_id, $matchday_number);
        $team['budget'] = intval($team['budget']) + 20000 * intval($team['rating']['points']);
        $team['rating']['missed_goals'] = $this->db->getMissedGoals($team['team_id'], $season_id, $matchday_number)['missed_goals'];

        $goalkeeper_list = $this->db->getPointsListByTeamAndMatchdayAndPosition($team['team_id'], $season_id, $matchday_number, 'GOALKEEPER');
        $defender_list = $this->db->getPointsListByTeamAndMatchdayAndPosition($team['team_id'], $season_id, $matchday_number, 'DEFENDER');
        $midfielder_list = $this->db->getPointsListByTeamAndMatchdayAndPosition($team['team_id'], $season_id, $matchday_number, 'MIDFIELDER');
        $forward_list = $this->db->getPointsListByTeamAndMatchdayAndPosition($team['team_id'], $season_id, $matchday_number, 'FORWARD');

        $team['rating']['formation']['343'] = $this->getBestByLimit($goalkeeper_list, 1) + $this->getBestByLimit($defender_list, 3) + $this->getBestByLimit($midfielder_list, 4) + $this->getBestByLimit($forward_list, 3);
        $team['rating']['formation']['352'] = $this->getBestByLimit($goalkeeper_list, 1) + $this->getBestByLimit($defender_list, 3) + $this->getBestByLimit($midfielder_list, 5) + $this->getBestByLimit($forward_list, 2);
        $team['rating']['formation']['433'] = $this->getBestByLimit($goalkeeper_list, 1) + $this->getBestByLimit($defender_list, 4) + $this->getBestByLimit($midfielder_list, 3) + $this->getBestByLimit($forward_list, 3);
        $team['rating']['formation']['442'] = $this->getBestByLimit($goalkeeper_list, 1) + $this->getBestByLimit($defender_list, 4) + $this->getBestByLimit($midfielder_list, 4) + $this->getBestByLimit($forward_list, 2);
        $team['rating']['formation']['451'] = $this->getBestByLimit($goalkeeper_list, 1) + $this->getBestByLimit($defender_list, 4) + $this->getBestByLimit($midfielder_list, 5) + $this->getBestByLimit($forward_list, 1);
        $team['rating']['max'] = max($team['rating']['formation']['343'], $team['rating']['formation']['352'], $team['rating']['formation']['433'], $team['rating']['formation']['442'], $team['rating']['formation']['451']);


        if ($team['rating']['invalid'] == 1) {
          $team['rating']['points'] = 0;
          $team['rating']['sds'] = 0;
          $team['rating']['goals'] = 0;
          $team['rating']['assists'] = 0;
          $team['rating']['clean_sheet'] = 0;
          $team['rating']['fine'] = 300;
        }

        $this->db->patchTeam($team);
        $this->db->patchTeamRating($team, $matchday_number);

        $reward = number_format(20000 * intval($team['rating']['points']), 0, '.', '.');
        $this->db->pushNotification(
          $team['manager_id'],
          $matchday_number . '. Spieltag abgeschlossen!',
          'Dein Team hat <b>' . $team['rating']['points'] . ' Punkte</b> erzielt. dafür bekommst du <b>' . $reward . '</b> <i class="fa-solid fa-peseta-sign"></i>.<br><br>
          Dein Team hat <b>' . $team['rating']['goals'] . ' Tore</b> erzielt und <b>' . $team['rating']['assists'] . ' Vorlagen</b> geliefert.<br>
          Mit einer optimalen Aufstellung hättest du <b>' . $team['rating']['max'] . ' Punkte</b> erzielt.<br><br>
          <a href="http://die-bestesten.de/liga/pro/2024-2025/' . $matchday_number . '">Zur Spieltagstabelle</a>'
        );
      } else {
        header('HTTP/1.1 400 Bad Request');
        return ['status' => 'Unauthorized', 'message' => 'Spieltag bereits abgeschlossen'];
      }
    }

    // return $team_list;

    $season_name = $this->db->getSeasonById($season_id)['season_name'];
    $rated_team_list = $this->db->getTeamRatingBySeasonAndMatchday($season_name, $matchday_number, $matchday_number);
    foreach ($rated_team_list as $team) {
      $this->db->setFine($team, $matchday_number);
    }

    return $rated_team_list;
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  private function getBestByLimit($points_list, $limit)
  {
    $list_length = count($points_list);
    if ($limit > $list_length) {
      return 0;
    }

    $points_list = array_slice($points_list, 0, $limit, false);
    $points = 0;
    foreach ($points_list as $e) {
      $points += $e['points'];
    }

    return $points;
  }

  private function getBestTeamByFormation($player_list, $defender_limit, $midfielder_limit, $forward_limit)
  {
    $name = $defender_limit . $midfielder_limit . $forward_limit;

    $team = ['GOALKEEPER' => [], 'DEFENDER' => [], 'MIDFIELDER' => [], 'FORWARD' => []];
    $points = 0;

    $goalkeeper = 0;
    $defender = 0;
    $midfielder = 0;
    $forward = 0;

    foreach ($player_list as $player) {
      if ($player['position'] == 'GOALKEEPER' && $goalkeeper < 1) {
        array_push($team['GOALKEEPER'], $player);
        $points += $player['points'];
        $goalkeeper++;
      }
      if ($player['position'] == 'DEFENDER' && $defender < $defender_limit) {
        array_push($team['DEFENDER'], $player);
        $points += $player['points'];
        $defender++;
      }
      if ($player['position'] == 'MIDFIELDER' && $midfielder < $midfielder_limit) {
        array_push($team['MIDFIELDER'], $player);
        $points += $player['points'];
        $midfielder++;
      }
      if ($player['position'] == 'FORWARD' && $forward < $forward_limit) {
        array_push($team['FORWARD'], $player);
        $points += $player['points'];
        $forward++;
      }
    }

    return ['name' => $name, 'points' => $points, 'team' => $team];
  }
}