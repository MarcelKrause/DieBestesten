<?php
class ActivityController extends _BaseController{

	protected function get(){
    $page = 1;
    if(isset($this->params['page'])) {
      $page = intval($this->params['page']);
    }
		if(isset($this->params['ref_id'])) {
			$ref_id = $this->params['ref_id'];
		}
		

		if(isset($this->params['ref_id'])) {
			$activity['total_elements'] = intval($this->db->getActivityCountByReference($ref_id)['count']);
		} else {
			$activity['total_elements'] = intval($this->db->getActivityCount()['count']);
		}

		$activity['max_pages'] = floor(1 + ($activity['total_elements'] / 10));
		if($page > $activity['max_pages']) {
			$page = $activity['max_pages'];
		}

		if($page == $activity['max_pages']) {
			$activity['next'] = null;
		} else {
			$activity['next'] = ($page + 1);
		}
		
		$activity['page'] = $page;
		if($page <= 1) {
			$activity['prev'] = null;
		} else {
			$activity['prev'] = ($page - 1);
		}

		if(isset($this->params['ref_id'])) {
			$activity['data'] = $this->db->getActivityByPageAndReference($page, $ref_id);
		} else {
			$activity['data'] = $this->db->getActivityByPage($page);
		}
		return $activity;
	
		
		foreach($activity['data'] as &$element) {
			$element['has_photo'] = boolval($element['has_photo']);
		}

		
		return $activity;
	}
	  
	protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }
}