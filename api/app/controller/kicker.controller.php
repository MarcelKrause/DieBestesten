<?php
class KickerController extends _BaseController
{

  protected function get()
  {
    $season_name = $this->params['season'];
    $matchday_number = $this->params['matchday'];

    if (!$season_name) {
      $now = date("Y-m-d H:i:s");
      $season = $this->db->getCurrentSeason($now);
      $matchday_number = $this->db->getCurrentMatchday($now, $season['season_id'])['number'];
    } else {
      $season = $this->db->getSeasonByName($season_name);
    }

    $club_list = $this->db->getBundesligaClubList();
    foreach ($club_list as &$club) {
      $club['players'] = $this->db->getPlayerRatingBySeasonAndMatchdayAndClub($season['season_id'], $matchday_number, $club['club_id']);
      unset($club['country_code']);
      unset($club['is_bundesliga']);
      unset($club['position']);
    }

    $data['season'] = $season['season_name'];
    $data['matchday'] = $matchday_number;
    $data['club_list'] = $club_list;

    return $data;

  }

  protected function post()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }
}