<?php
class SetupController extends _BaseController
{

  protected function get()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
  }

  protected function post()
  {
    $counter = 0;
    $now = date("Y-m-d H:i:s");
    $season = $this->db->getCurrentSeason($now);
    $player_list = $this->db->getPlayerList();

    foreach ($player_list as &$player) {
      $player['player_in_season'] = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], $season['season_id']);

      if (!$player['player_in_season']) {
        $this->db->postPlayerInSeason($player['player_id'], $season['season_id'], NULL, NULL);
        $counter++;
      }
    }
    return ['objects added' => $counter, 'season' => $season, 'player_list' => $player_list];
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];

    $now = date("Y-m-d H:i:s");
    $season_id = $this->db->getCurrentSeason($now)['season_id'];

    $team_list = $this->db->getTeamsBySeason($season_id);
    foreach ($team_list as &$team) {
      $team['budget'] = 50000000;
      $this->db->patchTeam($team);

      $offer_list = $this->db->getOfferListByTeam($team['team_id'], $season_id);
      foreach ($offer_list as $offer) {
        $this->db->deleteOffer($offer['offer_id']);
      }

      $player_in_team_list = $this->db->getPlayerInTeamByTeam($team['team_id']);
      foreach ($player_in_team_list as $player_in_team) {
        $this->db->deletePlayerInTeam($player_in_team['player_in_team_id']);
      }
    }

    $transferwindow_list = $this->db->getTransferwindowListBySeason($season_id);
    foreach ($transferwindow_list as $transferwindow) {
      $this->db->deleteTransferwindow($transferwindow['transferwindow_id']);
    }

    return $player_in_team_list;
  }


}