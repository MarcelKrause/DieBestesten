<?php
class TeamLineupController extends _BaseController
{

  protected function get()
  {
    $season_name = str_replace('-', '/', $this->params['season_name']);
    $team_name = str_replace('_', ' ', $this->params['team_name']);
    $matchday_number = $this->params['matchday_number'];

    if (!$season_name) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [season_name]'];
    }
    if (!$team_name) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [team_name]'];
    }
    if (!$matchday_number) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [matchday_number]'];
    }

    $now = date("Y-m-d H:i:s");
    $matchday = $this->db->getMatchdayBySeasonNameAndNumber($season_name, $matchday_number);
    $transferwindow = $this->db->getCurrentTransferwindow($now, $matchday['matchday_id']);

    if (!$matchday) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Spieltag existiert nicht'];
    }

    if ($transferwindow) {
      $data['phase'] = 'transfer_phase';
    } else {
      if ($now > $matchday['kickoff_date']) {
        $data['phase'] = 'matchday_phase';
      } else {
        $data['phase'] = 'lineup_phase';
      }
    }

    $team = $this->db->getTeamBySeasonAndName($season_name, $team_name);

    if ($data['phase'] == 'transfer_phase') {
      $data['lineup'] = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], $matchday_number - 1);
      $data['formation'] = $this->getFormationArray($data['lineup']);
      $data['is_valid'] = true;

      $squad = $this->db->getPlayerInTeamByMatchday($team['team_id'], $matchday_number, $team['season_id']);
      //$data['squad'] = $squad;
      foreach ($data['lineup'] as $key => &$player) {
        $player['points'] = null;
        $player['grade'] = null;
        $player['goals'] = null;
        $player['assists'] = null;
        $player['sds'] = null;
        $player['clean_sheet'] = null;

        if (!$this->isPlayerInSquad($player, $squad)) {
          unset($data['lineup'][$key]);
        }
      }
      $data['lineup'] = array_values($data['lineup']);

    } else if ($data['phase'] == 'lineup_phase') {
      $data['lineup'] = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], $matchday_number);
      $data['formation'] = $this->getFormationArray($data['lineup']);
      $data['is_valid'] = true;

      if (count($data['lineup']) == 0) {
        $data['is_valid'] = false;
        if ($matchday_number > 1) {
          $matchday = $matchday_number - 1;
        } else {
          $matchday = 1;
        }
        $data['matchday'] = $matchday;
        $data['lineup'] = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], $matchday);
        $data['formation'] = $this->getFormationArray($data['lineup']);

        $squad = $this->db->getPlayerInTeamByMatchday($team['team_id'], $matchday_number, $team['season_id']);

        foreach ($data['lineup'] as $key => &$player) {
          $player['points'] = null;
          $player['grade'] = null;
          $player['goals'] = null;
          $player['assists'] = null;
          $player['sds'] = null;
          $player['clean_sheet'] = null;

          if (!$this->isPlayerInSquad($player, $squad)) {
            unset($data['lineup'][$key]);
          }
        }
        $data['lineup'] = array_values($data['lineup']);

        foreach ($squad as &$squad_player) {
          if (!$this->isPlayerInSquad($squad_player, $data['lineup'])) {
            $squad_player['nominated'] = '0';
            array_push($data['lineup'], $squad_player);
          }
        }

        $new_formation = $this->getFormationArray($data['lineup']);
        $data['new_formation'] = $new_formation;
        if ($this->checkFormation($new_formation)) {
          $data['takeover'] = true;
          foreach ($data['lineup'] as $final_lineup_player) {
            $this->db->setTeamLineupByTeamAndMatchday($team['team_id'], $final_lineup_player, $matchday_number);
          }
          $data['is_valid'] = true;
          $data['lineup'] = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], $matchday_number);
        } else {
          $data['takeover_not_possible'] = true;
          unset($data['formation']);
          $data['lineup'] = $squad;
          $data['is_valid'] = false;
          foreach ($data['lineup'] as &$new_lineup_player) {
            $new_lineup_player['nominated'] = '0';
            $new_lineup_player['points'] = null;
            $new_lineup_player['grade'] = null;
            $new_lineup_player['goals'] = null;
            $new_lineup_player['assists'] = null;
            $new_lineup_player['sds'] = null;
            $new_lineup_player['clean_sheet'] = null;
          }
        }
      }
    } else if ($data['phase'] == 'matchday_phase') {
      $data['lineup'] = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], $matchday_number);
      $data['formation'] = $this->getFormationArray($data['lineup']);
      $data['is_valid'] = true;
    }

    return $data;

    /*
    if(count($lineup) == 0 && $matchday_number > 1) {
    $lineup = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], ($matchday_number - 1));
    $lineup_takeover = true;
    } 
    $formation = $this->getFormationArray($lineup);
    $is_valid_formation = $this->checkFormation($formation);
    $player_in_team_by_matchday = $this->db->getPlayerInTeamByMatchday($team['team_id'], $matchday_number, $team['season_id']);
    $is_valid_player_list = $this->checkPlayerList($lineup, $player_in_team_by_matchday);
    if($is_valid_formation && $is_valid_player_list) {
    $data['valid'] = true;
    $data['formation'] = $formation;
    
    if($lineup_takeover) {
    $data['lineup_takeover'] = true;
    foreach($lineup as $player) {
    if($player['nominated']) {
    // $this->db->setTeamLineupByTeamAndMatchday($team['team_id'], $player, $matchday_number);
    }
    }
    $lineup = $this->db->getTeamLineupByTeamAndMatchday($team['team_id'], $team['season_id'], $matchday_number);
    }
    
    $data['lineup'] = $lineup;
    } else {
    $data['valid'] = false;
    $data['formation'] = $formation;
    $data['is_valid_formation'] = $is_valid_formation;
    $data['is_valid_player_list'] = $is_valid_player_list;
    $data['lineup'] = $this->db->getPlayerInTeamByMatchday($team['team_id'], $matchday_number, $team['season_id']);
    foreach($data['lineup'] as &$player) {
    $player['nominated'] = '0';
    }
    if($lineup_takeover) {
    $data['takeover_not_possible'] = true;
    }
    }
    usort($data['lineup'], function ($a, $b) {
    if($a['nominated'] == $b['nominated']) {
    return $this->getPositionValue($a['position']) <=> $this->getPositionValue($b['position']);
    } else {
    return $b['nominated'] <=> $a['nominated']; 
    }
    });
    return $data;
    */
  }

  protected function post()
  {
    if (!isset($_POST['lineup'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [lineup]'];
    }
    if (!isset($_POST['team_id'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [team_id]'];
    }
    if (!isset($_POST['matchday_number'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [matchday_number]'];
    }

    $lineup = json_decode($_POST['lineup']);
    $team_id = $_POST['team_id'];
    $matchday_number = $_POST['matchday_number'];

    foreach ($lineup as &$player) {
      $player->team_lineup_id = $this->db->postTeamLineup($team_id, $matchday_number, $player->player_id, $player->nominated);
    }

    return $lineup;

  }

  protected function patch()
  {

    if (!isset($this->params['lineup'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [lineup]'];
    }

    $lineup = json_decode($this->params['lineup']);


    foreach ($lineup as $player) {
      $this->db->patchTeamLineup($player);
    }

    return $lineup;
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
  }

  function getPositionValue($position)
  {
    switch ($position) {
      case 'GOALKEEPER':
        return 1;
        break;
      case 'DEFENDER':
        return 2;
        break;
      case 'MIDFIELDER':
        return 3;
        break;
      case 'FORWARD':
        return 4;
        break;
      default:
        return 5;
    }
  }

  function getFormationArray($lineup)
  {
    $goalkeeper = count(array_filter($lineup, function ($player) {
      if ($player['position'] == 'GOALKEEPER' && $player['nominated'] == '1') {
        return true;
      }
    }));
    $defender = count(array_filter($lineup, function ($player) {
      if ($player['position'] == 'DEFENDER' && $player['nominated'] == '1') {
        return true;
      }
    }));
    $midfielder = count(array_filter($lineup, function ($player) {
      if ($player['position'] == 'MIDFIELDER' && $player['nominated'] == '1') {
        return true;
      }
    }));
    $forward = count(array_filter($lineup, function ($player) {
      if ($player['position'] == 'FORWARD' && $player['nominated'] == '1') {
        return true;
      }
    }));

    return [$goalkeeper, $defender, $midfielder, $forward];
  }

  function checkFormation($formation)
  {
    $valid_formations = [
      [1, 3, 4, 3],
      [1, 3, 5, 2],
      [1, 4, 3, 3],
      [1, 4, 4, 2],
      [1, 4, 5, 1],
      [1, 5, 3, 2],
      [1, 5, 4, 1],
    ];

    foreach ($valid_formations as $valid_formation) {
      if ($valid_formation == $formation) {
        return true;
      }
    }
    return false;
  }

  function checkPlayerList($lineup, $player_in_team_by_matchday)
  {
    foreach ($lineup as $lineup_player) {
      $exists = false;
      foreach ($player_in_team_by_matchday as $team_player) {
        if ($lineup_player['player_id'] == $team_player['player_id'] || $lineup_player['player_id']['nominated']) {
          $exists = true;
        }
      }
      if (!$exists) {
        return false;
      }
    }
    return true;
  }

  function isPlayerInSquad($player, $squad)
  {
    $is_in_squad = false;
    foreach ($squad as $squad_player) {
      if ($player['player_id'] == $squad_player['player_id']) {
        $is_in_squad = true;
      }
    }
    return $is_in_squad;
  }
}