<?php
class SeasonController extends _BaseController{

	protected function get(){
    $season_list = $this->db->getSeasonList();

    foreach($season_list as &$season) {
      if($season['start_date'] > '2017-01-01') {
        $season['matchday_available'] = true;
      }
    }
    return $season_list;
  }

  protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}
}
