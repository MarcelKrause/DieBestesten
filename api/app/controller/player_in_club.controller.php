<?php
class PlayerInClubController extends _BaseController{

	protected function get(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];			
	}
	  
	protected function post(){
		if($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 403 Forbidden');
    	return ['status' => 'Forbidden', 'message' => 'Keine Berechtigung einen Club für einen Spieler zu erstellen'];
		}

		if(!isset($_POST['player_id'])) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_id]'];
		}

		if(!isset($_POST['club_id'])) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [club_id]'];
		}

		if(!isset($_POST['from_date'])) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [from_date]'];
		}

		$player_in_club_id = $this->db->postPlayerInClub($_POST);
		if($player_in_club_id) {
			$player_in_club = $this->db->getPlayerInClubById($player_in_club_id);
			$this->db->postActivity('POST', $_POST['player_id'], 'player_in_club', null, 'spielerdatenbank', '['.$player_in_club['club_name'].'] als Club von ['.$player_in_club['displayname'].'] hinzugefügt');
			header('HTTP/1.1 201 Created');
			return $player_in_club;
		} else {
			header('HTTP/1.1 500 Internal Server Error');
    	return ['status' => 'Internal Server Error', 'message' => 'Aktion konnte serverseitig nicht durchgeführt werden'];
		}
	}
	  
	protected function patch(){
		if($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 401 Unauthorized');
    	return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung einen Club für einen Spieler zu bearbeiten'];
		}

		$player_in_club_id = $this->id;
		if(!isset($player_in_club_id)) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_in_club_id]'];
		}

		$player_in_club = $this->db->getPlayerInClubById($player_in_club_id);
		if(!$player_in_club) {
			header('HTTP/1.1 404 Not Found');
    	return ['status' => 'Not Found', 'message' => 'Es konnte kein Eintrag mit folgender ID gefunden werden: ['.$player_in_club_id.']'];
		}

		$patched_player_in_club = $player_in_club;
		$patched_player_in_club['from_date'] = $this->params['from_date'];
		$patched_player_in_club['to_date'] = $this->params['to_date'];
		$patched_player_in_club['is_loan'] = $this->params['is_loan'];

		$this->db->patchPlayerInClub($patched_player_in_club);

		$data_modified = false;
		if(strtotime($player_in_club['from_date']) != strtotime($this->params['from_date'])) {
			$data_modified = true;
			$this->db->postActivity('PATCH', $player_in_club['player_id'], 'player_in_club', 'from_date', 'spielerdatenbank', 'Startdatum von ['.$player_in_club['displayname'].'] bei ['.$player_in_club['club_name'].'] von ['.$player_in_club['from_date'].'] zu ['.$this->params['from_date'].'] geändert');
		}
		if(strtotime($player_in_club['to_date']) != strtotime($this->params['to_date'])) {
			$data_modified = true;
			$this->db->postActivity('PATCH', $player_in_club['player_id'], 'player_in_club', 'to_date', 'spielerdatenbank', 'Endzeitpunkt von ['.$player_in_club['displayname'].'] bei ['.$player_in_club['club_name'].'] von ['.$player_in_club['to_date'].'] zu ['.$this->params['to_date'].'] geändert');
		}
		if($player_in_club['is_loan'] != $this->params['is_loan']) {
			$data_modified = true;
			if(boolval($this->params['is_loan'])) {
				$this->db->postActivity('PATCH', $player_in_club['player_id'], 'player_in_club', 'is_loan', 'spielerdatenbank', 'Clubzuweisung von ['.$player_in_club['displayname'].'] bei ['.$player_in_club['club_name'].'] als Leihe markiert');
			}
			if(!boolval($this->params['is_loan'])) {
				$this->db->postActivity('PATCH', $player_in_club['player_id'], 'player_in_club', 'is_loan', 'spielerdatenbank', 'Leihmarkierung von ['.$player_in_club['displayname'].'] bei ['.$player_in_club['club_name'].'] entfernt');
			}
		}

		if(!$data_modified) {
			header('HTTP/1.1 406 Not Acceptable');
    	return ['status' => 'Not Acceptable', 'message' => 'Es wurden keine Änderungen an den Server geschickt'];
		} else {
			$player_in_club = $this->db->getPlayerInClubById($player_in_club_id);
			return $player_in_club;
		}
	}
	  
	protected function delete(){
		if($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 401 Unauthorized');
    	return ['status' => 'Unauthorized', 'message' => 'Keine Berechtigung einen Club für einen Spieler zu löschen'];
		}

		$player_in_club_id = $this->id;
		if(!isset($player_in_club_id)) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_in_club_id]'];
		}
		$player_in_club = $this->db->getPlayerInClubById($player_in_club_id);
		if(!$player_in_club) {
			header('HTTP/1.1 404 Not Found');
    	return ['status' => 'Not Found', 'message' => 'Es konnte kein Eintrag mit folgender ID gefunden werden: ['.$player_in_club_id.']'];
		}

		$this->db->deletePlayerInClub($player_in_club['player_in_club_id']);
		$deleted_player_in_club = $this->db->getPlayerInClubById($player_in_club_id);

		if($deleted_player_in_club) {
			header('HTTP/1.1 500 Internal Server Error');
    	return ['status' => 'Internal Server Error', 'message' => 'Aktion konnte serverseitig nicht durchgeführt werden', 'player_in_club' => $player_in_club];
		} else {
			$this->db->postActivity('DELETE', $player_in_club['player_id'], 'player_in_club', null, null, '['.$player_in_club['club_name'].'] als Club von ['.$player_in_club['displayname'].'] entfernt');
			return ['status' => 'OK', 'message' => 'PlayerInClub successfully deleted'];
		}
  }
}