<?php
class ClubController extends _BaseController{

	protected function get(){

		if($this->params['filter'] == 'bundesliga') {
			$club_list = $this->db->getBundesligaClubList();
		} else {
			$club_list = $this->db->getClubList();
		}
		
		return $club_list;
	}
	  
	protected function post(){
		if($_SERVER['role'] != 'admin' && $_SERVER['role'] != 'maintainer') {
			header('HTTP/1.1 403 Forbidden');
    	return ['status' => 'Forbidden', 'message' => 'Keine Berechtigung einen Spieler zu erstellen'];
		}

		if(!isset($_POST['country_code'])) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [country_code]'];
		}

		if(!isset($_POST['name'])) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [name]'];
		}

		$club = $this->db->getClubByName($_POST['name']);
		if($club) {
			header('HTTP/1.1 409 Conflict');
    	return ['status' => 'Not Found', 'message' => 'Es existiert bereits ein Club mit folgendem Namen: ['.$_POST['name'].']'];
		}

		$club_id = $this->db->postClub($_POST);

		if($club_id) {
			$club = $this->db->getClubById($club_id);
			$this->db->postActivity('POST', $club_id, 'club', null, 'club', 'Club ['.$club['name'].'] erstellt');
			header('HTTP/1.1 201 Created');
			return $club;
		} else {
			header('HTTP/1.1 500 Internal Server Error');
    	return ['status' => 'Internal Server Error', 'message' => 'Aktion konnte serverseitig nicht durchgeführt werden'];
		}
	}
	  
	protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }
}