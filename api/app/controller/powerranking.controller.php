<?php
class PowerrankingController extends _BaseController
{

  protected function get()
  {
    $now = date("Y-m-d H:i:s");
    $season_id = $this->db->getCurrentSeason($now)['season_id'];

    if (!isset($this->id)) {
      $powerranking = $this->db->getOwnPowerranking($season_id);

      if (!$powerranking) {
        $powerranking = $this->db->getTeamsBySeason($season_id);
      }
      return $powerranking;
    } else {
      if ($this->id == 'count') {
        $powerranking_count = intval($this->db->getPowerrankingCountBySeason($season_id)['count']);
        return $powerranking_count;
      }
      $manager_list = $this->db->getManagerList();
      foreach ($manager_list as &$manager) {
        $manager['powerranking'] = $this->db->getPowerrankingByManager($manager['manager_id'], $season_id);
      }

      return $manager_list;
    }

  }

  protected function post()
  {
    if (!isset($_POST['team_list'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [team_list]'];
    }
    $team_list = json_decode($_POST['team_list']);
    $now = date("Y-m-d H:i:s");
    $season_id = $this->db->getCurrentSeason($now)['season_id'];
    $powerranking = $this->db->getOwnPowerranking($season_id);

    $deadline = date("2024-08-23 20:30:00");

    if ($now > $deadline) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Zu spät'];
    }





    if (!$powerranking) {
      foreach ($team_list as $position => $team_id) {
        $this->db->createPowerranking($season_id, $team_id, ($position + 1));
      }
      return 'created';
    } else {
      foreach ($team_list as $position => $team_id) {
        $this->db->updatePowerranking($season_id, $team_id, ($position + 1));
      }
      return 'updated';
    }
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }
}