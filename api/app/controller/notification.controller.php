<?php
class NotificationController extends _BaseController
{
  protected function get()
  {
    $notification_list = $this->db->getNotificationList();
    return $notification_list;
  }

  protected function post()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET and PATCH Requests on this endpoint.'];
  }

  protected function patch()
  {
    $notification_id = $this->id;
    $action = $this->params['action'];

    try {
      if ($action == 'read') {
        $this->db->setNotificationToRead($notification_id);
      }
    } catch (Exception $e) {
      header('HTTP/1.1 500 Internal Server Error');
      return ['status' => 'Internal Server Error', 'message' => $e->getMessage()];
    }
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET and PATCH Requests on this endpoint.'];
  }
}