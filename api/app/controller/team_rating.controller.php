<?php
class TeamRatingController extends _BaseController{

	protected function get(){
    $season_name = str_replace('-', '/', $this->params['season_name']);

    if(!$season_name) {
			header('HTTP/1.1 400 Bad Request');
    	return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [season_name]'];
		}

    if($this->params['min_matchday_number']) {
      $min_matchday_number = $this->params['min_matchday_number'];
    } else {
      $min_matchday_number = 1;
    }

    if($this->params['max_matchday_number']) {
      $max_matchday_number = $this->params['max_matchday_number'];
    } else {
      $max_matchday_number = 34;
    }

    if($this->params['matchday_number']) {
      $matchday_number = $this->params['matchday_number'];
      $min_matchday_number = $this->params['matchday_number'];
      $max_matchday_number = $this->params['matchday_number'];

      $now = date("Y-m-d H:i:s");
		  $season = $this->db->getCurrentSeason($now);
		  $matchday = $this->db->getCurrentMatchday($now, $season['season_id']);

      if($matchday_number == $matchday['number'] && $season_name == $season['season_name'] && $now > $matchday['kickoff_date']) {
        $data['live'] = true;
      } else {
        $data['live'] = false;
      }
    }   



    $data['season'] = $this->db->getSeasonByName($season_name);
    if($min_matchday_number == $max_matchday_number) {
      $data['table'] = $this->db->getTeamRatingBySeasonAndMatchday($season_name, $min_matchday_number, $max_matchday_number);
    } else {
      $data['table'] = $this->db->getTeamRatingBySeasonAndMatchdayCompleteSeason($season_name, $min_matchday_number, $max_matchday_number);
    }

    if($data['live']) {
      
      foreach($data['table'] as &$team) {
        $live_data = $this->db->getLiveTeamRatingByTeamAndMatchday($team['team_id'], $matchday_number, $season['season_id']);


        $team['points'] = intval($live_data['points']);
        $team['sds'] = intval($live_data['sds']);
        $team['goals'] = intval($live_data['goals']);
        $team['assists'] = intval($live_data['assists']);
        $team['clean_sheet'] = intval($live_data['clean_sheet']);
        $team['player_count'] = intval($live_data['player_count']);
        $team['start_lineup'] = intval($live_data['start_lineup']);
        $team['substitution'] = intval($live_data['substitution']);

        if($team['sds'] > 0) {
          $data['live'] = false;
        }

        if($team['invalid'] == 1) {
          $team['points'] = 0;
        }
      }


    }

    usort($data['table'], function ($a, $b) {
      return $b['points'] <=> $a['points'];
    });

    foreach($data['table'] as &$team) {
      $team['points'] = intval($team['points']);
      $team['sds'] = intval($team['sds']);
      $team['goals'] = intval($team['goals']);
      $team['assists'] = intval($team['assists']);
      $team['clean_sheet'] = intval($team['clean_sheet']);
      $team['points_goalkeeper'] = intval($team['points_goalkeeper']);
      $team['points_defender'] = intval($team['points_defender']);
      $team['points_midfielder'] = intval($team['points_midfielder']);
      $team['points_forward'] = intval($team['points_forward']);
    }

    return $data;
  }

  protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}
}