<?php
class StatusController extends _BaseController
{

	protected function get()
	{
		$now = date("Y-m-d H:i:s");
		$status['season'] = $this->db->getCurrentSeason($now);
		$status['matchday'] = $this->db->getCurrentMatchday($now, $status['season']['season_id']);
		$status['transferwindow'] = $this->db->getCurrentTransferwindow($now, $status['matchday']['matchday_id']);
		$status['manager'] = $this->db->getManagerById($_SERVER['manager_id']);
		$status['team'] = $this->db->getTeamByManagerAndSeason($_SERVER['manager_id'], $status['season']['season_id']);
		$status['incoming_offers'] = $this->db->getIncomingOffersCount($status['team']['team_id'])['count'];
		$status['unread_notifications'] = $this->db->getUnreadNotificationsCount();

		if (!$status['team']) {
			$status['last_team'] = $this->db->getTeamsByManager($_SERVER['manager_id'])[0];
		}

		if ($this->params['device'] && $this->params['deviceType'] && $this->params['browser']) {
			$this->db->writeLog($this->params['device'], $this->params['deviceType'], $this->params['browser']);
		}


		return $status;
	}

	protected function post()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}

	protected function patch()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}

	protected function delete()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
}