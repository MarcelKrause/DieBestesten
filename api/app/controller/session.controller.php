<?php
class SessionController extends _BaseController{

	protected function get(){
    $method = $this->id;

    if($method == 'active') {
      $sessions = $this->db->getActiveSessions();
      foreach($sessions as &$session) {
        $session['diff'] = time() - $session['last_checkin_timestamp'];
        if($session['diff'] < 600) {
          $session['status'] = 'online';
        }
        else if($session['diff'] >= 600 && $session['diff'] < 3600) {
          $session['status'] = 'afk';
        }
        else if($session['diff'] >= 3600 && $session['diff'] < 86400) {
          $session['status'] = 'offline';
        }
        else if($session['diff'] >= 86400) {
          $session['status'] = 'away';
        }
      }
      return $sessions;
    }
    
    header('HTTP/1.1 400 Bad Request');
    return ['status' => 'Bad Request', 'message' => 'Unbekannte Anfrage: ['.$method.']'];
	}

  protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}

  protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
	}
}