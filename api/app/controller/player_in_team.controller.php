<?php
class PlayerInTeamController extends _BaseController
{

  protected function get()
  {
    $season_name = str_replace('-', '/', $this->params['season_name']);
    $team_name = str_replace('_', ' ', $this->params['team_name']);

    $team = $this->db->getTeamBySeasonAndName($season_name, $team_name);
    if (!$team) {
      header('HTTP/1.1 404 Not Found');
      return ['status' => 'Not Found', 'message' => 'Es konnte kein Team mit in der Saison [' . $season_name . '] mit dem Namen [' . $team_name . '] gefunden werden.'];
    } else {
      $now = date("Y-m-d H:i:s");
      $season_id = $this->db->getCurrentSeason($now)['season_id'];

      $data['squad']['current'] = $this->db->getCurrentPlayerInTeam($team['team_id'], $season_id);
      $data['squad']['former'] = $this->db->getFormerPlayerInTeam($team['team_id'], $season_id);

      usort($data['squad']['current'], function ($a, $b) {
        if ($a['position'] == $b['position']) {
          return intval($b['price']) <=> intval($a['price']);
        } else {
          return $this->getPositionValue($a['position']) <=> $this->getPositionValue($b['position']);
        }
      });
      usort($data['squad']['former'], function ($a, $b) {
        if ($a['position'] == $b['position']) {
          return intval($b['price']) <=> intval($a['price']);
        } else {
          return $this->getPositionValue($a['position']) <=> $this->getPositionValue($b['position']);
        }
      });
      return $data;
    }
  }

  protected function post()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

  protected function delete()
  {
    $player_in_team_id = $this->id;
    if (!isset($player_in_team_id)) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [player_in_team_id]'];
    }
    $player_in_team = $this->db->getPlayerInTeamById($player_in_team_id);
    if (!$player_in_team) {
      header('HTTP/1.1 404 Not Found');
      return ['status' => 'Not Found', 'message' => 'Es konnte kein Eintrag mit folgender ID gefunden werden: [' . $player_in_team_id . ']'];
    }
    if ($player_in_team['last_matchday']) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Der Spieler ist bereits verkauft worden'];
    }

    $team = $this->db->getTeamById($player_in_team['team_id']);
    if ($team['manager_id'] != $_SERVER['manager_id']) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Der Spieler befindet sich nicht in einem deiner Teams'];
    }

    $now = date("Y-m-d H:i:s");
    $season_id = $this->db->getCurrentSeason($now)['season_id'];
    if ($team['season_id'] != $season_id) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Der Spieler befindet sich in einem deiner alten Teams'];
    }

    $matchday = $this->db->getCurrentMatchday($now, $season_id);
    $transferwindow = $this->db->getCurrentTransferwindow($now, $matchday['matchday_id']);
    if (!$transferwindow) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Spieler dürfen nur während einer Transferphase verkauft werden'];
    }

    $player = $this->db->getPlayerById($player_in_team['player_id']);
    $player['season_list'] = $this->db->getPlayerInSeasonByPlayerAndSeason($player['player_id'], $season_id);
    $player['season_list'][0]['points'] = $this->db->getPlayerRatingSummaryByPlayerAndSeason($player['player_id'], $season_id)['points'];
    if (!$player['season_list'][0]['points']) {
      $player['season_list'][0]['points'] = 0;
    }

    $new_budget = intval($team['budget']);
    $new_budget += intval($player['season_list'][0]['price']) + (20000 * $player['season_list'][0]['points']);

    $team['budget'] = $new_budget;
    $this->db->patchTeam($team);

    $price = $player['season_list'][0]['price'] + 20000 * $player['season_list'][0]['points'];

    // get transferwindow_id
    $now = date("Y-m-d H:i:s");
    $status['season'] = $this->db->getCurrentSeason($now);
    $status['matchday'] = $this->db->getCurrentMatchday($now, $status['season']['season_id']);
    $transferwindow = $this->db->getCurrentTransferwindow($now, $status['matchday']['matchday_id']);
    $sell_id = $this->db->addSell($player['player_id'], $team['team_id'], $transferwindow['transferwindow_id'], $price);

    $matchday = $this->db->getCurrentMatchday($now, $season_id);
    $player_in_team['last_matchday'] = $matchday['number'];
    $player_in_team['sell_id'] = $sell_id;
    $this->db->patchPlayerInTeam($player_in_team);

    return $player_in_team;
  }

  function getPositionValue($position)
  {
    switch ($position) {
      case 'GOALKEEPER':
        return 1;
        break;
      case 'DEFENDER':
        return 2;
        break;
      case 'MIDFIELDER':
        return 3;
        break;
      case 'FORWARD':
        return 4;
        break;
      default:
        return 5;
    }
  }
}
