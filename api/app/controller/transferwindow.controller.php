<?php
class TransferwindowController extends _BaseController
{

	protected function get()
	{
		if ($this->params['season_id']) {
			$season_id = $this->params['season_id'];
		} else {
			$now = date("Y-m-d H:i:s");
			$season_id = $this->db->getCurrentSeason($now)['season_id'];
		}

		$now = date("Y-m-d H:i:s");
		$transferwindow_list_raw = $this->db->getTransferwindowListBySeason($season_id);

		foreach ($transferwindow_list_raw as &$transferwindow) {
			$transferwindow['now'] = $now;
			if (strtotime($now) > strtotime($transferwindow['end_date'])) {
				$transferwindow['status'] = 'closed';
				$transferwindow['message'] = 'Geschlossen';
			} else if (strtotime($now) > strtotime($transferwindow['start_date']) && strtotime($now) < strtotime($transferwindow['end_date'])) {
				$transferwindow['status'] = 'open';
				$transferwindow['message'] = 'Offen';
			} else if (strtotime($now) < strtotime($transferwindow['start_date'])) {
				$transferwindow['status'] = 'coming';
				$transferwindow['message'] = 'Bald';
			}
		}
		/*
													$transferwindow_list_past = array_filter(
														$transferwindow_list_raw,
														function ($transferwindow) {
															return new DateTime($transferwindow['start_date']) < new DateTime($now);
														}
													);

													$transferwindow_list_future = array_filter(
														$transferwindow_list_raw,
														function ($transferwindow) {
															return new DateTime($transferwindow['start_date']) > new DateTime($now);
														}
													);

													$transferwindow_list_future = array_values($transferwindow_list_future);
													$transferwindow_list_future = array_slice($transferwindow_list_future, count($transferwindow_list_future) - 2);

													$transferwindow_list_filtered = array_merge([], $transferwindow_list_future, $transferwindow_list_past);
													$transferwindow_list_filtered = array_values($transferwindow_list_filtered);
											*/
		return $transferwindow_list_raw;
	}

	protected function post()
	{
		if (!isset($_POST['start_date'])) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [start_date]'];
		}

		if (!isset($_POST['end_date'])) {
			header('HTTP/1.1 400 Bad Request');
			return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [end_date]'];
		}

		$matchday_id = 'c5b02a55-1fa9-48be-83fc-07721f808b62';
		$start_date = date("Y-m-d H:i:s", strtotime($_POST['start_date']));
		$end_date = date("Y-m-d H:i:s", strtotime($_POST['end_date']));

		$input = [$matchday_id, $start_date, $end_date];

		$transferwindow_id = $this->db->postTransferwindow($matchday_id, $start_date, $end_date);

		$transferwindow = $this->db->getTransferwindowById($transferwindow_id);

		return ['input' => $input, 'database' => $transferwindow];
		return $transferwindow_id;
	}

	protected function patch()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}

	protected function delete()
	{
		header('HTTP/1.1 405 Method Not Allowed');
		return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
}