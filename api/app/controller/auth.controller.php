<?php
use \Firebase\JWT\JWT;

class AuthController extends _BaseController{

    protected function get(){
        header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only POST Requests on this endpoint.'];
    }
    	  
	protected function post(){
        $manager_name = $_POST['name'];
        $password = $_POST['password'];

        if(!isset($manager_name) || !isset($password)) {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Name oder Password fehlen'];
        }

        $manager = $this->db->getAuthManagerByName($manager_name);
        if($manager) {
            if($manager['status'] == 'blocked') {
                header('HTTP/1.1 403 Forbidden');
                return ['status' => 'Forbidden', 'message' => 'Account wurde deaktiviert'];
            }
            else {
                if(password_verify($password, $manager['password'])) {

                    $sub = $manager['manager_id'];
                    $iat = time();
                    $exp = time() + (60 * 60 * 24 * 7);
                    $session_id = $this->db->postSession($sub, $iat, $exp);

                    $key = "quz7m53sXK0H6VtzfGGKOHjQk6FtMqYZ";
                    $token = array(
                        'sid' => $session_id,
                        'sub' => $sub,
                        'manager_name' => $manager['manager_name'],
                        'role' => $manager['role'],
                        'status' => $manager['status'],
                        'has_photo' => boolval($manager['has_photo'])
                    );
                    $jwt = JWT::encode($token, $key);
                    return ['token' => $jwt, 'message' => 'Erfolgreich angemeldet'];
                } else {
                    header('HTTP/1.1 400 Bad Request');
                    return ['status' => 'Bad Request', 'message' => 'Name oder Passwort inkorrekt'];
                }
            }            
        } else {
            header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Name oder Passwort inkorrekt'];
        }
	}
	  
	protected function patch(){
        if(!$this->params['password']) {
			header('HTTP/1.1 400 Bad Request');
            return ['status' => 'Bad Request', 'message' => 'Password fehlt'];
		}
        $password = $this->params['password'];

        $token = $_SERVER['HTTP_AUTHORIZATION'];
        $token = substr($token, 7);
        $key = 'quz7m53sXK0H6VtzfGGKOHjQk6FtMqYZ';
        try {
            $decoded = JWT::decode($token, $key, array('HS256'));
        
            $manager = $this->db->getAuthManagerById($decoded->sub);
            if($manager) {
                if($manager['status'] == 'pending') {
                    $hashed_password = password_hash($password, PASSWORD_BCRYPT);
                    $this->db->updatePassword($manager['manager_id'], $hashed_password);

                    $key = "quz7m53sXK0H6VtzfGGKOHjQk6FtMqYZ";
                    $token = array(
                        'exp' => time() + (24 * 60 * 60),
                        'sub' => $manager['manager_id'],
                        'manager_name' => $manager['manager_name'],
                        'role' => $manager['role'],
                        'status' => $manager['status'],
                        'has_photo' => boolval($manager['has_photo']),
                    );
                    $jwt = JWT::encode($token, $key);

                    return ['token' => $jwt, 'message' => 'Passwort erfolgreich gesetzt'];
                } else {
                    header('HTTP/1.1 400 Bad Request');
                    return ['status' => 'Bad Request', 'message' => 'Password wude bereits gesetzt'];
                }
            } else {
                header('HTTP/1.1 400 Bad Request');
                return ['status' => 'Bad Request', 'message' => 'Token ist nicht lesbar'];
            }
        } catch (Exception $e) {
            header('HTTP/1.1 400 Bad Request');
            return ['message' => 'Token cannot be decoded', 'error' => $e];
        }
	}
	  
	protected function delete(){
        $session = $this->db->getSessionById($_SERVER['session_id']);
        if($session['status'] == 'inactive') {
            header('HTTP/1.1 406 Not Acceptable');
            return ['status' => 'Not Acceptable',  'message' => 'Session ist bereits beendet'];
        }
        $session_id = $this->db->deactivateSession($_SERVER['session_id']);
        $session = $this->db->getSessionById($session_id);
        return $session;
  	}
}