<?php
class TeamController extends _BaseController
{

  protected function get()
  {
    $team_id = $this->id;
    if ($team_id) {
      $team = $this->db->getTeamById($team_id);
      return $team;
    } else {
      $season_id = $this->params['season_id'];
      $season_name = str_replace('-', '/', $this->params['season_name']);
      $team_name = str_replace('_', ' ', $this->params['team_name']);

      if ($season_id) {
        $team_list = $this->db->getTeamsBySeason($season_id);

        $is_sds_set = false;
        foreach ($team_list as &$team) {
          $squad = $this->db->getCurrentPlayerInTeam($team['team_id'], $season_id);
          $team['squad_count'] = $this->getSquadCount($squad);
          $team['is_valid'] = $this->isValidSquad($team['squad_count']);
          $team['total_price'] = array_reduce(
            $squad,
            function ($sum, $player) {
              $sum += intval($player['price']) + 20000 * intval($player['points']);
              return $sum;
            },
            0
          );
          $team['ratings'] = $this->db->getTeamRatingByTeamId($team['team_id']);
          $team['squad'] = $squad;

          $now = date("Y-m-d H:i:s");
          $season = $this->db->getCurrentSeason($now);
          $current_matchday = $this->db->getCurrentMatchday($now, $season['season_id']);

          if ($now > $current_matchday['kickoff_date']) {
            $team['ratings'][intval($current_matchday['number']) - 1]['live'] = true;
            $live_data = $this->db->getLiveTeamRatingByTeamAndMatchday($team['team_id'], ($current_matchday['number']), $season['season_id']);
            $team['ratings'][intval($current_matchday['number']) - 1]['goals'] = $live_data['goals'];
            $team['ratings'][intval($current_matchday['number']) - 1]['points'] = $live_data['points'];
            if (intval($live_data['sds']) > 0) {
              $is_sds_set = true;
            }
          }

          unset($team['budget']);
        }

        if ($is_sds_set) {
          foreach ($team_list as &$team) {
            $now = date("Y-m-d H:i:s");
            $season = $this->db->getCurrentSeason($now);
            $current_matchday = $this->db->getCurrentMatchday($now, $season['season_id']);
            $team['ratings'][intval($current_matchday['number']) - 1]['live'] = false;
          }
        }

        usort($team_list, function ($a, $b) {
          if ($a['total_price'] == $b['total_price']) {
            return $b['is_valid'] <=> $a['is_valid'];
          } else {
            return $b['total_price'] <=> $a['total_price'];
          }
        });

        return $team_list;
      }

      if ($season_name && $team_name) {
        $data['team'] = $this->db->getTeamBySeasonAndName($season_name, $team_name);

        if (!$data['team']) {
          header('HTTP/1.1 404 Not Found');
          return ['status' => 'Not Found', 'message' => 'Es konnte kein Team mit in der Saison [' . $season_name . '] mit dem Namen [' . $team_name . '] gefunden werden.'];
        } else {
          $data['team']['position'] = $this->getSeasonPosition($data['team']['team_id'], $data['team']['season_id']);
          $data['manager'] = $this->db->getManagerByTeam($data['team']['team_id']);
          $data['team']['honour_list'] = $this->db->getHonoursByTeam($data['team']['team_id']);
          return $data;
        }
      }

      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [season_id] oder [season_name] && [team_name]'];
    }
  }

  protected function post()
  {
    if (!isset($_POST['team_name'])) {
      header('HTTP/1.1 400 Bad Request');
      return ['status' => 'Bad Request', 'message' => 'Notwendiges Attribut fehlt: [team_name]'];
    }

    $now = date("Y-m-d H:i:s");
    $season_id = $this->db->getCurrentSeason($now)['season_id'];
    $manager_id = $_SERVER['manager_id'];
    $team_name = $_POST['team_name'];
    $budget = 50000000;

    $team = $this->db->getTeamByManagerAndSeason($manager_id, $season_id);
    if ($team) {
      header('HTTP/1.1 409 Conflict');
      return ['status' => 'Not Found', 'message' => 'Es existiert bereits ein Team für den Manager in der angegeben Saison'];
    }

    $team_id = $this->db->postTeam($manager_id, $season_id, $team_name, $budget);

    for ($i = 1; $i <= 34; $i++) {
      $team_rating_id = $this->db->postTeamRating($team_id, $i);
    }

    $team = $this->db->getTeamById($team_id);
    return $team;
  }

  protected function patch()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
  }

  protected function delete()
  {
    header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Method not allowed on this endpoint.'];
  }

  private function getSquadCount($squad)
  {
    $goalkeeper = count(array_filter($squad, function ($player) {
      if ($player['position'] == 'GOALKEEPER') {
        return true;
      }
    }));
    $defender = count(array_filter($squad, function ($player) {
      if ($player['position'] == 'DEFENDER') {
        return true;
      }
    }));
    $midfielder = count(array_filter($squad, function ($player) {
      if ($player['position'] == 'MIDFIELDER') {
        return true;
      }
    }));
    $forward = count(array_filter($squad, function ($player) {
      if ($player['position'] == 'FORWARD') {
        return true;
      }
    }));
    $total = $goalkeeper + $defender + $midfielder + $forward;

    return ['total' => $total, 'goalkeeper' => $goalkeeper, 'defender' => $defender, 'midfielder' => $midfielder, 'forward' => $forward];
  }

  private function isValidSquad($squad_count)
  {
    if (
      $squad_count['goalkeeper'] >= 1 && $squad_count['goalkeeper'] <= 2 &&
      $squad_count['defender'] >= 5 && $squad_count['defender'] <= 6 &&
      $squad_count['midfielder'] >= 5 && $squad_count['midfielder'] <= 6 &&
      $squad_count['forward'] >= 3 && $squad_count['forward'] <= 4
    ) {
      return true;
    } else {
      return false;
    }
  }

  private function getSeasonPosition($team_id, $season_id)
  {
    $season_table = $this->db->getTeamRatingBySeasonId($season_id);

    $position = 1;
    foreach ($season_table as $team) {
      if ($team['team_id'] == $team_id) {
        return $position;
      } else {
        $position++;
      }
    }
  }
}