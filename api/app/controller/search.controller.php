<?php
class SearchController extends _BaseController{

	protected function get(){
    $request = $this->id;

    if(strlen($request) < 3 ) {
      return ['success' => false, 'message' => 'Request should be bigger than 2 characters'];
    } else {

      $now = date("Y-m-d H:i:s");
		  $season_id = $this->db->getCurrentSeason($now)['season_id'];

      if($this->params['filter'] == 'bundesliga') {
        $club = $this->db->searchBundesligaClub($request);
        $player = $this->db->searchBundesligaPlayer($request, $season_id);
      } else {
        $club = $this->db->searchClub($request);
        $player = $this->db->searchPlayer($request, $season_id);
      }

      $data = [
        'success' => true,
        'result' => [
          'manager' => $this->db->searchManager($request),
          'team' => $this->db->searchTeam($request),
          'club' => $club,
          'player' => $player
        ]
      ];
        
      return $data;
    }
	}
	  
	protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
        return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  	}
}