<?php
class ManagerController extends _BaseController{

	protected function get(){

		
		if(isset($this->params['manager_name'])) {
			$manager_name = str_replace('-', '/', $this->params['manager_name']);
			$data['manager'] = $this->db->getManagerByName($manager_name);

			if(!$data['manager']) {
				header('HTTP/1.1 404 Not Found');
				return ['status' => 'Not Found', 'message' => 'Es konnte kein Manager mit folgendem Namen gefunden werden: ['.$manager_name.']'];
			} else {
				$team_list = $this->db->getTeamsByManager($data['manager']['manager_id']);
				$data['manager']['points'] = $this->sumPoints($team_list);
				$data['manager']['position'] = $this->getAlltimePosition($data['manager']['manager_id']);
				$data['manager']['team_list'] = $team_list;
				foreach($data['manager']['team_list'] as &$team) {
					$team['honour_list'] = $this->db->getHonoursByTeam($team['team_id']);
				}
				$data['honour_list'] = $this->db->getHonoursByManager($data['manager']['manager_id']);
				return $data;
			}

		} else {
			$manager_list = $this->db->getManagerList();
			foreach($manager_list as &$manager) {
				$total_points = 0;
				$manager['team_list'] = $this->db->getTeamsByManager($manager['manager_id']);
				foreach($manager['team_list'] as &$team) {
					$team['honour_list'] = $this->db->getHonoursByTeam($team['team_id']);
					$total_points += $team['points'];
				}
				$manager['points'] = $total_points;
			}

			usort($manager_list, function ($a, $b) {
				return $b['points'] <=> $a['points'];
			});

			return $manager_list;
		}
    
	}
	  
	protected function post(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function patch(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
	}
	  
	protected function delete(){
		header('HTTP/1.1 405 Method Not Allowed');
    return ['status' => 'Method Not Allowed', 'message' => 'Only GET Requests on this endpoint.'];
  }

	private function sumPoints($team_list) {
		$points = 0;
		foreach($team_list as $team) {
			$points += $team['points'];
		}
		return $points;
	}

	private function getAlltimePosition($manager_id) {
		$manager_list = $this->db->getManagerList();
		foreach($manager_list as &$manager) {
			$total_points = 0;
			$manager['team_list'] = $this->db->getTeamsByManager($manager['manager_id']);
			foreach($manager['team_list'] as &$team) {
				$total_points += $team['points'];
			}
			$manager['points'] = $total_points;
		}

		usort($manager_list, function ($a, $b) {
			return $b['points'] <=> $a['points'];
		});

		$position = 1;
		foreach($manager_list as $manager) {
			if($manager['manager_id'] == $manager_id) {
				return $position;
			}
			else {
				$position++;
			}
		}
	}
}