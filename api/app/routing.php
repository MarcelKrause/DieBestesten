<?php
class Route
{
	private $_name;
	private $_class;

	public function __construct($name, $class)
	{
		$this->_name = $name;
		$this->_class = $class;
	}

	public function getName()
	{
		return $this->_name;
	}

	public function getClass()
	{
		return $this->_class;
	}
}

class Routing
{
	private $_routes = array();

	function __construct()
	{
		$routes = array(
			new Route('auth', 'Auth'),
			new Route('status', 'Status'),
			new Route('stats', 'Stats'),
			new Route('search', 'Search'),
			new Route('player', 'Player'),
			new Route('player_in_club', 'PlayerInClub'),
			new Route('player_in_season', 'PlayerInSeason'),
			new Route('player_in_team', 'PlayerInTeam'),
			new Route('player_rating', 'PlayerRating'),
			new Route('player_rating_check', 'PlayerRatingCheck'),
			new Route('country', 'Country'),
			new Route('activity', 'Activity'),
			new Route('session', 'Session'),
			new Route('season', 'Season'),
			new Route('club', 'Club'),
			new Route('manager', 'Manager'),
			new Route('matchday', 'Matchday'),
			new Route('team', 'Team'),
			new Route('team_rating', 'TeamRating'),
			new Route('team_lineup', 'TeamLineup'),
			new Route('prodcast', 'Prodcast'),
			new Route('setup', 'Setup'),
			new Route('offer', 'Offer'),
			new Route('transferwindow', 'Transferwindow'),
			new Route('kicker', 'Kicker'),
			new Route('powerranking', 'Powerranking'),
			new Route('notification', 'Notification')
		);

		$this->_routes = $routes;
	}

	function getRoutes()
	{
		return $this->_routes;
	}

	function navigate($request)
	{
		if ($this->isValid($request['endpoint'])) {
			$route = $this->getRoute($request['endpoint']);
		} else {
			$route = $this->getRoute('error');
		}

		$controllerName = $route->getClass() . 'Controller';
		$controller = new $controllerName;
		return $controller;
	}

	function isValid($endpoint)
	{
		foreach ($this->_routes as $route) {
			if ($route->getName() == $endpoint) {
				return true;
			}
		}
		return false;
	}

	function getRoute($endpoint)
	{
		foreach ($this->_routes as $route) {
			if ($route->getName() == $endpoint) {
				return $route;
			}
		}
	}
}
?>