<?php
#echo phpversion();
#error_reporting(E_ALL);
#ini_set('display_errors', 'On');


require_once("app/guard.php");
require_once("app/routing.php");

$allowedOrigins = [
	'http://die-bestesten.de',
	'http://data.die-bestesten.de',
	'https://beta.die-bestesten.de',
	'http://localhost:4200',
	'http://192.168.178.22:4200',
];

$origin = $_SERVER['HTTP_ORIGIN'];
if (in_array($origin, $allowedOrigins)) {
	header('Access-Control-Allow-Origin: ' . $origin);
}

header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Access-Control-Request-Method, Access-Control-Request-Headers, Origin, Accept, X-Requested-With, Content-Type, Authorization, X-EMAIL, X-PASSWORD');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: GET, POST, PUT, OPTIONS, PATCH, DELETE');
header('Content-Type: application/json');

use \Firebase\JWT\JWT;

require __DIR__ . '/vendor/autoload.php';
require_once(__DIR__ . '/app/database/base.database.php');

# build the parameter
$url = urldecode($_SERVER['REQUEST_URI']);
list($request, $params) = explode('?', $url);
list($uri, $endpoint, $id) = explode('/', $request);

$request = [
	"endpoint" => $endpoint,
	"id" => $id,
	"params" => $params,
];

$guard = new Guard();
$authorized = $guard->authorize($request);

if ($authorized['status']) {
	foreach (glob("app/controller/*.controller.php") as $filename) {
		include $filename;
	}

	# create the routing and viewController
	$routing = new Routing();
	$controller = $routing->navigate($request);
	$controller->setRequest($request);

	# return the response
	echo json_encode($controller->getResponse());
} else {
	if ($_SERVER['REQUEST_METHOD'] != 'OPTIONS') {
		header('HTTP/1.1 401 Unauthorized');
	}
	//	header('HTTP/1.1 401 Unauthorized');
	echo json_encode($authorized);
}

?>