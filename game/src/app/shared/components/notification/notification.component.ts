import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'db-notification',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent {
  constructor(public notification_service: NotificationService) {}

  public onClose(): void {
    this.notification_service.remove();
  }
}
