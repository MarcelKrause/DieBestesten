import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api/api.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';
import { StateService } from 'src/app/core/services/state/state.service';

@Component({
  selector: 'db-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent {
  menu = [
    {
      icon: 'fi-br-trophy',
      label: 'Liga',
      link: 'liga',
      is_expanded: true,
      subitems: [
        { icon: '⚽', label: 'Spieltag', link: '/pro/' + this.getCurrentSeason() + '/' + this.getCurrentMatchdayNumber() },
        { icon: '🏆', label: 'Saison', link: '/pro/' + this.getCurrentSeason() },
        /*{ icon: '⚔️', label: 'Pokal', link: '/h2h' },*/
        { icon: '🏟️', label: 'Teamliste', link: '/teams' },
        { icon: '🎰', label: 'Powerranking', link: '/powerranking' },
        /*{ icon: '🙏', label: 'Steile Thesen', link: '/steile-thesen' },*/
        { icon: '🎖️', label: 'Ewige Tabelle', link: '/ewige-tabelle' },
      ],
    },
    {
      icon: 'fi-rr-users-alt',
      label: 'Team',
      link: 'team',
      is_expanded: true,
      subitems: [
        {
          icon: '🧮',
          label: 'Aufstellung',
          link: '/' + this.getCurrentSeason() + '/' + this.getTeamName() + '/aufstellung/' + this.getCurrentMatchdayNumber(),
        },
        { icon: '📒', label: 'Kader', link: '/' + this.getCurrentSeason() + '/' + this.getTeamName() + '/kader' },
        /*{ icon: '🥇', label: 'Ergebnisse', link: '/' + this.getCurrentSeason() + '/' + this.getTeamName() + '/ergebnisse' },*/
        /*{ icon: '🔀', label: 'Transfers', link: '/' + this.getCurrentSeason() + '/' + this.getTeamName() + '/transfers' },*/
        /*{ icon: '📈', label: 'Form', link: '/' + this.getCurrentSeason() + '/' + this.getTeamName() + '/form' },*/
      ],
    },
    {
      icon: 'fi-br-shop',
      label: 'Markt',
      link: 'spieler',
      is_expanded: true,
      subitems: [
        { icon: '👥', label: 'Spielerliste', link: '/' },
        { icon: '🛒', label: 'Transfermarkt', link: '/transfermarkt' },
        { icon: '🕰️', label: 'Transferphasen', link: '/transfermarkt/phasen' },
        { icon: '💰', label: 'Gebote', link: '/transfermarkt/gebote' },
        { icon: '💱', label: 'Angebote', link: '/transfermarkt/angebote' },
        /*{ icon: '💵', label: 'Finanzen', link: '/saison' },*/
      ],
    },
  ];

  search_input: string = '';
  search_result_visible: boolean = false;
  search_result: { club: any[]; manager: any[]; player: any[]; team: any[] } = { club: [], manager: [], player: [], team: [] };
  logout_activated: boolean = false;

  constructor(
    public api_service: ApiService,
    public auth_service: AuthService,
    public state_service: StateService,
    private router: Router,
    private notification_service: NotificationService
  ) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.onNavigate();
      }
    });
  }

  public onExpandItem(label: string) {
    const item = this.menu.find((menu_item) => menu_item.label === label);
    item.is_expanded = !item.is_expanded;
  }

  public formatSeason(season: string): string {
    if (season) {
      return season.replace('/', '-');
    }
  }

  public formatTeam(team: string): string {
    if (team) {
      return team.replace(' ', '_');
    }
  }

  public onNavigate(): void {
    this.state_service.setMenu(false);
    this.search_result_visible = false;
  }

  public isSelectedRoute(route: string): boolean {
    return route.toLowerCase() === this.router.url.split('/')[1].toLowerCase();
  }

  public getTeamName(): string {
    if (!this.api_service.status) {
      return;
    }
    return this.formatTeam(this.api_service.status.team.team_name);
  }

  public getCurrentSeason(): string {
    if (!this.api_service.status) {
      return;
    }
    return this.formatSeason(this.api_service.status.season.season_name);
  }

  public getCurrentMatchdayNumber(): number {
    if (!this.api_service.status) {
      return;
    }
    const now = new Date();
    if (new Date(this.api_service.status.matchday.kickoff_date) < now || !this.api_service.status.transferwindow) {
      return this.api_service.status.matchday.number;
    } else {
      const prev_matchday = this.api_service.status.matchday.number - 1;
      if (prev_matchday === 0) {
        return 1;
      } else {
        return this.api_service.status.matchday.number - 1;
      }
    }
  }

  public onSelectSearch(input_element: HTMLInputElement): void {
    input_element.select();
  }

  public onSearch(event): void {
    this.search_input = event;
    if (this.search_input.length >= 3) {
      this.api_service.getSearchResult(this.search_input).subscribe((res) => {
        if (res.success) {
          this.search_result_visible = true;
          this.search_result = res.result;
        }
      });
    } else {
      this.search_result_visible = false;
      this.search_result = { club: [], manager: [], player: [], team: [] };
    }
  }

  public onLogout() {
    this.logout_activated = true;
    setTimeout(() => {
      this.auth_service.logout();
    }, 400);
  }
}
