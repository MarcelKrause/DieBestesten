import { ChangeDetectorRef, Component, Directive, ViewChild, ViewContainerRef } from '@angular/core';
import { PopupService } from 'src/app/core/services/popup/popup.service';
import { CommonModule } from '@angular/common';

@Directive({
  standalone: true,
  selector: '[dynamicChildLoader]',
})
export class ComponentLoaderDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}

@Component({
  selector: 'db-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
  standalone: true,
  imports: [CommonModule, ComponentLoaderDirective],
})
export class PopupComponent {
  @ViewChild(ComponentLoaderDirective, { static: false }) componentLoader!: ComponentLoaderDirective;

  constructor(public popup_service: PopupService, private changeDetector: ChangeDetectorRef) {
    this.popup_service.open_event.subscribe((component: any) => {
      if (!component) {
        return;
      }
      this.loadComponent(component);
    });
  }

  public loadComponent(component: any): void {
    this.changeDetector.detectChanges();
    this.componentLoader.viewContainerRef.createComponent(component);
  }

  public onClose(): void {
    this.popup_service.close();
  }
}
