import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api/api.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'db-ribbon',
  templateUrl: './ribbon.component.html',
  styleUrls: ['./ribbon.component.scss'],
})
export class RibbonComponent {
  search_input: string = '';
  search_result_visible: boolean = false;
  search_result: { club: any[]; manager: any[]; player: any[]; team: any[] } = { club: [], manager: [], player: [], team: [] };

  phase: 'live' | 'lineup' | 'market' | undefined;
  phase_progress: number = 0;

  constructor(public api_service: ApiService, public auth_service: AuthService, private router: Router) {
    if (this.auth_service.isLoggedIn()) {
      setInterval(() => {
        this.calcPhase();
      }, 50);

      this.router.events.subscribe((val) => {
        if (val instanceof NavigationEnd) {
          this.onNavigate();
        }
      });
    }
  }

  private calcPhase(): void {
    const now = new Date().getTime();

    if (this.api_service.status.transferwindow) {
      this.phase = 'market';
      const start = new Date(this.api_service.status.transferwindow.start_date).getTime();
      const end = new Date(this.api_service.status.transferwindow.end_date).getTime();
      this.phase_progress = (100 * (now - start)) / (end - start);
    } else {
      const kickoff_date = new Date(this.api_service.status.matchday.kickoff_date).getTime();
      if (now < kickoff_date) {
        this.phase = 'lineup';
        const start = new Date(this.api_service.status.matchday.start_date).getTime();
        const end = new Date(this.api_service.status.matchday.kickoff_date).getTime();
        this.phase_progress = (100 * (now - start)) / (end - start);
      } else {
        this.phase = 'live';
        const start = new Date(this.api_service.status.matchday.kickoff_date).getTime();
        const end = new Date(this.api_service.status.matchday.result_date).getTime();
        this.phase_progress = (100 * (now - start)) / (end - start);
      }
    }

    if (this.phase_progress >= 100) {
      //window.location.href = environment.baseRef;
    }
  }

  public getGradient(): string {
    if (!this.phase_progress) {
      return '';
    }
    return `conic-gradient(#C01C00 ${this.phase_progress}%, 0, white ${100 - this.phase_progress}%)`;
  }

  public getTooltip(): string {
    switch (this.phase) {
      case 'market':
        return 'Transfermarkt geöffnet';
      case 'lineup':
        return 'Aufstellungsphase';
      case 'live':
        return 'Spieltag läuft!';
    }
  }

  public onSelectSearch(input_element: HTMLInputElement): void {
    input_element.select();
    if (this.search_input.length >= 0) {
      this.search_result_visible = true;
    }
  }

  public onSearch(event): void {
    this.search_input = event;
    if (this.search_input.length >= 3) {
      this.api_service.getSearchResult(this.search_input).subscribe((res) => {
        if (res.success) {
          this.search_result_visible = true;
          this.search_result = res.result;
        }
      });
    } else {
      this.search_result_visible = false;
      this.search_result = { club: [], manager: [], player: [], team: [] };
    }
  }

  public isEmptySearchResult(): boolean {
    return (
      this.search_result.club.length === 0 &&
      this.search_result.manager.length === 0 &&
      this.search_result.player.length === 0 &&
      this.search_result.team.length === 0
    );
  }

  public hideSearchResults(): void {
    this.search_result_visible = false;
  }

  public onNavigate(): void {
    this.search_result_visible = false;
  }
}
