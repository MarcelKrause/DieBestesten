import { Component } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { StateService } from 'src/app/core/services/state/state.service';

@Component({
  selector: 'db-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  constructor(public api_service: ApiService, public auth_service: AuthService, private state_service: StateService) {}

  public onToggleMenu(): void {
    this.state_service.toggleMenu();
  }
}
