import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuComponent } from './components/menu/menu.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { FormsModule } from '@angular/forms';
import { RibbonComponent } from './components/ribbon/ribbon.component';
import { SearchComponent } from './components/search/search.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { LoadingComponent } from './components/loading/loading.component';

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule, MatTooltipModule],
  declarations: [MenuComponent, HeaderComponent, RibbonComponent, SearchComponent, LoadingComponent],
  exports: [MenuComponent, HeaderComponent, RibbonComponent, LoadingComponent],
})
export class SharedModule {}
