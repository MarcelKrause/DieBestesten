export class Notification {
  message: string;
  type: 'positive' | 'neutral' | 'negative' = 'neutral';

  constructor(message: string, type?: 'positive' | 'neutral' | 'negative') {
    this.message = message;
    this.type = type ? type : 'neutral';
  }
}
