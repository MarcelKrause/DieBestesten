import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';

/*
0 = ASK Traktor Achmer
1 = Blutgrätsche 69
2 = Bonn 17
3 = Concordia Hachmannsfeld
4 = Fab
5 = Fiasko Fantasto
6 = Kackbratzen
7 = SV Spielabbruch
8 = Sackflanke
9 = Schlaggy
10 = US Töfte Calcio
11 = Vorwärts Fliese
*/

@Component({
  selector: 'app-cup',
  templateUrl: './cup.component.html',
  styleUrls: ['./cup.component.scss'],
})
export class CupComponent implements OnInit {
  teamList: any;
  season_id: string = '0534e075-037e-11ed-b2e3-c81f66ca5915';
  isLoading: boolean = false;

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.getTeamList();
  }

  private getTeamList(): void {
    this.isLoading = true;
    this.apiService.team.getTeamListBySeason(this.season_id).subscribe((data) => {
      this.teamList = data;
      this.teamList.sort(this.sortByName);
      this.isLoading = false;
    });
  }

  public getTeam(index: number): any {
    if (this.teamList.length > 0) {
      return this.teamList[index];
    }
  }

  private sortByName(a: any, b: any) {
    if (a.team_name > b.team_name) {
      return 1;
    } else {
      return -1;
    }
  }
}
