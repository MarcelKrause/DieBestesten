import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CoppaTommassaComponent } from './coppa-tommassa.component';

describe('CoppaTommassaComponent', () => {
  let component: CoppaTommassaComponent;
  let fixture: ComponentFixture<CoppaTommassaComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CoppaTommassaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoppaTommassaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
