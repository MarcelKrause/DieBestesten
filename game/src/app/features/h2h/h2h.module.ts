// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { H2hRoutingModule } from './h2h.routing';

// Components
import { IndexComponent } from './pages/index/index.component';
import { LeagueComponent } from './pages/league/league.component';
import { CupComponent } from './pages/cup/cup.component';
import { TableRowComponent } from './components/table-row/table-row.component';
import { MatchComponent } from './components/match/match.component';

@NgModule({
  declarations: [IndexComponent, LeagueComponent, CupComponent, TableRowComponent, MatchComponent],
  imports: [CommonModule, H2hRoutingModule],
})
export class H2hModule {}
