import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';

/*
0 = ASK Traktor Achmer
1 = Blutgrätsche 69
2 = Bonn 17
3 = Concordia Hachmannsfeld
4 = Fab
5 = Fiasko Fantasto
6 = Kackbratzen
7 = SV Spielabbruch
8 = Sackflanke
9 = Schlaggy
10 = US Töfte Calcio
11 = Vorwärts Fliese
*/

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.scss'],
})
export class LeagueComponent implements OnInit {
  teamList: any;
  season_id: string = '289db61a-286c-11ee-9124-c81f66ca5914';
  isLoading: boolean = false;

  selected_matchday = 1;

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.getTeamList();
    this.setSelectedMatchday();
  }

  private getTeamList(): void {
    this.isLoading = true;
    this.apiService.team.getTeamListBySeason(this.season_id).subscribe((data) => {
      this.teamList = data;
      this.teamList.sort(this.sortByName);
      this.teamList = this.fillArrayWithEmptyObjects(this.teamList, 12);
      this.isLoading = false;
    });
  }

  private setSelectedMatchday(): void {
    switch (this.apiService.status.matchday.number) {
      case 1:
        this.selected_matchday = 1;
      case 2:
        this.selected_matchday = 1;
      case 3:
        this.selected_matchday = 1;
      case 4:
        this.selected_matchday = 2;
      case 5:
        this.selected_matchday = 2;
      case 6:
        this.selected_matchday = 3;
      case 7:
        this.selected_matchday = 3;
      case 8:
        this.selected_matchday = 4;
      case 9:
        this.selected_matchday = 5;
      case 10:
        this.selected_matchday = 5;
      case 11:
        this.selected_matchday = 6;
      case 12:
        this.selected_matchday = 7;
      case 13:
        this.selected_matchday = 7;
      case 14:
        this.selected_matchday = 8;
      case 15:
        this.selected_matchday = 8;
      case 16:
        this.selected_matchday = 9;
      case 17:
        this.selected_matchday = 10;
      case 18:
        this.selected_matchday = 11;
      case 19:
        this.selected_matchday = 12;
      case 20:
        this.selected_matchday = 12;
      case 21:
        this.selected_matchday = 13;
      case 22:
        this.selected_matchday = 13;
      case 23:
        this.selected_matchday = 14;
      case 24:
        this.selected_matchday = 15;
      case 25:
        this.selected_matchday = 16;
      case 26:
        this.selected_matchday = 17;
      case 27:
        this.selected_matchday = 17;
      case 28:
        this.selected_matchday = 18;
      case 29:
        this.selected_matchday = 18;
      case 30:
        this.selected_matchday = 19;
      case 31:
        this.selected_matchday = 20;
      case 32:
        this.selected_matchday = 21;
      case 33:
        this.selected_matchday = 22;
      case 34:
        this.selected_matchday = 22;
    }
  }

  public getTeam(index: number): any {
    if (this.teamList.length > 0) {
      return this.teamList[index];
    }
  }

  private sortByName(a: any, b: any) {
    if (a.team_name > b.team_name) {
      return 1;
    } else {
      return -1;
    }
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  private fillArrayWithEmptyObjects(arr: any[], fixedSize: number): any[] {
    const newArray = [...arr]; // Erstelle eine Kopie des vorhandenen Arrays

    while (newArray.length < fixedSize) {
      newArray.push({}); // Füge leere Objekte hinzu, bis die gewünschte Größe erreicht ist
    }

    return newArray;
  }
}
