import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CupComponent } from './cup.component';

describe('CupComponent', () => {
  let component: CupComponent;
  let fixture: ComponentFixture<CupComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
