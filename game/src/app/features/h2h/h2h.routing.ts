// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { LeagueComponent } from './pages/league/league.component';
import { CupComponent } from './pages/cup/cup.component';

const routes: Routes = [
  { path: '', component: CupComponent },
  { path: 'liga', component: LeagueComponent },
  { path: 'pokal', component: CupComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class H2hRoutingModule {}
