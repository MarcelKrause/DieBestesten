import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-row',
  templateUrl: './table-row.component.html',
  styleUrls: ['./table-row.component.scss'],
})
export class TableRowComponent implements OnInit {
  @Input() team: any;
  @Input() goals: string;
  @Input() points: number;

  season_id: string = '289db61a-286c-11ee-9124-c81f66ca5914';

  constructor() {}

  ngOnInit(): void {}

  public getHomeGoals(): number {
    let goals: number = 0;
    this.team.ratings.forEach((rating) => {
      goals += +rating.goals;
    });
    return goals;
  }
}
