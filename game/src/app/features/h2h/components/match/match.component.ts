import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss'],
})
export class MatchComponent implements OnInit {
  @Input() matchday: number;
  @Input() home: any;
  @Input() away: any;
  @Input() mode: string;
  @Input() winner: string;

  season_id: string = '289db61a-286c-11ee-9124-c81f66ca5914';

  constructor() {}

  ngOnInit(): void {
    this.matchday--;
  }

  public getValue(self: any, opponent: any): any {
    if (this.mode === 'points') {
      return self.points;
    }

    if (!self.points) {
      return '-';
    }

    let result = +self.goals - +opponent.sds_defender;

    if (result < 0) {
      result = 0;
    }

    return result;
  }

  public getDefenseSds(self: any): any {
    if (self.sds_defender) {
      const sds = +self.sds_defender;
      return sds;
    } else {
      return '-';
    }
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }
}
