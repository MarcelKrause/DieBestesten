// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from './login.routing';
import { FormsModule } from '@angular/forms';

// Components
import { IndexComponent } from './pages/index/index.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [CommonModule, LoginRoutingModule, FormsModule, RouterModule],
  declarations: [IndexComponent],
})
export class LoginModule {}
