import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  username: string;
  password: string;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  public onLogin(): void {
    this.authService.login(this.username, this.password);
  }
}
