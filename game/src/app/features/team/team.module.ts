// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TeamRoutingModule } from './team.routing';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CoreModule } from 'src/app/core/core.module';

// Components
import { IndexComponent } from './pages/index/index.component';
import { SquadComponent } from './pages/squad/squad.component';
import { LineupComponent } from './pages/lineup/lineup.component';
import { SeasonComponent } from './pages/season/season.component';
import { LineupPlayerComponent } from './components/lineup-player/lineup-player.component';
import { SubstitutionPlayerComponent } from './components/substitution-player/substitution-player.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [CommonModule, RouterModule, CoreModule, TeamRoutingModule, MatTooltipModule, SharedModule],
  declarations: [IndexComponent, SquadComponent, LineupComponent, SeasonComponent, LineupPlayerComponent, SubstitutionPlayerComponent],
})
export class TeamModule {}
