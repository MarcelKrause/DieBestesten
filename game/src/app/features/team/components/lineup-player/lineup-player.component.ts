import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-lineup-player',
  templateUrl: './lineup-player.component.html',
  styleUrls: ['./lineup-player.component.scss'],
})
export class LineupPlayerComponent implements OnInit {
  @Input() team: any;
  @Input() player: any;
  @Input() isInteractable: boolean;
  @Output() removePlayer = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  public onRemovePlayer(playerId: string): void {
    if (this.isInteractable) {
      this.removePlayer.emit(playerId);
    }
  }

  public arrayOne(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }
}
