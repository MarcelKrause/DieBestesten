import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-substitution-player',
  templateUrl: './substitution-player.component.html',
  styleUrls: ['./substitution-player.component.scss'],
})
export class SubstitutionPlayerComponent implements OnInit {
  @Input() team: any;
  @Input() player: any;
  @Input() isInteractable: boolean;
  @Input() isAddable: boolean;
  @Output() addPlayer = new EventEmitter<string>();

  constructor() {}

  ngOnInit(): void {}

  public onAddPlayer(playerId: string): void {
    if (this.isAddable) {
      this.addPlayer.emit(playerId);
    }
  }
}
