import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SubstitutionPlayerComponent } from './substitution-player.component';

describe('SubstitutionPlayerComponent', () => {
  let component: SubstitutionPlayerComponent;
  let fixture: ComponentFixture<SubstitutionPlayerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SubstitutionPlayerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubstitutionPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
