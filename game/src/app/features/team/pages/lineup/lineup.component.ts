import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Team } from 'src/app/core/models/team/team';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-lineup',
  templateUrl: './lineup.component.html',
  styleUrls: ['./lineup.component.scss'],
})
export class LineupComponent implements OnInit {
  seasonName: string;
  teamName: string;
  matchdayNumber: number;

  matchdayList: number[];
  formationList = [
    [1, 3, 4, 3],
    [1, 3, 5, 2],
    [1, 4, 3, 3],
    [1, 4, 4, 2],
    [1, 4, 5, 1],
    [1, 5, 3, 2],
    [1, 5, 4, 1],
  ];

  team: Team;

  phase: string;
  isValidSquad: boolean;
  isValidLineup: boolean;
  lineup: any[];
  formation: number[];

  isTeamOwner: boolean;
  isBeforeDeadline: boolean;
  transferwindow: any;

  status: string;

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private location: Location,
    private notificationService: NotificationService
  ) {
    this.route.parent.params.subscribe((params) => {
      this.seasonName = this.route.parent.snapshot.paramMap.get('season_name');
      this.teamName = this.route.parent.snapshot.paramMap.get('team_name');
      this.matchdayNumber = +this.route.snapshot.paramMap.get('matchday_number');
      this.getTeam(this.seasonName, this.teamName);
      this.getLineup();
      this.createMatchdayList();
      this.isBeforeDeadline = this.checkDeadline();
      this.transferwindow = this.apiService.status.transferwindow;
    });
  }

  ngOnInit(): void {}

  private getTeam(seasonName: string, teamName: string): void {
    this.apiService.team.getTeamBySeasonAndName(seasonName, teamName).subscribe((data) => {
      this.team = new Team(data.team);
      if (this.apiService.status.team.team_id === this.team.teamId) {
        this.isTeamOwner = true;
      } else {
        this.isTeamOwner = false;
      }
    });
  }

  private getLineup(): void {
    this.apiService.teamLineup.getLineup(this.seasonName, this.teamName, this.matchdayNumber).subscribe((data) => {
      this.phase = data.phase;
      this.isValidLineup = Boolean(Number(data.is_valid));
      this.formation = data.formation;
      this.lineup = data.lineup;
      this.isValidSquad = this.checkSquad();

      if (data.lineup_takeover) {
        this.notificationService.push('Aufstellung vom letzten Spieltag übernommen', 'positive');
      } else if (data.takeover_not_possible) {
        this.notificationService.push('Aufstellung konnte nicht vom letzten Spieltag übernommen werden', 'negative');
      }
    });
  }

  public onSelectMatchday(target: any): void {
    this.matchdayNumber = +target.value;
    this.getLineup();
    this.isBeforeDeadline = this.checkDeadline();

    this.location.replaceState(`team/${this.seasonName}/${this.teamName}/aufstellung/${this.matchdayNumber}`);
  }

  public onSelectFormation(formation: any, newLineup: boolean = false): void {
    this.isValidLineup = true;

    switch (formation) {
      case 343:
        this.formation = [1, 3, 4, 3];
        break;
      case 352:
        this.formation = [1, 3, 5, 2];
        break;
      case 433:
        this.formation = [1, 4, 3, 3];
        break;
      case 442:
        this.formation = [1, 4, 4, 2];
        break;
      case 451:
        this.formation = [1, 4, 5, 1];
        break;
      case 532:
        this.formation = [1, 5, 3, 2];
        break;
      case 541:
        this.formation = [1, 5, 4, 1];
        break;
    }

    this.lineup.forEach((player) => {
      player.nominated = 0;
    });

    this.lineup
      .filter((player) => player.position === 'GOALKEEPER')
      .slice(0, this.formation[0])
      .forEach((player) => {
        player.nominated = 1;
      });

    this.lineup
      .filter((player) => player.position === 'DEFENDER')
      .slice(0, this.formation[1])
      .forEach((player) => {
        player.nominated = 1;
      });

    this.lineup
      .filter((player) => player.position === 'MIDFIELDER')
      .slice(0, this.formation[2])
      .forEach((player) => {
        player.nominated = 1;
      });

    this.lineup
      .filter((player) => player.position === 'FORWARD')
      .slice(0, this.formation[3])
      .forEach((player) => {
        player.nominated = 1;
      });

    if (this.checkLineup()) {
      if (newLineup) {
        this.postLineup();
      } else {
        this.patchLineup();
      }
    } else {
      this.status = 'isModified';
    }
  }

  public onRemovePlayer(playerId: string): void {
    this.status = 'isModified';
    this.lineup.find((player) => player.player_id === playerId).nominated = 0;
    this.lineup.sort(this.sortByPosition);

    if (this.checkLineup()) {
      this.patchLineup();
    }
  }

  public onAddPlayer(playerId: string): void {
    this.status = 'isModified';
    this.lineup.find((player) => player.player_id === playerId).nominated = 1;
    this.lineup.sort(this.sortByPosition);

    if (this.checkLineup()) {
      this.patchLineup();
    }
  }

  public getFormationName(): string {
    if (this.lineup) {
      const defender = this.lineup.filter((player) => player.nominated === 1 && player.position === 'DEFENDER').length;
      const midfielder = this.lineup.filter((player) => player.nominated === 1 && player.position === 'MIDFIELDER').length;
      const forward = this.lineup.filter((player) => player.nominated === 1 && player.position === 'FORWARD').length;
      return `${defender}-${midfielder}-${forward}`;
    } else {
      return null;
    }
  }

  private checkDeadline(): boolean {
    if (+this.matchdayNumber !== +this.apiService.status.matchday.number) {
      return false;
    } else {
      const now = new Date(Date.now());
      const kickoff = new Date(this.apiService.status.matchday.kickoff_date.replace(' ', 'T'));
      if (now < kickoff) {
        return true;
      } else {
        return false;
      }
    }
  }

  public getPoints(): number {
    if (this.lineup) {
      return this.lineup.filter((player) => player.nominated === 1).reduce((totalPoints, player) => totalPoints + +player.points, 0);
    } else {
      return null;
    }
  }

  public getPlayerByPosition(position: string): any[] {
    let positionIndex: number;
    switch (position) {
      case 'DEFENDER':
        positionIndex = 1;
        break;
      case 'MIDFIELDER':
        positionIndex = 2;
        break;
      case 'FORWARD':
        positionIndex = 3;
        break;
      default:
        positionIndex = 0;
    }

    if (this.lineup) {
      const nominatedPlayer = this.lineup.filter((player) => player.nominated === 1 && player.position === position);

      while (nominatedPlayer.length < this.formation[positionIndex]) {
        nominatedPlayer.push(null);
      }

      return nominatedPlayer;
    }
  }

  public getSubstitution(): any[] {
    if (this.lineup) {
      return this.lineup.filter((player) => player.nominated === 0);
    }
  }

  public getFormationString(formation: number[]): string {
    if (formation) {
      return `${formation[1]}${formation[2]}${formation[3]}`;
    }
  }

  private createMatchdayList(): void {
    let maxMatchdayNumber;
    if (this.formatSeasonToOriginal(this.seasonName) === this.apiService.status.season.season_name) {
      maxMatchdayNumber = +this.apiService.status.matchday.number;
    } else {
      maxMatchdayNumber = 34;
    }

    this.matchdayList = [];
    for (let i = maxMatchdayNumber; i >= 1; i--) {
      this.matchdayList.push(i);
    }
  }

  public hasPositionEmptySlots(position: string): boolean {
    if (this.formation && this.lineup) {
      let positionIndex: number;
      switch (position) {
        case 'DEFENDER':
          positionIndex = 1;
          break;
        case 'MIDFIELDER':
          positionIndex = 2;
          break;
        case 'FORWARD':
          positionIndex = 3;
          break;
        default:
          positionIndex = 0;
      }

      const nominatedPlayer = this.lineup.filter((player) => player.nominated === 1 && player.position === position);
      if (nominatedPlayer.length < this.formation[positionIndex]) {
        return true;
      } else {
        return false;
      }
    }
  }

  public checkLineup(): boolean {
    const goalkeeper = this.lineup.filter((player) => player.nominated === 1 && player.position === 'GOALKEEPER');
    const defender = this.lineup.filter((player) => player.nominated === 1 && player.position === 'DEFENDER');
    const midfielder = this.lineup.filter((player) => player.nominated === 1 && player.position === 'MIDFIELDER');
    const forward = this.lineup.filter((player) => player.nominated === 1 && player.position === 'FORWARD');

    if (
      goalkeeper.length === this.formation[0] &&
      defender.length === this.formation[1] &&
      midfielder.length === this.formation[2] &&
      forward.length === this.formation[3]
    ) {
      return true;
    } else {
      return false;
    }
  }

  private checkSquad(): boolean {
    const goalkeeper = this.lineup.filter((player) => player.position === 'GOALKEEPER');
    const defender = this.lineup.filter((player) => player.position === 'DEFENDER');
    const midfielder = this.lineup.filter((player) => player.position === 'MIDFIELDER');
    const forward = this.lineup.filter((player) => player.position === 'FORWARD');
    if (
      goalkeeper.length >= 1 &&
      goalkeeper.length <= 2 &&
      defender.length >= 5 &&
      defender.length <= 6 &&
      midfielder.length >= 5 &&
      midfielder.length <= 6 &&
      forward.length >= 3 &&
      forward.length <= 4
    ) {
      return true;
    } else {
      return false;
    }
  }

  private postLineup(): void {
    if (!this.isTeamOwner) {
      return;
    }

    this.status = 'isSaving';
    this.apiService.teamLineup.postLineup(this.lineup, this.team.teamId, this.matchdayNumber).subscribe(
      (data) => {
        this.status = 'saved';
        setTimeout(() => {
          this.status = null;
        }, 4000);
      },
      (error) => {
        this.status = 'error';
      },
      () => {
        if (this.status === 'isSaving') {
          this.status = 'error';
        }
      }
    );
  }

  private patchLineup(): void {
    if (!this.isTeamOwner) {
      return;
    }

    this.status = 'isSaving';
    this.apiService.teamLineup.patchLineup(this.lineup).subscribe(
      (data) => {
        this.status = 'saved';
        setTimeout(() => {
          this.status = null;
        }, 4000);
      },
      (error) => {
        this.status = 'error';
      },
      () => {
        if (this.status === 'isSaving') {
          this.status = 'error';
        }
      }
    );
  }

  private sortByPosition(a: any, b: any): number {
    let aPositionValue;
    let bPositionValue;
    switch (a.position) {
      case 'GOALKEEPER':
        aPositionValue = 0;
        break;
      case 'DEFENDER':
        aPositionValue = 1;
        break;
      case 'MIDFIELDER':
        aPositionValue = 2;
        break;
      case 'FORWARD':
        aPositionValue = 3;
        break;
    }
    switch (b.position) {
      case 'GOALKEEPER':
        bPositionValue = 0;
        break;
      case 'DEFENDER':
        bPositionValue = 1;
        break;
      case 'MIDFIELDER':
        bPositionValue = 2;
        break;
      case 'FORWARD':
        bPositionValue = 3;
        break;
    }

    if (aPositionValue === bPositionValue) {
      if (a.points > b.points) {
        return 1;
      } else {
        return 0;
      }
    } else if (aPositionValue > bPositionValue) {
      return 1;
    } else {
      return -1;
    }
  }

  public formatSeasonToOriginal(season: string): string {
    if (season) {
      return season.replace('-', '/');
    }
  }
}
