import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/core/services/api/api.service';
import { Team } from 'src/app/core/models/team/team';
import { Manager } from 'src/app/core/models/manager/manager';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  seasonName: string;
  teamName: string;

  team: Team;
  manager: Manager;

  constructor(private route: ActivatedRoute, public apiService: ApiService) {
    this.route.params.subscribe((params) => {
      this.seasonName = this.route.snapshot.paramMap.get('season_name');
      this.teamName = this.route.snapshot.paramMap.get('team_name');
      this.getTeam(this.seasonName, this.teamName);
    });
  }

  ngOnInit(): void {}

  private getTeam(seasonName: string, teamName: string): void {
    this.apiService.team.getTeamBySeasonAndName(seasonName, teamName).subscribe((data) => {
      this.team = new Team(data.team);
      this.manager = new Manager(data.manager);
    });
  }
}
