import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/core/models/team/team';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-squad',
  templateUrl: './squad.component.html',
  styleUrls: ['./squad.component.scss'],
})
export class SquadComponent implements OnInit {
  loading: boolean;
  seasonName: string;
  teamName: string;

  team: Team;
  currentSquad: any[];
  formerSquad: any[];

  isValidSquad: boolean;

  topPlayerPoints: any;
  topPlayerPrice: any;

  conditionList = [
    {
      position: 'Torhüter',
      positionValue: 'GOALKEEPER',
      min: 1,
      max: 2,
    },
    {
      position: 'Abwehr',
      positionValue: 'DEFENDER',
      min: 5,
      max: 6,
    },
    {
      position: 'Mittelfeld',
      positionValue: 'MIDFIELDER',
      min: 5,
      max: 6,
    },
    {
      position: 'Sturm',
      positionValue: 'FORWARD',
      min: 3,
      max: 4,
    },
  ];

  orderBy: 'position' | 'team' | 'name' | 'price' | 'points' = 'position';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  constructor(private apiService: ApiService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.seasonName = this.route.parent.snapshot.paramMap.get('season_name');
    this.teamName = this.route.parent.snapshot.paramMap.get('team_name');

    this.getTeam(this.seasonName, this.teamName);
    this.getSquad(this.seasonName, this.teamName);
  }

  private getTeam(seasonName: string, teamName: string): void {
    this.apiService.team.getTeamBySeasonAndName(seasonName, teamName).subscribe((data) => {
      this.team = new Team(data.team);
    });
  }

  private getSquad(seasonName: string, teamName: string): void {
    this.loading = true;
    this.currentSquad = [];
    this.formerSquad = [];
    this.apiService.playerInTeam.getSquadByTeam(seasonName, teamName).subscribe(
      (data) => {
        data.squad.current.forEach((player) => {
          player.price = +player.price + 20000 * +player.points;
          this.currentSquad.push(player);
        });

        data.squad.former.forEach((player) => {
          player.price = +player.price + 20000 * +player.points;
          this.formerSquad.push(player);
        });
      },
      (error) => {},
      () => {
        this.loading = false;
        const tmpSquad = JSON.parse(JSON.stringify(this.currentSquad));
        this.topPlayerPoints = tmpSquad.sort(this.sortByPoints)[tmpSquad.length - 1];
        this.topPlayerPrice = tmpSquad.sort(this.sortByPrice)[tmpSquad.length - 1];
        this.isValidSquad = this.checkIfSquadIsValid();
      }
    );
  }

  public getTotalPrice(): number {
    return this.currentSquad.reduce((totalPrice, player) => totalPrice + +player.price, 0);
  }

  public getPlayerCountAtPosition(position: string): number {
    return this.currentSquad.filter((player) => player.position === position).length;
  }

  private checkIfSquadIsValid(): boolean {
    const goalkeeper = this.currentSquad.filter((player) => player.position === 'GOALKEEPER');
    const defender = this.currentSquad.filter((player) => player.position === 'DEFENDER');
    const midfielder = this.currentSquad.filter((player) => player.position === 'MIDFIELDER');
    const forward = this.currentSquad.filter((player) => player.position === 'FORWARD');

    if (
      goalkeeper.length >= 1 &&
      goalkeeper.length <= 2 &&
      defender.length >= 5 &&
      defender.length <= 6 &&
      midfielder.length >= 5 &&
      midfielder.length <= 6 &&
      forward.length >= 3 &&
      forward.length <= 4
    ) {
      return true;
    } else {
      return false;
    }
  }

  public sortBy(key: 'position' | 'name' | 'price' | 'points') {
    switch (key) {
      case 'position':
        this.currentSquad.sort(this.sortByPosition);
        break;
      case 'name':
        this.currentSquad.sort(this.sortByName);
        break;
      case 'price':
        this.currentSquad.sort(this.sortByPrice);
        break;
      case 'points':
        this.currentSquad.sort(this.sortByPoints);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.currentSquad.reverse();
    }
  }

  private sortByPosition(a, b): number {
    let aPositionValue = 0;
    let bPositionValue = 0;

    switch (a.position) {
      case 'GOALKEEPER':
        aPositionValue = 0;
      case 'DEFENDER':
        aPositionValue = 1;
      case 'MIDFIELDER':
        aPositionValue = 2;
      case 'FORWARD':
        aPositionValue = 3;
    }

    switch (b.position) {
      case 'GOALKEEPER':
        bPositionValue = 0;
      case 'DEFENDER':
        bPositionValue = 1;
      case 'MIDFIELDER':
        bPositionValue = 2;
      case 'FORWARD':
        bPositionValue = 3;
    }

    if (aPositionValue < bPositionValue) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByName(a, b): number {
    if (a.lastname < b.lastname) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByPrice(a, b): number {
    if (+a.price > +b.price) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByPoints(a, b): number {
    if (+a.points > +b.points) {
      return 1;
    } else {
      return -1;
    }
  }

  public getPositionValue(position: string): number {
    console.log(position);
    switch (position) {
      case 'GOALKEEPER':
        return 0;
      case 'DEFENDER':
        return 1;
      case 'MIDFIELDER':
        return 2;
      case 'FORWARD':
        return 3;
      default:
        return 0;
    }
  }
}
