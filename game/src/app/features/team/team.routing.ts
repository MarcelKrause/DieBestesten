// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { IndexComponent } from './pages/index/index.component';
import { SeasonComponent } from './pages/season/season.component';
import { LineupComponent } from './pages/lineup/lineup.component';
import { SquadComponent } from './pages/squad/squad.component';

const routes: Routes = [
  {
    path: ':season_name/:team_name',
    component: IndexComponent,
    children: [
      { path: '', redirectTo: 'kader' },
      { path: 'kader', component: SquadComponent },
      { path: 'aufstellung/:matchday_number', component: LineupComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TeamRoutingModule {}
