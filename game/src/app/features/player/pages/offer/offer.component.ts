import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Position } from 'src/app/core/models/position/position';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss'],
})
export class OfferComponent {
  offerList: any;
  squad: any;

  positionList = [
    {
      type: 'GOALKEEPER',
      name: 'Torwart',
    },
    {
      type: 'DEFENDER',
      name: 'Abwehr',
    },
    {
      type: 'MIDFIELDER',
      name: 'Mittelfeld',
    },
    {
      type: 'FORWARD',
      name: 'Sturm',
    },
  ];

  constructor(private apiService: ApiService, private dialog: MatDialog, private notification_service: NotificationService) {
    this.getOfferList();
    this.getSquad();
  }

  ngOnInit(): void {}

  private getOfferList(): void {
    this.apiService.offer.getOwnOfferList().subscribe((data) => {
      this.offerList = data.offer_to_me_list;
    });
  }

  private getSquad(): void {
    if (this.apiService.status) {
      this.apiService.playerInTeam
        .getSquadByTeam(this.apiService.status.season.season_name, this.apiService.status.team.team_name)
        .subscribe((data) => {
          this.squad = data.squad.current;
        });
    }
  }

  public getStatusText(status: string): string {
    switch (status) {
      case 'pending':
        return 'Ausstehend';
      case 'success':
        return 'Gewonnen';
      case 'lost':
        return 'Verloren';
      case 'rejected':
        return 'Abgelehnt';
      case 'accepted':
        return 'Angenommen';
    }
  }

  public getPositionName(position): string {
    // TODO: make more performance - attribute in model or something
    return Position[position];
  }

  public getPendingOfferCount(position: string): number {
    if (!this.offerList) {
      return null;
    } else {
      return this.offerList.filter((offer) => offer.status === 'pending' && offer.position === position).length;
    }
  }

  public getMinMaxSlots(position: string, type: string): number {
    let min = 0;
    let max = 0;
    switch (position) {
      case 'GOALKEEPER':
        min = 1;
        max = 2;
        break;
      case 'DEFENDER':
        min = 5;
        max = 6;
        break;
      case 'MIDFIELDER':
        min = 5;
        max = 6;
        break;
      case 'FORWARD':
        min = 3;
        max = 4;
        break;
    }

    if (!this.squad) {
      return null;
    } else {
      if (type === 'min') {
        return min - this.squad.filter((player) => player.position === position).length;
      }
      if (type === 'max') {
        return max - this.squad.filter((player) => player.position === position).length;
      }
    }
  }

  public onRefuseOffer(offer: any): void {
    if (confirm(`Angebot für ${offer.displayname} wirklich ablehnen?`)) {
      this.apiService.offer.refuseOffer(offer.offer_id).subscribe((data) => {
        this.notification_service.push('Das Angebot wurde erfolgreich abgelehnt.', 'positive');
        this.getOfferList();
      });
    }
  }

  public onAcceptOffer(offer: any): void {
    if (confirm(`Angebot für ${offer.displayname} wirklich annehmen?`)) {
      this.apiService.offer.acceptOffer(offer.offer_id).subscribe((data) => {
        this.notification_service.push('Das Angebot wurde angenommen.', 'positive');
        this.getOfferList();
      });
    }
  }
}
