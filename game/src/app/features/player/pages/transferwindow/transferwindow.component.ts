import { Component, isDevMode, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Position } from 'src/app/core/models/position/position';
import { ApiService } from 'src/app/core/services/api/api.service';
import { AddTransferwindowComponent } from '../../dialogs/add-transferwindow/add-transferwindow.component';
import { ListOfferComponent } from '../../dialogs/list-offer/list-offer.component';
import { ListOfferPresentationComponent } from '../../dialogs/list-offer-presentation/list-offer-presentation.component';

@Component({
  selector: 'app-transferwindow',
  templateUrl: './transferwindow.component.html',
  styleUrls: ['./transferwindow.component.scss'],
})
export class TransferwindowComponent implements OnInit {
  transferwindowList: any[];
  currentProgress: number;

  selected_season_id: string;
  seasonList: any[];

  sold_players: any[] = [];

  is_loading: boolean = false;

  constructor(private apiService: ApiService, private router: Router, private dialog: MatDialog) {
    this.getSeasonList();
    this.getSoldPlayers();
  }

  ngOnInit(): void {}

  public getSeasonList(): void {
    this.seasonList = [];
    this.apiService.season.getSeasonList().subscribe({
      next: (data) => {
        data.forEach((element) => {
          this.seasonList.push({ season_id: element.season_id, season_name: element.season_name });
        });
      },
      error: (error) => {
        console.error(error);
      },
      complete: () => {
        this.selected_season_id = this.seasonList[0].season_id;
        this.getTransferwindowList(this.selected_season_id);
      },
    });
  }

  private getSoldPlayers(): void {
    this.apiService.player.getSoldPlayers().subscribe({
      next: (data) => {
        this.sold_players = data;
      },
      error: (error) => {},
      complete: () => {},
    });
  }

  public getPositionName(position): string {
    // TODO: make more performance - attribute in model or something
    return Position[position];
  }

  public onSelectSeason(season_id: any): void {
    this.selected_season_id = season_id;
    this.getTransferwindowList(this.selected_season_id);
  }

  private getTransferwindowList(season_id: string): void {
    this.is_loading = true;
    this.transferwindowList = [];
    this.apiService.transferwindow.getTransferwindowList(season_id).subscribe((data) => {
      this.transferwindowList = data;
      this.transferwindowList.forEach((transferwindow) => {
        if (this.getStatusByTransferwindow(transferwindow) === 'open') {
          const now = new Date();
          const startDate = new Date(transferwindow.start_date);
          const endDate = new Date(transferwindow.end_date);
          const range = endDate.getTime() - startDate.getTime();
          const progress = endDate.getTime() - now.getTime();
          this.currentProgress = (1 - progress / range) * 100;
          return;
        }
      });
      this.is_loading = false;
    });
  }

  public getStatusByTransferwindow(transferwindow: any): any {
    return { name: transferwindow.message, class: transferwindow.status };

    const now = new Date().getTime();
    const startDate = new Date(transferwindow.start_date.replace(' ', 'T')).getTime();
    const endDate = new Date(transferwindow.end_date.replace(' ', 'T')).getTime();
    if (now < startDate) {
      return { name: 'Bald', class: 'coming' };
    } else if (now > endDate) {
      return { name: 'Geschlossen', class: 'closed' };
    } else if (now > startDate && now < endDate) {
      return { name: 'Offen', class: 'open' };
    }
    return 'error';
  }

  public onOpenTransferwindow(transferwindow: any): void {
    const status = this.getStatusByTransferwindow(transferwindow).class;
    if (status === 'open') {
      this.router.navigate(['/spieler/transfermarkt']);
    } else if (status === 'closed') {
      const dialogRef = this.dialog.open(ListOfferComponent, {
        data: {
          transferwindow,
        },
        width: '640px',
        panelClass: 'full-dialog',
        autoFocus: false,
        maxHeight: '95vh', //you can adjust the value as per your view
      });
    }
  }

  public onOpenTransferwindowPresentation(transferwindow: any): void {
    this.dialog.open(ListOfferPresentationComponent, {
      data: {
        transferwindow,
      },
      width: '640px',
      panelClass: 'full-dialog',
      autoFocus: false,
      maxHeight: '95vh', //you can adjust the value as per your view
    });
  }

  public onOpenMarket(): void {
    this.router.navigate(['/spieler/transfermarkt']);
  }
  public onOpenList(transferwindow: any): void {
    this.dialog.open(ListOfferComponent, {
      data: {
        transferwindow,
      },
      width: '640px',
      panelClass: 'full-dialog',
      autoFocus: false,
      maxHeight: '95vh', //you can adjust the value as per your view
    });
  }
  public onOpenPresentation(transferwindow: any): void {
    this.dialog.open(ListOfferPresentationComponent, {
      data: {
        transferwindow,
      },
      width: '640px',
      panelClass: 'full-dialog',
      autoFocus: false,
    });
  }

  public onAddTransferwindow(): void {
    const dialogRef = this.dialog.open(AddTransferwindowComponent, {});

    dialogRef.afterClosed().subscribe((result) => {
      location.reload();
    });
  }

  public onResetSeason(): void {
    if (confirm('Saison wirklich zurücksetzen?')) {
      this.apiService.resetSeason().subscribe((data) => {
        location.reload();
      });
    }
  }

  public onOpenCurrentTransferwindow(): void {
    const currentTransferwindow = this.transferwindowList.filter(
      (transferwindow) => this.getStatusByTransferwindow(transferwindow).class === 'open'
    )[0];

    if (currentTransferwindow) {
      const dialogRef = this.dialog.open(ListOfferComponent, {
        data: {
          transferwindow: currentTransferwindow,
        },
        width: '640px',
        panelClass: 'full-dialog',
        autoFocus: false,
        maxHeight: '95vh', //you can adjust the value as per your view
      });
    }
  }

  public isDev(): boolean {
    return isDevMode();
  }
}
