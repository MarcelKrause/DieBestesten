import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransferwindowComponent } from './transferwindow.component';

describe('TransferwindowComponent', () => {
  let component: TransferwindowComponent;
  let fixture: ComponentFixture<TransferwindowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferwindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferwindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
