import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Position } from 'src/app/core/models/position/position';
import { ApiService } from 'src/app/core/services/api/api.service';
import { EditOfferComponent } from '../../dialogs/edit-offer/edit-offer.component';
import { SentOfferComponent } from '../../dialogs/sent-offer/sent-offer.component';

import Chart from 'chart.js/auto';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  seasonId: string;
  displayname: string;
  player: any;
  transferwindow: any;
  offerList: any[];

  player_rating_list: any[];
  matchday_list = new Array(34);

  selected_season: any;

  chart: any;

  constructor(private route: ActivatedRoute, private apiService: ApiService, public dialog: MatDialog, private router: Router) {
    this.route.params.subscribe((params) => {
      this.displayname = this.route.snapshot.paramMap.get('displayname');
      this.getPlayer(this.displayname);
    });
    if (this.apiService.status) {
      this.seasonId = this.apiService.status.season.season_id;
      this.transferwindow = this.apiService.status.transferwindow;
      this.getOfferList();
    }
  }

  ngOnInit(): void {}

  private getPlayer(displayname: string): void {
    this.apiService.player.getPlayerByDisplayname(displayname).subscribe((data) => {
      this.player = data;
      this.player.season_list[0].price = +this.player.season_list[0].price + 20000 * +this.player.season_list[0].points;
      this.getPlayerRating(this.player.player_id, this.player.season_list[0].season_id);
      this.selected_season = this.player.season_list[0];
    });
  }

  private getPlayerRating(player_id: string, season_id: string): void {
    this.apiService.player.getPlayerRating(player_id, season_id).subscribe((data) => {
      this.player_rating_list = data;

      if (this.chart) {
        this.updateChart();
      } else {
        this.generateChart();
      }
    });
  }

  private getOfferList(): void {
    this.apiService.offer.getOwnOfferList().subscribe((data) => {
      this.offerList = data.offer_list;
    });
  }

  public existsOfferForPlayer(playerId: string): boolean {
    if (!this.offerList) {
      return false;
    }

    if (this.offerList.filter((offer) => offer.player_id === playerId && offer.status === 'pending').length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public isPlayerAvailable(player: any): boolean {
    if (player.season_list[0].price === 0) {
      return false;
    }

    if (player.team_list.length === 0) {
      return true;
    } else {
      if (player.team_list[0].last_matchday) {
        return true;
      } else {
        return false;
      }
    }
  }

  public isForeignCity(city: string): string {
    const possibleCountryCode = city.substr(city.length - 4, 4);
    if (possibleCountryCode.charAt(0) === '(' && possibleCountryCode.charAt(3) === ')') {
      return possibleCountryCode.substr(1, 2);
    }
    return null;
  }

  public getAge(input: string): number {
    const dateOfBirth: Date = new Date(input);
    const today = new Date();
    let age = today.getFullYear() - dateOfBirth.getFullYear();
    const m = today.getMonth() - dateOfBirth.getMonth();

    if (m < 0 || (m === 0 && today.getDate() < dateOfBirth.getDate())) {
      age--;
    }

    return age;
  }

  public onSentOffer(): void {
    const dialogRef = this.dialog.open(SentOfferComponent, {
      width: '480px',
      disableClose: true,
      panelClass: 'full-dialog',
      data: {
        player: this.player,
        has_owner: !this.isPlayerAvailable(this.player),
      },
    });
  }

  public onEditOffer(): void {
    const offer = this.offerList.filter((o) => o.player_id === this.player.player_id && o.status === 'pending')[0];
    const dialogRef = this.dialog.open(EditOfferComponent, {
      width: '480px',
      disableClose: true,
      panelClass: 'full-dialog',
      data: {
        offer,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        location.reload();
      }
    });
  }

  public onDeleteOffer(): void {
    const offerId = this.offerList.filter((offer) => offer.player_id === this.player.player_id && offer.status === 'pending')[0].offer_id;
    if (confirm(`Angebot für ${this.player.displayname} wirklich löschen?`)) {
      this.apiService.offer.deleteOffer(offerId).subscribe((data) => {
        location.reload();
      });
    }
  }

  public getPositionName(position): string {
    return Position[position];
  }

  public isOwnTeam(player: any): boolean {
    if (player.team_list.length === 0) {
      return false;
    } else {
      if (this.apiService.status.team.team_id === player.team_list[0].team_id && !player.team_list[0].last_matchday) {
        return true;
      } else {
        return false;
      }
    }
  }

  public onSellPlayer(): void {
    if (confirm(`${this.player.displayname} wirklich verkaufen?`)) {
      this.apiService.playerInTeam.sellPlayer(this.player.team_list[0].player_in_team_id).subscribe((data) => {
        location.reload();
      });
    }
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  public getMatchdayResultbyIndex(index: number): any {
    return this.player_rating_list.find((rating) => rating.matchday === index + 1);
  }

  private generateChart(): void {
    this.chart = new Chart('season-chart', {
      type: 'bar',
      data: {
        labels: this.player_rating_list.map((item) => item.matchday),
        datasets: [
          {
            data: this.player_rating_list.map((item) => item.points),
            backgroundColor: this.generateBarColorArray(),
            borderRadius: (context) => {
              // Passe den Border-Radius dynamisch an die Fenstergröße an
              return window.innerWidth < 768 ? 1 : 5; // Beispielwerte für Mobile und Desktop
            },
          },
        ],
      },
      options: {
        plugins: {
          legend: { display: false },
          tooltip: { enabled: false },
        },
        scales: {
          x: {
            grid: {
              display: false,
            },
          },
          y: {
            suggestedMax: 10,
            grid: {
              display: true,
              color: (ctx) => (ctx.tick.value === 0 ? 'rgba(0, 0, 0, 0.1)' : 'transparent'),
            },
            ticks: {
              stepSize: 1,
            },
          },
        },
      },
    });
  }

  private updateChart(): void {
    this.chart.data.labels = this.player_rating_list.map((item) => item.matchday);
    this.chart.data.datasets[0].data = this.player_rating_list.map((item) => item.points);
    this.chart.data.datasets[0].backgroundColor = this.generateBarColorArray();

    this.chart.update();
  }

  private generateBarColorArray(): string[] {
    const colors = [];
    for (let rating of this.player_rating_list) {
      switch (rating.grade) {
        case 1:
          colors.push('#1565c0');
          break;
        case 1.5:
          colors.push('#2e7d32');
          break;
        case 2:
          colors.push('#43a047');
          break;
        case 2.5:
          colors.push('#7cb342');
          break;
        case 3:
          colors.push('#c0ca33');
          break;
        case 3.5:
          colors.push('#fdd835');
          break;
        case 4:
          colors.push('#ffb300');
          break;
        case 4.5:
          colors.push('#fb8c00');
          break;
        case 5:
          colors.push('#f4511e');
          break;
        case 5.5:
          colors.push('#d84315');
          break;
        case 6:
          colors.push('#212121');
          break;
        default:
          colors.push('#dfe6e9');
          break;
      }
    }
    return colors;
  }

  public getPositionLabel(position: string): string {
    switch (position) {
      case 'GOALKEEPER':
        return 'TOR';
      case 'DEFENDER':
        return 'ABW';
      case 'MIDFIELDER':
        return 'MIT';
      case 'FORWARD':
        return 'STU';
      default:
        return '';
    }
  }

  public getAverageGrade(): number {
    if (this.player_rating_list.length === 0) {
      return 0;
    }

    const validGrades = this.player_rating_list.filter((item) => item.grade !== null && item.grade !== 0).map((item) => item.grade);

    return validGrades.reduce((sum, grade) => sum + grade, 0) / validGrades.length;
  }

  public onSelectSeason(season: any): void {
    this.selected_season = season;
    this.getPlayerRating(this.player.player_id, season.season_id);
  }

  public navigateToTeam(team_name: string, team: any): void {
    console.log(team);
    this.router.navigate(['team', '2024-2025', team_name]);
  }
}
