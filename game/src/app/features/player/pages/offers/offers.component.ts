import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Position } from 'src/app/core/models/position/position';
import { ApiService } from 'src/app/core/services/api/api.service';
import { EditOfferComponent } from '../../dialogs/edit-offer/edit-offer.component';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.scss'],
})
export class OffersComponent implements OnInit {
  offerList: any;
  squad: any;

  positionList = [
    {
      type: 'GOALKEEPER',
      name: 'Torwart',
    },
    {
      type: 'DEFENDER',
      name: 'Abwehr',
    },
    {
      type: 'MIDFIELDER',
      name: 'Mittelfeld',
    },
    {
      type: 'FORWARD',
      name: 'Sturm',
    },
  ];

  constructor(private apiService: ApiService, private dialog: MatDialog) {
    this.getOfferList();
    this.getSquad();
  }

  ngOnInit(): void {}

  private getOfferList(): void {
    this.apiService.offer.getOwnOfferList().subscribe((data) => {
      this.offerList = data.offer_list;
    });
  }

  private getSquad(): void {
    if (this.apiService.status) {
      this.apiService.playerInTeam
        .getSquadByTeam(this.apiService.status.season.season_name, this.apiService.status.team.team_name)
        .subscribe((data) => {
          this.squad = data.squad.current;
        });
    }
  }

  public getStatusText(status: string): string {
    switch (status) {
      case 'pending':
        return 'Ausstehend';
      case 'success':
        return 'Gewonnen';
      case 'lost':
        return 'Verloren';
      case 'rejected':
        return 'Abgelehnt';
      case 'accepted':
        return 'Angenommen';
    }
  }

  public onEditOffer(offer: any): void {
    const dialogRef = this.dialog.open(EditOfferComponent, {
      width: '480px',
      disableClose: true,
      panelClass: 'full-dialog',
      data: {
        offer,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      location.reload();
    });
  }

  public getPendingFofferCount(): number {
    return this.offerList.filter((offer) => offer.status === 'pending').length;
  }

  public onDeleteOffer(offer: any): void {
    if (confirm(`Angebot für ${offer.displayname} wirklich löschen?`)) {
      this.apiService.offer.deleteOffer(offer.offer_id).subscribe((data) => {
        this.getOfferList();
      });
    }
  }

  public getPositionName(position): string {
    // TODO: make more performance - attribute in model or something
    return Position[position];
  }

  public getPendingOfferCount(position: string): number {
    if (!this.offerList) {
      return null;
    } else {
      return this.offerList.filter((offer) => offer.status === 'pending' && offer.position === position).length;
    }
  }

  public getMinMaxSlots(position: string, type: string): number {
    let min = 0;
    let max = 0;
    switch (position) {
      case 'GOALKEEPER':
        min = 1;
        max = 2;
        break;
      case 'DEFENDER':
        min = 5;
        max = 6;
        break;
      case 'MIDFIELDER':
        min = 5;
        max = 6;
        break;
      case 'FORWARD':
        min = 3;
        max = 4;
        break;
    }

    if (!this.squad) {
      return null;
    } else {
      if (type === 'min') {
        return min - this.squad.filter((player) => player.position === position).length;
      }
      if (type === 'max') {
        return max - this.squad.filter((player) => player.position === position).length;
      }
    }
  }
}
