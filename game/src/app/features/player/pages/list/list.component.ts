import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { Position } from 'src/app/core/models/position/position';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  loading: boolean;
  clubList: any[] = [];
  playerList: any[] = [];
  filteredPlayerList: any[];
  maxLimit = 20;

  filter: any;

  orderBy: 'player_name' | 'price' | 'points' = 'points';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  constructor(private apiService: ApiService, private storageService: StorageService) {
    this.getBundesligaClubList();
    this.getBundesligaPlayerList();
    this.filter = this.storageService.getItem('filter');
    this.onChangeFilter();
  }

  ngOnInit(): void {}

  private getBundesligaClubList(): void {
    this.apiService.club.getBundesligaClubList().subscribe((data) => {
      this.clubList = data;
    });
  }

  private getBundesligaPlayerList(): void {
    // TODO: cache
    /*if (this.storageService.getItem('player')) {
      const json = this.storageService.getItem('player');
      // tslint:disable-next-line:forin
      for (const element in json) {
        this.playerList.push(json[element]);
      }
      return;
    }*/

    this.loading = true;
    this.apiService.player.getBundesligaPlayerList().subscribe(
      (data) => {
        this.playerList = data;
        //this.storageService.setItem('player', data.reverse());
      },
      (error) => {},
      () => {
        this.loading = false;
        this.onChangeFilter();
      }
    );
  }

  public isPlayerVisible(player: any): boolean {
    if (this.filter.player.items.selectedPositions.length > 0) {
      if (!this.filter.player.items.selectedPositions.includes(player.season_list[0].position)) {
        return false;
      }
    }

    if (this.filter.player.items.selectedClubs.length > 0) {
      if (!this.filter.player.items.selectedClubs.includes(player.club_list[0].club_id)) {
        return false;
      }
    }

    if (this.filter.player.items.range.min) {
      if (+player.season_list[0].price / 1000000 < this.filter.player.items.range.min) {
        return false;
      }
    }

    if (this.filter.player.items.range.max) {
      if (+player.season_list[0].price / 1000000 > this.filter.player.items.range.max) {
        return false;
      }
    }
    return true;
  }

  private onChangeFilter(): void {
    this.filteredPlayerList = [];
    this.playerList.forEach((player) => {
      if (this.isPlayerVisible(player)) {
        this.filteredPlayerList.push(player);
      }
    });
  }

  public onSelectClub(clubId: string): void {
    if (!this.filter.player.items.selectedClubs.includes(clubId)) {
      // add to selection
      this.filter.player.items.selectedClubs.push(clubId);
    } else {
      // remove from selection
      const index = this.filter.player.items.selectedClubs.indexOf(clubId, 0);
      this.filter.player.items.selectedClubs.splice(index, 1);
    }
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }
  public isClubSelected(clubId: string): boolean {
    if (this.filter.player.items.selectedClubs.length === 0) {
      return true;
    } else {
      if (this.filter.player.items.selectedClubs.includes(clubId)) {
        return true;
      } else {
        return false;
      }
    }
  }
  public onClearClubSelection(): void {
    this.filter.player.items.selectedClubs = [];
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }

  public onSelectPosition(position: string): void {
    if (!this.filter.player.items.selectedPositions.includes(position)) {
      // add to selection
      this.filter.player.items.selectedPositions.push(position);
    } else {
      // remove from selection
      const index = this.filter.player.items.selectedPositions.indexOf(position, 0);
      this.filter.player.items.selectedPositions.splice(index, 1);
    }

    if (this.filter.player.items.selectedPositions.length === 4) {
      this.onClearPositionSelection();
    }
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }
  public isPositionSelected(position: string): boolean {
    if (this.filter.player.items.selectedPositions.length === 0) {
      return true;
    } else {
      if (this.filter.player.items.selectedPositions.includes(position)) {
        return true;
      } else {
        return false;
      }
    }
  }
  public onClearPositionSelection(): void {
    this.filter.player.items.selectedPositions = [];
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }

  public onSetMinRange(minValue): void {
    this.filter.player.items.range.min = +minValue.value;
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }
  public onSetMaxRange(maxValue): void {
    this.filter.player.items.range.max = +maxValue.value;
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }
  public onClearRangeSelection(): void {
    this.filter.player.items.range.min = null;
    this.filter.player.items.range.max = null;
    this.storageService.setItem('filter', this.filter);
    this.onChangeFilter();
  }

  public onToggleFilter(): void {
    this.filter.player.isExpanded = !this.filter.player.isExpanded;
    this.storageService.setItem('filter', this.filter);
  }

  public onLoad(amount: number = 9999): void {
    this.maxLimit += amount;
  }

  public getPositionName(position): string {
    // TODO: make more performance - attribute in model or something
    return Position[position];
  }

  public sortBy(key: 'player_name' | 'price' | 'points') {
    switch (key) {
      case 'player_name':
        this.filteredPlayerList.sort(this.sortByPlayerName);
        break;
      case 'points':
        this.filteredPlayerList.sort(this.sortByPoints);
        break;
      case 'price':
        this.filteredPlayerList.sort(this.sortByPrice);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.filteredPlayerList.reverse();
    }
  }

  private sortByPlayerName(a: any, b: any) {
    if (a.displayname < b.displayname) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByPrice(a: any, b: any) {
    if (+a.season_list[0].price === +b.season_list[0].price) {
      if (+a.points < +b.points) {
        return -1;
      } else {
        return 1;
      }
    } else if (+a.season_list[0].price < +b.season_list[0].price) {
      return -1;
    } else {
      return 1;
    }
  }

  private sortByPoints(a: any, b: any) {
    if (+a.points === +b.points) {
      if (+a.season_list[0].price === +b.season_list[0].price) {
        if (a.displayname < b.displayname) {
          return -1;
        } else {
          return 1;
        }
      } else if (+a.season_list[0].price < +b.season_list[0].price) {
        return -1;
      } else {
        return 1;
      }
    } else if (+a.points < +b.points) {
      return -1;
    } else {
      return 1;
    }
  }
}
