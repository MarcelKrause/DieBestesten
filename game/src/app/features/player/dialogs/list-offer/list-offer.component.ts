import { Component, Inject, isDevMode, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-list-offer',
  templateUrl: './list-offer.component.html',
  styleUrls: ['./list-offer.component.scss'],
})
export class ListOfferComponent implements OnInit {
  seasonId: string;
  transferwindow;
  groupedOfferList: any;

  onlyOwnOffers = false;
  selectedPositions = [];
  loading = false;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private apiService: ApiService) {
    this.seasonId = this.apiService.status.season.season_id;
    this.transferwindow = data.transferwindow;
    this.getGroupedOfferList();
  }

  ngOnInit(): void {}

  private getGroupedOfferList(): void {
    this.loading = true;
    this.apiService.offer.getOfferListByTransferwindow(this.transferwindow.transferwindow_id).subscribe((data) => {
      this.groupedOfferList = data;
      this.loading = true;

      this.groupedOfferList.forEach((offer) => {
        if (offer.status === 'pending') {
        }
      });
    });
  }

  public hasParticipated(groupedOffer: any): boolean {
    if (groupedOffer.offer_list.filter((offer) => offer.team_id === this.apiService.status.team.team_id).length > 0) {
      return true;
    } else {
      return false;
    }
  }

  public isOwnTeam(teamId: string): boolean {
    if (this.apiService.status.team.team_id === teamId) {
      return true;
    } else {
      return false;
    }
  }

  public getOwnOfferValue(groupedOffer: any): number {
    const ownOffer = groupedOffer.offer_list.filter((offer) => offer.team_id === this.apiService.status.team.team_id);
    if (ownOffer.length > 0) {
      return ownOffer[0].offer_value;
    } else {
      return null;
    }
  }

  public isVisible(groupedOffer: any): boolean {
    let isVisible = true;
    if (this.onlyOwnOffers) {
      if (!this.hasParticipated(groupedOffer)) {
        isVisible = false;
      }
    }
    if (this.selectedPositions.length > 0) {
      if (!this.selectedPositions.includes(groupedOffer.player.season_list[0].position)) {
        return false;
      }
    }

    return isVisible;
  }

  public isPositionSelected(position: string): boolean {
    if (this.selectedPositions.length === 0) {
      return true;
    } else {
      if (this.selectedPositions.includes(position)) {
        return true;
      } else {
        return false;
      }
    }
  }

  public onSelectPosition(position: string): void {
    if (!this.selectedPositions.includes(position)) {
      // add to selection
      this.selectedPositions.push(position);
    } else {
      // remove from selection
      const index = this.selectedPositions.indexOf(position, 0);
      this.selectedPositions.splice(index, 1);
    }

    if (this.selectedPositions.length === 4) {
      this.selectedPositions = [];
    }
  }

  public getTimestampString(offer: any): string {
    let date = new Date(offer.offer_date);
    let day = ('0' + date.getDate()).slice(-2);
    let month = ('0' + (date.getMonth() + 1)).slice(-2);
    let year = date.getFullYear();
    let hours = ('0' + date.getHours()).slice(-2);
    let minutes = ('0' + date.getMinutes()).slice(-2);
    return `${day}.${month}.${year} - ${hours}:${minutes} Uhr`;
  }

  public isDev(): boolean {
    return isDevMode();
  }
}
