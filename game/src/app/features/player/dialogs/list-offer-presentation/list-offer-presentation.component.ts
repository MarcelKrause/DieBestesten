import { Component, ElementRef, Inject, OnInit, Renderer2, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-list-offer-presentation',
  templateUrl: './list-offer-presentation.component.html',
  styleUrls: ['./list-offer-presentation.component.scss'],
})
export class ListOfferPresentationComponent implements OnInit {
  seasonId: string;
  transferwindow;
  groupedOfferList: any;
  loading = false;

  index: number = 0;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private apiService: ApiService, private renderer: Renderer2, private elem: ElementRef) {
    this.seasonId = this.apiService.status.season.season_id;
    this.transferwindow = data.transferwindow;
    this.getGroupedOfferList();
  }

  ngOnInit(): void {}

  private getGroupedOfferList(): void {
    this.loading = true;
    this.apiService.offer.getOfferListByTransferwindow(this.transferwindow.transferwindow_id).subscribe((data) => {
      this.groupedOfferList = data.reverse();
      this.loading = false;

      this.groupedOfferList.forEach((offer) => {
        if (offer.status === 'pending') {
        }
      });
    });
  }

  public onUpdateIndex(direction: number): void {
    this.index = this.index + direction;
    this.refresh();
  }

  private refresh(): void {
    let fadeInElements = this.elem.nativeElement.querySelectorAll('.fade-in');

    fadeInElements.forEach((element) => {
      element.classList.remove('fade-in');
      setTimeout(() => {
        element.classList.add('fade-in');
      }, 1);
    });

    let blurInElements = this.elem.nativeElement.querySelectorAll('.blur-in');

    blurInElements.forEach((element) => {
      element.classList.remove('blur-in');
      setTimeout(() => {
        element.classList.add('blur-in');
      }, 1);
    });
  }
}
