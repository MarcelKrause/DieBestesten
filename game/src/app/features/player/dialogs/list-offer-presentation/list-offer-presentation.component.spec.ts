import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ListOfferPresentationComponent } from './list-offer-presentation.component';

describe('ListOfferPresentationComponent', () => {
  let component: ListOfferPresentationComponent;
  let fixture: ComponentFixture<ListOfferPresentationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfferPresentationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfferPresentationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
