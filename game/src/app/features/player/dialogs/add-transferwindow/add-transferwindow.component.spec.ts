import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AddTransferwindowComponent } from './add-transferwindow.component';

describe('AddTransferwindowComponent', () => {
  let component: AddTransferwindowComponent;
  let fixture: ComponentFixture<AddTransferwindowComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTransferwindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTransferwindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
