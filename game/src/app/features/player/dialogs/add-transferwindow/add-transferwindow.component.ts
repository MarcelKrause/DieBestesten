import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ApiService } from 'src/app/core/services/api/api.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-add-transferwindow',
  templateUrl: './add-transferwindow.component.html',
  styleUrls: ['./add-transferwindow.component.scss'],
})
export class AddTransferwindowComponent implements OnInit {
  startDateDay: Date;
  startDateHour: number;
  startDateMinutes: number;

  endDateDay: Date;
  endDateHour: number;
  endDateMinutes: number;

  startDateString: string;
  endDateString: string;

  constructor(
    private dialog: MatDialogRef<AddTransferwindowComponent>,
    private apiService: ApiService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.startDateDay = new Date();
    this.endDateDay = new Date();
  }

  public onCancel(): void {
    this.dialog.close();
  }

  public onSubmit(): void {
    if (this.startDateDay == null || this.startDateHour == null || this.startDateMinutes == null) {
      this.notificationService.push('Startzeitpunkt nicht korrekt', 'negative');
      return;
    }

    if (this.endDateDay == null || this.endDateHour == null || this.endDateMinutes == null) {
      this.notificationService.push('Endzeitpunkt nicht korrekt', 'negative');
      return;
    }

    this.startDateString = `${this.startDateDay} ${this.startDateHour}:${this.startDateMinutes}:00`;
    this.endDateString = `${this.endDateDay} ${this.endDateHour}:${this.endDateMinutes}:00`;

    this.apiService.transferwindow.postTransferwindow(this.startDateString, this.endDateString).subscribe(
      (data) => {
        this.dialog.close();
      },
      (error) => {
        this.notificationService.push(error, 'negative');
      }
    );
  }
}
