import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-edit-offer',
  templateUrl: './edit-offer.component.html',
  styleUrls: ['./edit-offer.component.scss'],
})
export class EditOfferComponent implements OnInit {
  step = 1;

  player: any;
  offer: any;
  initialOfferValue: number;
  offerValue: number;

  offerList: any[];
  budget: number;

  price: number;

  offerE10000000: number;
  offerE1000000: number;
  offerE100000: number;
  offerE10000: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private dialog: MatDialogRef<EditOfferComponent>,
    private apiService: ApiService,
    private router: Router
  ) {
    this.offer = data.offer;
    this.initialOfferValue = +this.offer.offer_value;
    this.offerValue = this.initialOfferValue;
    this.getPlayer(this.offer.displayname);

    this.getBudget();
    this.getOfferList();
  }

  ngOnInit(): void {}

  private getPlayer(displayname: string): void {
    this.apiService.player.getPlayerByDisplayname(displayname).subscribe((data) => {
      this.player = data;
      this.price = +this.player.season_list[0].price;
      this.splitOfferValue();
    });
  }

  public onNext(): void {
    this.apiService.offer.patchOffer(this.offer.offer_id, this.offerValue).subscribe((data) => {
      this.step++;
    });
  }

  public onCancel(): void {
    this.dialog.close(false);
  }

  public onMarket(): void {
    this.dialog.close(false);
    this.router.navigate(['/spieler/transfermarkt']);
  }

  public onOffer(): void {
    this.dialog.close(true);
    this.router.navigate(['/spieler/transfermarkt/gebote']);
  }

  private getBudget(): void {
    const teamId = this.apiService.status.team.team_id;
    this.apiService.team.getTeam(teamId).subscribe((data) => {
      this.budget = +data.budget;
    });
  }

  private getOfferList(): void {
    this.apiService.offer.getOwnOfferList().subscribe((data) => {
      this.offerList = data.offer_list;
    });
  }

  public getOpenOfferValue(): number {
    let offerValue = 0;
    if (this.offerList) {
      this.offerList.forEach((offer) => {
        if (offer.status === 'pending') {
          offerValue += +offer.offer_value;
        }
      });
    }
    return offerValue;
  }

  private splitOfferValue(): void {
    const offerValueString = this.offerValue.toString();

    this.offerE10000000 = +offerValueString.charAt(offerValueString.length - 8);
    this.offerE1000000 = +offerValueString.charAt(offerValueString.length - 7);
    this.offerE100000 = +offerValueString.charAt(offerValueString.length - 6);
    this.offerE10000 = +offerValueString.charAt(offerValueString.length - 5);
  }

  public updateOffer(value: number, property: string): void {
    switch (property) {
      case 'offerE10000000':
        this.offerE10000000 += value;
        break;
      case 'offerE1000000':
        this.offerE1000000 += value;
        break;
      case 'offerE100000':
        this.offerE100000 += value;
        break;
      case 'offerE10000':
        this.offerE10000 += value;
        break;
    }

    if (this.offerE10000 > 9) {
      this.offerE100000 += 1;
      this.offerE10000 = 0;
    }
    if (this.offerE10000 < 0) {
      this.offerE10000 = 0;
    }

    if (this.offerE1000000 > 9) {
      this.offerE10000000 += 1;
      this.offerE1000000 = 0;
    }
    if (this.offerE1000000 < 0) {
      this.offerE1000000 = 0;
    }

    if (this.offerE100000 > 9) {
      this.offerE1000000 += 1;
      this.offerE100000 = 0;
    }
    if (this.offerE100000 < 0) {
      this.offerE100000 = 0;
    }

    if (this.offerE10000000 > 9) {
      this.offerE10000000 = 9;
    }
    if (this.offerE10000000 < 0) {
      this.offerE10000000 = 0;
    }

    this.onChangeOfferValue();
  }

  public onChangeInput(input: any, property: string): void {
    switch (property) {
      case 'offerE10000000':
        this.offerE10000000 = input.value;
        break;
      case 'offerE1000000':
        this.offerE1000000 = input.value;
        break;
      case 'offerE100000':
        this.offerE100000 = input.value;
        break;
      case 'offerE10000':
        this.offerE10000 = input.value;
        break;
    }

    this.onChangeOfferValue();
  }

  public onChangeOfferValue(): void {
    this.offerValue = +`${this.offerE10000000}${this.offerE1000000}${this.offerE100000}${this.offerE10000}0000`;
  }

  public onAllIn(): void {
    const maxOfferValueString = (this.budget - this.getOpenOfferValue()).toString();

    this.offerE10000000 = +maxOfferValueString.charAt(maxOfferValueString.length - 8);
    this.offerE1000000 = +maxOfferValueString.charAt(maxOfferValueString.length - 7);
    this.offerE100000 = +maxOfferValueString.charAt(maxOfferValueString.length - 6);
    this.offerE10000 = +maxOfferValueString.charAt(maxOfferValueString.length - 5);
    this.onChangeOfferValue();
  }

  public isValidOffer(): boolean {
    if (this.offerValue >= this.price && this.offerValue <= this.budget) {
      return true;
    } else {
      return false;
    }
  }
}
