// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlayerRoutingModule } from './player.routing';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CoreModule } from 'src/app/core/core.module';
import { MatTooltipModule } from '@angular/material/tooltip';

// Components
import { MarketComponent } from './pages/market/market.component';
import { OffersComponent } from './pages/offers/offers.component';
import { DetailComponent } from './pages/detail/detail.component';
import { ListComponent } from './pages/list/list.component';
import { TransferwindowComponent } from './pages/transferwindow/transferwindow.component';
import { SentOfferComponent } from './dialogs/sent-offer/sent-offer.component';
import { ListOfferComponent } from './dialogs/list-offer/list-offer.component';
import { EditOfferComponent } from './dialogs/edit-offer/edit-offer.component';

import { AddTransferwindowComponent } from './dialogs/add-transferwindow/add-transferwindow.component';
import { ListOfferPresentationComponent } from './dialogs/list-offer-presentation/list-offer-presentation.component';
import { OfferComponent } from './pages/offer/offer.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [CommonModule, PlayerRoutingModule, CoreModule, RouterModule, FormsModule, SharedModule, MatTooltipModule],
  declarations: [
    MarketComponent,
    OffersComponent,
    DetailComponent,
    ListComponent,
    TransferwindowComponent,
    SentOfferComponent,
    ListOfferComponent,
    EditOfferComponent,
    AddTransferwindowComponent,
    ListOfferPresentationComponent,
    OfferComponent,
  ],
})
export class PlayerModule {}
