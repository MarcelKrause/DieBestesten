// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { ListComponent } from './pages/list/list.component';
import { MarketComponent } from './pages/market/market.component';
import { OffersComponent } from './pages/offers/offers.component';
import { TransferwindowComponent } from './pages/transferwindow/transferwindow.component';
import { DetailComponent } from './pages/detail/detail.component';
import { OfferComponent } from './pages/offer/offer.component';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'details/:displayname', component: DetailComponent },
  {
    path: 'transfermarkt',
    children: [
      { path: '', component: MarketComponent },
      { path: 'gebote', component: OffersComponent },
      { path: 'angebote', component: OfferComponent },
      { path: 'phasen', component: TransferwindowComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PlayerRoutingModule {}
