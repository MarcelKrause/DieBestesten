import { Component, OnInit } from '@angular/core';
import { Manager } from 'src/app/core/models/manager/manager';
import { ApiService } from 'src/app/core/services/api/api.service';
import { MatDialog } from '@angular/material/dialog';
import { now } from 'moment';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  managerList: Manager[];

  events = [
    {
      date: new Date('2025-05-17'),
      title: 'Saisonfinale',
    },
    {
      date: new Date('2024-12-20'),
      title: 'Kegeln',
    },
    {
      date: new Date('2024-08-23'),
      title: 'Saisonstart',
    },
    {
      date: new Date('2024-08-05'),
      title: 'Auslosung',
    },
  ];

  orderBy: 'manager_name' | 'seasons' | 'average' | 'points' = 'points';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  constructor(public apiService: ApiService, private dialog: MatDialog) {}

  ngOnInit(): void {
    this.getManagerList();
  }

  private getManagerList(): void {
    this.apiService.manager.getManagerList().subscribe((data) => {
      this.managerList = [];
      data.forEach((element) => {
        this.managerList.push(new Manager(element));
      });
    });
  }

  public getHonourByManagerCount(manager: Manager): number {
    let honourCount = 0;

    manager.teamList.forEach((team) => {
      honourCount += team.honourList.filter((honour) => honour.honourName === 'Meisterschaft').length;
    });

    return honourCount;
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  public sortBy(key: 'manager_name' | 'seasons' | 'average' | 'points') {
    switch (key) {
      case 'manager_name':
        this.managerList.sort(this.sortByManagerName);
        break;
      case 'seasons':
        this.managerList.sort(this.sortBySeasons);
        break;
      case 'average':
        this.managerList.sort(this.sortByAverage);
        break;
      case 'points':
        this.managerList.sort(this.sortByPoints);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.managerList.reverse();
    }
  }

  private sortByManagerName(a: Manager, b: Manager) {
    if (a.managerName < b.managerName) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortBySeasons(a: Manager, b: Manager) {
    if (a.teamList.length === b.teamList.length) {
      if (a.points === b.points) {
        if (a.managerName > b.managerName) {
          return 1;
        } else {
          return -1;
        }
      } else if (a.points > b.points) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.teamList.length > b.teamList.length) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByAverage(a: Manager, b: Manager) {
    if (a.getAveragePoints() === b.getAveragePoints()) {
      if (a.points === b.points) {
        if (a.teamList.length > b.teamList.length) {
          return 1;
        } else {
          return -1;
        }
      } else if (a.points > b.points) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.getAveragePoints() > b.getAveragePoints()) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByPoints(a: Manager, b: Manager) {
    if (a.points === b.points) {
      if (a.teamList.length === b.teamList.length) {
        if (a.managerName > b.managerName) {
          return 1;
        } else {
          return -1;
        }
      } else if (a.teamList.length > b.teamList.length) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.points > b.points) {
      return 1;
    } else {
      return -1;
    }
  }
}
