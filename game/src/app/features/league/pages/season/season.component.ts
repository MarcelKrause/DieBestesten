import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/core/models/team/team';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-season',
  templateUrl: './season.component.html',
  styleUrls: ['./season.component.scss'],
})
export class SeasonComponent implements OnInit {
  seasonName: string;
  seasonList: string[];
  matchdayList: number[];

  minMatchdayNumber = 1;
  maxMatchdayNumber = 34;

  season: any;
  table: Team[];

  settingsType: string;

  orderBy:
    | 'team_name'
    | 'sds'
    | 'goals'
    | 'assists'
    | 'clean_sheets'
    | 'pos_goalkeeper'
    | 'pos_defender'
    | 'pos_midfielder'
    | 'pos_forward'
    | 'missed_goals'
    | 'missed_points'
    | 'maximal_points'
    | 'percentage'
    | 'points' = 'points';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  mode: 'stats' | 'position' | 'formation' | 'financial' = 'stats';

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
    this.seasonName = this.route.snapshot.paramMap.get('season_name');
    this.getSeasonTable(this.seasonName);
    this.getSeasonList();
    this.createMatchdayList();
    this.settingsType = 'complete';
  }

  ngOnInit(): void {}

  private getSeasonTable(seasonName: string): void {
    this.apiService.teamRating.getSeasonTable(seasonName).subscribe((data) => {
      this.season = data.season;
      this.table = [];
      data.table.forEach((element) => {
        this.table.push(new Team(element));
      });
    });
  }

  private getSeasonTableRange(seasonName: string, minMatchdayNumber: number, maxMatchdayNumber: number): void {
    this.apiService.teamRating.getSeasonTableRange(seasonName, minMatchdayNumber, maxMatchdayNumber).subscribe((data) => {
      this.season = data.season;
      this.table = [];
      data.table.forEach((element) => {
        this.table.push(new Team(element));
      });
    });
  }

  public getSeasonList(): void {
    this.seasonList = [];
    this.apiService.season.getSeasonList().subscribe((data) => {
      data.forEach((element) => {
        this.seasonList.push(element.season_name);
      });
    });
  }

  public onSelectSeason(target: any): void {
    this.seasonName = this.formatSeasonToUrl(target.value);
    this.getSeasonTable(this.seasonName);
    this.createMatchdayList();
  }

  public onChangeSettings(value: string): void {
    this.settingsType = value;

    switch (value) {
      case 'complete':
        this.getSeasonTable(this.seasonName);
        break;
      case 'range':
        this.getSeasonTableRange(this.seasonName, this.minMatchdayNumber, this.maxMatchdayNumber);
        break;
    }
  }

  public onSelectMinMatchdayNumber(target: any): void {
    this.minMatchdayNumber = +target.value;
    this.getSeasonTableRange(this.seasonName, this.minMatchdayNumber, this.maxMatchdayNumber);
  }

  public onSelectMaxMatchdayNumber(target: any): void {
    this.maxMatchdayNumber = +target.value;
    this.getSeasonTableRange(this.seasonName, this.minMatchdayNumber, this.maxMatchdayNumber);
  }

  public formatSeasonToUrl(season: string): string {
    if (season) {
      return season.replace('-', '/');
    }
  }

  public formatSeasonToOriginal(season: string): string {
    if (season) {
      return season.replace('-', '/');
    }
  }

  private createMatchdayList(): void {
    // TODO:
    if (!this.apiService.status) {
      return;
    }

    let maxMatchdayNumber;
    if (this.formatSeasonToOriginal(this.seasonName) === this.apiService.status.season.season_name) {
      maxMatchdayNumber = +this.apiService.status.matchday.number;
    } else {
      maxMatchdayNumber = 34;
    }

    this.matchdayList = [];
    for (let i = maxMatchdayNumber; i >= 1; i--) {
      this.matchdayList.push(i);
    }
  }

  public sortBy(
    key:
      | 'team_name'
      | 'sds'
      | 'goals'
      | 'assists'
      | 'clean_sheets'
      | 'pos_goalkeeper'
      | 'pos_defender'
      | 'pos_midfielder'
      | 'pos_forward'
      | 'missed_goals'
      | 'missed_points'
      | 'maximal_points'
      | 'percentage'
      | 'points'
  ) {
    switch (key) {
      case 'team_name':
        this.table.sort(this.sortByTeamName);
        break;
      case 'sds':
        this.table.sort(this.sortBySDS);
        break;
      case 'goals':
        this.table.sort(this.sortByGoals);
        break;
      case 'assists':
        this.table.sort(this.sortByAssists);
        break;
      case 'clean_sheets':
        this.table.sort(this.sortByCleanSheets);
        break;
      case 'pos_goalkeeper':
        this.table.sort(this.sortByPositionGoalkeeper);
        break;
      case 'pos_defender':
        this.table.sort(this.sortByPositionDefender);
        break;
      case 'pos_midfielder':
        this.table.sort(this.sortByPositionMidfielder);
        break;
      case 'pos_forward':
        this.table.sort(this.sortByPositionForward);
        break;
      case 'missed_goals':
        this.table.sort(this.sortByMissedGoals);
        break;
      case 'missed_points':
        this.table.sort(this.sortByMissedPoints);
        break;
      case 'maximal_points':
        this.table.sort(this.sortByMaximalPoints);
        break;
      case 'percentage':
        this.table.sort(this.sortByPercentage);
        break;
      case 'points':
        this.table.sort(this.sortByPoints);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.table.reverse();
    }
  }

  private sortByTeamName(a: Team, b: Team) {
    if (a.teamName < b.teamName) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortBySDS(a: Team, b: Team) {
    if (a.sds === b.sds) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.sds < b.sds) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByGoals(a: Team, b: Team) {
    if (a.goals === b.goals) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.goals < b.goals) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByAssists(a: Team, b: Team) {
    if (a.assists === b.assists) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.assists < b.assists) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByCleanSheets(a: Team, b: Team) {
    if (a.clean_sheet === b.clean_sheet) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.clean_sheet < b.clean_sheet) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByPositionGoalkeeper(a: Team, b: Team) {
    if (a.points_goalkeeper === b.points_goalkeeper) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.points_goalkeeper < b.points_goalkeeper) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByPositionDefender(a: Team, b: Team) {
    if (a.points_defender === b.points_defender) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.points_defender < b.points_defender) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByPositionMidfielder(a: Team, b: Team) {
    if (a.points_midfielder === b.points_midfielder) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.points_midfielder < b.points_midfielder) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByPositionForward(a: Team, b: Team) {
    if (a.points_forward === b.points_forward) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.points_forward < b.points_forward) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByMissedGoals(a: Team, b: Team) {
    if (a.missed_goals === b.missed_goals) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.missed_goals < b.missed_goals) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByMissedPoints(a: Team, b: Team) {
    if (a.maximal_points - a.points === b.maximal_points - b.points) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.maximal_points - a.points < b.maximal_points - b.points) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByMaximalPoints(a: Team, b: Team) {
    if (a.maximal_points === b.maximal_points) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a.maximal_points < b.maximal_points) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByPercentage(a: Team, b: Team) {
    const a_percentage = (a.points / a.maximal_points) * 100;
    const b_percentage = (b.points / b.maximal_points) * 100;

    if (a_percentage === b_percentage) {
      if (a.points < b.points) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (a_percentage < b_percentage) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private sortByPoints(a: Team, b: Team) {
    if (a.points === b.points) {
      if (a.teamName > b.teamName) {
        return -1;
      } else {
        return 1;
      }
    }
    if (a.points > b.points) {
      return 1;
    } else {
      return -1;
    }
  }

  public formatSeason(season: string): string {
    if (season) {
      return season.replace('/', '-');
    }
  }

  public formatTeam(team: string): string {
    if (team) {
      return team.replace(' ', '_');
    }
  }
}
