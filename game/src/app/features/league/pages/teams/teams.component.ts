import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class TeamsComponent implements OnInit {
  teamList: any;
  season: any;

  orderBy: 'team_name' | 'average_price' | 'total_price' = 'total_price';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  constructor(private apiService: ApiService) {
    this.season = this.apiService.status.season;
    this.getTeamList();
  }

  ngOnInit(): void {}

  private getTeamList(): void {
    this.apiService.team.getTeamListBySeason(this.season.season_id).subscribe((data) => {
      this.teamList = data;
      // this.teamList.sort(this.sortByAveragePrice);
    });
  }

  public getTotalTeamSize(squadCount: any): number {
    return squadCount['goalkeeper'] + squadCount['defender'] + squadCount['midfielder'] + squadCount['forward'];
  }

  public formateSquadCount(squadCount: any): string {
    return `${squadCount['goalkeeper']}-${squadCount['defender']}-${squadCount['midfielder']}-${squadCount['forward']}`;
  }

  public sortBy(key: 'team_name' | 'average_price' | 'total_price') {
    switch (key) {
      case 'team_name':
        this.teamList.sort(this.sortByTeamName);
        break;
      case 'average_price':
        this.teamList.sort(this.sortByAveragePrice);
        break;
      case 'total_price':
        this.teamList.sort(this.sortByTotalPrice);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.teamList.reverse();
    }
  }

  private sortByTeamName(a: any, b: any) {
    if (a.team_name < b.team_name) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByAveragePrice(a: any, b: any) {
    if (
      +a.total_price /
        (a.squad_count['goalkeeper'] + a.squad_count['defender'] + a.squad_count['midfielder'] + a.squad_count['forward']) ===
      +b.total_price / (b.squad_count['goalkeeper'] + b.squad_count['defender'] + b.squad_count['midfielder'] + b.squad_count['forward'])
    ) {
      if (a.team_name > b.team_name) {
        return 1;
      } else {
        return -1;
      }
    }
    if (
      a.total_price / (a.squad_count['goalkeeper'] + a.squad_count['defender'] + a.squad_count['midfielder'] + a.squad_count['forward']) >
      b.total_price / (b.squad_count['goalkeeper'] + b.squad_count['defender'] + b.squad_count['midfielder'] + b.squad_count['forward'])
    ) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByTotalPrice(a: any, b: any) {
    if (+a.total_price === +b.total_price) {
      if (a.team_name > b.team_name) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.total_price > b.total_price) {
      return 1;
    } else {
      return -1;
    }
  }

  public formatSeason(season: string): string {
    if (season) {
      return season.replace('/', '-');
    }
  }

  public formatTeam(team: string): string {
    if (team) {
      return team.replace(' ', '_');
    }
  }
}
