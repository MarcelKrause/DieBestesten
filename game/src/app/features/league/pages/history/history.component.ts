import { Component } from '@angular/core';
import { Manager } from 'src/app/core/models/manager/manager';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'db-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss'],
})
export class HistoryComponent {
  manager_list: Manager[];

  orderBy: 'manager_name' | 'seasons' | 'average' | 'points' = 'points';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  constructor(public api_service: ApiService) {
    this.getManagerList();
  }

  private getManagerList(): void {
    this.api_service.manager.getManagerList().subscribe((data) => {
      this.manager_list = [];
      data.forEach((element) => {
        this.manager_list.push(new Manager(element));
      });
    });
  }

  public getHonourByManagerCount(manager: Manager): number {
    let honourCount = 0;

    manager.teamList.forEach((team) => {
      honourCount += team.honourList.filter((honour) => honour.honourName === 'Meisterschaft').length;
    });

    return honourCount;
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  public sortBy(key: 'manager_name' | 'seasons' | 'average' | 'points') {
    switch (key) {
      case 'manager_name':
        this.manager_list.sort(this.sortByManagerName);
        break;
      case 'seasons':
        this.manager_list.sort(this.sortBySeasons);
        break;
      case 'average':
        this.manager_list.sort(this.sortByAverage);
        break;
      case 'points':
        this.manager_list.sort(this.sortByPoints);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.manager_list.reverse();
    }
  }

  private sortByManagerName(a: Manager, b: Manager) {
    if (a.managerName < b.managerName) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortBySeasons(a: Manager, b: Manager) {
    if (a.teamList.length === b.teamList.length) {
      if (a.points === b.points) {
        if (a.managerName > b.managerName) {
          return 1;
        } else {
          return -1;
        }
      } else if (a.points > b.points) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.teamList.length > b.teamList.length) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByAverage(a: Manager, b: Manager) {
    if (a.getAveragePoints() === b.getAveragePoints()) {
      if (a.points === b.points) {
        if (a.teamList.length > b.teamList.length) {
          return 1;
        } else {
          return -1;
        }
      } else if (a.points > b.points) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.getAveragePoints() > b.getAveragePoints()) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByPoints(a: Manager, b: Manager) {
    if (a.points === b.points) {
      if (a.teamList.length === b.teamList.length) {
        if (a.managerName > b.managerName) {
          return 1;
        } else {
          return -1;
        }
      } else if (a.teamList.length > b.teamList.length) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.points > b.points) {
      return 1;
    } else {
      return -1;
    }
  }
}
