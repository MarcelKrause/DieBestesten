import { Component } from '@angular/core';
import { Team } from 'src/app/core/models/team/team';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'db-powerranking',
  templateUrl: './powerranking.component.html',
  styleUrls: ['./powerranking.component.scss'],
})
export class PowerrankingComponent {
  manager_list: any[];
  league_table: Team[];
  // TODO: hardcoded season_id
  season_id: string = '324ea2d2-4368-11ef-ab96-c81f66ca5914';

  constructor(private api_service: ApiService) {
    this.getSeasonTable();
    this.getManagerList();
  }

  private getSeasonTable(): void {
    this.api_service.teamRating.getSeasonTable('2024/2025').subscribe((data) => {
      this.league_table = [];
      data.table.forEach((element) => {
        this.league_table.push(new Team(element));
      });
    });
  }

  private getManagerList(): void {
    this.api_service.powerranking.getPowerrankingOverview().subscribe({
      next: (data) => {
        this.manager_list = data;
        console.log(this.manager_list);
        this.manager_list.sort((a, b) => {
          return this.getDiffOfManager(a) > this.getDiffOfManager(b) ? 1 : -1;
        });
      },
    });
  }

  public getDiffOfSingleTeam(team_id: string, position: number): number {
    let league_position = this.league_table.findIndex((team) => team.teamId === team_id) + 1;
    let powerranking_position = position + 1;
    return Math.abs(league_position - powerranking_position);
  }

  public getDiffOfManager(manager: any): number {
    let diff = 0;
    manager.powerranking.forEach((element, index) => {
      diff += this.getDiffOfSingleTeam(element.team_id, index);
    });
    return diff;
  }
}
