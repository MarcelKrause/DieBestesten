import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PowerrankingComponent } from './powerranking.component';

describe('PowerrankingComponent', () => {
  let component: PowerrankingComponent;
  let fixture: ComponentFixture<PowerrankingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PowerrankingComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PowerrankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
