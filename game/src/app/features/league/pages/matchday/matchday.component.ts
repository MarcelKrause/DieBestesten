import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Team } from 'src/app/core/models/team/team';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-matchday',
  templateUrl: './matchday.component.html',
  styleUrls: ['./matchday.component.scss'],
})
export class MatchdayComponent implements OnInit {
  seasonName: string;
  matchdayNumber: number;

  seasonList: string[];
  matchdayList: number[];

  season: any;
  table: Team[];
  live: boolean;

  orderBy: 'team_name' | 'points' = 'points';
  orderDirection: 'ASC' | 'DESC' = 'DESC';

  mode: 'stats' | 'position' | 'formation' | 'live' = 'stats';

  market_team: any;
  compare_status: string;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
    this.seasonName = this.route.snapshot.paramMap.get('season_name');
    this.matchdayNumber = +this.route.snapshot.paramMap.get('matchday_number');
    this.getSeasonTable(this.seasonName, this.matchdayNumber);
    this.getSeasonList();
    this.createMatchdayList();
    this.getMarketTeam();
  }

  ngOnInit(): void {}

  private getSeasonTable(seasonName: string, matchdayNumber: number): void {
    this.apiService.teamRating.getMatchdayTable(seasonName, matchdayNumber).subscribe((data) => {
      this.live = data.live;
      this.season = data.season;
      this.table = [];
      data.table.forEach((element) => {
        this.table.push(new Team(element));
      });
    });
  }

  private getSeasonList(): void {
    this.seasonList = [];
    this.apiService.season.getSeasonList().subscribe((data) => {
      data.forEach((element) => {
        if (element.matchday_available) {
          this.seasonList.push(element.season_name);
        }
      });
    });
  }

  private getMarketTeam(): void {
    this.market_team = null;
    this.apiService.team.getMarketTeam(this.seasonName, this.matchdayNumber).subscribe((data) => {
      this.market_team = data;
      this.compareMarketTeam();
    });
  }

  public onSelectSeason(target: any): void {
    this.seasonName = this.formatSeasonToUrl(target.value);
    this.getSeasonTable(this.seasonName, this.matchdayNumber);
    this.createMatchdayList();
    this.getMarketTeam();
  }

  public onSelectMatchday(target: any): void {
    this.matchdayNumber = +target.value;
    this.getSeasonTable(this.seasonName, this.matchdayNumber);
    this.getMarketTeam();
  }

  public formatSeasonToOriginal(season: string): string {
    if (season) {
      return season.replace('-', '/');
    }
  }

  public formatSeasonToUrl(season: string): string {
    if (season) {
      return season.replace('/', '-');
    }
  }

  public compareMarketTeam(): void {
    this.table.forEach((element) => {
      if (this.apiService.status.team.team_id == element.teamId) {
        if (this.market_team.points === element.points) {
          this.compare_status = 'draw';
        } else if (element.points > this.market_team.points) {
          this.compare_status = 'victory';
        } else {
          this.compare_status = 'defeat';
        }
      }
    });
  }

  private createMatchdayList(): void {
    if (!this.apiService.status) {
      setTimeout(() => {
        this.createMatchdayList();
      }, 1000);
      return;
    }

    let maxMatchdayNumber;
    if (this.formatSeasonToOriginal(this.seasonName) === this.apiService.status.season.season_name) {
      maxMatchdayNumber = +this.apiService.status.matchday.number;
    } else {
      maxMatchdayNumber = 34;
    }

    this.matchdayList = [];
    for (let i = maxMatchdayNumber; i >= 1; i--) {
      this.matchdayList.push(i);
    }
  }

  public sortBy(key: 'team_name' | 'points') {
    switch (key) {
      case 'team_name':
        this.table.sort(this.sortByTeamName);
        break;
      case 'points':
        this.table.sort(this.sortByPoints);
        break;
    }

    if (this.orderBy === key) {
      if (this.orderDirection === 'ASC') {
        this.orderDirection = 'DESC';
      } else {
        this.orderDirection = 'ASC';
      }
    }
    this.orderBy = key;

    if (this.orderDirection === 'DESC') {
      this.table.reverse();
    }
  }

  private sortByTeamName(a: Team, b: Team) {
    if (a.teamName < b.teamName) {
      return 1;
    } else {
      return -1;
    }
  }

  private sortByPoints(a: Team, b: Team) {
    if (a.points === b.points) {
      if (a.teamName > b.teamName) {
        return 1;
      } else {
        return -1;
      }
    }
    if (a.points > b.points) {
      return 1;
    } else {
      return -1;
    }
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  public formatSeason(season: string): string {
    if (season) {
      return season.replace('/', '-');
    }
  }

  public formatTeam(team: string): string {
    if (team) {
      return team.replace(' ', '_');
    }
  }
}
