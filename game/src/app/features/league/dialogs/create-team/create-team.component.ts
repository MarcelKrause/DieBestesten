import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.scss'],
})
export class CreateTeamComponent implements OnInit {
  step = 1;
  teamname: string;

  preview = false;
  selectedBadge;

  apiService: ApiService;

  constructor(@Inject(MAT_DIALOG_DATA) public data, private dialog: MatDialogRef<CreateTeamComponent>) {
    this.apiService = data.apiService;
    this.teamname = this.apiService.status.last_team.team_name;
  }

  ngOnInit(): void {}

  public onNext(): void {
    this.step++;
  }

  public onBack(): void {
    this.step--;
  }

  public onClose(): void {
    this.dialog.close();
  }

  public onSubmit(): void {
    this.apiService.team.postTeam(this.teamname).subscribe(
      (data) => {
        this.apiService.status.team = data;

        let takeover;
        if (!this.selectedBadge) {
          takeover = {
            season_id: this.apiService.status.last_team.season_id,
            team_id: this.apiService.status.last_team.team_id,
          };
        }

        this.apiService.team.postTeamBadge(data.season_id, data.team_id, this.selectedBadge, takeover).subscribe(
          (badgeResponse) => {
            this.step++;
          },
          (error) => {
            console.error(error);
          }
        );
      },
      (error) => {
        console.error(error);
      }
    );
  }

  public onFileChange(event): void {
    if (event.target.files && event.target.files[0]) {
      this.preview = true;
      const reader = new FileReader();

      reader.onload = (e: any) => {
        document.getElementById('preview').setAttribute('src', e.target.result);
      };
      reader.readAsDataURL(event.target.files[0]);

      this.selectedBadge = event.target.files[0];
    }
  }
}
