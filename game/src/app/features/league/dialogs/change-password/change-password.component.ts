import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { NotificationService } from 'src/app/core/services/notification/notification.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  managerName: string;
  step = 1;
  password = '';
  passwordConfirm = '';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    private authService: AuthService,
    private dialog: MatDialogRef<ChangePasswordComponent>,
    private notificationService: NotificationService
  ) {
    this.managerName = data.managerName;
  }

  ngOnInit(): void {}

  public onNext(): void {
    this.step++;
  }

  public onClose(): void {
    this.dialog.close();
  }

  public onSubmit(): void {
    if (this.password !== this.passwordConfirm) {
      this.notificationService.push('Passwörter stimmen nicht überein.', 'negative');
      return;
    }
    if (this.password.length < 6) {
      this.notificationService.push('Dein neues Passwort muss mindestens 6 Zeichen lang sein.', 'negative');
      return;
    }

    this.authService.changePassword(this.password).subscribe(
      (data) => {
        this.step++;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}
