// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeagueRoutingModule } from './league.routing';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

// Components
import { IndexComponent } from './pages/index/index.component';
import { MatchdayComponent } from './pages/matchday/matchday.component';
import { SeasonComponent } from './pages/season/season.component';
import { CreateTeamComponent } from './dialogs/create-team/create-team.component';
import { ChangePasswordComponent } from './dialogs/change-password/change-password.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { PowerrankingComponent } from './pages/powerranking/powerranking.component';
import { HistoryComponent } from './pages/history/history.component';

@NgModule({
  declarations: [IndexComponent, MatchdayComponent, SeasonComponent, CreateTeamComponent, ChangePasswordComponent, TeamsComponent, PowerrankingComponent, HistoryComponent],
  imports: [CommonModule, LeagueRoutingModule, RouterModule, FormsModule, MatTooltipModule],
})
export class LeagueModule {}
