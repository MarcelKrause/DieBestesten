// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { IndexComponent } from './pages/index/index.component';
import { SeasonComponent } from './pages/season/season.component';
import { MatchdayComponent } from './pages/matchday/matchday.component';
import { TeamsComponent } from './pages/teams/teams.component';
import { PowerrankingComponent } from './pages/powerranking/powerranking.component';
import { HistoryComponent } from './pages/history/history.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'pro/:season_name', component: SeasonComponent },
  { path: 'pro/:season_name/:matchday_number', component: MatchdayComponent },
  { path: 'teams', component: TeamsComponent },
  { path: 'powerranking', component: PowerrankingComponent },
  { path: 'ewige-tabelle', component: HistoryComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class LeagueRoutingModule {}
