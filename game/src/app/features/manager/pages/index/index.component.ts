import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Honour } from 'src/app/core/models/honour/honour';
import { Manager } from 'src/app/core/models/manager/manager';
import { ApiService } from 'src/app/core/services/api/api.service';

import Chart from 'chart.js/auto';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent implements OnInit {
  managerName: string;
  manager: Manager;
  honourList: Honour[];
  chartTeamList: any[];

  orderBy: 'season' | 'team_name' | 'points' = 'season';
  orderDirection: 'ASC' | 'DESC' = 'ASC';

  chart: any;

  constructor(private route: ActivatedRoute, private apiService: ApiService, private router: Router) {
    this.route.params.subscribe((params) => {
      this.managerName = this.route.snapshot.paramMap.get('manager_name');
      this.getManager(this.managerName);
    });
  }

  ngOnInit(): void {}

  private getManager(managerName: string): void {
    this.apiService.manager.getManagerByName(managerName).subscribe({
      next: (data) => {
        this.manager = new Manager(data.manager);
        this.chartTeamList = this.manager.teamList.slice().reverse();
        this.honourList = [];
        data.honour_list.forEach((honour) => {
          this.honourList.push(new Honour(honour));
        });
      },
      complete: () => {
        if (this.chart) {
          this.updateChart();
        } else {
          this.generateChart();
        }
      },
    });
  }

  public formatSeason(season: string): string {
    if (season) {
      return season.replace('/', '-');
    }
  }

  public formatTeam(team: string): string {
    if (team) {
      return team.replace(' ', '_');
    }
  }

  public navigateToTeam(team_name: string, team: any): void {
    this.router.navigate(['team', '2024-2025', team_name]);
  }

  public generateChart(): void {
    const div = document.getElementById('season-chart');

    if (!div) {
      setTimeout(() => {
        this.generateChart();
      }, 100);
      return;
    }

    this.chart = new Chart('season-chart', {
      type: 'bar',
      data: {
        labels: this.manager.teamList
          .slice()
          .reverse()
          .map((item) => item.seasonName),
        datasets: [
          {
            data: this.manager.teamList
              .slice()
              .reverse()
              .map((item) => item.points),
            backgroundColor: ['#c01c00'],
          },
        ],
      },
      options: {
        plugins: {
          legend: { display: false },
          tooltip: { enabled: false },
        },
      },
    });
  }

  private updateChart(): void {
    this.chart.data.labels = this.manager.teamList
      .slice()
      .reverse()
      .map((item) => item.seasonName);
    this.chart.data.datasets[0].data = this.manager.teamList
      .slice()
      .reverse()
      .map((item) => item.points);

    this.chart.update();
  }
}
