// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { IndexComponent } from './pages/index/index.component';

const routes: Routes = [{ path: ':manager_name', component: IndexComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class ManagerRoutingModule {}
