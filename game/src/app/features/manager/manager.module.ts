// Modules
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ManagerRoutingModule } from './manager.routing';
import { CoreModule } from 'src/app/core/core.module';
import { MatTooltipModule } from '@angular/material/tooltip';

// Components
import { IndexComponent } from './pages/index/index.component';

@NgModule({
  imports: [CommonModule, RouterModule, ManagerRoutingModule, CoreModule, MatTooltipModule],
  declarations: [IndexComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ManagerModule {}
