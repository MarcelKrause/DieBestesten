import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, isDevMode } from '@angular/core';
import { environment } from 'src/environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { DeviceDetectorService } from 'ngx-device-detector';

// Services
import { AuthService } from '../auth/auth.service';
import { ManagerService } from './manager/manager.service';
import { ClubService } from './club/club.service';
import { TeamService } from './team/team.service';
import { TeamRatingService } from './team_rating/team-rating.service';
import { TeamLineupService } from './team_lineup/team-lineup.service';
import { SeasonService } from './season/season.service';
import { PlayerService } from './player/player.service';
import { PlayerInTeamService } from './player_in_team/player-in-team.service';
import { OfferService } from './offer/offer.service';
import { ProdcastService } from './prodcast/prodcast.service';
import { TransferwindowService } from './transferwindow/transferwindow.service';
import { PowerrankingService } from './powerranking/powerranking.service';

// Components
import { CreateTeamComponent } from 'src/app/features/league/dialogs/create-team/create-team.component';
import { ChangePasswordComponent } from 'src/app/features/league/dialogs/change-password/change-password.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public status;

  public manager: ManagerService;
  public club: ClubService;
  public team: TeamService;
  public teamRating: TeamRatingService;
  public season: SeasonService;
  public player: PlayerService;
  public playerInTeam: PlayerInTeamService;
  public teamLineup: TeamLineupService;
  public offer: OfferService;
  public prodcast: ProdcastService;
  public transferwindow: TransferwindowService;
  public powerranking: PowerrankingService;

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private deviceService: DeviceDetectorService
  ) {
    this.manager = new ManagerService(this.http);
    this.club = new ClubService(this.http);
    this.team = new TeamService(this.http);
    this.teamRating = new TeamRatingService(this.http);
    this.teamLineup = new TeamLineupService(this.http);
    this.season = new SeasonService(this.http);
    this.player = new PlayerService(this.http);
    this.playerInTeam = new PlayerInTeamService(this.http);
    this.offer = new OfferService(this.http);
    this.prodcast = new ProdcastService(this.http);
    this.transferwindow = new TransferwindowService(this.http);
    this.powerranking = new PowerrankingService(this.http);

    if (this.authService.isLoggedIn()) {
      this.getStatus();
    }

    if (this.authService.isLoggedIn()) {
      this.getNotifications();
    }
  }

  public getStatus(): void {
    let deviceType = '';
    if (this.deviceService.isDesktop()) {
      deviceType = 'desktop';
    }
    if (this.deviceService.isTablet()) {
      deviceType = 'tablet';
    }
    if (this.deviceService.isMobile()) {
      deviceType = 'mobile';
    }
    let device = this.deviceService.device;
    let browser = this.deviceService.browser;

    let log_params = '';

    if (window.location.origin != 'http://localhost:4200') {
      log_params = `?device=${device}&deviceType=${deviceType}&browser=${browser}`;
    }

    this.http.get(`${environment.api}/status${log_params}`).subscribe((data) => {
      this.status = data;

      if (!this.status.team) {
        const dialogRef = this.dialog.open(CreateTeamComponent, {
          data: {
            apiService: this,
          },
          disableClose: true,
        });
      }

      if (this.status.manager.status === 'pending') {
        const dialogRef = this.dialog.open(ChangePasswordComponent, {
          data: {
            managerName: this.status.manager.manager_name,
          },
          disableClose: true,
          maxWidth: '480px',
        });
      }
    });
  }

  public loadData(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.authService.isLoggedIn()) {
        this.http.get(`${environment.api}/status`).subscribe((data) => {
          this.status = data;

          if (!this.status.team) {
            const dialogRef = this.dialog.open(CreateTeamComponent, {
              data: {
                apiService: this,
              },
              disableClose: true,
            });
          }

          if (this.status.manager.status === 'pending') {
            const dialogRef = this.dialog.open(ChangePasswordComponent, {
              data: {
                managerName: this.status.manager.manager_name,
              },
              disableClose: true,
              maxWidth: '480px',
            });
          }

          resolve(true);
        });
      } else {
        resolve(true);
      }
    });
  }

  public getNotifications(): Observable<any> {
    return this.http.get(`${environment.api}/notification`);
  }

  public readNotification(notification_id: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('action', 'read');
    return this.http.patch(`${environment.api}/notification/${notification_id}`, body.toString());
  }

  public getSearchResult(searchRequest: string): Observable<any> {
    return this.http.get(`${environment.api}/search/${searchRequest}?filter=bundesliga`);
  }

  public resetSeason(): Observable<any> {
    return this.http.delete(`${environment.api}/setup/`);
  }

  public getKickerData(season: string, matchday: string): Observable<any> {
    let params = '';
    if (season && matchday) {
      params = `?season=${season}&matchday=${matchday}`;
    }
    return this.http.get(`${environment.api}/kicker${params}`);
  }
}
