import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlayerService {
  constructor(private http: HttpClient) {}

  public getBundesligaPlayerList(): Observable<any> {
    return this.http.get(`${environment.api}/player?filter=bundesliga`);
  }

  public getMarketPlayerList(): Observable<any> {
    return this.http.get(`${environment.api}/player?filter=market`);
  }

  public getPlayerByDisplayname(displayname: string): Observable<any> {
    return this.http.get(`${environment.api}/player?displayname=${displayname}`);
  }

  public getSoldPlayers(): Observable<any> {
    return this.http.get(`${environment.api}/player?filter=sold`);
  }

  public getPlayerRating(player_id: string, season_id: string): Observable<any> {
    return this.http.get(`${environment.api}/player_rating/${player_id}?season_id=${season_id}`);
  }
}
