import { TestBed } from '@angular/core/testing';

import { TransferwindowService } from './transferwindow.service';

describe('TransferwindowService', () => {
  let service: TransferwindowService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransferwindowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
