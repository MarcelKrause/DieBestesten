import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TransferwindowService {
  constructor(private http: HttpClient) {}

  public getTransferwindowList(season_id: string): Observable<any> {
    return this.http.get(`${environment.api}/transferwindow?season_id=${season_id}`);
  }

  public postTransferwindow(startDate: string, endDate: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('start_date', startDate);
    body.set('end_date', endDate);

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };
    return this.http.post(`${environment.api}/transferwindow`, body.toString(), options);
  }
}
