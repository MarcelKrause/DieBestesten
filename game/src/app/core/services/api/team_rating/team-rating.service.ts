import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TeamRatingService {
  constructor(private http: HttpClient) {}

  public getMatchdayTable(seasonName: string, matchdayNumber: number): Observable<any> {
    return this.http.get(`${environment.api}/team_rating?season_name=${seasonName}&matchday_number=${matchdayNumber}`);
  }

  public getSeasonTable(seasonName: string): Observable<any> {
    return this.http.get(`${environment.api}/team_rating?season_name=${seasonName}`);
  }

  public getSeasonTableRange(seasonName: string, minMatchdayNumber: number, maxMatchdayNumber: number): Observable<any> {
    return this.http.get(
      `${environment.api}/team_rating?season_name=${seasonName}&min_matchday_number=${minMatchdayNumber}&max_matchday_number=${maxMatchdayNumber}`
    );
  }
}
