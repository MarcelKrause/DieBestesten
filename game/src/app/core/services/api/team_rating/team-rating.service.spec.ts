import { TestBed } from '@angular/core/testing';

import { TeamRatingService } from './team-rating.service';

describe('TeamRatingService', () => {
  let service: TeamRatingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TeamRatingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
