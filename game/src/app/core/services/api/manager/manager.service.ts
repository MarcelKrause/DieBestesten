import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ManagerService {
  constructor(private http: HttpClient) {}

  public getManagerList(): Observable<any> {
    return this.http.get(`${environment.api}/manager`);
  }

  public getManagerByName(managerName: string): Observable<any> {
    return this.http.get(`${environment.api}/manager?manager_name=${encodeURIComponent(managerName)}`);
  }
}
