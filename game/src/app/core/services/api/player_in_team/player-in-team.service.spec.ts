import { TestBed } from '@angular/core/testing';

import { PlayerInTeamService } from './player-in-team.service';

describe('PlayerInTeamService', () => {
  let service: PlayerInTeamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerInTeamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
