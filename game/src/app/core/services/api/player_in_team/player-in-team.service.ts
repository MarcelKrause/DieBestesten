import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlayerInTeamService {
  constructor(private http: HttpClient) {}

  public getSquadByTeam(seasonName: string, teamName: string): Observable<any> {
    return this.http.get(`${environment.api}/player_in_team?season_name=${seasonName}&team_name=${encodeURIComponent(teamName)}`);
  }

  public sellPlayer(playerInTeamId: string): Observable<any> {
    return this.http.delete(`${environment.api}/player_in_team/${playerInTeamId}`);
  }
}
