import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TeamService {
  constructor(private http: HttpClient) {}

  public getTeam(teamId: string): Observable<any> {
    return this.http.get(`${environment.api}/team/${teamId}`);
  }

  public getTeamBySeasonAndName(seasonName: string, teamName: string): Observable<any> {
    return this.http.get(`${environment.api}/team?season_name=${seasonName}&team_name=${encodeURIComponent(teamName)}`);
  }

  public getTeamListBySeason(seasonId: string): Observable<any> {
    return this.http.get(`${environment.api}/team?season_id=${seasonId}`);
  }

  public postTeam(teamName: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('team_name', teamName);

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };
    return this.http.post(`${environment.api}/team`, body.toString(), options);
  }

  public postTeamBadge(seasonId: string, teamId: string, photo: File, takeover: any): Observable<any> {
    const formData = new FormData();
    formData.append('season_id', seasonId);
    formData.append('team_id', teamId);
    formData.append('image', photo);

    if (takeover) {
      formData.append('takeover', 'true');
      formData.append('last_season_id', takeover.season_id);
      formData.append('last_team_id', takeover.team_id);
    }
    return this.http.post(`${environment.image_api}/team/`, formData);
  }

  public getMarketTeam(season_name: string, matchday_number: number): Observable<any> {
    return this.http.get(`${environment.api}/matchday?action=market-team&season_name=${season_name}&matchday_number=${matchday_number}`);
  }
}
