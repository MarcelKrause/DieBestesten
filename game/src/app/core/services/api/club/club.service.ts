import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ClubService {
  constructor(private http: HttpClient) {}

  public getBundesligaClubList(): Observable<any> {
    return this.http.get(`${environment.api}/club?filter=bundesliga`);
  }
}
