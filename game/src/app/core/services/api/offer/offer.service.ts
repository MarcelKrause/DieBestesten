import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OfferService {
  constructor(private http: HttpClient) {}

  public getOwnOfferList(): Observable<any> {
    return this.http.get(`${environment.api}/offer`);
  }

  public getOfferListByTransferwindow(transferwindowId): Observable<any> {
    return this.http.get(`${environment.api}/offer?transferwindow_id=${transferwindowId}`);
  }

  public postOffer(
    playerId: string,
    teamId: string,
    transferwindowId: string,
    offerValue: number,
    priceSnapshot,
    owner_team_id: string
  ): Observable<any> {
    const body = new URLSearchParams();
    body.set('player_id', playerId);
    body.set('team_id', teamId);
    body.set('owner_team_id', owner_team_id);
    body.set('transferwindow_id', transferwindowId);
    body.set('offer_value', offerValue.toString());
    body.set('price_snapshot', priceSnapshot.toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post(`${environment.api}/offer`, body.toString(), options);
  }

  public patchOffer(offerId: string, offerValue: number): Observable<any> {
    const body = new URLSearchParams();
    body.set('action', 'edit');
    body.set('offer_value', offerValue.toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/offer/${offerId}`, body.toString(), options);
  }

  public deleteOffer(offerId: string): Observable<any> {
    return this.http.delete(`${environment.api}/offer/${offerId}`);
  }

  public acceptOffer(offerId: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('action', 'react');
    body.set('status', 'accepted');

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/offer/${offerId}`, body.toString(), options);
  }

  public refuseOffer(offerId: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('action', 'react');
    body.set('status', 'rejected');

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/offer/${offerId}`, body.toString(), options);
  }
}
