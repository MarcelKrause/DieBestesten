import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PowerrankingService {
  constructor(private http: HttpClient) {}

  public getPowerranking(): Observable<any> {
    return this.http.get(`${environment.api}/powerranking`);
  }

  public getPowerrankingOverview(): Observable<any> {
    return this.http.get(`${environment.api}/powerranking/overview`);
  }

  public savePowerranking(team_list: any[]): Observable<any> {
    const body = new URLSearchParams();
    team_list = team_list.map((team) => team.team_id);
    body.set('team_list', JSON.stringify(team_list));

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post(`${environment.api}/powerranking`, body.toString(), options);
  }

  public getCount(): Observable<number> {
    return this.http.get<number>(`${environment.api}/powerranking/count`);
  }
}
