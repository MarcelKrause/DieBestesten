import { TestBed } from '@angular/core/testing';

import { PowerrankingService } from './powerranking.service';

describe('PowerrankingService', () => {
  let service: PowerrankingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PowerrankingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
