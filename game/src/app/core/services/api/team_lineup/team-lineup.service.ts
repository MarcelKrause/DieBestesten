import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TeamLineupService {
  constructor(private http: HttpClient) {}

  public getLineup(seasonName: string, teamName: string, matchdayNumber: number): Observable<any> {
    return this.http.get(
      `${environment.api}/team_lineup?season_name=${seasonName}&team_name=${encodeURIComponent(teamName)}&matchday_number=${matchdayNumber}`
    );
  }

  public postLineup(lineup: any[], teamId: string, matchdayNumber: number): Observable<any> {
    const body = new URLSearchParams();
    body.set('lineup', JSON.stringify(lineup));
    body.set('team_id', teamId);
    body.set('matchday_number', matchdayNumber.toString());

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.post(`${environment.api}/team_lineup`, body.toString(), options);
  }

  public patchLineup(lineup: any[]): Observable<any> {
    const body = new URLSearchParams();
    body.set('lineup', JSON.stringify(lineup));

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/team_lineup`, body.toString(), options);
  }
}
