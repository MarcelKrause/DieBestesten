import { TestBed } from '@angular/core/testing';

import { TeamLineupService } from './team-lineup.service';

describe('TeamLineupService', () => {
  let service: TeamLineupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TeamLineupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
