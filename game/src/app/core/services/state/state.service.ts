import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  public is_menu_expanded = false;

  constructor() {}

  public toggleMenu(): void {
    this.is_menu_expanded = !this.is_menu_expanded;
  }

  public setMenu(input: boolean): void {
    this.is_menu_expanded = input;
  }
}
