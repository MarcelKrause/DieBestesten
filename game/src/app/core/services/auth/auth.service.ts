import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { NotificationService } from '../notification/notification.service';
import { Observable } from 'rxjs';
import jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private renderer: Renderer2;

  constructor(private http: HttpClient, private notificationService: NotificationService, rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  public isLoggedIn(): boolean {
    const token = localStorage.getItem('token');

    if (token) {
      // TODO check token
      this.updateThemeColor('#fff');
      return true;
    } else {
      this.updateThemeColor('#101010');
      return false;
    }
  }

  public setToken(token: string): any {
    return localStorage.setItem('token', token);
  }

  public getToken(): any {
    return localStorage.getItem('token');
  }

  public getManager(): any {
    return jwt_decode(localStorage.getItem('token'));
  }

  public login(username: string, password: string): void {
    if (!username || !password || username === '' || password === '') {
      this.notificationService.push('Nicht alle Felder ausgefüllt', 'negative', 100000);
      return;
    }

    const body = new FormData();
    body.append('name', username);
    body.append('password', password);

    this.http.post(`${environment.api}/auth`, body).subscribe(
      (data: any) => {
        localStorage.clear();
        localStorage.setItem('token', data.token);
        window.location.reload();
      },
      (error) => {
        this.notificationService.push(error, 'negative');
      }
    );
  }

  public logout(): void {
    this.http.delete(`${environment.api}/auth`).subscribe((data) => {
      localStorage.clear();
      window.location.href = environment.baseRef;
    });
  }

  public changePassword(password: string): Observable<any> {
    const body = new URLSearchParams();
    body.set('password', password);

    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
    };

    return this.http.patch(`${environment.api}/auth`, body.toString(), options);
  }

  private updateThemeColor(color: string): void {
    const metaTag = document.querySelector('meta[name="theme-color"]');
    if (metaTag) {
      this.renderer.setAttribute(metaTag, 'content', color);
    }
  }
}
