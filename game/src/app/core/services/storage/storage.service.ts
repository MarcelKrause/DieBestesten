import { Injectable } from '@angular/core';

const initialFilter = {
  player: {
    isExpanded: true,
    items: {
      selectedPositions: [],
      selectedClubs: [],
      range: {
        min: null,
        max: null,
      },
    },
  },
};

const initialProdcast = {
  lastView: null,
  podcasts: [],
};

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  constructor() {
    if (!localStorage.getItem('filter')) {
      localStorage.setItem('filter', JSON.stringify(initialFilter));
    }

    if (!localStorage.getItem('prodcast')) {
      localStorage.setItem('prodcast', JSON.stringify(initialProdcast));
    }
  }

  public setItem(key: string, item: JSON): void {
    localStorage.setItem(key, JSON.stringify(item));
  }

  public getItem(key: string): JSON {
    return JSON.parse(localStorage.getItem(key));
  }
}
