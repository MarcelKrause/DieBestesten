import { Injectable } from '@angular/core';
import { Notification } from 'src/app/shared/models/notification/notification';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  notification?: Notification;
  constructor() {}

  public push(message: string, type?: 'positive' | 'neutral' | 'negative', duration: number = 5000): void {
    if (!this.notification) {
      this.notification = new Notification(message, type);
      this.fadeOutInMilliseconds(duration);
    } else {
      this.fadeOutInMilliseconds(0);
      setTimeout(() => {
        this.notification = new Notification(message, type);
      }, 400);
    }
  }

  public remove(): void {
    this.fadeOutInMilliseconds(0);
  }

  private fadeOutInMilliseconds(ms: number): void {
    setTimeout(() => {
      const notification_element = document.getElementById('notification');
      notification_element?.classList.add('fade-out');
      this.removeInMilliseconds(360);
    }, ms);
  }

  private removeInMilliseconds(ms: number): void {
    setTimeout(() => {
      this.notification = undefined;
    }, ms);
  }
}
