import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PopupService {
  is_open: boolean = false;
  is_closing: boolean = false;

  dynamic_injector?: Injector;

  public open_event: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private router: Router) {
    this.router.events.subscribe(() => {
      if (this.is_open) {
        this.close();
      }
    });
  }

  public open(component: any): void {
    this.is_open = true;
    this.open_event.next(component);
  }

  public close() {
    this.is_closing = true;
    setTimeout(() => {
      this.is_open = false;
      this.is_closing = false;
    }, 400);
  }
}
