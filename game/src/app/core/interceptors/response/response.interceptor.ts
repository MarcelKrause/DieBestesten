import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { NotificationService } from '../../services/notification/notification.service';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor(private notificationService: NotificationService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      tap((event) => {}),
      catchError((error: any) => {
        if (error.status >= 400 && error.status < 500) {
          // CLIENT ERROR
          this.notificationService.push(error.error.message, 'negative');

          if (error.status === 401) {
            localStorage.removeItem('token');
            location.reload();
          }
        }
        if (error.status >= 500 && error.status < 600) {
          // SERVER ERROR
          this.notificationService.push(error.statusText, 'negative');
        }
        return of(error);
      })
    );
  }
}
