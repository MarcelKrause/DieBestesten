import { NumberSymbol } from '@angular/common';

export class Honour {
  honourId: string;
  honourName: string;
  seasonName: string;
  weight: number;
  icon: string;

  constructor(element: any) {
    this.honourId = element.honour_id;
    this.honourName = element.honour_name;
    this.seasonName = element.season_name;
    this.weight = +element.weight;

    switch (this.honourName) {
      case 'Meisterschaft':
        this.icon = 'trophy';
        break;
      case 'Goldene Bürste':
        this.icon = 'brush';
        break;
      case 'Hölzerne Bank':
        this.icon = 'bench';
        break;
    }
  }
}
