export enum Position {
  GOALKEEPER = 'Tor',
  DEFENDER = 'Abwehr',
  MIDFIELDER = 'Mittelfeld',
  FORWARD = 'Sturm',
}
