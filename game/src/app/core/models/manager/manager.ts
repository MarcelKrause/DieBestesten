import { Team } from '../team/team';

export class Manager {
  managerId: string;
  managerName: string;
  alias: string;
  hasPhoto: boolean;
  birthdate: Date;
  city: string;

  teamList: Team[];

  // meta
  position: number;
  points: number;

  constructor(element: any) {
    this.managerId = element.manager_id;
    this.managerName = element.manager_name;
    this.alias = element.alias;
    this.hasPhoto = Boolean(Number(element.has_photo));
    this.birthdate = new Date(element.birthdate);
    this.city = element.city;

    this.teamList = [];
    if (element.team_list) {
      element.team_list.forEach((team) => {
        this.teamList.push(new Team(team));
      });
    }

    this.position = +element.position;
    this.points = +element.points;
  }

  public getAveragePoints(): number {
    let matchdays = this.teamList.reduce((acc, team) => {
      return acc + team.matchdays;
    }, 0);
    return this.points / matchdays;
  }
}
