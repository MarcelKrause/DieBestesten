import { Honour } from './../honour/honour';

export class Team {
  teamId: string;
  seasonId: string;
  seasonName: string;
  teamName: string;
  color: string;

  honourList: Honour[];

  matchdays: number;

  // meta
  position: number;
  points: number;
  sds: number;
  goals: number;
  assists: number;
  clean_sheet: number;
  fine: string;
  missed_goals: number;

  // position
  points_goalkeeper: number;
  points_defender: number;
  points_midfielder: number;
  points_forward: number;

  // formation
  formation_343: number;
  formation_352: number;
  formation_433: number;
  formation_442: number;
  formation_451: number;
  maximal_points: number;

  started_player: number;
  substituted_player: number;
  unused_player: number;
  pending_player: number;

  invalid: boolean;

  constructor(element: any) {
    this.teamId = element.team_id;
    this.seasonId = element.season_id;
    this.seasonName = element.season_name;
    this.teamName = element.team_name;
    this.color = element.color;

    this.matchdays = +element.matchdays;

    this.honourList = [];
    if (element.honour_list) {
      element.honour_list.forEach((honour) => {
        this.honourList.push(new Honour(honour));
      });
    }

    this.position = +element.position;
    this.points = element.points;
    this.goals = element.goals;
    this.assists = element.assists;
    this.sds = element.sds;
    this.clean_sheet = +element.clean_sheet;
    this.fine = element.fine;
    this.missed_goals = +element.missed_goals;

    this.points_goalkeeper = +element.points_goalkeeper;
    this.points_defender = +element.points_defender;
    this.points_midfielder = +element.points_midfielder;
    this.points_forward = +element.points_forward;

    this.formation_343 = +element.formation_343;
    this.formation_352 = +element.formation_352;
    this.formation_433 = +element.formation_433;
    this.formation_442 = +element.formation_442;
    this.formation_451 = +element.formation_451;
    this.maximal_points = +element.maximal_points;

    this.started_player = +element.start_lineup;
    this.substituted_player = +element.substitution;
    this.unused_player = +element.player_count - this.started_player - this.substituted_player;
    this.pending_player = 11 - +element.player_count;

    this.invalid = Boolean(Number(element.invalid));
  }

  public getShortSeasonName(): string {
    if (this.seasonName) {
      return this.seasonName.substr(2, 2) + '/' + this.seasonName.substr(7, 2);
    }
  }

  public getMissedPoints(): number {
    return this.maximal_points - this.points;
  }

  public getFine(): number {
    return +this.fine / 100;
  }
}
