import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule, MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { DatePipeFormatPipe } from './pipes/date-pipe-format/date-pipe-format.pipe';

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule, HttpClientModule, MatDialogModule, MatTooltipModule, MatSnackBarModule, FormsModule],
  declarations: [DatePipeFormatPipe],
  exports: [DatePipeFormatPipe],
  providers: [
    {
      provide: MatSnackBarRef,
      useValue: {},
    },
    {
      provide: MAT_SNACK_BAR_DATA,
      useValue: {},
    },
  ],
})
export class CoreModule {}
