import { Component } from '@angular/core';
import { ApiService } from 'src/app/core/services/api/api.service';

@Component({
  selector: 'db-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent {
  notification_list: any[] = [];
  selected_notification: any;

  constructor(private api_service: ApiService) {
    this.getNotifications();
  }

  private getNotifications() {
    this.api_service.getNotifications().subscribe((response) => {
      this.notification_list = response;
    });
  }

  public getSenderPhoto(notification: any): string {
    if (!notification.sender_id) {
      return '/assets/img/logo/square.png';
    }
  }

  public onSelectNotification(notification: any) {
    if (!notification.read_at) {
      this.api_service.readNotification(notification.notification_id).subscribe({
        next: () => {
          this.api_service.status.unread_notifications -= 1;
          notification.read_at = new Date().toISOString();
          this.selected_notification = notification;
        },
        error: (error) => {},
        complete: () => {},
      });
    } else {
      this.selected_notification = notification;
    }
  }
}
