import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { ApiService } from 'src/app/core/services/api/api.service';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  standalone: true,
  selector: 'db-podcast',
  imports: [MatTooltipModule, RouterModule, SharedModule, CommonModule],
  templateUrl: './podcast.component.html',
  styleUrls: ['./podcast.component.scss'],
})
export class PodcastComponent {
  prodcastStorage: any;
  prodcastList: any[];

  constructor(private apiService: ApiService) {
    this.getProdcastList();
  }

  ngOnInit(): void {}

  private getProdcastList(): void {
    this.apiService.prodcast.getProdcastList().subscribe((data) => {
      this.prodcastList = data;
    });
  }
}
