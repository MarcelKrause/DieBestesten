import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KickerComponent } from './kicker.component';

describe('KickerComponent', () => {
  let component: KickerComponent;
  let fixture: ComponentFixture<KickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KickerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
