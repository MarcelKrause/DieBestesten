import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { ApiService } from 'src/app/core/services/api/api.service';
import { SharedModule } from 'src/app/shared/shared.module';

@Component({
  standalone: true,
  imports: [MatTooltipModule, RouterModule, SharedModule, CommonModule],
  selector: 'db-kicker',
  templateUrl: './kicker.component.html',
  styleUrls: ['./kicker.component.scss'],
})
export class KickerComponent {
  season: string;
  matchday: string;
  club_list: any;

  is_loading: boolean = false;
  is_selecting: boolean = false;

  modus: 'kicker' | 'ligainsider' = 'kicker';

  matchdays: number[] = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
  ];

  constructor(private route: ActivatedRoute, private api_service: ApiService) {
    this.route.paramMap.subscribe((paramMap) => {
      this.season = paramMap.get('year');
      this.matchday = paramMap.get('matchday');
    });

    this.getData();
  }

  private getData() {
    this.is_loading = true;
    this.api_service.getKickerData(this.season, this.matchday).subscribe((data) => {
      this.season = data['season'];
      this.matchday = data['matchday'];
      this.club_list = data['club_list'];
      this.is_loading = false;
    });
  }

  public onRefresh(): void {
    this.getData();
  }

  public sumPoints(club: any): number {
    return club.players.reduce((n, player) => n + this.calcPoints(player, this.modus), 0);
  }

  public getPositionShort(position: string): string {
    switch (position) {
      case 'GOALKEEPER':
        return 'TOR';
      case 'DEFENDER':
        return 'ABW';
      case 'MIDFIELDER':
        return 'MIT';
      case 'FORWARD':
        return 'STU';
      default:
        return '';
    }
  }

  public getPositionLong(position: string): string {
    switch (position) {
      case 'GOALKEEPER':
        return 'Torwart';
      case 'DEFENDER':
        return 'Abwehr';
      case 'MIDFIELDER':
        return 'Mittelfeld';
      case 'FORWARD':
        return 'Sturm';
      default:
        return '';
    }
  }

  public getSeasonLong(): string {
    if (!this.season) {
      return '2023/2024';
    }
    return this.season;
  }

  public getSeasonShort(): string {
    if (!this.season) {
      return '23/24';
    }
    return this.season.slice(2, 4) + '/' + this.season.slice(7, 9);
  }

  public createArray(n: any): any[] {
    if (n === '-') {
      n = 0;
    }
    return Array(n);
  }

  public toggleSelect(): void {
    this.is_selecting = !this.is_selecting;

    if (this.is_selecting) {
      document.body.classList.add('fixed');
    } else {
      document.body.classList.remove('fixed');
    }
  }

  public selectMatchday(matchday: number): void {
    this.matchday = matchday.toString();
    this.getData();
    this.is_selecting = false;
    document.body.classList.remove('fixed');
  }

  public calcPoints(player: any, modus: 'kicker' | 'ligainsider'): number {
    switch (modus) {
      case 'kicker':
        if (player.start_lineup === 1) {
          return player.points + 2;
        } else if (player.substitution === 1) {
          return player.points + 1;
        } else {
          return player.points;
        }
      case 'ligainsider':
        let points = 0;
        player.start_lineup === 1 ? (points += 4) : null;
        player.substitution === 1 ? (points += 2) : null;
        player.ligainsider_grade === 1 ? (points += 10) : null;
        player.ligainsider_grade === 1.5 ? (points += 8) : null;
        player.ligainsider_grade === 2 ? (points += 6) : null;
        player.ligainsider_grade === 2.5 ? (points += 4) : null;
        player.ligainsider_grade === 3 ? (points += 2) : null;
        player.ligainsider_grade === 4 ? (points -= 2) : null;
        player.ligainsider_grade === 4.5 ? (points -= 4) : null;
        player.ligainsider_grade === 5 ? (points -= 6) : null;
        player.ligainsider_grade === 5.5 ? (points -= 8) : null;
        player.ligainsider_grade === 6 ? (points -= 10) : null;
        player.yellow_red_card === 1 ? (points -= 3) : null;
        player.red_card === 1 ? (points -= 6) : null;
        player.sds === 1 ? (points += 3) : null;
        player.clean_sheet === 1 ? (points += 2) : null;
        points += player.assists;
        switch (player.position) {
          case 'GOALKEEPER':
            points += player.goals * 6;
            break;
          case 'DEFENDER':
            points += player.goals * 5;
            break;
          case 'MIDFIELDER':
            points += player.goals * 4;
            break;
          case 'FORWARD':
            points += player.goals * 3;
            break;
        }
        return points;
    }
  }

  public toggleModus(): void {
    this.modus = this.modus === 'kicker' ? 'ligainsider' : 'kicker';
  }
}
