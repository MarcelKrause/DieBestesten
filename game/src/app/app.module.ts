import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';
import { AuthInterceptor } from './core/interceptors/auth/auth.interceptor';
import { ResponseInterceptor } from './core/interceptors/response/response.interceptor';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { SharedModule } from './shared/shared.module';
import { ApiService } from './core/services/api/api.service';
import { PopupComponent } from './shared/components/popup/popup.component';
import { NotificationsComponent } from './standalone/notifications/notifications.component';
import { ImprintComponent } from './standalone/imprint/imprint.component';
import { ErrorComponent } from './standalone/error/error.component';
import { NotificationComponent } from './shared/components/notification/notification.component';

registerLocaleData(localeDe, 'de-DE');

@NgModule({
  imports: [BrowserModule, AppRoutingModule, CoreModule, SharedModule, BrowserAnimationsModule, PopupComponent, NotificationComponent],
  declarations: [AppComponent, NotificationsComponent, ImprintComponent, ErrorComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ResponseInterceptor, multi: true },
    { provide: LOCALE_ID, useValue: 'de-de' },
    {
      provide: APP_INITIALIZER,
      useFactory: (api_service: ApiService) => () => api_service.loadData(),
      deps: [ApiService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
