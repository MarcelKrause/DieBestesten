import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuestGuard } from './core/guards/guest/guest.guard';
import { UserGuard } from './core/guards/user/user.guard';
import { NotificationsComponent } from './standalone/notifications/notifications.component';
import { KickerComponent } from './standalone/kicker/kicker.component';
import { ImprintComponent } from './standalone/imprint/imprint.component';
import { PodcastComponent } from './standalone/podcast/podcast.component';
import { ErrorComponent } from './standalone/error/error.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/login/login.module').then((m) => m.LoginModule),
    canActivate: [GuestGuard],
  },
  {
    path: 'liga',
    loadChildren: () => import('./features/league/league.module').then((m) => m.LeagueModule),
    canActivate: [UserGuard],
  },
  {
    path: 'h2h',
    loadChildren: () => import('./features/h2h/h2h.module').then((m) => m.H2hModule),
    canActivate: [UserGuard],
  },
  {
    path: 'team',
    loadChildren: () => import('./features/team/team.module').then((m) => m.TeamModule),
    canActivate: [UserGuard],
  },
  {
    path: 'manager',
    loadChildren: () => import('./features/manager/manager.module').then((m) => m.ManagerModule),
    canActivate: [UserGuard],
  },
  {
    path: 'spieler',
    loadChildren: () => import('./features/player/player.module').then((m) => m.PlayerModule),
    canActivate: [UserGuard],
  },
  {
    path: 'podcasts',
    component: PodcastComponent,
    canActivate: [UserGuard],
  },
  {
    path: 'kicker',
    component: KickerComponent,
  },
  {
    path: 'impressum',
    component: ImprintComponent,
  },
  {
    path: 'benachrichtigungen',
    component: NotificationsComponent,
  },
  {
    path: '**',
    component: ErrorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
