import { Component } from '@angular/core';
import { AuthService } from './core/services/auth/auth.service';
import { StateService } from './core/services/state/state.service';
import { NavigationEnd, Router } from '@angular/router';
import packageJson from './../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  version = packageJson.version;
  fullscreen_available_routes: string[] = ['benachrichtigungen'];

  fullscreen: boolean = false;
  constructor(public stateService: StateService, public auth_service: AuthService, private router: Router) {
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        const route = this.router.url.split('/')[1].toLowerCase();
        this.fullscreen = this.fullscreen_available_routes.includes(route);
      }
    });

    console.info('Version:', this.version);
  }
}
