export const environment = {
  production: true,
  api: 'http://api.die-bestesten.de',
  image_api: 'http://img.die-bestesten.de',
  baseRef: 'http://die-bestesten.de',
};
