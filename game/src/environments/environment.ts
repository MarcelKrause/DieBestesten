export const environment = {
  production: false,
  api: 'http://api.die-bestesten.de',
  image_api: 'http://img.die-bestesten.de',
  baseRef: 'http://localhost:4200',
};
