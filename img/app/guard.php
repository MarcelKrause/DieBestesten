<?php
    use \Firebase\JWT\JWT;
    require __DIR__ . '/../vendor/autoload.php';
    require_once(__DIR__.'/database/base.database.php');

  class Guard
	{
    public $db;

    function __construct(){
      $this->db = Database::getInstance();
    }
        
    function authorize($request) {
      if($request['endpoint'] == 'auth' && $_SERVER['REQUEST_METHOD'] == 'POST') {
        return ['status' => true];
      }

      $token = $_SERVER['HTTP_AUTHORIZATION'];
      if(!isset($token)) {
        return ['status' => false, 'message' => 'Authorization Token nicht gesendet'];
      }

      $key = 'quz7m53sXK0H6VtzfGGKOHjQk6FtMqYZ';
      $token = substr($token, 7);
      try {
        $decoded = JWT::decode($token, $key, array('HS256'));
      
        $session = $this->db->getSessionById($decoded->sid);

        if($session['status'] == 'inactive') {
          return ['status' => false, 'message' => 'Session beendet'];
        }

        if($session['expiry_timestamp'] < time()) {
          $this->db->deactivateSession($decoded->sid);
          return ['status' => false, 'message' => 'Session abgelaufen'];
        }

        $manager = $this->db->getManagerById($decoded->sub);
        if(!isset($manager)) {
          return ['status' => false, 'message' => 'Authorization Token enthält fehlerhafte Manager-ID'];
        } else {
          $_SERVER['session_id'] = $decoded->sid;
          $_SERVER['manager_id'] = $manager['manager_id'];
          $_SERVER['role'] = $manager['role'];

          $this->db->refreshSession();

          return ['status' => true, 'refresh_token' => $token];
        }
      } catch (Exception $e) {
        return ['status' => false, 'message' => $e->getMessage()];
      }
      return ['status' => false, 'message' => 'Unbekannter Fehler'];
    }
  }
?>