<?php
    class Route
	{
	    private $_name;
	    private $_class;

	    public function __construct($name, $class)
	    {
	        $this->_name = $name;
	        $this->_class = $class;
	    }

	    public function getName()
	    {
	        return $this->_name;  
	    }

	    public function getClass()
	    {
	        return $this->_class;  
	    }
    }
    
	class Routing
	{
		private $_routes = array();

		function __construct(){
			$routes = array(
				new Route('auth', 'Auth'),
				new Route('player', 'Player'),
				new Route('team', 'Team'),
			);

			$this->_routes = $routes;
		}

		function getRoutes(){
			return $this->_routes;
		}

		function navigate($request){
			if($this->isValid($request['endpoint'])) {
				$route = $this->getRoute($request['endpoint']);				
			} else {
				$route = $this->getRoute('error');
			}

			$controllerName = $route->getClass().'Controller';
			$controller = new $controllerName;
			return $controller;
		}

		function isValid($endpoint){
			foreach ($this->_routes as $route) {
				if($route->getName() == $endpoint){
					return true;
				}
			}
			return false;
		}

		function getRoute($endpoint){
			foreach ($this->_routes as $route) {
				if($route->getName() == $endpoint){
					return $route;
				}
			}
		}
	}
?>