<?php

  trait PlayerTrait {

    function getPlayerById($player_id) {
      $query = $this->con->prepare("SELECT * FROM player AS p LEFT JOIN country AS c ON p.country_code = c.country_code WHERE p.player_id = :player_id LIMIT 1");
      $query->execute(array('player_id' => $player_id));
      $result = $query->fetch(PDO::FETCH_ASSOC);
      return $result;
    }
  }