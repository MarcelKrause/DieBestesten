<?php

  trait SeasonTrait {

    function getSeasonById($season_id) {
      $query = $this->con->prepare("SELECT * FROM season AS s WHERE s.season_id = :season_id LIMIT 1");
      $query->execute(array('season_id' => $season_id));
      $result = $query->fetch(PDO::FETCH_ASSOC);
      return $result;
    }
  }