<?php
	require_once 'activity.database.php';
	require_once 'player.database.php';
	require_once 'player_in_season.database.php';
	require_once 'season.database.php';
	require_once 'session.database.php';

	class Database
	{
		use ActivityTrait;
		use PlayerTrait;
		use PlayerInSeasonTrait;
		use SeasonTrait;
		use SessionTrait;

		private $prod = ['host' => 'sql3.udmedia.de', 'user' => 'ud16_151', 'password' => 'Observed84udmedia', 'database' => 'usr_ud16_151_2'];
		private $dev = ['host' => 'sql3.udmedia.de', 'user' => 'ud16_151m1', 'password' => 'rBhsX5t_d66e', 'database' => 'usr_ud16_151_5'];
	        
		#database config
		protected $host;
		protected $user;
		protected $password;
		protected $database;
		private $con;

		protected static $_instance = null;

		public static function getInstance(){
			if (null === self::$_instance){
				self::$_instance = new self;
			}
			return self::$_instance;
		}

		protected function __clone() {}

    protected function __construct() {
			$config = $this->prod;
			$this->host = $config['host'];
			$this->user = $config['user'];
			$this->password = $config['password'];
			$this->database = $config['database'];
      $this->connect();
    }
		
		function connect(){
			try {
				$this->con = new PDO("mysql:host=$this->host;dbname=$this->database", $this->user, $this->password);
				$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->con->exec("set names utf8");
			}
			catch(PDOException $e) {
				echo "Connection failed: " . $e->getMessage();
			}
		}

		function close(){
			$this->con->close();
		}
   
		function error(){
			return $this->con->error;
		}

		################# AUTH ######################

		function getManagerById($manager_id) {
			$query = $this->con->prepare("SELECT * FROM manager AS m WHERE m.manager_id = :manager_id LIMIT 1");
			$query->execute(array(':manager_id' => $manager_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

		function generateGUID()
		{
			return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
		}
	}