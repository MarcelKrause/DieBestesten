<?php

  trait ActivityTrait {
     
		function postActivity($method, $ref_id, $ref_table, $ref_column, $ref_route, $message) {
			$activity_id = $this->generateGUID();
      $author_id = $_SERVER['manager_id'];
			$timestamp = date('Y-m-d H:i:s');
			$query = $this->con->prepare("INSERT INTO activity (activity_id, author_id, method, ref_id, ref_table, ref_column, ref_route, activity_timestamp, message) VALUES (:activity_id, :author_id, :method, :ref_id, :ref_table, :ref_column, :ref_route, :timestamp, :message) ");
			$query->execute(array(
				':activity_id' => $activity_id,
				':author_id' => $author_id,
				':method' => $method,
				':ref_id' => $ref_id,
				':ref_table' => $ref_table,
				':ref_column' => $ref_column,
				':ref_route' => $ref_route,
				':timestamp' => $timestamp,
				':message' => $message
			));
		}
  }