<?php

  trait SessionTrait {

    function getSessionById($session_id) {
			$query = $this->con->prepare("SELECT * FROM session AS s WHERE s.session_id = :session_id LIMIT 1");
			$query->execute(array(':session_id' => $session_id));
			$result = $query->fetch(PDO::FETCH_ASSOC);
			return $result;
		}

    function refreshSession() {
      $expiry_timestamp = time() + (60 * 60 * 24 * 7);
      $last_checkin_timestamp = time();
      $query = $this->con->prepare("UPDATE session AS s SET s.expiry_timestamp = :expiry_timestamp, s.last_checkin_timestamp = :last_checkin_timestamp WHERE s.session_id = :session_id");			
			$query->execute(array(
        ':expiry_timestamp' => $expiry_timestamp,
        ':last_checkin_timestamp' => $last_checkin_timestamp,
        ':session_id' => $_SERVER['session_id'] ));
      return $_SERVER['session_id'];
    }

    function deactivateSession($session_id) {
      $query = $this->con->prepare("UPDATE session AS s SET s.status = 'inactive' WHERE s.session_id = :session_id");
			$query->execute(array(':session_id' => $session_id));
      return $_SERVER['session_id'];
    }
    
  }