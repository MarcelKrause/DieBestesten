<?php

  trait PlayerInSeasonTrait {
     
		// post
		function setPhoto($player_id, $season_id) {
      $query = $this->con->prepare("UPDATE player_in_season AS ps SET ps.has_photo = 1 WHERE ps.player_id = :player_id AND ps.season_id = :season_id");			
			$query->execute(array(
        ':player_id' => $player_id,
        ':season_id' => $season_id
      ));
		}
  }